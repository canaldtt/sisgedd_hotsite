﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Business.PesquisaRespBusiness
// Assembly: FI.Deloitte.Sisgedd.Business, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 170931A6-A2F0-40B5-B763-F38503D1EBDA
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Business.dll

using FI.Deloitte.Sisgedd.DataAccessLayer;
using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;

namespace FI.Deloitte.Sisgedd.Business
{
  public class PesquisaRespBusiness
  {
    private bool _dispose = true;
    private DataAccessLayerFactory _objDao;

    public PesquisaRespBusiness()
    {
      this._objDao = new DataAccessLayerFactory();
    }

    public PesquisaRespBusiness(string conexao, string provider)
    {
      this._objDao = new DataAccessLayerFactory(conexao, provider);
    }

    public PesquisaRespBusiness(ref DataAccessLayerFactory objDaoInstance)
    {
      this._objDao = objDaoInstance;
      this._dispose = false;
    }

    public int Add(PesquisaRespEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new PesquisaRespDAL().add(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Add(objEntity);
        return 0;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public bool Update(PesquisaRespEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new PesquisaRespDAL().update(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Update(objEntity);
        return false;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public bool Remove(PesquisaRespEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new PesquisaRespDAL().remove(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Remove(objEntity);
        return false;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public PesquisaRespEntity FindByPk(PesquisaRespEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new PesquisaRespDAL().findByPK(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.FindByPk(objEntity);
        return (PesquisaRespEntity) null;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public List<PesquisaRespEntity> Find(PesquisaRespEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new PesquisaRespDAL().find(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Find(objEntity);
        throw ex;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }
  }
}
