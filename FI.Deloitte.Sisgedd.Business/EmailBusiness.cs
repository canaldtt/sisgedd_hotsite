﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Business.EmailBusiness
// Assembly: FI.Deloitte.Sisgedd.Business, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 170931A6-A2F0-40B5-B763-F38503D1EBDA
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Business.dll

using FI.Deloitte.Sisgedd.DataAccessLayer;
using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FI.Deloitte.Sisgedd.Business
{
    public class EmailBusiness
    {
        private bool _dispose = true;
        private DataAccessLayerFactory _objDao;

        public EmailBusiness()
        {
            this._objDao = new DataAccessLayerFactory();
        }

        public EmailBusiness(ref DataAccessLayerFactory objDaoInstance)
        {
            this._objDao = objDaoInstance;
            this._dispose = false;
        }

        public int Add(EmailEntity objEntity)
        {
            try
            {
                this._objDao.startConnection();
                EmailDAL emailDal1 = new EmailDAL();
                EmailDAL emailDal2 = emailDal1;
                EmailEntity emailEntity1 = new EmailEntity();
                emailEntity1.MESSAGE_ID = objEntity.MESSAGE_ID;
                DataAccessLayerFactory objDao = this._objDao;
                EmailEntity objEntity1 = emailEntity1;
                DataAccessLayerFactory local = objDao;
                List<EmailEntity> source = emailDal2.find(objEntity1, ref local);
                int num;
                if (source != null && source.Count > 0)
                {
                    EmailEntity emailEntity2 = new EmailEntity();
                    EmailEntity objEntity2 = source.FirstOrDefault<EmailEntity>();
                    objEntity2.STATUS = objEntity.STATUS;
                    num = !emailDal1.update(objEntity2, ref this._objDao) ? 0 : 1;
                }
                else
                    num = emailDal1.add(objEntity, ref this._objDao);
                return num;
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.Add(objEntity);
                return 0;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public bool Update(EmailEntity objEntity)
        {
            try
            {
                this._objDao.startConnection();
                return new EmailDAL().update(objEntity, ref this._objDao);
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.Update(objEntity);
                return false;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public bool Remove(EmailEntity objEntity)
        {
            try
            {
                this._objDao.startConnection();
                return new EmailDAL().remove(objEntity, ref this._objDao);
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.Remove(objEntity);
                return false;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public EmailEntity FindByPk(EmailEntity objEntity)
        {
            try
            {
                this._objDao.startConnection();
                return new EmailDAL().findByPK(objEntity, ref this._objDao);
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.FindByPk(objEntity);
                return (EmailEntity)null;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public List<EmailEntity> Find(EmailEntity objEntity)
        {
            try
            {
                this._objDao.startConnection();
                return new EmailDAL().find(objEntity, ref this._objDao);
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.Find(objEntity);
                throw ex;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }
    }
}
