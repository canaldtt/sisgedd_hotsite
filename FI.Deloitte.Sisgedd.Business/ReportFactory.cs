﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Business.ReportFactory
// Assembly: FI.Deloitte.Sisgedd.Business, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 170931A6-A2F0-40B5-B763-F38503D1EBDA
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Business.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FI.Deloitte.Sisgedd.Business
{
  public class ReportFactory
  {
    public SortedList<string, int> RelatorioResumoMensal(int empresaId)
    {
      try
      {
        List<RelatoEntity> source = new RelatoBusiness().Find(new RelatoEntity() { EMPRESA_ID = empresaId });
        SortedList<string, int> sortedList = new SortedList<string, int>();
        foreach (var data in source.GroupBy<RelatoEntity, string>((Func<RelatoEntity, string>) (info => info.STATUS)).Select(group => new{ Status = group.Key, Count = group.Count<RelatoEntity>() }).OrderBy(x => x.Status))
          sortedList.Add(data.Status, data.Count);
        return sortedList;
      }
      catch
      {
        return (SortedList<string, int>) null;
      }
    }
  }
}
