﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Business.ConfiguracaoBusiness
// Assembly: FI.Deloitte.Sisgedd.Business, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 170931A6-A2F0-40B5-B763-F38503D1EBDA
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Business.dll

using FI.Deloitte.Sisgedd.DataAccessLayer;
using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;

namespace FI.Deloitte.Sisgedd.Business
{
  public class ConfiguracaoBusiness
  {
    private bool _dispose = true;
    private DataAccessLayerFactory _objDao;

    public ConfiguracaoBusiness()
    {
      this._objDao = new DataAccessLayerFactory();
    }

    public ConfiguracaoBusiness(string conexao, string provider)
    {
      this._objDao = new DataAccessLayerFactory(conexao, provider);
    }

    public ConfiguracaoBusiness(ref DataAccessLayerFactory objDaoInstance)
    {
      this._objDao = objDaoInstance;
      this._dispose = false;
    }

    public int Add(ConfiguracaoEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        string str = objEntity.CRITICIDADE.ToUpper() == "ALTA" || objEntity.CRITICIDADE.ToUpper() == "HIGH" ? "HIGH" : (objEntity.CRITICIDADE.ToUpper() == "BAIXA" || objEntity.CRITICIDADE.ToUpper() == "LOW" ? "LOW" : "MODERATE");
        objEntity.CRITICIDADE = str;
        return new ConfiguracaoDAL().add(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Add(objEntity);
        return 0;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public int Gerar(ConfiguracaoEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new ConfiguracaoDAL().gerar(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Gerar(objEntity);
        return 0;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public bool Update(ConfiguracaoEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new ConfiguracaoDAL().update(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Update(objEntity);
        return false;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public bool Remove(ConfiguracaoEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new ConfiguracaoDAL().remove(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Remove(objEntity);
        return false;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public ConfiguracaoEntity FindByPk(ConfiguracaoEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new ConfiguracaoDAL().findByPK(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.FindByPk(objEntity);
        return (ConfiguracaoEntity) null;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public List<ConfiguracaoEntity> Find(ConfiguracaoEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new ConfiguracaoDAL().find(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Find(objEntity);
        throw ex;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }
  }
}
