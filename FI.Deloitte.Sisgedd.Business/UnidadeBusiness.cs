﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Business.UnidadeBusiness
// Assembly: FI.Deloitte.Sisgedd.Business, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 170931A6-A2F0-40B5-B763-F38503D1EBDA
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Business.dll

using FI.Deloitte.Sisgedd.DataAccessLayer;
using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;

namespace FI.Deloitte.Sisgedd.Business
{
  public class UnidadeBusiness
  {
    private bool _dispose = true;
    private DataAccessLayerFactory _objDao;

    public UnidadeBusiness()
    {
      this._objDao = new DataAccessLayerFactory();
    }

    public UnidadeBusiness(ref DataAccessLayerFactory objDaoInstance)
    {
      this._objDao = objDaoInstance;
      this._dispose = false;
    }

    public int Add(UnidadeEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new UnidadeDAL().add(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Add(objEntity);
        return 0;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public bool Update(UnidadeEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new UnidadeDAL().update(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Update(objEntity);
        return false;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public bool Remove(UnidadeEntity objEntity)
    {
      try
      {
        if (this._dispose)
        {
          this._objDao.startConnection();
          this._objDao.hasTransaction();
        }
        ProdutoBusiness produtoBusiness = new ProdutoBusiness(ref this._objDao);
        List<ProdutoEntity> produtoEntityList = new ProdutoBusiness(ref this._objDao).Find(new ProdutoEntity() { UNIDADE_ID = objEntity.UNIDADE_ID });
        if (produtoEntityList != null && produtoEntityList.Count > 0)
        {
          foreach (ProdutoEntity objEntity1 in produtoEntityList)
            produtoBusiness.Remove(objEntity1);
        }
        int num = new UnidadeDAL().remove(objEntity, ref this._objDao) ? 1 : 0;
        if (this._dispose)
          this._objDao.commitTransaction();
        return (uint) num > 0U;
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Remove(objEntity);
        if (this._dispose)
          this._objDao.rollbackTransaction();
        return false;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public UnidadeEntity FindByPk(UnidadeEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new UnidadeDAL().findByPK(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.FindByPk(objEntity);
        return (UnidadeEntity) null;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public List<UnidadeEntity> Find(UnidadeEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new UnidadeDAL().find(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Find(objEntity);
        throw ex;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }
  }
}
