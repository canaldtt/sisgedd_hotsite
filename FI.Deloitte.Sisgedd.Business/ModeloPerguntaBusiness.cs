﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Business.ModeloPerguntaBusiness
// Assembly: FI.Deloitte.Sisgedd.Business, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 170931A6-A2F0-40B5-B763-F38503D1EBDA
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Business.dll

using FI.Deloitte.Sisgedd.DataAccessLayer;
using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;

namespace FI.Deloitte.Sisgedd.Business
{
  public class ModeloPerguntaBusiness
  {
    private bool _dispose = true;
    private DataAccessLayerFactory _objDao;

    public ModeloPerguntaBusiness()
    {
      this._objDao = new DataAccessLayerFactory();
    }

    public ModeloPerguntaBusiness(ref DataAccessLayerFactory objDaoInstance)
    {
      this._objDao = objDaoInstance;
      this._dispose = false;
    }

    public int Add(ModeloPerguntaEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new ModeloPerguntaDAL().add(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Add(objEntity);
        return 0;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public bool Update(ModeloPerguntaEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new ModeloPerguntaDAL().update(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Update(objEntity);
        return false;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public bool Remove(ModeloPerguntaEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new ModeloPerguntaDAL().remove(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Remove(objEntity);
        return false;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public ModeloPerguntaEntity FindByPk(ModeloPerguntaEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new ModeloPerguntaDAL().findByPK(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.FindByPk(objEntity);
        return (ModeloPerguntaEntity) null;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public List<ModeloPerguntaEntity> Find(ModeloPerguntaEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new ModeloPerguntaDAL().find(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Find(objEntity);
        throw ex;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }
  }
}
