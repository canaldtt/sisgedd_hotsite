﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Business.EmpresaBusiness
// Assembly: FI.Deloitte.Sisgedd.Business, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 170931A6-A2F0-40B5-B763-F38503D1EBDA
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Business.dll

using FI.Deloitte.Sisgedd.DataAccessLayer;
using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;

namespace FI.Deloitte.Sisgedd.Business
{
  public class EmpresaBusiness
  {
    private bool _dispose = true;
    private DataAccessLayerFactory _objDao;

    public EmpresaBusiness()
    {
      this._objDao = new DataAccessLayerFactory();
    }

    public EmpresaBusiness(string conexao, string provider)
    {
      this._objDao = new DataAccessLayerFactory(conexao, provider);
    }

    public EmpresaBusiness(ref DataAccessLayerFactory objDaoInstance)
    {
      this._objDao = objDaoInstance;
      this._dispose = false;
    }

    public int Add(EmpresaEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new EmpresaDAL().add(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Add(objEntity);
        return 0;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public bool Update(EmpresaEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new EmpresaDAL().update(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Update(objEntity);
        return false;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public bool Remove(EmpresaEntity objEntity)
    {
      try
      {
        if (this._dispose)
        {
          this._objDao.startConnection();
          this._objDao.hasTransaction();
        }
        UnidadeBusiness unidadeBusiness = new UnidadeBusiness(ref this._objDao);
        List<UnidadeEntity> unidadeEntityList = unidadeBusiness.Find(new UnidadeEntity() { EMPRESA_ID = objEntity.EMPRESA_ID });
        if (unidadeEntityList != null && unidadeEntityList.Count > 0)
        {
          foreach (UnidadeEntity objEntity1 in unidadeEntityList)
            unidadeBusiness.Remove(objEntity1);
        }
        int num = new EmpresaDAL().remove(objEntity, ref this._objDao) ? 1 : 0;
        if (this._dispose)
          this._objDao.commitTransaction();
        return (uint) num > 0U;
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Remove(objEntity);
        if (this._dispose)
          this._objDao.rollbackTransaction();
        return false;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public EmpresaEntity FindByPk(EmpresaEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new EmpresaDAL().findByPK(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.FindByPk(objEntity);
        return (EmpresaEntity) null;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public List<EmpresaEntity> Find(EmpresaEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new EmpresaDAL().find(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Find(objEntity);
        throw ex;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }
  }
}
