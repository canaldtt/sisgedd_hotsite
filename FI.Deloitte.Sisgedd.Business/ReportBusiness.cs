﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Business.ReportBusiness
// Assembly: FI.Deloitte.Sisgedd.Business, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 170931A6-A2F0-40B5-B763-F38503D1EBDA
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Business.dll

using FI.Deloitte.Sisgedd.DataAccessLayer;
using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;

namespace FI.Deloitte.Sisgedd.Business
{
  public class ReportBusiness
  {
    private bool _dispose = true;
    private DataAccessLayerFactory _objDao;

    public ReportBusiness()
    {
      this._objDao = new DataAccessLayerFactory();
    }

    public ReportBusiness(ref DataAccessLayerFactory objDaoInstance)
    {
      this._objDao = objDaoInstance;
      this._dispose = false;
    }

    public List<ReportEntity> RelResumoMensal(int mes, int empresaId)
    {
      try
      {
        this._objDao.startConnection();
        int year = DateTime.Now.Year;
        DateTime dateTime1 = Convert.ToDateTime(string.Format("01/{0}/2016 00:00:00", (object) 2, (object) year));
        DateTime dateTime2 = Convert.ToDateTime(string.Format("28/{0}/2016 23:59:59", (object) 2, (object) (year - 1)));
        ReportEntity objEntity = new ReportEntity();
        objEntity.EMPRESA_ID = empresaId;
        objEntity.DT_PARAM_INICIO = dateTime1;
        objEntity.DT_PARAM_FIM = dateTime2;
        return new ReportDAL().relResumoMensal(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.RelResumoMensal(mes, empresaId);
        throw ex;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }
  }
}
