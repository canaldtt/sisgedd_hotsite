﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Business.RelatoBusiness
// Assembly: FI.Deloitte.Sisgedd.Business, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 170931A6-A2F0-40B5-B763-F38503D1EBDA
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Business.dll

using FI.Deloitte.Sisgedd.DataAccessLayer;
using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FI.Deloitte.Sisgedd.Business
{
    public class RelatoBusiness
    {
        private bool _dispose = true;
        private DataAccessLayerFactory _objDao;

        public RelatoBusiness()
        {
            this._objDao = new DataAccessLayerFactory();
        }

        public RelatoBusiness(string conexao, string provider)
        {
            this._objDao = new DataAccessLayerFactory(conexao, provider);
        }

        public RelatoBusiness(ref DataAccessLayerFactory objDaoInstance)
        {
            this._objDao = objDaoInstance;
            this._dispose = false;
        }

        public string Add(RelatoEntity objEntity, LogEntity objLogEntity)
        {
            try
            {
                this._objDao.startConnection();
                this._objDao.hasTransaction();
                RelatoDAL relatoDal = new RelatoDAL();
                LogBusiness logBusiness = new LogBusiness(ref this._objDao);
                UserBusiness userBusiness = new UserBusiness(ref this._objDao);
                NaturezaBusiness naturezaBusiness1 = new NaturezaBusiness(ref this._objDao);
                TipoNaturezaBusiness naturezaBusiness2 = new TipoNaturezaBusiness(ref this._objDao);
                AnexoBusiness anexoBusiness = new AnexoBusiness(ref this._objDao);
                if (string.IsNullOrEmpty(objEntity.GUID))
                    return "required";
                objEntity.DT_INICIO = DateTime.Now;
                objEntity.SEQ = relatoDal.getSequencial(objEntity.EMPRESA_ID, ref this._objDao);
                ++objEntity.SEQ;
                if (objLogEntity != null && objLogEntity.USER_ID > 0)
                    objEntity.USER_ID = objLogEntity.USER_ID;
                DateTime dateTime1 = objEntity.DT_INICIO;
                string str1 = dateTime1.ToString("yy/MM/dd").Replace("/", "").Replace(":", "").Replace(" ", "").Trim();
                objEntity.SENHA_ACESSO = Common.GerarSenhaAleatoria(6);
                EmpresaEntity byPk1 = new EmpresaBusiness(ref this._objDao).FindByPk(new EmpresaEntity() { EMPRESA_ID = objEntity.EMPRESA_ID });
                objEntity.EMPRESA = byPk1.NOME;
                if (objEntity.NATUREZA_ID > 0 && objEntity.TIPONATUREZA_ID > 0 && (!string.IsNullOrEmpty(objEntity.CRITICIDADE) && objEntity.EMPRESA_ID > 0))
                {
                    ConfiguracaoEntity configuracaoEntity = new ConfiguracaoBusiness(ref this._objDao).Find(new ConfiguracaoEntity() { NATUREZA_ID = objEntity.NATUREZA_ID, TIPONATUREZA_ID = objEntity.TIPONATUREZA_ID, CRITICIDADE = objEntity.CRITICIDADE.ToUpper(), EMPRESA_ID = objEntity.EMPRESA_ID }).FirstOrDefault<ConfiguracaoEntity>();
                    if (configuracaoEntity != null && configuracaoEntity.PRAZO > 0)
                    {
                        RelatoEntity relatoEntity = objEntity;
                        dateTime1 = DateTime.Now;
                        DateTime dateTime2 = dateTime1.AddDays((double)configuracaoEntity.PRAZO);
                        relatoEntity.DT_MAX = dateTime2;
                    }
                }
                if (objEntity.TIPONATUREZA_ID > 0)
                    objEntity.TIPO_NATUREZA = naturezaBusiness2.FindByPk(new TipoNaturezaEntity()
                    {
                        TIPONATUREZA_ID = objEntity.TIPONATUREZA_ID
                    }).DESC;
                if (objEntity.NATUREZA_ID > 0)
                    objEntity.NATUREZA = naturezaBusiness1.FindByPk(new NaturezaEntity()
                    {
                        NATUREZA_ID = objEntity.NATUREZA_ID
                    }).DESC;
                objEntity.STATUS = "CASO SOB ANÁLISE PRELIMINAR";
                string str2 = byPk1.PREFIXO + str1 + (object)objEntity.SEQ;
                objEntity.NMR_PROTOCOLO = str2;
                string str3 = string.Empty;
                if (objLogEntity.USER_ID > 0)
                {
                    UserEntity byPk2 = userBusiness.FindByPk(new UserEntity() { USER_ID = objLogEntity.USER_ID });
                    if (byPk2 != null)
                    {
                        objLogEntity.Mensagem = string.Format("Usuário {0} criou relato de protocolo: {1} em {2}", (object)byPk2.LOGIN, (object)str2, (object)objEntity.DT_INICIO);
                        objLogEntity.LOG_ID = logBusiness.Add(objLogEntity);
                        objEntity.USER = byPk2.LOGIN;
                        objEntity.NOME_ATENDENTE = byPk2.NOME;
                        str3 = byPk2.LOGIN;
                    }
                }
                RelatoEntity relatoEntity1 = objEntity;
                string str4;
                if (string.IsNullOrEmpty(objEntity.HIST_ATEND))
                {
                    str4 = string.Empty;
                }
                else
                {
                    RelatoEntity relatoEntity2 = objEntity;
                    string[] strArray = new string[7];
                    dateTime1 = DateTime.Now;
                    strArray[0] = dateTime1.ToString("dd/MM/yyyy");
                    strArray[1] = " - <b>";
                    strArray[2] = str3;
                    strArray[3] = "<b>";
                    strArray[4] = " - ";
                    strArray[5] = objEntity.HIST_ATEND;
                    strArray[6] = "<br/>";
                    string str5;
                    string str6 = str5 = string.Concat(strArray);
                    relatoEntity2.HIST_ATEND = str6;
                    str4 = str5;
                }
                string str7 = str4;
                relatoEntity1.HIST_ATEND = str7;
                if (objEntity.TIPO_PUBLICO == "0")
                    objEntity.TIPO_PUBLICO = string.Empty;
                if (objEntity.TIPO_RELATO == "0")
                    objEntity.TIPO_RELATO = string.Empty;
                if (objEntity.DEN1_RELACAO == "0")
                    objEntity.DEN1_RELACAO = string.Empty;
                if (objEntity.DEN1_PARTICIPACAO == "0")
                    objEntity.DEN1_PARTICIPACAO = string.Empty;
                if (objEntity.DEN2_RELACAO == "0")
                    objEntity.DEN2_RELACAO = string.Empty;
                if (objEntity.DEN2_PARTICIPACAO == "0")
                    objEntity.DEN2_PARTICIPACAO = string.Empty;
                if (objEntity.DEN3_RELACAO == "0")
                    objEntity.DEN3_RELACAO = string.Empty;
                if (objEntity.DEN3_PARTICIPACAO == "0")
                    objEntity.DEN3_PARTICIPACAO = string.Empty;
                if (objEntity.DEN4_RELACAO == "0")
                    objEntity.DEN4_RELACAO = string.Empty;
                if (objEntity.DEN4_PARTICIPACAO == "0")
                    objEntity.DEN4_PARTICIPACAO = string.Empty;
                if (objEntity.DEN5_RELACAO == "0")
                    objEntity.DEN5_RELACAO = string.Empty;
                if (objEntity.DEN5_PARTICIPACAO == "0")
                    objEntity.DEN5_PARTICIPACAO = string.Empty;
                objEntity.RELATO_ID = relatoDal.add(objEntity, ref this._objDao);
                if (objEntity.RELATO_ID > 0)
                {
                    List<AnexoEntity> anexoEntityList = anexoBusiness.Find(new AnexoEntity() { GUID = objEntity.GUID });
                    if (anexoEntityList != null && anexoEntityList.Count > 0)
                    {
                        foreach (AnexoEntity objEntity1 in anexoEntityList)
                        {
                            objEntity1.PROTOCOLO = objEntity.NMR_PROTOCOLO;
                            objEntity1.RELATO_ID = objEntity.RELATO_ID;
                            anexoBusiness.Update(objEntity1);
                        }
                    }
                    this._objDao.commitTransaction();
                    string msg = string.Empty;
                    string idioma = objEntity.IDIOMA;
                    if (!(idioma == "pt-BR"))
                    {
                        if (!(idioma == "en-US"))
                        {
                            if (!(idioma == "es-ES"))
                            {
                                if (idioma == "fr-FR")
                                    msg = string.Format("\r\n<p> Protocole: <span>{0}</span>\r\n<br />\r\n<br />Mot de passe: <span>{1}</span>\r\n<br />\r\n<br /><p align=\"justify\">Important: Cette information est nécessaire pour accéder au statut de votre manifestation. De plus, nous vous recommandons de surveiller votre protocole chaque semaine pour assurer l’efficacité de l’ensemble du processus. \r\n<br />       \r\n<br />Votre contribution est très importante du résultat de l'enquête et de la réponse.  Suivre ou ajouter des informations à votre rapport <a href=\"https://etica.deloitte.com.br/hotsite/\" target=\"_blank\">cliquez ici</a>. \r\n<br />\r\n<br />{2}</p>\r\n</p>\r\n", (object)str2, (object)objEntity.SENHA_ACESSO, !string.IsNullOrEmpty(byPk1.TITULO) ? (object)byPk1.TITULO : (object)byPk1.NOME);
                            }
                            else
                                msg = string.Format("\r\n<p> Protocolo: <span>{0}</span>\r\n<br />\r\n<br />Contraseña: <span>{1}</span>\r\n<br />\r\n<br /><p align=\"justify\">Importante: Esta información es necesaria para acceder al estado de su manifestación. Además, recomendamos el monitoreo semanal de su protocolo para garantizar la efectividad de todo el proceso. \r\n<br />     \r\n<br />Su contribución es muy importante como resultado de la investigación y la respuesta.  Siga o agregue información a su informe <a href=\"https://etica.deloitte.com.br/hotsite/\" target=\"_blank\">haciendo click aqui</a>. \r\n<br />\r\n<br />{2}</p>\r\n</p>\r\n", (object)str2, (object)objEntity.SENHA_ACESSO, !string.IsNullOrEmpty(byPk1.TITULO) ? (object)byPk1.TITULO : (object)byPk1.NOME);
                        }
                        else
                            msg = string.Format("\r\n<p> Protocol: <span>{0}</span>\r\n<br />\r\n<br />Password: <span>{1}</span>\r\n<br />\r\n<br /><p align=\"justify\">Important: This information is required to access the status of your manifestation. In addition, we recommend weekly monitoring of your protocol to ensure the effectiveness of the entire process. \r\n<br />   \r\n<br />Your contribution is very important the result of the investigation and response.  Follow or add information to your report by <a href=\"https://etica.deloitte.com.br/hotsite/\" target=\"_blank\">clicking here</a>. \r\n<br />\r\n<br />{2}</p>\r\n</p>\r\n", (object)str2, (object)objEntity.SENHA_ACESSO, !string.IsNullOrEmpty(byPk1.TITULO) ? (object)byPk1.TITULO : (object)byPk1.NOME);
                    }
                    else
                        msg = string.Format("\r\n<p> Protocolo: <span>{0}</span>\r\n<br />\r\n<br />Senha: <span>{1}</span>\r\n<br />\r\n<br /><p align=\"justify\">Importante: Essas informações são obrigatórias para acessar o status da sua manifestação. Além disso, recomendamos o acompanhamento semanal do seu protocolo para garantir a eficácia de todo processo. \r\n<br />\r\n<br />Sua contribuição é muito importante o resultado da apuração e resposta.  Acompanhe ou adicione informações ao seu relato <a href=\"https://etica.deloitte.com.br/hotsite/\" target=\"_blank\">clicando aqui</a>. \r\n<br /> \r\n<br />{2}</p>\r\n</p>\r\n", (object)str2, (object)objEntity.SENHA_ACESSO, !string.IsNullOrEmpty(byPk1.TITULO) ? (object)byPk1.TITULO : (object)byPk1.NOME);
                    if (!string.IsNullOrEmpty(objEntity.EMAIL) && !string.IsNullOrEmpty(objEntity.NOME))
                        Common.SendMailProtocolo(objEntity.EMAIL, objEntity.NOME, msg);
                    else if (!string.IsNullOrEmpty(objEntity.EMAIL) && string.IsNullOrEmpty(objEntity.NOME))
                        Common.SendMailProtocolo(objEntity.EMAIL, "DENUNCIANTE NÃO IDENTIFICADO", msg);
                    else if (!string.IsNullOrEmpty(objEntity.EMAIL_ANONIMO))
                        Common.SendMailProtocolo(objEntity.EMAIL_ANONIMO, "DENUNCIANTE NÃO IDENTIFICADO", msg);
                    return msg;
                }
                this._objDao.rollbackTransaction();
                return "alert";
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.Add(objEntity, objLogEntity);
                this._objDao.rollbackTransaction();
                return "error";
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public int AddFromEmail(RelatoEntity objEntity)
        {
            try
            {
                this._objDao.startConnection();
                if (string.IsNullOrEmpty(objEntity.GUID))
                    return 0;
                return new RelatoDAL().add(objEntity, ref this._objDao);
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.AddFromEmail(objEntity);
                return 0;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public string UpdateFromEmail(RelatoEntity objEntity, LogEntity objLogEntity)
        {
            try
            {
                this._objDao.startConnection();
                this._objDao.hasTransaction();
                RelatoDAL relatoDal = new RelatoDAL();
                LogBusiness logBusiness = new LogBusiness(ref this._objDao);
                UserBusiness userBusiness = new UserBusiness(ref this._objDao);
                NaturezaBusiness naturezaBusiness1 = new NaturezaBusiness(ref this._objDao);
                TipoNaturezaBusiness naturezaBusiness2 = new TipoNaturezaBusiness(ref this._objDao);
                AnexoBusiness anexoBusiness = new AnexoBusiness(ref this._objDao);
                RelatoEntity byPk1 = relatoDal.findByPK(objEntity, ref this._objDao);
                if (string.IsNullOrEmpty(byPk1.GUID))
                    return "required";
                byPk1.DT_INICIO = DateTime.Now;
                byPk1.SEQ = relatoDal.getSequencial(byPk1.EMPRESA_ID, ref this._objDao);
                ++byPk1.SEQ;
                if (objLogEntity != null && objLogEntity.USER_ID > 0)
                    byPk1.USER_ID = objLogEntity.USER_ID;
                string str1 = byPk1.DT_INICIO.ToString("yy/MM/dd HH:mm:ss").Replace("/", "").Replace(":", "").Replace(" ", "");
                byPk1.SENHA_ACESSO = Common.GerarSenhaAleatoria(6);
                byPk1.STATUS = "CASO SOB ANÁLISE PRELIMINAR";
                EmpresaEntity byPk2 = new EmpresaBusiness(ref this._objDao).FindByPk(new EmpresaEntity() { EMPRESA_ID = byPk1.EMPRESA_ID });
                string str2 = byPk2.PREFIXO + str1 + (object)byPk1.SEQ;
                byPk1.NMR_PROTOCOLO = str2;
                string empty = string.Empty;
                if (objLogEntity.USER_ID > 0)
                {
                    UserEntity byPk3 = userBusiness.FindByPk(new UserEntity() { USER_ID = objLogEntity.USER_ID });
                    if (byPk3 != null)
                    {
                        objLogEntity.Mensagem = string.Format("Usuário {0} criou relato de protocolo: {1} em {2}", (object)byPk3.LOGIN, (object)str2, (object)objEntity.DT_INICIO);
                        objLogEntity.LOG_ID = logBusiness.Add(objLogEntity);
                        byPk1.USER = byPk3.LOGIN;
                        byPk1.NOME_ATENDENTE = byPk3.NOME;
                        string login = byPk3.LOGIN;
                    }
                }
                byPk1.FORMA_CONTATO = "EMAIL";
                byPk1.FLG_FLUXO = "R";
                if (relatoDal.update(byPk1, ref this._objDao))
                {
                    List<AnexoEntity> anexoEntityList = anexoBusiness.Find(new AnexoEntity() { GUID = byPk1.GUID });
                    if (anexoEntityList != null && anexoEntityList.Count > 0)
                    {
                        foreach (AnexoEntity objEntity1 in anexoEntityList)
                        {
                            objEntity1.PROTOCOLO = byPk1.NMR_PROTOCOLO;
                            objEntity1.RELATO_ID = byPk1.RELATO_ID;
                            anexoBusiness.Update(objEntity1);
                        }
                    }
                    this._objDao.commitTransaction();
                    return string.Format("\r\n<p> \r\n<br />Protocolo: <span>{0}</span>\r\n<br />\r\n<br />Senha: <span>{1}</span>\r\n<br />\r\n<br />Sua participação foi recebida com sucesso pelo {2}. \r\n<br />\r\n<br />É importante que anote corretamente o número de protocolo e senha do seu atendimento. \r\n<br />\r\n<br />Agradecemos sua colaboração!\r\n<br />\r\n<br />{2}\r\n<br />\r\n</p>\r\n", (object)str2, (object)byPk1.SENHA_ACESSO, !string.IsNullOrEmpty(byPk2.TITULO) ? (object)byPk2.TITULO : (object)byPk2.NOME);
                }
                this._objDao.rollbackTransaction();
                return "alert";
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.UpdateFromEmail(objEntity, objLogEntity);
                this._objDao.rollbackTransaction();
                return "error";
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public bool UpdateStatus(int id, string fluxo)
        {
            try
            {
                this._objDao.startConnection();
                RelatoDAL relatoDal = new RelatoDAL();
                RelatoEntity byPk = relatoDal.findByPK(new RelatoEntity() { RELATO_ID = id }, ref this._objDao);
                byPk.FLG_FLUXO = fluxo;
                return relatoDal.update(byPk, ref this._objDao);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public bool Update(RelatoEntity objEntity, LogEntity objLogEntity)
        {
            try
            {
                this._objDao.startConnection();
                this._objDao.hasTransaction();
                RelatoDAL relatoDal = new RelatoDAL();
                LogBusiness logBusiness = new LogBusiness(ref this._objDao);
                UserBusiness userBusiness = new UserBusiness(ref this._objDao);
                NaturezaBusiness naturezaBusiness1 = new NaturezaBusiness(ref this._objDao);
                TipoNaturezaBusiness naturezaBusiness2 = new TipoNaturezaBusiness(ref this._objDao);
                HistoricoBusiness historicoBusiness = new HistoricoBusiness(ref this._objDao);
                RelatoEntity byPk1 = relatoDal.findByPK(objEntity, ref this._objDao);
                UserEntity byPk2 = userBusiness.FindByPk(new UserEntity() { USER_ID = objLogEntity.USER_ID });
                AnexoBusiness anexoBusiness = new AnexoBusiness(ref this._objDao);
                objEntity.USER_ID = byPk1.USER_ID;
                objEntity.SEQ = byPk1.SEQ;
                objEntity.USER = byPk1.USER;
                objEntity.RELATO = byPk1.RELATO;
                objEntity.NMR_PROTOCOLO = byPk1.NMR_PROTOCOLO;
                objEntity.SENHA_ACESSO = byPk1.SENHA_ACESSO;
                objEntity.DT_INICIO = byPk1.DT_INICIO;
                objEntity.RELATO_RESUMO = !string.IsNullOrEmpty(objEntity.RELATO_RESUMO) ? objEntity.RELATO_RESUMO : byPk1.RELATO_RESUMO;
                objEntity.GUID = !string.IsNullOrEmpty(byPk1.GUID) ? byPk1.GUID : objEntity.GUID;
                objEntity.NOME_ATENDENTE = !string.IsNullOrEmpty(objEntity.NOME_ATENDENTE) ? objEntity.NOME_ATENDENTE : byPk1.NOME_ATENDENTE;
                objEntity.STATUS = !string.IsNullOrEmpty(objEntity.STATUS) ? objEntity.STATUS : byPk1.STATUS;
                if (objEntity.FLG_FLUXO == "B")
                {
                    objEntity.STATUS = "CASO SOB PROCESSO DE INVESTIGAÇÃO";
                    historicoBusiness.Find(new HistoricoEntity()
                    {
                        RELATO_ID = objEntity.RELATO_ID,
                        TIPO = 1
                    });
                    historicoBusiness.Find(new HistoricoEntity()
                    {
                        RELATO_ID = objEntity.RELATO_ID,
                        TIPO = 2
                    });
                    historicoBusiness.Find(new HistoricoEntity()
                    {
                        RELATO_ID = objEntity.RELATO_ID,
                        TIPO = 3
                    });
                }
                if (!string.IsNullOrEmpty(objEntity.COMENTARIOS) && objEntity.COMENTARIOS != byPk1.COMENTARIOS)
                    byPk1.COMENTARIOS = byPk1.COMENTARIOS + "<br>" + objEntity.COMENTARIOS;
                else if (!string.IsNullOrEmpty(byPk1.COMENTARIOS))
                    byPk1.COMENTARIOS = byPk1.COMENTARIOS;
                DateTime now;
                if (!string.IsNullOrEmpty(objEntity.HIST_ATEND) && objEntity.HIST_ATEND != byPk1.HIST_ATEND)
                {
                    RelatoEntity relatoEntity = objEntity;
                    string[] strArray = new string[8];
                    strArray[0] = byPk1.HIST_ATEND;
                    now = DateTime.Now;
                    strArray[1] = now.ToString("dd/MM/yyyy");
                    strArray[2] = " - <b>";
                    strArray[3] = !string.IsNullOrEmpty(byPk2.LOGIN) ? byPk2.LOGIN : string.Empty;
                    strArray[4] = "<b>";
                    strArray[5] = " - ";
                    strArray[6] = objEntity.HIST_ATEND;
                    strArray[7] = "<br/>";
                    string str = string.Concat(strArray);
                    relatoEntity.HIST_ATEND = str;
                }
                else if (!string.IsNullOrEmpty(byPk1.HIST_ATEND))
                    objEntity.HIST_ATEND = byPk1.HIST_ATEND;
                if (objEntity.NATUREZA_ID > 0 && objEntity.TIPONATUREZA_ID > 0 && (!string.IsNullOrEmpty(objEntity.CRITICIDADE) && objEntity.EMPRESA_ID > 0))
                {
                    ConfiguracaoEntity configuracaoEntity = new ConfiguracaoBusiness(ref this._objDao).Find(new ConfiguracaoEntity() { NATUREZA_ID = objEntity.NATUREZA_ID, TIPONATUREZA_ID = objEntity.TIPONATUREZA_ID, CRITICIDADE = objEntity.CRITICIDADE.ToUpper(), EMPRESA_ID = objEntity.EMPRESA_ID, TIPO_MANIFESTACAO = objEntity.TIPO_MANIFESTACAO }).FirstOrDefault<ConfiguracaoEntity>();
                    if (configuracaoEntity != null && configuracaoEntity.PRAZO > 0)
                    {
                        RelatoEntity relatoEntity = objEntity;
                        now = DateTime.Now;
                        DateTime dateTime = now.AddDays((double)configuracaoEntity.PRAZO);
                        relatoEntity.DT_MAX = dateTime;
                    }
                }
                if (objEntity.TIPONATUREZA_ID > 0 && objEntity.TIPONATUREZA_ID != byPk1.TIPONATUREZA_ID || objEntity.TIPONATUREZA_ID > 0 && string.IsNullOrEmpty(objEntity.TIPO_NATUREZA))
                    objEntity.TIPO_NATUREZA = naturezaBusiness2.FindByPk(new TipoNaturezaEntity()
                    {
                        TIPONATUREZA_ID = objEntity.TIPONATUREZA_ID
                    }).DESC;
                if (objEntity.NATUREZA_ID > 0 && objEntity.NATUREZA_ID != byPk1.NATUREZA_ID || objEntity.NATUREZA_ID > 0 && string.IsNullOrEmpty(objEntity.NATUREZA))
                    objEntity.NATUREZA = naturezaBusiness1.FindByPk(new NaturezaEntity()
                    {
                        NATUREZA_ID = objEntity.NATUREZA_ID
                    }).DESC;
                List<AnexoEntity> anexoEntityList = anexoBusiness.Find(new AnexoEntity() { GUID = objEntity.GUID });
                if (anexoEntityList != null && anexoEntityList.Count > 0)
                {
                    foreach (AnexoEntity objEntity1 in anexoEntityList)
                    {
                        if (objEntity1.RELATO_ID == 0)
                        {
                            objEntity1.PROTOCOLO = byPk1.NMR_PROTOCOLO;
                            objEntity1.RELATO_ID = byPk1.RELATO_ID;
                            anexoBusiness.Update(objEntity1);
                        }
                    }
                }
                int num = relatoDal.update(objEntity, ref this._objDao) ? 1 : 0;
                if (num != 0)
                {
                    if (objLogEntity.USER_ID > 0)
                    {
                        objLogEntity.Mensagem = !(byPk2.PERFIL == "2") || !(objEntity.ACTION == "A") ? string.Format("Usuário {0} atualizou o relato de protocolo: {1} em {2}", (object)byPk2.LOGIN, (object)byPk1.NMR_PROTOCOLO, (object)DateTime.Now) : string.Format("Revisor {0} devolveu o relato de protocolo: {1} em {2} para o atendente", (object)byPk2.LOGIN, (object)byPk1.NMR_PROTOCOLO, (object)DateTime.Now);
                        objLogEntity.LOG_ID = logBusiness.Add(objLogEntity);
                    }
                    this._objDao.commitTransaction();
                }
                else
                    this._objDao.rollbackTransaction();
                return (uint)num > 0U;
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.Update(objEntity, objLogEntity);
                return false;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public bool UpdateBWise(RelatoEntity objEntity)
        {
            try
            {
                this._objDao.startConnection();
                RelatoDAL relatoDal = new RelatoDAL();
                bool flag = false;
                RelatoEntity relatoEntity = new RelatoEntity();
                relatoEntity.NMR_PROTOCOLO = objEntity.NMR_PROTOCOLO;
                DataAccessLayerFactory objDao = this._objDao;
                RelatoEntity objEntity1 = relatoEntity;
                DataAccessLayerFactory local = objDao;
                RelatoEntity objEntity2 = relatoDal.find(objEntity1, ref local).FirstOrDefault<RelatoEntity>();
                if (objEntity2 != null)
                {
                    objEntity2.STATUS = !string.IsNullOrEmpty(objEntity.STATUS) ? objEntity.STATUS : objEntity2.STATUS;
                    objEntity2.DT_ENCERRAMENTO = objEntity.DT_ENCERRAMENTO != DateTime.MinValue ? objEntity.DT_ENCERRAMENTO : DateTime.MinValue;
                    objEntity2.FLG_FLUXO = !string.IsNullOrEmpty(objEntity.FLG_FLUXO) ? objEntity.FLG_FLUXO : objEntity2.FLG_FLUXO;
                    objEntity2.COMENTARIOS = objEntity.COMENTARIOS;
                    objEntity2.DT_ATUALIZACAO = objEntity.DT_ATUALIZACAO != DateTime.MinValue ? objEntity.DT_ATUALIZACAO : objEntity2.DT_ATUALIZACAO;
                    objEntity2.RESPONSIBLE = !string.IsNullOrEmpty(objEntity.RESPONSIBLE) ? objEntity.RESPONSIBLE : objEntity2.RESPONSIBLE;
                    flag = new RelatoDAL().update(objEntity2, ref this._objDao);
                }
                int num = flag ? 1 : 0;
                return flag;
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.UpdateBWise(objEntity);
                return false;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public bool UpdateSite(int relatoId, string comentarios)
        {
            try
            {
                bool flag = false;
                this._objDao.startConnection();
                this._objDao.hasTransaction();
                DataAccessLayerFactory local = this._objDao;

                RelatoDAL relatoDal1 = new RelatoDAL();
                RelatoDAL relatoDal2 = relatoDal1;
                RelatoEntity relatoEntity1 = new RelatoEntity();
                relatoEntity1.RELATO_ID = relatoId;

                RelatoEntity relatoEntity2 = relatoDal2.findByPK(relatoEntity1, ref local);

                HistoricoBusiness historicoBusiness = new HistoricoBusiness(ref this._objDao);
                //Gera novo registro de histórico do relato
                HistoricoEntity historicoEntity = new HistoricoEntity()
                {
                    COMENTARIO = comentarios,
                    RELATO_ID = relatoId,
                    NMR_PROTOCOLO = relatoEntity2.NMR_PROTOCOLO,
                    DT_COMENTARIO = DateTime.Now,
                    TIPO = 2,
                    AUTOR = "DENUNCIANTE"
                };

                relatoEntity2.FLG_FLUXO = "R";
                relatoEntity2.STATUS = "CASO SOB PROCESSO DE INVESTIGAÇÃO";
                relatoEntity2.DT_ATUALIZACAO = DateTime.Now;

                AnexoBusiness anexoBusiness = new AnexoBusiness(ref this._objDao);
                List<AnexoEntity> anexoEntityList = anexoBusiness.Find(new AnexoEntity() { GUID = relatoEntity2.GUID });
                if (anexoEntityList != null && anexoEntityList.Count > 0)
                {
                    foreach (AnexoEntity objEntity3 in anexoEntityList)
                    {
                        if (objEntity3.RELATO_ID == 0)
                        {
                            objEntity3.PROTOCOLO = relatoEntity2.NMR_PROTOCOLO;
                            objEntity3.RELATO_ID = relatoEntity2.RELATO_ID;
                            anexoBusiness.Update(objEntity3);
                        }
                    }
                }
                if (relatoDal1.update(relatoEntity2, ref this._objDao))
                {
                    historicoBusiness.Add(historicoEntity);
                    this._objDao.commitTransaction();

                    //TODO: Alerta de ACOMPANHAMENTO por E-mail
                    #region Alerta de E-mail (Acompanhamento)

                    //Serviço de E-mail (Exchange)
                    //ExchangeService service = new ExchangeService(); //email                 //senha              
                    //service.Credentials = new NetworkCredential("canbr@deloitte.com", "480a5@FLOks"); //aqui são as credenciais do e-mail que vai enviar onde ambas as infos tem que estar entre " "
                    //service.Url = new Uri("https://amemail.deloitte.com/ews/exchange.asmx");
                    ////Mensagem
                    //EmailMessage message = new EmailMessage(service);
                    ////Assunto
                    //message.Subject = $"Alerta de Acompanhamento - {relatoEntity2.NMR_PROTOCOLO}";
                    ////Corpo
                    //message.Body = $"<br>Prezado(a),<br> O relato <b style='color: red'>{relatoEntity2.NMR_PROTOCOLO}</b> a seguir recebeu um novo acompanhamento para revisão. <br /><br /> <br /><br /> Thanks & Regards, <br />SISGEDD® HOTSITE";
                    ////Destinatário
                    //message.ToRecipients.Add("mihsilva@deloitte.com");

                    ////Enviar & Salvar
                    //message.SendAndSaveCopy();
                    #endregion
                }
                else
                    this._objDao.rollbackTransaction();
                return flag;
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.UpdateSite(relatoId, comentarios);
                this._objDao.rollbackTransaction();
                return false;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public bool SendRelatoToBWise(int relatoId)
        {
            try
            {
                this._objDao.startConnection();
                RelatoDAL relatoDal = new RelatoDAL();
                RelatoEntity byPk = relatoDal.findByPK(new RelatoEntity() { RELATO_ID = relatoId }, ref this._objDao);
                LogEntity logEntity = new LogEntity() { Mensagem = "Enviado para o Bwise", DT_LOG = DateTime.Now };
                byPk.FLG_FLUXO = "B";
                return relatoDal.SendRelatoToBWise(byPk, ref this._objDao);
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.SendRelatoToBWise(relatoId);
                return false;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public bool Update(RelatoEntity objEntity)
        {
            try
            {
                this._objDao.startConnection();
                return new RelatoDAL().update(objEntity, ref this._objDao);
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.Update(objEntity);
                return false;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public bool Remove(RelatoEntity objEntity)
        {
            try
            {
                this._objDao.startConnection();
                return new RelatoDAL().remove(objEntity, ref this._objDao);
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.Remove(objEntity);
                return false;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public RelatoEntity FindByPk(RelatoEntity objEntity)
        {
            try
            {
                this._objDao.startConnection();
                return new RelatoDAL().findByPK(objEntity, ref this._objDao);
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.FindByPk(objEntity);
                return (RelatoEntity)null;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public List<RelatoEntity> Find(RelatoEntity objEntity)
        {
            try
            {
                this._objDao.startConnection();
                return new RelatoDAL().find(objEntity, ref this._objDao);
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.Find(objEntity);
                throw ex;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public List<RelatoEntity> FindRelatoOutBwise()
        {
            try
            {
                this._objDao.startConnection();
                return new RelatoDAL().FindRelatoOutBwise(ref this._objDao);
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.FindRelatoOutBwise();
                throw ex;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public RelatoEntity FindByLogin(int empresaId, string protocolo, string senha)
        {
            try
            {
                this._objDao.startConnection();
                RelatoDAL relatoDal = new RelatoDAL();
                RelatoEntity relatoEntity = new RelatoEntity();
                relatoEntity.EMPRESA_ID = empresaId;
                relatoEntity.NMR_PROTOCOLO = protocolo;
                relatoEntity.SENHA_ACESSO = senha;
                DataAccessLayerFactory objDao = this._objDao;
                RelatoEntity objEntity = relatoEntity;
                DataAccessLayerFactory local = objDao;
                return relatoDal.find(objEntity, ref local).FirstOrDefault<RelatoEntity>();
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.FindByLogin(empresaId, protocolo, senha);
                throw ex;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public bool ExistRelatoByEmail(string emailId)
        {
            try
            {
                this._objDao.startConnection();
                return new RelatoDAL().getRelatoByEmail(emailId, ref this._objDao) != null;
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.ExistRelatoByEmail(emailId);
                return false;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }
    }
}
