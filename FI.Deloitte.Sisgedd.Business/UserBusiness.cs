﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Business.UserBusiness
// Assembly: FI.Deloitte.Sisgedd.Business, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 170931A6-A2F0-40B5-B763-F38503D1EBDA
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Business.dll

using FI.Deloitte.Sisgedd.DataAccessLayer;
using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;

namespace FI.Deloitte.Sisgedd.Business
{
  public class UserBusiness
  {
    private bool _dispose = true;
    private DataAccessLayerFactory _objDao;

    public UserBusiness()
    {
      this._objDao = new DataAccessLayerFactory();
    }

    public UserBusiness(ref DataAccessLayerFactory objDaoInstance)
    {
      this._objDao = objDaoInstance;
      this._dispose = false;
    }

    public int Add(UserEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new UserDAL().add(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Add(objEntity);
        return 0;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public bool Update(UserEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        UserEntity byPk = new UserDAL().findByPK(objEntity, ref this._objDao);
        if (string.IsNullOrEmpty(objEntity.SENHA) || string.IsNullOrEmpty(objEntity.FLG_STATUS))
        {
          objEntity.SENHA = string.IsNullOrEmpty(objEntity.SENHA) ? byPk.SENHA : objEntity.SENHA;
          objEntity.FLG_STATUS = string.IsNullOrEmpty(objEntity.FLG_STATUS) ? byPk.FLG_STATUS : objEntity.FLG_STATUS;
        }
        return new UserDAL().update(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Update(objEntity);
        return false;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public bool Remove(UserEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        this._objDao.hasTransaction();
        new UserDAL().findByPK(objEntity, ref this._objDao);
        int num = new UserDAL().remove(objEntity, ref this._objDao) ? 1 : 0;
        this._objDao.commitTransaction();
        return (uint) num > 0U;
      }
      catch (Exception ex)
      {
        this._objDao.rollbackTransaction();
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Remove(objEntity);
        return false;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public UserEntity FindByPk(UserEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new UserDAL().findByPK(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.FindByPk(objEntity);
        throw ex;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public UserEntity AutenticateByEmail(UserEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new UserDAL().autenticateByEmail(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Autenticate(objEntity);
        return (UserEntity) null;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public UserEntity Autenticate(UserEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new UserDAL().autenticate(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Autenticate(objEntity);
        return (UserEntity) null;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public List<UserEntity> Find(UserEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new UserDAL().find(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Find(objEntity);
        throw ex;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }
  }
}
