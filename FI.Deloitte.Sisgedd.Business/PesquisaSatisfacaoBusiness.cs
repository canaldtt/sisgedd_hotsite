﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Business.PesquisaSatisfacaoBusiness
// Assembly: FI.Deloitte.Sisgedd.Business, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 170931A6-A2F0-40B5-B763-F38503D1EBDA
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Business.dll

using FI.Deloitte.Sisgedd.DataAccessLayer;
using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FI.Deloitte.Sisgedd.Business
{
  public class PesquisaSatisfacaoBusiness
  {
    private bool _dispose = true;
    private DataAccessLayerFactory _objDao;

    public PesquisaSatisfacaoBusiness()
    {
      this._objDao = new DataAccessLayerFactory();
    }

    public PesquisaSatisfacaoBusiness(ref DataAccessLayerFactory objDaoInstance)
    {
      this._objDao = objDaoInstance;
      this._dispose = false;
    }

    public PesquisaSatisfacaoBusiness(string conexao, string provider)
    {
      this._objDao = new DataAccessLayerFactory(conexao, provider);
    }

    public int Add(PesquisaSatisfacaoEntity objEntity)
    {
      try
      {
        int num = 0;
        this._objDao.startConnection();
        this._objDao.hasTransaction();
        PesquisaSatisfacaoDAL pesquisaSatisfacaoDal = new PesquisaSatisfacaoDAL();
        RelatoBusiness relatoBusiness = new RelatoBusiness(ref this._objDao);
        RelatoEntity objEntity1 = relatoBusiness.Find(new RelatoEntity() { NMR_PROTOCOLO = objEntity.NMR_PROTOCOLO }).FirstOrDefault<RelatoEntity>();
        objEntity1.FLG_FLUXO = "B";
        objEntity1.FLG_RESP_PESQ = "S";
        if (relatoBusiness.Update(objEntity1))
          num = pesquisaSatisfacaoDal.add(objEntity, ref this._objDao);
        if (num > 0)
          this._objDao.commitTransaction();
        else
          this._objDao.rollbackTransaction();
        return num;
      }
      catch (Exception ex)
      {
        this._objDao.rollbackTransaction();
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Add(objEntity);
        return 0;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public bool Update(PesquisaSatisfacaoEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new PesquisaSatisfacaoDAL().update(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Update(objEntity);
        return false;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public bool Remove(PesquisaSatisfacaoEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new PesquisaSatisfacaoDAL().remove(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Remove(objEntity);
        return false;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public PesquisaSatisfacaoEntity FindByPk(
      PesquisaSatisfacaoEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new PesquisaSatisfacaoDAL().findByPK(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.FindByPk(objEntity);
        return (PesquisaSatisfacaoEntity) null;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public List<PesquisaSatisfacaoEntity> Find(
      PesquisaSatisfacaoEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new PesquisaSatisfacaoDAL().find(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Find(objEntity);
        throw ex;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }
  }
}
