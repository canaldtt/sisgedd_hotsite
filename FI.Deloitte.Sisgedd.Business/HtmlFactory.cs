﻿using FI.Deloitte.Sisgedd.Domain;
using ImapX;
using ImapX.Enums;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Authentication;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using WinSCP;

namespace FI.Deloitte.Sisgedd.Business
{
    public static class HtmlFactory
    {
        public static void DownloadFtp(string strFileName)
        {
            FtpWebRequest ftpWebRequest = (FtpWebRequest)WebRequest.Create(Common.App.GetValue("FTPHOST", typeof(string)).ToString() + strFileName);
            ftpWebRequest.Method = "RETR";
            ftpWebRequest.Credentials = (ICredentials)new NetworkCredential(Common.App.GetValue("FTPLOGIN", typeof(string)).ToString(), Common.App.GetValue("FTPPWD", typeof(string)).ToString());
            ftpWebRequest.UsePassive = true;
            ftpWebRequest.UseBinary = true;
            ftpWebRequest.KeepAlive = true;
            FtpWebResponse response = (FtpWebResponse)ftpWebRequest.GetResponse();
            Stream responseStream = response.GetResponseStream();
            byte[] buffer = new byte[2048];
            FileStream fileStream = new FileStream(Common.App.GetValue("CLIENTPATH", typeof(string)).ToString() + strFileName, FileMode.Create);
            for (int count = responseStream.Read(buffer, 0, buffer.Length); count > 0; count = responseStream.Read(buffer, 0, buffer.Length))
                fileStream.Write(buffer, 0, count);
            fileStream.Close();
            responseStream.Close();
            response.Close();
        }

        public static bool UploadFtp(string strFileName, int tipo)
        {
            try
            {
                AppSettingsReader appSettingsReader = new AppSettingsReader();
                FileInfo fileInfo = new FileInfo(strFileName);
                FtpWebRequest ftpWebRequest = tipo == 1 ? (FtpWebRequest)WebRequest.Create(appSettingsReader.GetValue("APP_FTP_PHOTOPATH", typeof(string)).ToString() + fileInfo.Name) : (FtpWebRequest)WebRequest.Create(appSettingsReader.GetValue("APP_FTP_DOWNPATH", typeof(string)).ToString() + fileInfo.Name);
                ftpWebRequest.Method = "STOR";
                ftpWebRequest.Credentials = (ICredentials)new NetworkCredential(appSettingsReader.GetValue("APP_FTP_USER", typeof(string)).ToString(), appSettingsReader.GetValue("APP_FTP_PWD", typeof(string)).ToString());
                ftpWebRequest.UsePassive = true;
                ftpWebRequest.UseBinary = true;
                ftpWebRequest.KeepAlive = false;
                ftpWebRequest.ContentLength = fileInfo.Length;
                Stream requestStream = ftpWebRequest.GetRequestStream();
                byte[] buffer = new byte[2048];
                FileStream fileStream = fileInfo.OpenRead();
                for (int count = fileStream.Read(buffer, 0, buffer.Length); count > 0; count = fileStream.Read(buffer, 0, buffer.Length))
                    requestStream.Write(buffer, 0, count);
                fileStream.Close();
                requestStream.Close();
                return true;
            }
            catch (WebException ex)
            {
                Common.GetException((Exception)ex);
                return false;
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                return false;
            }
        }

        public static bool UploadSftp(string strFileName, int tipo)
        {
            bool flag;
            try
            {
                SessionOptions sessionOptions = new SessionOptions() { Protocol = Protocol.Sftp, HostName = "sftp2.deloitte.com.br", UserName = "brcrmcd", Password = "bR4c@MCD1", PortNumber = 22, SshHostKeyFingerprint = "ssh-rsa 2048 0a:76:cd:f5:0d:dc:80:05:2e:3d:24:6e:5e:7a:c9:c6" };
                using (Session session = new Session())
                {
                    session.Open(sessionOptions);
                    session.PutFiles(strFileName, "downloads/", false, new TransferOptions()
                    {
                        TransferMode = TransferMode.Binary,
                        FilePermissions = (FilePermissions)null,
                        PreserveTimestamp = false,
                        ResumeSupport = {
              State = TransferResumeSupportState.Off
            }
                    }).Check();
                }
                flag = true;
                if (flag)
                    HtmlFactory.DownloadSftp(strFileName);
            }
            catch (WebException ex)
            {
                Common.GetException((Exception)ex);
                flag = false;
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                flag = false;
            }
            return flag;
        }

        public static bool DownloadSftp(string strFileName)
        {
            try
            {
                SessionOptions sessionOptions = new SessionOptions() { Protocol = Protocol.Sftp, HostName = "sftp2.deloitte.com.br", UserName = "brcrmcd", Password = "bR4c@MCD1", PortNumber = 22, SshHostKeyFingerprint = "ssh-rsa 2048 0a:76:cd:f5:0d:dc:80:05:2e:3d:24:6e:5e:7a:c9:c6" };
                using (Session session = new Session())
                {
                    session.Open(sessionOptions);
                    TransferOptions options = new TransferOptions();
                    options.TransferMode = TransferMode.Binary;
                    options.FilePermissions = (FilePermissions)null;
                    options.PreserveTimestamp = false;
                    options.ResumeSupport.State = TransferResumeSupportState.Off;
                    if (strFileName.Contains("\\"))
                        strFileName = ((IEnumerable<string>)strFileName.Split('\\')).Last<string>();
                    if (strFileName.Contains("/"))
                        strFileName = ((IEnumerable<string>)strFileName.Split('/')).Last<string>();
                    session.GetFiles("/downloads/" + strFileName, Common.App.GetValue("APP_DOWNLOADS", typeof(string)).ToString() + strFileName, false, options).Check();
                }
                return true;
            }
            catch (WebException ex)
            {
                Common.GetException((Exception)ex);
                return false;
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                return false;
            }
        }

        public static class AdminView
        {
            public static string LogOut(int id)
            {
                try
                {
                    string empty = string.Empty;
                    return "success";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string LogIn(string user, string pwd, string persisted)
            {
                try
                {
                    string empty = string.Empty;
                    UserEntity objEntity = new UserEntity();
                    objEntity.FLG_STATUS = "A";
                    if (string.IsNullOrEmpty(pwd) || string.IsNullOrEmpty(user))
                        return "warning";
                    objEntity.SENHA = pwd;
                    if (user.Contains("@"))
                        objEntity.EMAIL = user;
                    else
                        objEntity.LOGIN = user;
                    UserEntity userEntity = new UserBusiness().Autenticate(objEntity);
                    string str;
                    if (userEntity != null)
                    {
                        str = "success";
                        if (persisted.ToUpper() == "S")
                        {
                            int num = 15;
                            HttpCookie cookie = HttpContext.Current.Request.Cookies["FI_PGP_LOGIN"] == null ? new HttpCookie("FI_EQU_LOGIN") : HttpContext.Current.Request.Cookies["FI_PGP_LOGIN"];
                            cookie.Expires = num > 0 ? DateTime.Now.AddDays((double)num) : DateTime.Now.AddMinutes(30.0);
                            cookie.Value = userEntity.USER_ID.ToString();
                            HttpContext.Current.Response.Cookies.Add(cookie);
                        }
                        HttpContext.Current.Session["usuario"] = (object)userEntity;
                    }
                    else
                        str = "warning";
                    return str;
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string LoginByEmail(string email)
            {
                try
                {
                    string empty = string.Empty;
                    UserEntity userEntity = new UserBusiness().AutenticateByEmail(new UserEntity() { FLG_STATUS = "A", EMAIL = email });
                    string str;
                    if (userEntity != null)
                    {
                        str = "success";
                        HttpContext.Current.Session["usuario"] = (object)userEntity;
                    }
                    else
                        str = "warning";
                    return str;
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string ReAutenticate(int id, string pwd)
            {
                try
                {
                    string empty = string.Empty;
                    UserEntity byPk = new UserBusiness().FindByPk(new UserEntity() { USER_ID = id });
                    byPk.SENHA = pwd;
                    string str;
                    if (byPk != null)
                    {
                        if (new UserBusiness().Autenticate(byPk) != null)
                        {
                            HttpContext.Current.Session["usuario"] = (object)byPk;
                            str = "success";
                        }
                        else
                            str = "warning";
                    }
                    else
                        str = "warning";
                    return str;
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string LoginSite(string prt, string pwd)
            {
                try
                {
                    string empty = string.Empty;
                    RelatoEntity objEntity = new RelatoEntity();
                    if (string.IsNullOrEmpty(pwd) || string.IsNullOrEmpty(prt))
                        return "warning";
                    objEntity.SENHA_ACESSO = pwd;
                    objEntity.NMR_PROTOCOLO = prt;
                    RelatoEntity relatoEntity = new RelatoBusiness().Find(objEntity).FirstOrDefault<RelatoEntity>();
                    string str;
                    if (relatoEntity != null)
                    {
                        str = "success";
                        HttpContext.Current.Session["relato"] = (object)relatoEntity;
                    }
                    else
                        str = "warning";
                    return str;
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string LogInSite(string prt, string pwd, string eid)
            {
                try
                {
                    string empty = string.Empty;
                    RelatoEntity byLogin = new RelatoBusiness().FindByLogin(Convert.ToInt32(HtmlFactory.AdminView.CryptoService(HtmlFactory.AdminView.CryptoService(eid, "D", Common.App.GetValue("SECURITY_KEY", typeof(string)).ToString()), "D", "Batata")), prt, pwd);
                    string str;
                    if (byLogin != null)
                    {
                        str = "success";
                        HttpContext.Current.Session["relato"] = (object)byLogin;
                    }
                    else
                        str = "warning";
                    return str;
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string SaveSupport(
              int id,
              string title,
              string comment,
              string desc,
              string status,
              int user,
              string posteddate)
            {
                try
                {
                    SupportEntity objEntity = new SupportEntity() { SUPPORT_ID = id, TITLE = title, COMMENT = comment, DESCRIPTION = desc, CLOSEDDATE = status == "F" ? DateTime.Now : DateTime.MinValue, POSTEDDATE = id == 0 ? DateTime.Now : (!string.IsNullOrEmpty(posteddate) ? Convert.ToDateTime(posteddate) : DateTime.MinValue), USER_ID = user, STATUS = id == 0 ? "A" : status };
                    string empty = string.Empty;
                    return id <= 0 ? (new SupportBusiness().Add(objEntity) <= 0 ? "alert" : "success") : (!new SupportBusiness().Update(objEntity) ? "alert" : "success");
                }
                catch
                {
                    Common.GetException(new Exception("Erro ao adicionar/atualizar beneficiário."));
                    return "error";
                }
            }

            public static string FindSupport(
              int index,
              int pagesize,
              string status,
              string startdate,
              string enddate,
              string lv,
              int user)
            {
                try
                {
                    SupportBusiness supportBusiness = new SupportBusiness();
                    SupportEntity supportEntity1 = new SupportEntity();
                    supportEntity1.POSTEDDATE_INICIO = !string.IsNullOrEmpty(startdate) ? Convert.ToDateTime(startdate + " 00:00:00.000") : DateTime.MinValue;
                    supportEntity1.POSTEDDATE_FIM = !string.IsNullOrEmpty(enddate) ? Convert.ToDateTime(enddate + " 23:59:59.000") : DateTime.MinValue;
                    supportEntity1.STATUS = status != "0" ? status : string.Empty;
                    supportEntity1.PAGEINDEX = index;
                    supportEntity1.PAGESIZE = pagesize;
                    supportEntity1.USER_ID = lv != "UF" ? user : 0;
                    SupportEntity objEntity = supportEntity1;
                    List<SupportEntity> source = supportBusiness.Find(objEntity);
                    StringBuilder stringBuilder1 = new StringBuilder();
                    if (source.Count > 0)
                    {
                        stringBuilder1.AppendFormat("\r\n                             <table class=\"table responsive table-header-style2\" cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n\t\t                    <caption>\r\n\t\t\t                    <i class=\"fa fa-wrench\"></i> Chamados<span class=\"info\">{0} registros encontrados</span>\r\n                            </caption>\r\n                            <thead>\r\n                            <tr>\r\n                                <th class=\"centeralign width5\" scope=\"col\">Aberto</th>\r\n                                <th class=\"leftalign \" scope=\"col\">Titulo</th>\r\n                                <th class=\"centeralign width5\" scope=\"col\">Fechado</th>\r\n                                <th class=\"centeralign width10\" scope=\"col\">Status</th>\r\n                                <th class=\"centeralign width10\" colspan=\"3\">Ações</th>\r\n\t\t                        </tr>\r\n                            </thead>\r\n                            <tbody>", (object)source.FirstOrDefault<SupportEntity>().COUNTER);
                        foreach (SupportEntity supportEntity2 in source)
                        {
                            StringBuilder stringBuilder2 = stringBuilder1;
                            object[] objArray1 = new object[9];
                            objArray1[0] = (object)supportEntity2.SUPPORT_ID;
                            objArray1[1] = (object)supportEntity2.TITLE;
                            string str1;
                            DateTime dateTime;
                            if (!(supportEntity2.POSTEDDATE != DateTime.MinValue))
                            {
                                str1 = string.Empty;
                            }
                            else
                            {
                                dateTime = supportEntity2.POSTEDDATE;
                                str1 = dateTime.ToShortDateString();
                            }
                            objArray1[2] = (object)str1;
                            string str2;
                            if (!(supportEntity2.CLOSEDDATE != DateTime.MinValue))
                            {
                                str2 = string.Empty;
                            }
                            else
                            {
                                dateTime = supportEntity2.CLOSEDDATE;
                                str2 = dateTime.ToShortDateString();
                            }
                            objArray1[3] = (object)str2;
                            objArray1[4] = supportEntity2.STATUS == "0" ? (object)string.Empty : (supportEntity2.STATUS == "E" ? (object)"EM ANÁLISE" : (supportEntity2.STATUS == "A" ? (object)"ABERTO" : (object)"FECHADO"));
                            objArray1[5] = string.IsNullOrEmpty(lv) || !(lv == "UF") ? (object)"" : (object)string.Format("<a class=\"btn\" href=\"#\" rel=\"{0}\"  data-action=\"remove\" data-id=\"{0}\" tooltip=\"tooltip\" data-title=\"Clique para deletar\"><i class=\"fa fa-trash-o fa-lg\"></i></a>", (object)supportEntity2.SUPPORT_ID);
                            objArray1[6] = string.IsNullOrEmpty(lv) || !(lv == "UF") ? (object)"" : (object)string.Format("<a class=\"btn\" href=\"suporte/editar.aspx?id={0}\" tooltip=\"tooltip\" data-title=\"Clique para editar\"><i class=\"fa fa-pencil fa-lg\"></i></a>", (object)supportEntity2.SUPPORT_ID);
                            objArray1[7] = !string.IsNullOrEmpty(supportEntity2.DESCRIPTION) ? (object)supportEntity2.DESCRIPTION : (object)string.Empty;
                            objArray1[8] = !string.IsNullOrEmpty(supportEntity2.COMMENT) ? (object)supportEntity2.COMMENT : (object)string.Empty;
                            object[] objArray2 = objArray1;
                            stringBuilder2.AppendFormat("\r\n                                        <tr style=\"display: table-row; opacity: 1;\">\t\t\t                        \r\n                                            <td class=\"centeralign width5\">{2}</td>\r\n                                            <td class=\"leftalign \">{1}</td>\r\n                                            <td class=\"centeralign width5\">{3}</td>\r\n                                            <td class=\"centeralign width10\">{4}</td>\r\n                                            <td class=\"centeralign width5\">{5}</td>\r\n                                            <td class=\"centeralign width5\">{6}</td>\r\n                                            <td class=\"centeralign width5\"><a class=\"btn\" href=\"#\"  tooltip=\"tooltip\" data-title=\"Clique para visualizar\" data-toggle=\"collapse\" data-target=\"#view_{0}\" id=\"btn_view_{0}\" data-id=\"{0}\"  onclick=\"CHECK_COLLAPSE({0});return false;\"><i class=\"fa fa-eye fa-lg\"></i></a></td>\r\n\t\t                                </tr>\r\n                                         <tr id=\"view_{0}\" class=\"collapse\" >\r\n                                            <td colspan=\"7\" >\r\n                                                <div class=\"col-md-12 container-view\">\r\n                                                    <div class=\"container-view-inner\" id=\"container-view-inner_{0}\" >\r\n                                                        <form id=\"frm-view-{0}\" class=\"form-horizontal view i-validate\" novalidate=\"novalidate\">\r\n                                                        <h5 class=\"view-title\"><b>{1}</b></h5><span class=\"pull-right btn btn-default hidden-print\" data-print=\"true\" data-title=\"imprimir\" data-target=\"container-view-inner_{0}\" tooltip=\"tooltip\"><i class=\"fa fa-print\"></i> Imprimir</span>\r\n                                                        <br/>em: <b>{2}</b><br/>\r\n                                                        Situação: <b>{4}</b><br/>                                                \r\n                                                        Finalizado em: <b>{3}</b><br/>\r\n                                                        <hr/>\r\n                                                        Descrição da Ocorrência: <br/><b>{7}</b><br/>\r\n                                                        <hr/>\r\n                                                        Parecer do Suporte: <br/><b>{8}</b><br/>\r\n                                                        <hr/>\r\n                                                        <br/>\r\n                                                        <br/>\r\n                                                        </form>\r\n                                                    </div>\r\n                                                </div>                                           \r\n                                            </td>\r\n                                        </tr>", objArray2);
                        }
                        stringBuilder1.AppendFormat("</tbody><tfoot><td colspan=\"7\" class=\"\"><span class=\"info\">XXXQTDXXX registros encontrados</span><input type=\"hidden\" id=\"pager\" name=\"pager\" value=\"{0}\" /></td></tfoot></table>", (object)source.FirstOrDefault<SupportEntity>().COUNTER).Replace("XXXQTDXXX", source.FirstOrDefault<SupportEntity>().COUNTER.ToString());
                    }
                    else
                        stringBuilder1.Append("<h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>");
                    return stringBuilder1.ToString();
                }
                catch
                {
                    Common.GetException(new Exception("Erro ao preencher o grid de suporte"));
                    return "error";
                }
            }

            public static string FindSupportByPk(int id)
            {
                try
                {
                    SupportEntity byPk = new SupportBusiness().FindByPk(new SupportEntity() { SUPPORT_ID = id });
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("{\"support\":\"[ ");
                    if (byPk != null)
                    {
                        stringBuilder.Append("{");
                        stringBuilder.AppendFormat(" 'id':'{0}' ,'title':'{1}','description':'{2}','comment':'{3}','status':'{4}','posteddate':'{5}','closeddate':'{6}' ", (object)byPk.SUPPORT_ID, (object)byPk.TITLE, (object)byPk.DESCRIPTION, (object)byPk.COMMENT, (object)byPk.STATUS, byPk.POSTEDDATE != DateTime.MinValue ? (object)byPk.POSTEDDATE.ToShortDateString() : (object)string.Empty, byPk.CLOSEDDATE != DateTime.MinValue ? (object)byPk.CLOSEDDATE.ToShortDateString() : (object)string.Empty);
                        stringBuilder.Append("}");
                    }
                    stringBuilder.Append(" ]\"}");
                    return stringBuilder.ToString();
                }
                catch
                {
                    Common.GetException(new Exception("Erro ao buscar usuarios"));
                    return "error";
                }
            }

            public static string RemoveSupport(int id)
            {
                try
                {
                    return new SupportBusiness().Remove(new SupportEntity() { SUPPORT_ID = id }) ? "success" : "warning";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string SavePerfil(
              int id,
              string nome,
              string email,
              string photo,
              string login,
              string senha,
              string uf)
            {
                try
                {
                    UserEntity byPk = new UserBusiness().FindByPk(new UserEntity() { USER_ID = id });
                    byPk.NOME = nome;
                    byPk.LOGIN = login;
                    byPk.UF = uf;
                    byPk.PHOTO = photo;
                    byPk.SENHA = senha;
                    byPk.FLG_STATUS = "A";
                    byPk.EMAIL = email;
                    string empty = string.Empty;
                    return !new UserBusiness().Update(byPk) ? "alert" : "success";
                }
                catch
                {
                    Common.GetException(new Exception("Erro ao atualizar usuário."));
                    return "error";
                }
            }

            /// <summary>
            /// Salva o Histórico
            /// </summary>
            /// <param name="tipo">1=historico atendente/revisor,2=historico revisor/denunciante, 3=historico revisor/bwise</param>
            /// <param name="autor">ATENDENTE/REVISOR/MANIFESTANTE</param>
            /// <param name="comentario">Informação adicional/Réplica</param>
            /// <param name="relatoId">ID do Relato</param>
            /// <returns></returns>
            public static string SaveHistorico(int tipo, string autor, string comentario, int relatoId)
            {
                try
                {
                    RelatoEntity relatoByPk = new RelatoBusiness().FindByPk(new RelatoEntity() { RELATO_ID = relatoId });


                    if (new HistoricoBusiness().Add(new HistoricoEntity()
                    {
                        RELATO_ID = relatoId,
                        AUTOR = autor.ToUpper(),
                        COMENTARIO = comentario,
                        TIPO = tipo,
                        NMR_PROTOCOLO = relatoByPk.NMR_PROTOCOLO,
                        DT_COMENTARIO = DateTime.Now
                    }) > 0)
                    {
                        relatoByPk.DT_ATUALIZACAO = DateTime.Now;
                        return new RelatoBusiness().Update(relatoByPk) ? "success" : "alert";
                    }

                    else
                    {
                        return "alert";

                    }
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string RemoveHistorico(int id)
            {
                try
                {
                    return new HistoricoBusiness().Remove(new HistoricoEntity() { HISTORICO_ID = id }) ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string FindHistorico(int index, int pagesize, int relatoId, int tipo)
            {
                try
                {
                    RelatoEntity byPk = new RelatoBusiness().FindByPk(new RelatoEntity() { RELATO_ID = relatoId });
                    HistoricoBusiness historicoBusiness = new HistoricoBusiness();
                    HistoricoEntity historicoEntity1 = new HistoricoEntity();
                    historicoEntity1.TIPO = tipo;
                    historicoEntity1.PAGEINDEX = index;
                    historicoEntity1.PAGESIZE = pagesize;
                    historicoEntity1.RELATO_ID = relatoId;
                    HistoricoEntity objEntity = historicoEntity1;
                    List<HistoricoEntity> source = historicoBusiness.Find(objEntity);
                    StringBuilder stringBuilder = new StringBuilder();
                    if (source != null && source.Count > 0)
                    {
                        stringBuilder.AppendFormat("\r\n                    <table class=\"table responsive table-header-style2\" cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n                    <caption><i class=\"fa fa-comments-o\"></i>Historico<span class=\"info\">{0} registros encontrados</span></caption>\r\n                    <thead>\r\n                    <tr>\r\n                        <th class=\"width15\">Data/Hora</th>\r\n                        <th  class=\"width15\">Autor</th>\r\n                        <th >Comentário</th>\r\n                        \r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>", (object)source.FirstOrDefault<HistoricoEntity>().COUNTER);
                        foreach (HistoricoEntity historicoEntity2 in source)
                            stringBuilder.AppendFormat("\r\n                    <tr style=\"display: table-row; opacity: 1;\" style=\"vertical-align\">\r\n                        <td class=\"width15\"><b>{1}</b></td>     \r\n                        <td class=\"width15\"><b>{2}</b></td>   \r\n                        <td class=\"\"><b>{3}</b></td>                    \r\n                    </tr>\r\n                    ", (object)historicoEntity2.HISTORICO_ID, (object)historicoEntity2.DT_COMENTARIO.ToString("dd/MM/yyyy HH:mm"), !string.IsNullOrEmpty(historicoEntity2.AUTOR) ? (object)historicoEntity2.AUTOR : (object)string.Empty, !string.IsNullOrEmpty(historicoEntity2.COMENTARIO) ? (object)historicoEntity2.COMENTARIO : (object)string.Empty);
                        stringBuilder.AppendFormat("</tbody><tfoot><td colspan=\"3\" class=\"\"><span class=\"info\">{0} registros encontrados</span><input type=\"hidden\" id=\"pager\" name=\"pager\" value=\"{0}\" /></td></tfoot></table>", (object)source.FirstOrDefault<HistoricoEntity>().COUNTER);
                    }
                    else
                        stringBuilder.AppendFormat("");
                    //stringBuilder.Append(HtmlFactory.AdminView.FindHistoricoBWise(byPk.NMR_PROTOCOLO));
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "<h4 class=\"alert_error\">Ocorreu um erro.</h4>";
                }
            }

            public static string FindHistoricoBWise(string protocolo)
            {
                try
                {
                    List<HistoricoEntity> bwise = new HistoricoBusiness().FindBWise(protocolo);
                    StringBuilder stringBuilder = new StringBuilder();
                    if (bwise != null && bwise.Count > 0)
                    {
                        stringBuilder.AppendFormat("\r\n                    <table class=\"table responsive table-header-style2\" cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n                    \r\n                    <thead>\r\n                    <tr>                        \r\n                        <th >Resposta </th>                        \r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>", (object)bwise.FirstOrDefault<HistoricoEntity>().COUNTER);
                        foreach (HistoricoEntity historicoEntity in bwise)
                            stringBuilder.AppendFormat("\r\n                    <tr style=\"display: table-row; opacity: 1;\" style=\"vertical-align\">\r\n                       \r\n                        <td class=\"\"><b>{0}</b></td>                    \r\n                    </tr>\r\n                    ", !string.IsNullOrEmpty(historicoEntity.COMENTARIO) ? (object)historicoEntity.COMENTARIO : (object)string.Empty);
                        stringBuilder.AppendFormat("</tbody></table>");
                    }
                    else
                        stringBuilder.AppendFormat("<h4 class=\"alert-no-result\">Sem resposta...</h4>");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "";
                }
            }

            public static string LoadNatureza()
            {
                try
                {
                    List<NaturezaEntity> naturezaEntityList = new NaturezaBusiness().Find(new NaturezaEntity());
                    StringBuilder stringBuilder = new StringBuilder();
                    if (naturezaEntityList.Count > 0)
                    {
                        foreach (NaturezaEntity naturezaEntity in naturezaEntityList)
                            stringBuilder.AppendFormat("<option value=\"{0}\">{1}</option>", (object)naturezaEntity.NATUREZA_ID, (object)naturezaEntity.DESC);
                    }
                    else
                        stringBuilder.Append("<option>Nenhum registro encontrado!</option>");
                    return stringBuilder.ToString();
                }
                catch
                {
                    Common.GetException(new Exception("Erro ao buscar empresas"));
                    return "error";
                }
            }

            public static string SaveNatureza(string nome, int id)
            {
                try
                {
                    NaturezaEntity objEntity = new NaturezaEntity() { NATUREZA_ID = id, DESC = nome };
                    if (id > 0)
                        return new NaturezaBusiness().Update(objEntity) ? "success" : "alert";
                    return new NaturezaBusiness().Add(objEntity) > 0 ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string RemoveNatureza(int id)
            {
                try
                {
                    return new NaturezaBusiness().Remove(new NaturezaEntity() { NATUREZA_ID = id }) ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string FindNatureza(int index, int pagesize, string nome)
            {
                try
                {
                    NaturezaBusiness naturezaBusiness = new NaturezaBusiness();
                    NaturezaEntity naturezaEntity1 = new NaturezaEntity();
                    naturezaEntity1.DESC = !string.IsNullOrEmpty(nome) ? nome : string.Empty;
                    naturezaEntity1.PAGEINDEX = index;
                    naturezaEntity1.PAGESIZE = pagesize;
                    NaturezaEntity objEntity = naturezaEntity1;
                    List<NaturezaEntity> source = naturezaBusiness.Find(objEntity);
                    StringBuilder stringBuilder = new StringBuilder();
                    if (source != null && source.Count > 0)
                    {
                        stringBuilder.AppendFormat("\r\n                    <table class=\"table responsive table-header-style2\" cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n                    <caption><i class=\"fa fa-table\"></i> Natureza <span class=\"info\">{0} registros encontrados</span></caption>\r\n                    <thead>\r\n                    <tr>\r\n                        \r\n                        <th >Descrição</th>\r\n                        <th class=\"centeralign width10\" colspan=\"2\">Ações</th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>", (object)source.FirstOrDefault<NaturezaEntity>().COUNTER);
                        foreach (NaturezaEntity naturezaEntity2 in source)
                            stringBuilder.AppendFormat("\r\n                    <tr style=\"display: table-row; opacity: 1;\" >\r\n                        <td class=\"w150\"><b>{1}</b></td>   \r\n                        <td class=\"centeralign width5\"><a class=\"btn\"href=\"../natureza/editar.aspx?id={0}\" tooltip=\"tooltip\" data-title=\"Clique para editar\"><i class=\"fa fa-pencil fa-lg\"></i></a></td>\r\n                        <td class=\"centeralign width5\"><a class=\"btn\" href=\"#\" data-action=\"remove\"  tooltip=\"tooltip\" data-title=\"Clique para remover\" data-id=\"{0}\"  ><i class=\"fa fa-trash fa-lg\"></i></a></td>\r\n                    </tr>\r\n                    ", (object)naturezaEntity2.NATUREZA_ID, !string.IsNullOrEmpty(naturezaEntity2.DESC) ? (object)naturezaEntity2.DESC : (object)string.Empty);
                        stringBuilder.AppendFormat("</tbody><tfoot><td colspan=\"3\" class=\"\"><span class=\"info\">{0} registros encontrados</span><input type=\"hidden\" id=\"pager\" name=\"pager\" value=\"{0}\" /></td></tfoot></table>", (object)source.FirstOrDefault<NaturezaEntity>().COUNTER);
                    }
                    else
                        stringBuilder.AppendFormat("<h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "<h4 class=\"alert_error\">Ocorreu um erro.</h4>";
                }
            }

            public static string FindNaturezaByPk(int id)
            {
                try
                {
                    NaturezaEntity byPk = new NaturezaBusiness().FindByPk(new NaturezaEntity() { NATUREZA_ID = id });
                    StringBuilder stringBuilder = new StringBuilder();
                    if (byPk == null)
                        return (string)null;
                    stringBuilder.Append("{\"natureza\":\"[{ ");
                    stringBuilder.AppendFormat("'data_id':'{0}','data_nome':'{1}'", (object)byPk.NATUREZA_ID, !string.IsNullOrEmpty(byPk.DESC) ? (object)byPk.DESC : (object)string.Empty);
                    stringBuilder.Append("}]\"}");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return string.Empty;
                }
            }

            public static string SaveTipoNatureza(string nome, int id)
            {
                try
                {
                    TipoNaturezaEntity objEntity = new TipoNaturezaEntity() { TIPONATUREZA_ID = id, DESC = nome };
                    if (id > 0)
                        return new TipoNaturezaBusiness().Update(objEntity) ? "success" : "alert";
                    return new TipoNaturezaBusiness().Add(objEntity) > 0 ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string RemoveTipoNatureza(int id)
            {
                try
                {
                    return new TipoNaturezaBusiness().Remove(new TipoNaturezaEntity() { TIPONATUREZA_ID = id }) ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string FindTipoNatureza(int index, int pagesize, string nome)
            {
                try
                {
                    TipoNaturezaBusiness naturezaBusiness = new TipoNaturezaBusiness();
                    TipoNaturezaEntity tipoNaturezaEntity1 = new TipoNaturezaEntity();
                    tipoNaturezaEntity1.DESC = !string.IsNullOrEmpty(nome) ? nome : string.Empty;
                    tipoNaturezaEntity1.PAGEINDEX = index;
                    tipoNaturezaEntity1.PAGESIZE = pagesize;
                    TipoNaturezaEntity objEntity = tipoNaturezaEntity1;
                    List<TipoNaturezaEntity> source = naturezaBusiness.Find(objEntity);
                    StringBuilder stringBuilder = new StringBuilder();
                    if (source != null && source.Count > 0)
                    {
                        stringBuilder.AppendFormat("\r\n                    <table class=\"table responsive table-header-style2\" cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n                    <caption><i class=\"fa fa-building\"></i> Tipo Natureza <span class=\"info\">{0} registros encontrados</span></caption>\r\n                    <thead>\r\n                    <tr>\r\n                        \r\n                        <th >Descrição</th>\r\n                        <th class=\"centeralign width10\" colspan=\"2\">Ações</th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>", (object)source.FirstOrDefault<TipoNaturezaEntity>().COUNTER);
                        foreach (TipoNaturezaEntity tipoNaturezaEntity2 in source)
                            stringBuilder.AppendFormat("\r\n                    <tr style=\"display: table-row; opacity: 1;\" >\r\n                        <td class=\"w150\"><b>{1}</b></td>   \r\n                        <td class=\"centeralign width5\"><a class=\"btn\"href=\"../tiponatureza/editar.aspx?id={0}\" tooltip=\"tooltip\" data-title=\"Clique para editar\"><i class=\"fa fa-pencil fa-lg\"></i></a></td>\r\n                        <td class=\"centeralign width5\"><a class=\"btn\" href=\"#\" data-action=\"remove\"  tooltip=\"tooltip\" data-title=\"Clique para remover\" data-id=\"{0}\"  ><i class=\"fa fa-trash fa-lg\"></i></a></td>\r\n                    </tr>\r\n                    ", (object)tipoNaturezaEntity2.TIPONATUREZA_ID, !string.IsNullOrEmpty(tipoNaturezaEntity2.DESC) ? (object)tipoNaturezaEntity2.DESC : (object)string.Empty);
                        stringBuilder.AppendFormat("</tbody><tfoot><td colspan=\"3\" class=\"\"><span class=\"info\">{0} registros encontrados</span><input type=\"hidden\" id=\"pager\" name=\"pager\" value=\"{0}\" /></td></tfoot></table>", (object)source.FirstOrDefault<TipoNaturezaEntity>().COUNTER);
                    }
                    else
                        stringBuilder.AppendFormat("<h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "<h4 class=\"alert_error\">Ocorreu um erro.</h4>";
                }
            }

            public static string FindTipoNaturezaByPk(int id)
            {
                try
                {
                    TipoNaturezaEntity byPk = new TipoNaturezaBusiness().FindByPk(new TipoNaturezaEntity() { TIPONATUREZA_ID = id });
                    StringBuilder stringBuilder = new StringBuilder();
                    if (byPk == null)
                        return (string)null;
                    stringBuilder.Append("{\"tiponatureza\":\"[{ ");
                    stringBuilder.AppendFormat("'data_id':'{0}','data_nome':'{1}'", (object)byPk.TIPONATUREZA_ID, !string.IsNullOrEmpty(byPk.DESC) ? (object)byPk.DESC : (object)string.Empty);
                    stringBuilder.Append("}]\"}");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return string.Empty;
                }
            }

            public static string LoadTipoNatureza()
            {
                try
                {
                    List<TipoNaturezaEntity> tipoNaturezaEntityList = new TipoNaturezaBusiness().Find(new TipoNaturezaEntity());
                    StringBuilder stringBuilder = new StringBuilder();
                    if (tipoNaturezaEntityList.Count > 0)
                    {
                        foreach (TipoNaturezaEntity tipoNaturezaEntity in tipoNaturezaEntityList)
                            stringBuilder.AppendFormat("<option value=\"{0}\">{1}</option>", (object)tipoNaturezaEntity.TIPONATUREZA_ID, (object)tipoNaturezaEntity.DESC);
                    }
                    else
                        stringBuilder.Append("<option>Nenhum registro encontrado!</option>");
                    return stringBuilder.ToString();
                }
                catch
                {
                    Common.GetException(new Exception("Erro ao buscar empresas"));
                    return "error";
                }
            }

            public static string SaveConfig(
              int eid,
              int tip,
              int nid,
              int dia,
              string crt,
              string instrucao,
              string tpm)
            {
                try
                {
                    return new ConfiguracaoBusiness().Add(new ConfiguracaoEntity() { INSTRUCAO = instrucao, EMPRESA_ID = eid, TIPONATUREZA_ID = tip, NATUREZA_ID = nid, PRAZO = dia, CRITICIDADE = crt, TIPO_MANIFESTACAO = tpm }) > 0 ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string GerarConfig(int eid)
            {
                try
                {
                    return new ConfiguracaoBusiness().Gerar(new ConfiguracaoEntity() { EMPRESA_ID = eid }) > 0 ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string RemoveConfiguracao(int id)
            {
                try
                {
                    return new ConfiguracaoBusiness().Remove(new ConfiguracaoEntity() { CONFIGURACAO_ID = id }) ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string LoadConfiguracao(int index, int pagesize, int empresaId)
            {
                try
                {
                    ConfiguracaoBusiness configuracaoBusiness = new ConfiguracaoBusiness();
                    ConfiguracaoEntity configuracaoEntity1 = new ConfiguracaoEntity();
                    configuracaoEntity1.EMPRESA_ID = empresaId;
                    configuracaoEntity1.PAGEINDEX = index;
                    configuracaoEntity1.PAGESIZE = pagesize;
                    ConfiguracaoEntity objEntity = configuracaoEntity1;
                    List<ConfiguracaoEntity> source = configuracaoBusiness.Find(objEntity);
                    StringBuilder stringBuilder = new StringBuilder();
                    if (source != null && source.Count > 0)
                    {
                        stringBuilder.AppendFormat("\r\n                    <table class=\"table responsive table-header-style2\" cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n                    <caption><i class=\"fa fa-building\"></i> Configuracao <span class=\"info\">{0} registros encontrados</span></caption>\r\n                    <thead>\r\n                    <tr>\r\n                        <th class=\"width5 text-center\">Prioridade</th>\r\n                        <th class=\"width5 text-center\">Prazo</th>\r\n                        <th class=\"w150\">Natureza</th>\r\n                        <th>Tipo Natureza</th>\r\n                        <th>Tipo Relato</th>\r\n                        <th class=\"centeralign width5\">Ações</th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>", (object)source.FirstOrDefault<ConfiguracaoEntity>().COUNTER);
                        foreach (ConfiguracaoEntity configuracaoEntity2 in source)
                        {
                            NaturezaEntity byPk1 = new NaturezaBusiness().FindByPk(new NaturezaEntity() { NATUREZA_ID = configuracaoEntity2.NATUREZA_ID });
                            TipoNaturezaEntity byPk2 = new TipoNaturezaBusiness().FindByPk(new TipoNaturezaEntity() { TIPONATUREZA_ID = configuracaoEntity2.TIPONATUREZA_ID });
                            stringBuilder.AppendFormat("\r\n                        <tr style=\"display: table-row; opacity: 1;\" style=\"vertical-align\">\r\n                        <td class=\"width5 text-center {7}\"><b>{1}</b></td>\r\n                        <td class=\"width5 text-center\">{6}</td>                        \r\n                        <td class=\"w150\"> <span class=\"text-left\" data-title=\"{2}\" tooltip=\"tooltip\">{3}</span></td>\r\n                        <td>                <span class=\"text-left\" data-title=\"{4}\" tooltip=\"tooltip\">{5}</span></td>                    \r\n                        <td>                <span class=\"text-left\" data-title=\"{8}\" tooltip=\"tooltip\">{8}</span></td>                    \r\n                        <td class=\"width5 text-center\"><a class=\"btn\" href=\"#\" data-action=\"remove\"  tooltip=\"tooltip\" data-title=\"Clique para remover\" data-id=\"{0}\"  ><i class=\"fa fa-trash fa-lg\"></i></a></td>\r\n                    </tr>\r\n                    ", (object)configuracaoEntity2.CONFIGURACAO_ID, string.IsNullOrEmpty(configuracaoEntity2.CRITICIDADE) || !(configuracaoEntity2.CRITICIDADE.ToUpper() == "HIGH") ? (string.IsNullOrEmpty(configuracaoEntity2.CRITICIDADE) || !(configuracaoEntity2.CRITICIDADE.ToUpper() == "LOW") ? (object)"MODERADA" : (object)"BAIXA") : (object)"ALTA", !string.IsNullOrEmpty(byPk1.DESC) ? (object)byPk1.DESC : (object)string.Empty, !string.IsNullOrEmpty(byPk1.DESC) ? (object)Common.SetDots(byPk1.DESC, 250) : (object)string.Empty, !string.IsNullOrEmpty(byPk2.DESC) ? (object)byPk2.DESC : (object)string.Empty, !string.IsNullOrEmpty(byPk2.DESC) ? (object)Common.SetDots(byPk2.DESC, 250) : (object)string.Empty, (object)configuracaoEntity2.PRAZO, configuracaoEntity2.CRITICIDADE.ToUpper() == "HIGH" || configuracaoEntity2.CRITICIDADE.ToUpper() == "ALTA" ? (object)"danger" : (configuracaoEntity2.CRITICIDADE.ToUpper() == "LOW" || configuracaoEntity2.CRITICIDADE.ToUpper() == "BAIXA" ? (object)"success" : (object)"warning"), !string.IsNullOrEmpty(configuracaoEntity2.TIPO_MANIFESTACAO) ? (object)configuracaoEntity2.TIPO_MANIFESTACAO : (object)string.Empty);
                        }
                        stringBuilder.AppendFormat("</tbody><tfoot><td colspan=\"6\" class=\"\"><span class=\"info\">{0} registros encontrados</span><input type=\"hidden\" id=\"pager\" name=\"pager\" value=\"{0}\" /></td></tfoot></table>", (object)source.FirstOrDefault<ConfiguracaoEntity>().COUNTER);
                    }
                    else
                        stringBuilder.AppendFormat("<h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "<h4 class=\"alert_error\">Ocorreu um erro.</h4>";
                }
            }

            public static string FindConfiguracao(int eid, int nid, int tid, string tpm)
            {
                try
                {
                    ConfiguracaoEntity configuracaoEntity = new ConfiguracaoBusiness().Find(new ConfiguracaoEntity() { EMPRESA_ID = eid, NATUREZA_ID = nid, TIPONATUREZA_ID = tid, TIPO_MANIFESTACAO = tpm }).FirstOrDefault<ConfiguracaoEntity>();
                    if (configuracaoEntity != null && !string.IsNullOrEmpty(configuracaoEntity.INSTRUCAO))
                        return configuracaoEntity.INSTRUCAO;
                    return "Nenhuma instrução encontrada para os parametros informado!";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "Ocorreu um erro";
                }
            }

            public static string FindCriticidade(int eid, int nid, int tid, string tpm)
            {
                try
                {
                    ConfiguracaoEntity configuracaoEntity = new ConfiguracaoBusiness().Find(new ConfiguracaoEntity() { EMPRESA_ID = eid, NATUREZA_ID = nid, TIPONATUREZA_ID = tid, TIPO_MANIFESTACAO = tpm }).FirstOrDefault<ConfiguracaoEntity>();
                    if (configuracaoEntity != null && !string.IsNullOrEmpty(configuracaoEntity.CRITICIDADE))
                        return configuracaoEntity.CRITICIDADE;
                    return "LOW";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "Ocorreu um erro";
                }
            }

            public static string FindConfiguracaoByPk(int id)
            {
                try
                {
                    ConfiguracaoEntity byPk = new ConfiguracaoBusiness().FindByPk(new ConfiguracaoEntity() { CONFIGURACAO_ID = id });
                    StringBuilder stringBuilder = new StringBuilder();
                    if (byPk == null)
                        return (string)null;
                    stringBuilder.Append("{\"unidade\":\"[{ ");
                    stringBuilder.AppendFormat("'data_id':'{0}','data_natureza_id':'{1}','data_tiponaturaza_id':'{2}','data_prazo':'{3}','data_criticidade':'{4}','data_instrucao':'{5}'", (object)byPk.CONFIGURACAO_ID, (object)byPk.NATUREZA_ID, (object)byPk.TIPONATUREZA_ID, (object)byPk.PRAZO, (object)byPk.CRITICIDADE, (object)byPk.INSTRUCAO);
                    stringBuilder.Append("}]\"}");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return string.Empty;
                }
            }

            public static string FindConfigByKeys(int eid, int nid, int tid, string tpm)
            {
                try
                {
                    ConfiguracaoEntity configuracaoEntity = new ConfiguracaoBusiness().Find(new ConfiguracaoEntity() { EMPRESA_ID = eid, NATUREZA_ID = nid, TIPONATUREZA_ID = tid, TIPO_MANIFESTACAO = tpm }).FirstOrDefault<ConfiguracaoEntity>();
                    StringBuilder stringBuilder = new StringBuilder();
                    if (configuracaoEntity == null)
                        return (string)null;
                    stringBuilder.Append("{\"config\":\"[{ ");
                    stringBuilder.AppendFormat("'data_id':'{0}','data_natureza_id':'{1}','data_tiponatureza_id':'{2}','data_prazo':'{3}','data_criticidade':'{4}','data_instrucao':'{5}'", (object)configuracaoEntity.CONFIGURACAO_ID, (object)configuracaoEntity.NATUREZA_ID, (object)configuracaoEntity.TIPONATUREZA_ID, (object)configuracaoEntity.PRAZO, (object)configuracaoEntity.CRITICIDADE, (object)configuracaoEntity.INSTRUCAO);
                    stringBuilder.Append("}]\"}");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return string.Empty;
                }
            }

            public static string RemoveAnexo(int aid, int uid)
            {
                try
                {
                    return new AnexoBusiness().Remove(new AnexoEntity() { ANEXO_ID = aid }) ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string RemoveAnexo(int aid)
            {
                try
                {
                    return new AnexoBusiness().Remove(new AnexoEntity() { ANEXO_ID = aid }) ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string FindAnexoFinal(string guid)
            {
                try
                {
                    if (string.IsNullOrEmpty(guid))
                        return " <h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>";
                    AnexoEntity anexoEntity = new AnexoBusiness().Find(new AnexoEntity() { GUID = guid, STATUS = true }).FirstOrDefault<AnexoEntity>();
                    StringBuilder stringBuilder = new StringBuilder();
                    if (anexoEntity == null)
                        return stringBuilder.ToString();
                    stringBuilder.Append("<table id=\"tbanexocontainerrev\" class=\"table responsive table-header-style2 tablesorteranx\" cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n                            <caption><i class=\"fa fa-file\"></i> Anexo Final</caption>\r\n                            <thead>\r\n                            <tr>\r\n                            <th class=\"width5\">Data</th>\r\n                            <th class=\"w150\">Nome</th>\r\n                            <th class=\"text-center width5\" >Ações</th>\r\n                            </tr>\r\n                            </thead>\r\n                            <tbody");
                    if (!string.IsNullOrEmpty(anexoEntity.URL))
                    {
                        HtmlFactory.DownloadSftp(((IEnumerable<string>)anexoEntity.URL.Split('/')).Last<string>());
                        stringBuilder.AppendFormat("<tr data-anexoid=\"{0}\">", (object)anexoEntity.ANEXO_ID);
                        stringBuilder.AppendFormat("<td class=\"width5\"><b>{0}</b></td>", anexoEntity.DT_ANEXO != DateTime.MinValue ? (object)anexoEntity.DT_ANEXO.ToString("dd/MM/yyyy") : (object)string.Empty);
                        stringBuilder.AppendFormat("<td class=\"w150\"><b><a href=\"{0}\" onclick=\"window.open(this.href);return false;\">{1}</a></b></td>", (object)(Common.App.GetValue("APP_ROOT_DOWN", typeof(string)).ToString() + ((IEnumerable<string>)anexoEntity.URL.Split('/')).Last<string>()), (object)anexoEntity.NOME);
                        stringBuilder.AppendFormat("<td class=\"width5\"><a onclick=\"SHOW_MODAL_ANEXOFINAL_REMOVE({0}); return false;\" class=\"btn btn-default btn-block\"><i class=\"fa fa-trash-o\"></i></a></td>", (object)anexoEntity.ANEXO_ID);
                        stringBuilder.AppendFormat("</tr>");
                    }
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "<h4 class=\"alert_error\">Tente novamente mais tarde!</h4>";
                }
            }

            public static string FindAnexo(string guid)
            {
                try
                {
                    if (string.IsNullOrEmpty(guid))
                        return " <h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>";
                    List<AnexoEntity> anexoEntityList = new AnexoBusiness().Find(new AnexoEntity() { GUID = guid, STATUS = false });
                    StringBuilder stringBuilder = new StringBuilder();
                    if (anexoEntityList == null || anexoEntityList.Count == 0)
                    {
                        stringBuilder.Append(" <h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>");
                    }
                    else
                    {
                        stringBuilder.Append("<table id=\"tbanexocontainer\" class=\"table responsive table-header-style2 tablesorteranx\" cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n                            <caption><i class=\"fa fa-file\"></i> Anexos</caption>\r\n                            <thead>\r\n                            <tr>\r\n                            <th class=\"width5\">Data</th>\r\n                            <th class=\"w150\">Nome</th>\r\n                            <th class=\"text-center width5\" >Ações</th>\r\n                            </tr>\r\n                            </thead>\r\n                            <tbody");
                        foreach (AnexoEntity anexoEntity in anexoEntityList)
                        {
                            if (!string.IsNullOrEmpty(anexoEntity.URL))
                            {
                                string str = string.Empty;
                                if (anexoEntity.URL.Contains("\\"))
                                    str = ((IEnumerable<string>)anexoEntity.URL.Split('\\')).Last<string>();
                                if (anexoEntity.URL.Contains("/"))
                                    str = ((IEnumerable<string>)anexoEntity.URL.Split('/')).Last<string>();
                                anexoEntity.URL = Common.App.GetValue("APP_ROOT_DOWN", typeof(string)).ToString() + str;
                                stringBuilder.AppendFormat("<tr data-anexoid=\"{0}\">", (object)anexoEntity.ANEXO_ID);
                                stringBuilder.AppendFormat("<td class=\"width5\"><b>{0}</b></td>", anexoEntity.DT_ANEXO != DateTime.MinValue ? (object)anexoEntity.DT_ANEXO.ToString("dd/MM/yyyy") : (object)string.Empty);
                                stringBuilder.AppendFormat("<td class=\"w150\"><b><a href=\"{0}\" onclick=\"window.open(this.href);return false;\">{1}</a></b></td>", (object)anexoEntity.URL, (object)anexoEntity.NOME);
                                stringBuilder.AppendFormat("<td class=\"width5\"><a onclick=\"SHOW_MODAL_ANEXOFINAL_REMOVE({0}); return false;\" class=\"btn btn-default btn-block\"><i class=\"fa fa-trash-o\"></i></a></td>", (object)anexoEntity.ANEXO_ID);
                                stringBuilder.AppendFormat("</tr>");
                            }
                        }
                    }
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "<h4 class=\"alert_error\">Tente novamente mais tarde!</h4>";
                }
            }

            public static string AddAnexo(string nome, string url, string guid)
            {
                try
                {
                    return new AnexoBusiness().Add(new AnexoEntity() { NOME = nome, STATUS = false, GUID = guid, URL = url }) > 0 ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string AddAnexo(string nome, string url, string guid, string pfl)
            {
                try
                {
                    return new AnexoBusiness().Add(new AnexoEntity() { NOME = nome, STATUS = pfl == "2", GUID = guid, URL = url }) > 0 ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string FindAnexoAcompanhe(string guid)
            {
                try
                {
                    if (string.IsNullOrEmpty(guid))
                        return string.Empty;
                    List<AnexoEntity> anexoEntityList = new AnexoBusiness().Find(new AnexoEntity() { GUID = guid, STATUS = false });
                    StringBuilder stringBuilder = new StringBuilder();
                    if (anexoEntityList == null || anexoEntityList.Count <= 0)
                        return stringBuilder.ToString();
                    stringBuilder.Append("<table id=\"tbanexocontainer\" class=\"table responsive table-header-style2 tablesorteranx\" cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n                            <caption><i class=\"fa fa-file\"></i> Anexos</caption>\r\n                            <thead>\r\n                            <tr>\r\n                            <th class=\"width5\">Data</th>\r\n                            <th class=\"w150\">Nome</th>\r\n                            <th class=\"text-center width5\" >Ações</th>\r\n                            </tr>\r\n                            </thead>\r\n                            <tbody");
                    foreach (AnexoEntity anexoEntity in anexoEntityList)
                    {
                        if (!string.IsNullOrEmpty(anexoEntity.URL))
                        {
                            HtmlFactory.DownloadSftp(((IEnumerable<string>)anexoEntity.URL.Split('/')).Last<string>());
                            stringBuilder.AppendFormat("\r\n                                <tr data-anexoid=\"{3}\">\r\n                                <td class=\"width5\"><b>{0}</b></td>     \r\n                                <td class=\"w150\"><b><a href=\"{2}\" onclick=\"window.open(this.href);return false;\">{1}</a></b></td> \r\n                                <td class=\"width5\">{4}</td>", anexoEntity.DT_ANEXO > DateTime.MinValue ? (object)anexoEntity.DT_ANEXO.ToString("dd/MM/yyyy") : (object)string.Empty, (object)anexoEntity.NOME, (object)(Common.App.GetValue("APP_ROOT_DOWN", typeof(string)).ToString() + ((IEnumerable<string>)anexoEntity.URL.Split('/')).Last<string>()), (object)anexoEntity.ANEXO_ID, anexoEntity.RELATO_ID > 0 ? (object)string.Empty : (object)string.Format("<a onclick=\"SHOW_MODAL_ANEXO_REMOVE({0}); return false;\" class=\"btn btn-default btn-block\"><i class=\"fa fa-trash-o\"></i></a>", (object)anexoEntity.ANEXO_ID));
                        }
                    }
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "<h4 class=\"alert_error\">Nenhum anexo encontrado.</h4>";
                }
            }

            public static string SaveEmpresa(
              string prefix,
              string nome,
              string titulocanal,
              string titulocanales,
              string titulocanalen,
              string titulocanalfr,
              string logo,
              string exibeu,
              string exibea,
              string exibep,
              int id,
              string email,
              int modelo)
            {
                try
                {
                    EmpresaEntity objEntity = new EmpresaEntity() { EMPRESA_ID = id, NOME = nome.ToUpper(), EXIBE_AREA = exibea, EXIBE_PRODUTO = exibep, EXIBE_UNIDADE = exibeu, PREFIXO = prefix, LOGO = logo, TITULO = titulocanal, TITULO_EN = titulocanalen, TITULO_ES = titulocanales, TITULO_FR = titulocanalfr, EMAIL = email, MODELO_ID = modelo };
                    if (id > 0)
                        return new EmpresaBusiness().Update(objEntity) ? "success" : "alert";
                    return new EmpresaBusiness().Add(objEntity) > 0 ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string RemoveEmpresa(int id)
            {
                try
                {
                    return new EmpresaBusiness().Remove(new EmpresaEntity() { EMPRESA_ID = id }) ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string FindEmpresa(int index, int pagesize, string nome)
            {
                try
                {
                    EmpresaBusiness empresaBusiness = new EmpresaBusiness();
                    EmpresaEntity empresaEntity1 = new EmpresaEntity();
                    empresaEntity1.NOME = !string.IsNullOrEmpty(nome) ? nome : string.Empty;
                    empresaEntity1.PAGEINDEX = index;
                    empresaEntity1.PAGESIZE = pagesize;
                    EmpresaEntity objEntity = empresaEntity1;
                    List<EmpresaEntity> source = empresaBusiness.Find(objEntity);
                    StringBuilder stringBuilder = new StringBuilder();
                    if (source != null && source.Count > 0)
                    {
                        stringBuilder.AppendFormat("\r\n                    <table class=\"table responsive table-header-style2\" cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n                    <caption><i class=\"fa fa-building\"></i> Empresa <span class=\"info\">{0} registros encontrados</span></caption>\r\n                    <thead>\r\n                    <tr>\r\n                        <th class=\"width5\">Sigla</th>\r\n                        <th >Código / Nome</th>\r\n                        <th >Email</th>\r\n                        <th class=\"centeralign width10\" colspan=\"2\">Ações</th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>", (object)source.FirstOrDefault<EmpresaEntity>().COUNTER);
                        foreach (EmpresaEntity empresaEntity2 in source)
                            stringBuilder.AppendFormat("\r\n                    <tr style=\"display: table-row; opacity: 1;\" >\r\n                        <td class=\"width5\"><b>{1}</b></td>     \r\n                        <td class=\"w150\"><b>{0}-{2}</b></td>   \r\n                        <td class=\"w150\"><b>{3}</b></td>                    \r\n                        <td class=\"centeralign width5\"><a class=\"btn\"href=\"../empresas/editar.aspx?id={0}\" tooltip=\"tooltip\" data-title=\"Clique para editar\"><i class=\"fa fa-pencil fa-lg\"></i></a></td>\r\n                        <td class=\"centeralign width5\"><a class=\"btn\" href=\"#\" data-action=\"remove\"  tooltip=\"tooltip\" data-title=\"Clique para remover\" data-id=\"{0}\"  ><i class=\"fa fa-trash fa-lg\"></i></a></td>\r\n                    </tr>\r\n                    ", (object)empresaEntity2.EMPRESA_ID, (object)empresaEntity2.PREFIXO, !string.IsNullOrEmpty(empresaEntity2.NOME) ? (object)empresaEntity2.NOME : (object)string.Empty, !string.IsNullOrEmpty(empresaEntity2.EMAIL) ? (object)empresaEntity2.EMAIL : (object)string.Empty);
                        stringBuilder.AppendFormat("</tbody><tfoot><td colspan=\"5\" class=\"\"><span class=\"info\">{0} registros encontrados</span><input type=\"hidden\" id=\"pager\" name=\"pager\" value=\"{0}\" /></td></tfoot></table>", (object)source.FirstOrDefault<EmpresaEntity>().COUNTER);
                    }
                    else
                        stringBuilder.AppendFormat("<h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "<h4 class=\"alert_error\">Ocorreu um erro.</h4>";
                }
            }

            public static string FindEmpresaByPk(int id)
            {
                try
                {
                    EmpresaEntity byPk = new EmpresaBusiness().FindByPk(new EmpresaEntity() { EMPRESA_ID = id });
                    StringBuilder stringBuilder1 = new StringBuilder();
                    if (byPk == null)
                        return (string)null;
                    if (!string.IsNullOrEmpty(byPk.LOGO))
                        HtmlFactory.DownloadSftp(((IEnumerable<string>)byPk.LOGO.Split('/')).Last<string>());
                    stringBuilder1.Append("{\"empresa\":\"[{ ");
                    StringBuilder stringBuilder2 = stringBuilder1;
                    object[] objArray = new object[13];
                    objArray[0] = (object)byPk.EMPRESA_ID;
                    objArray[1] = !string.IsNullOrEmpty(byPk.NOME) ? (object)byPk.NOME : (object)string.Empty;
                    string str;
                    if (string.IsNullOrEmpty(byPk.LOGO))
                        str = Common.App.GetValue("APP_NOIMAGE", typeof(string)).ToString();
                    else
                        str = Common.App.GetValue("APP_ROOT_IMG", typeof(string)).ToString() + ((IEnumerable<string>)byPk.LOGO.Split('/')).Last<string>();
                    objArray[2] = (object)str;
                    objArray[3] = !string.IsNullOrEmpty(byPk.TITULO) ? (object)byPk.TITULO : (object)string.Empty;
                    objArray[4] = !string.IsNullOrEmpty(byPk.EXIBE_AREA) ? (object)byPk.EXIBE_AREA : (object)string.Empty;
                    objArray[5] = !string.IsNullOrEmpty(byPk.EXIBE_PRODUTO) ? (object)byPk.EXIBE_PRODUTO : (object)string.Empty;
                    objArray[6] = !string.IsNullOrEmpty(byPk.EXIBE_UNIDADE) ? (object)byPk.EXIBE_UNIDADE : (object)string.Empty;
                    objArray[7] = !string.IsNullOrEmpty(byPk.PREFIXO) ? (object)byPk.PREFIXO : (object)string.Empty;
                    objArray[8] = !string.IsNullOrEmpty(byPk.EMAIL) ? (object)byPk.EMAIL : (object)string.Empty;
                    objArray[9] = (object)byPk.MODELO_ID;
                    objArray[10] = !string.IsNullOrEmpty(byPk.TITULO_EN) ? (object)byPk.TITULO_EN : (object)string.Empty;
                    objArray[11] = !string.IsNullOrEmpty(byPk.TITULO_ES) ? (object)byPk.TITULO_ES : (object)string.Empty;
                    objArray[12] = !string.IsNullOrEmpty(byPk.TITULO_FR) ? (object)byPk.TITULO_FR : (object)string.Empty;
                    stringBuilder2.AppendFormat("'data_id':'{0}','data_nome':'{1}','data_logo':'{2}','data_titulocanal':'{3}','data_exibea':'{4}','data_exibep':'{5}','data_exibeu':'{6}','data_prefixo':'{7}','data_email':'{8}','data_modelo':'{9}','data_tituloen':'{10}','data_tituloes':'{11}','data_titulofr':'{12}'", objArray);
                    stringBuilder1.Append("}]\"}");
                    return stringBuilder1.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return string.Empty;
                }
            }

            /// <summary>
            /// Carrega todas as empresas ATIVAS
            /// </summary>
            /// <returns></returns>
            public static string LoadEmpresa()
            {
                try
                {
                    List<EmpresaEntity> empresaEntityList = new EmpresaBusiness().Find(new EmpresaEntity()
                    {
                        FLG_ATIVA = "S"
                    });
                    StringBuilder stringBuilder = new StringBuilder();
                    if (empresaEntityList.Count > 0)
                    {
                        stringBuilder.Append("<option value=\"\" >Selecione...</option>");
                        foreach (EmpresaEntity empresaEntity in empresaEntityList)
                            stringBuilder.AppendFormat("<option value=\"{0}\">{1}</option>", (object)empresaEntity.EMPRESA_ID, (object)empresaEntity.NOME);
                    }
                    else
                        stringBuilder.Append("<option>Nenhum registro encontrado!</option>");
                    return stringBuilder.ToString();
                }
                catch
                {
                    Common.GetException(new Exception("Erro ao buscar empresas"));
                    return "error";
                }
            }

            public static string LoadEmpresa(int id)
            {
                try
                {
                    List<EmpresaEntity> empresaEntityList = new EmpresaBusiness().Find(new EmpresaEntity() { EMPRESA_ID = id });
                    StringBuilder stringBuilder = new StringBuilder();
                    if (empresaEntityList.Count > 0)
                    {
                        foreach (EmpresaEntity empresaEntity in empresaEntityList)
                            stringBuilder.AppendFormat("<option value=\"{0}\">{1}</option>", (object)empresaEntity.EMPRESA_ID, (object)empresaEntity.NOME);
                    }
                    else
                        stringBuilder.Append("<option value=\"0\" selected=\"selected\">Nenhum...</option>");
                    return stringBuilder.ToString();
                }
                catch
                {
                    Common.GetException(new Exception("Erro ao buscar empresas"));
                    return "error";
                }
            }

            public static string SaveUnidade(string nome, int empresaId, int id, string idioma)
            {
                try
                {
                    UnidadeEntity objEntity = new UnidadeEntity() { UNIDADE_ID = id, NOME = nome.ToUpper(), EMPRESA_ID = empresaId, IDIOMA = idioma };
                    if (id > 0)
                        return new UnidadeBusiness().Update(objEntity) ? "success" : "alert";
                    return new UnidadeBusiness().Add(objEntity) > 0 ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string RemoveUnidade(int id)
            {
                try
                {
                    return new UnidadeBusiness().Remove(new UnidadeEntity() { UNIDADE_ID = id }) ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string FindUnidade(
              int index,
              int pagesize,
              string nome,
              string idioma,
              int empresaId)
            {
                try
                {
                    UnidadeBusiness unidadeBusiness = new UnidadeBusiness();
                    UnidadeEntity unidadeEntity1 = new UnidadeEntity();
                    unidadeEntity1.NOME = !string.IsNullOrEmpty(nome) ? nome : string.Empty;
                    unidadeEntity1.EMPRESA_ID = empresaId;
                    unidadeEntity1.PAGEINDEX = index;
                    unidadeEntity1.PAGESIZE = pagesize;
                    unidadeEntity1.IDIOMA = idioma;
                    UnidadeEntity objEntity = unidadeEntity1;
                    List<UnidadeEntity> source = unidadeBusiness.Find(objEntity);
                    StringBuilder stringBuilder = new StringBuilder();
                    if (source != null && source.Count > 0)
                    {
                        stringBuilder.AppendFormat("\r\n                    <table class=\"table responsive table-header-style2\" cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n                    <caption><i class=\"fa fa-building\"></i> Unidade <span class=\"info\">{0} registros encontrados</span></caption>\r\n                    <thead>\r\n                    <tr>\r\n                        <th scope=\"col\">Nome</th>\r\n                        <th class=\"centeralign width5\">Ações</th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>", (object)source.FirstOrDefault<UnidadeEntity>().COUNTER);
                        foreach (UnidadeEntity unidadeEntity2 in source)
                            stringBuilder.AppendFormat("\r\n                    <tr style=\"display: table-row; opacity: 1;\" style=\"vertical-align\">\r\n                    <td class=\"w150\"><b>{1}</b></td>                    \r\n                    <td class=\"centeralign width5\"><a class=\"btn\" href=\"#\" data-action=\"remove\"  tooltip=\"tooltip\" data-title=\"Clique para remover\" data-id=\"{0}\"  ><i class=\"fa fa-trash fa-lg\"></i></a></td>\r\n                    </tr>\r\n                    ", (object)unidadeEntity2.UNIDADE_ID, !string.IsNullOrEmpty(unidadeEntity2.NOME) ? (object)unidadeEntity2.NOME : (object)string.Empty);
                        stringBuilder.AppendFormat("</tbody><tfoot><td colspan=\"2\" class=\"\"><span class=\"info\">{0} registros encontrados</span><input type=\"hidden\" id=\"pager\" name=\"pager\" value=\"{0}\" /></td></tfoot></table>", (object)source.FirstOrDefault<UnidadeEntity>().COUNTER);
                    }
                    else
                        stringBuilder.AppendFormat("<h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "<h4 class=\"alert_error\">Ocorreu um erro.</h4>";
                }
            }

            public static string FindUnidadeByPk(int id)
            {
                try
                {
                    UnidadeEntity byPk = new UnidadeBusiness().FindByPk(new UnidadeEntity() { UNIDADE_ID = id });
                    StringBuilder stringBuilder = new StringBuilder();
                    if (byPk == null)
                        return (string)null;
                    stringBuilder.Append("{\"unidade\":\"[{ ");
                    stringBuilder.AppendFormat("'data_id':'{0}','data_nome':'{1}'", (object)byPk.UNIDADE_ID.ToString(), !string.IsNullOrEmpty(byPk.NOME) ? (object)byPk.NOME : (object)string.Empty);
                    stringBuilder.Append("}]\"}");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return string.Empty;
                }
            }

            public static string LoadUnidade(int empresaId, string idioma)
            {
                try
                {
                    if (empresaId <= 0)
                        return "required";
                    List<UnidadeEntity> unidadeEntityList = new UnidadeBusiness().Find(new UnidadeEntity() { EMPRESA_ID = empresaId, IDIOMA = idioma });
                    StringBuilder stringBuilder = new StringBuilder();
                    if (unidadeEntityList.Count > 0)
                    {
                        foreach (UnidadeEntity unidadeEntity in unidadeEntityList)
                            stringBuilder.AppendFormat("<option value=\"{0}\">{1}</option>", (object)unidadeEntity.UNIDADE_ID, (object)unidadeEntity.NOME);
                    }
                    else
                        stringBuilder.Append("<option value=\"0\" selected=\"selected\">Nenhum...</option>");
                    return stringBuilder.ToString();
                }
                catch
                {
                    Common.GetException(new Exception("Erro ao buscar unidades"));
                    return "error";
                }
            }

            public static string SaveProduto(string nome, int unidadeId, int id)
            {
                try
                {
                    ProdutoEntity objEntity = new ProdutoEntity() { PRODUTO_ID = id, NOME = nome.ToUpper(), UNIDADE_ID = unidadeId };
                    string empty = string.Empty;
                    if (id > 0)
                        return new ProdutoBusiness().Update(objEntity) ? "success" : "alert";
                    return new ProdutoBusiness().Add(objEntity) > 0 ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string RemoveProduto(int id)
            {
                try
                {
                    string empty = string.Empty;
                    return !new ProdutoBusiness().Remove(new ProdutoEntity() { PRODUTO_ID = id }) ? "alert" : "success";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string FindProduto(int index, int pagesize, string nome, int unidadeId)
            {
                try
                {
                    ProdutoBusiness produtoBusiness = new ProdutoBusiness();
                    ProdutoEntity produtoEntity1 = new ProdutoEntity();
                    produtoEntity1.UNIDADE_ID = unidadeId;
                    produtoEntity1.NOME = !string.IsNullOrEmpty(nome) ? nome : string.Empty;
                    produtoEntity1.PAGEINDEX = index;
                    produtoEntity1.PAGESIZE = pagesize;
                    ProdutoEntity objEntity = produtoEntity1;
                    List<ProdutoEntity> source = produtoBusiness.Find(objEntity);
                    StringBuilder stringBuilder = new StringBuilder();
                    if (source != null && source.Count > 0)
                    {
                        stringBuilder.AppendFormat("\r\n                    <table class=\"table responsive table-header-style2\" cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n                    <caption><i class=\"fa fa-building\"></i> Produto <span class=\"info\">{0} registros encontrados</span></caption>\r\n                    <thead>\r\n                    <tr>\r\n                        <th scope=\"col\">Nome</th>\r\n                        <th class=\"centeralign width5\">Ações</th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>", (object)source.FirstOrDefault<ProdutoEntity>().COUNTER);
                        foreach (ProdutoEntity produtoEntity2 in source)
                            stringBuilder.AppendFormat("\r\n                    <tr style=\"display: table-row; opacity: 1;\" style=\"vertical-align\">\r\n                    <td class=\"w150\"><b>{1}</b></td>                    \r\n                    <td class=\"centeralign width5\"><a class=\"btn\" href=\"#\" data-action=\"remove\"  tooltip=\"tooltip\" data-title=\"Clique para remover\" data-id=\"{0}\"  ><i class=\"fa fa-trash fa-lg\"></i></a></td>\r\n                    </tr>\r\n                    ", (object)produtoEntity2.PRODUTO_ID, !string.IsNullOrEmpty(produtoEntity2.NOME) ? (object)produtoEntity2.NOME : (object)string.Empty);
                        stringBuilder.AppendFormat("</tbody><tfoot><td colspan=\"2\" class=\"\"><span class=\"info\">{0} registros encontrados</span><input type=\"hidden\" id=\"pager\" name=\"pager\" value=\"{0}\" /></td></tfoot></table>", (object)source.FirstOrDefault<ProdutoEntity>().COUNTER);
                    }
                    else
                        stringBuilder.AppendFormat("<h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "<h4 class=\"alert_error\">Ocorreu um erro.</h4>";
                }
            }

            public static string FindProdutoByPk(int id)
            {
                try
                {
                    ProdutoEntity byPk = new ProdutoBusiness().FindByPk(new ProdutoEntity() { PRODUTO_ID = id });
                    StringBuilder stringBuilder = new StringBuilder();
                    if (byPk == null)
                        return (string)null;
                    stringBuilder.Append("{\"produto\":\"[{ ");
                    stringBuilder.AppendFormat("'data_id':'{0}','data_nome':'{1}'", (object)byPk.PRODUTO_ID.ToString(), !string.IsNullOrEmpty(byPk.NOME) ? (object)byPk.NOME : (object)string.Empty);
                    stringBuilder.Append("}]\"}");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return string.Empty;
                }
            }

            public static string LoadProduto(int unidadeId)
            {
                try
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    if (unidadeId > 0)
                    {
                        List<ProdutoEntity> produtoEntityList = new ProdutoBusiness().Find(new ProdutoEntity() { UNIDADE_ID = unidadeId });
                        if (produtoEntityList.Count > 0)
                        {
                            foreach (ProdutoEntity produtoEntity in produtoEntityList)
                                stringBuilder.AppendFormat("<option value=\"{0}\">{1}</option>", (object)produtoEntity.PRODUTO_ID, (object)produtoEntity.NOME);
                        }
                        else
                            stringBuilder.Append("<option value=\"0\" selected=\"selected\">Nenhum...</option>");
                    }
                    else
                        stringBuilder.Append("<option value=\"0\" selected=\"selected\">Nenhum...</option>");
                    return stringBuilder.ToString();
                }
                catch
                {
                    Common.GetException(new Exception("Erro ao buscar produtos"));
                    return "error";
                }
            }

            public static string SaveArea(string nome, int produtoId, int id)
            {
                try
                {
                    AreaEntity objEntity = new AreaEntity() { AREA_ID = id, NOME = nome.ToUpper(), PRODUTO_ID = produtoId };
                    string empty = string.Empty;
                    return id <= 0 ? (new AreaBusiness().Add(objEntity) <= 0 ? "alert" : "success") : (!new AreaBusiness().Update(objEntity) ? "alert" : "success");
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string RemoveArea(int id)
            {
                try
                {
                    string empty = string.Empty;
                    return !new AreaBusiness().Remove(new AreaEntity() { AREA_ID = id }) ? "alert" : "success";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string FindArea(int index, int pagesize, string nome, int produtoId)
            {
                try
                {
                    AreaBusiness areaBusiness = new AreaBusiness();
                    AreaEntity areaEntity1 = new AreaEntity();
                    areaEntity1.NOME = !string.IsNullOrEmpty(nome) ? nome : string.Empty;
                    areaEntity1.PRODUTO_ID = produtoId;
                    areaEntity1.PAGEINDEX = index;
                    areaEntity1.PAGESIZE = pagesize;
                    AreaEntity objEntity = areaEntity1;
                    List<AreaEntity> source = areaBusiness.Find(objEntity);
                    StringBuilder stringBuilder = new StringBuilder();
                    if (source != null && source.Count > 0)
                    {
                        stringBuilder.AppendFormat("\r\n                    <table class=\"table responsive table-header-style2\" cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n                    <caption><i class=\"fa fa-building\"></i> Area <span class=\"info\">{0} registros encontrados</span></caption>\r\n                    <thead>\r\n                    <tr>\r\n                        <th scope=\"col\">Nome</th>\r\n                        <th class=\"centeralign width5\">Ações</th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>", (object)source.FirstOrDefault<AreaEntity>().COUNTER);
                        foreach (AreaEntity areaEntity2 in source)
                            stringBuilder.AppendFormat("\r\n                    <tr style=\"display: table-row; opacity: 1;\" style=\"vertical-align\">\r\n                    <td class=\"w150\"><b>{1}</b></td>                    \r\n                    <td class=\"centeralign width5\"><a class=\"btn\" href=\"#\" data-action=\"remove\"  tooltip=\"tooltip\" data-title=\"Clique para remover\" data-id=\"{0}\"  ><i class=\"fa fa-trash fa-lg\"></i></a></td>\r\n                    </tr>\r\n                    ", (object)areaEntity2.AREA_ID, !string.IsNullOrEmpty(areaEntity2.NOME) ? (object)areaEntity2.NOME : (object)string.Empty);
                        stringBuilder.AppendFormat("</tbody><tfoot><td colspan=\"2\" class=\"\"><span class=\"info\">{0} registros encontrados</span><input type=\"hidden\" id=\"pager\" name=\"pager\" value=\"{0}\" /></td></tfoot></table>", (object)source.FirstOrDefault<AreaEntity>().COUNTER);
                    }
                    else
                        stringBuilder.AppendFormat("<h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "<h4 class=\"alert_error\">Ocorreu um erro.</h4>";
                }
            }

            public static string FindAreaByPk(int id)
            {
                try
                {
                    AreaEntity byPk = new AreaBusiness().FindByPk(new AreaEntity() { AREA_ID = id });
                    StringBuilder stringBuilder = new StringBuilder();
                    if (byPk == null)
                        return (string)null;
                    stringBuilder.Append("{\"area\":\"[{ ");
                    stringBuilder.AppendFormat("'data_id':'{0}','data_nome':'{1}'", (object)byPk.AREA_ID.ToString(), !string.IsNullOrEmpty(byPk.NOME) ? (object)byPk.NOME : (object)string.Empty);
                    stringBuilder.Append("}]\"}");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return string.Empty;
                }
            }

            public static string LoadArea(int produtoId)
            {
                try
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    if (produtoId > 0)
                    {
                        List<AreaEntity> areaEntityList = new AreaBusiness().Find(new AreaEntity() { PRODUTO_ID = produtoId });
                        if (areaEntityList.Count > 0)
                        {
                            foreach (AreaEntity areaEntity in areaEntityList)
                                stringBuilder.AppendFormat("<option value=\"{0}\">{1}</option>", (object)areaEntity.AREA_ID, (object)areaEntity.NOME);
                        }
                        else
                            stringBuilder.Append("<option value=\"0\" selected=\"selected\">Nenhum...</option>");
                    }
                    else
                        stringBuilder.Append("<option value=\"0\" selected=\"selected\">Nenhum...</option>");
                    return stringBuilder.ToString();
                }
                catch
                {
                    Common.GetException(new Exception("Erro ao buscar areas"));
                    return "error";
                }
            }

            public static string AddRelatoSite(
              string identificado,
              string tipoPublico,
              string nome,
              string cargo,
              string email,
              string celular,
              string telefone,
              string genero,
              string emailAnonimo,
              int empresaId,
              int unidadeId,
              string unidade,
              int produtoId,
              string produto,
              int areaId,
              string area,
              string relato,
              string tipoRelato,
              string possivelIdentificarPessoas,
              string den1Nome,
              string den1Participacao,
              string den1Relacao,
              string den1Email,
              string den1Cargo,
              string den1Telefone,
              string den1Cidade,
              string den1Estado,
              string den2Nome,
              string den2Participacao,
              string den2Relacao,
              string den2Email,
              string den2Cargo,
              string den2Telefone,
              string den2Cidade,
              string den2Estado,
              string den3Nome,
              string den3Participacao,
              string den3Relacao,
              string den3Email,
              string den3Cargo,
              string den3Telefone,
              string den3Cidade,
              string den3Estado,
              string den4Nome,
              string den4Participacao,
              string den4Relacao,
              string den4Email,
              string den4Cargo,
              string den4Telefone,
              string den4Cidade,
              string den4Estado,
              string den5Nome,
              string den5Participacao,
              string den5Relacao,
              string den5Email,
              string den5Cargo,
              string den5Telefone,
              string den5Cidade,
              string den5Estado,
              string evidencias,
              string nmrProtocoloAnterior,
              string periodoEventoOcorreu,
              string existeRecorrencia,
              string comoFicouCiente,
              string perdaFinanceira,
              string quantoAoRelato,
              string sugestao,
              string tipoManifestacao,
              string idioma,
              string guid)
            {
                try
                {
                    RelatoEntity objEntity = new RelatoEntity();
                    string empty = string.Empty;
                    objEntity.IDENTIFICADO = identificado;
                    objEntity.TIPO_PUBLICO = tipoPublico;
                    objEntity.NOME = nome;
                    objEntity.CARGO = cargo;
                    objEntity.EMAIL = email;
                    objEntity.CELULAR = celular;
                    objEntity.TELEFONE = telefone;
                    objEntity.GENERO = genero;
                    objEntity.EMAIL_ANONIMO = emailAnonimo;
                    objEntity.EMPRESA_ID = empresaId;
                    objEntity.UNIDADE_ID = unidadeId;
                    objEntity.UNIDADE = unidade;
                    objEntity.PRODUTO_ID = produtoId;
                    objEntity.PRODUTO = produto;
                    objEntity.AREA_ID = areaId;
                    objEntity.AREA = area;
                    objEntity.RELATO = relato;
                    objEntity.TIPO_RELATO = tipoRelato;
                    objEntity.IDENTIFICAR_PESSOAS = possivelIdentificarPessoas;
                    objEntity.DEN1_NOME = den1Nome;
                    objEntity.DEN1_PARTICIPACAO = den1Participacao;
                    objEntity.DEN1_RELACAO = den1Relacao;
                    objEntity.DEN1_EMAIL = den1Email;
                    objEntity.DEN1_CARGO = den1Cargo;
                    objEntity.DEN1_TELEFONE = den1Telefone;
                    objEntity.DEN1_CIDADE = den1Cidade;
                    objEntity.DEN1_ESTADO = den1Estado;
                    objEntity.DEN2_NOME = den2Nome;
                    objEntity.DEN2_PARTICIPACAO = den2Participacao;
                    objEntity.DEN2_RELACAO = den2Relacao;
                    objEntity.DEN2_EMAIL = den2Email;
                    objEntity.DEN2_CARGO = den2Cargo;
                    objEntity.DEN2_TELEFONE = den2Telefone;
                    objEntity.DEN2_CIDADE = den2Cidade;
                    objEntity.DEN2_ESTADO = den2Estado;
                    objEntity.DEN3_NOME = den3Nome;
                    objEntity.DEN3_PARTICIPACAO = den3Participacao;
                    objEntity.DEN3_RELACAO = den3Relacao;
                    objEntity.DEN3_EMAIL = den3Email;
                    objEntity.DEN3_CARGO = den3Cargo;
                    objEntity.DEN3_TELEFONE = den3Telefone;
                    objEntity.DEN3_CIDADE = den3Cidade;
                    objEntity.DEN3_ESTADO = den3Estado;
                    objEntity.DEN4_NOME = den4Nome;
                    objEntity.DEN4_PARTICIPACAO = den4Participacao;
                    objEntity.DEN4_RELACAO = den4Relacao;
                    objEntity.DEN4_EMAIL = den4Email;
                    objEntity.DEN4_CARGO = den4Cargo;
                    objEntity.DEN4_TELEFONE = den4Telefone;
                    objEntity.DEN4_CIDADE = den4Cidade;
                    objEntity.DEN4_ESTADO = den4Estado;
                    objEntity.DEN5_NOME = den5Nome;
                    objEntity.DEN5_PARTICIPACAO = den5Participacao;
                    objEntity.DEN5_RELACAO = den5Relacao;
                    objEntity.DEN5_EMAIL = den5Email;
                    objEntity.DEN5_CARGO = den5Cargo;
                    objEntity.DEN5_TELEFONE = den5Telefone;
                    objEntity.DEN5_CIDADE = den5Cidade;
                    objEntity.DEN5_ESTADO = den5Estado;
                    objEntity.EVIDENCIAS = evidencias;
                    objEntity.NMR_PROTOCOLO_ANTERIOR = nmrProtocoloAnterior;
                    objEntity.PERIODO_EVENTO_OCORREU = periodoEventoOcorreu;
                    objEntity.EXISTE_RECORRENCIA = existeRecorrencia;
                    objEntity.COMO_FICOU_CIENTE = comoFicouCiente;
                    objEntity.PERDA_FINANCEIRA = perdaFinanceira;
                    objEntity.QUANTO_AO_RELATO = quantoAoRelato;
                    objEntity.RELATO_SUGESTAO = sugestao;
                    objEntity.TIPO_MANIFESTACAO = tipoManifestacao;
                    objEntity.IDIOMA = idioma;
                    objEntity.GUID = guid;
                    objEntity.FORMA_CONTATO = "HOTSITE";
                    objEntity.FLG_FLUXO = "R";
                    return new RelatoBusiness().Add(objEntity, new LogEntity());
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string UpdateRelatoSite(int relatoId, string comentarios)
            {
                try
                {
                    return new RelatoBusiness().UpdateSite(relatoId, comentarios) ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string SendRelatoToBWise(int relatoId)
            {
                try
                {
                    return new RelatoBusiness().SendRelatoToBWise(relatoId) ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string AddRelatoByEmail(int rid, int uid)
            {
                try
                {
                    string empty = string.Empty;
                    string str;
                    if (rid <= 0)
                    {
                        str = "warning";
                    }
                    else
                    {
                        RelatoEntity byPk = new RelatoBusiness().FindByPk(new RelatoEntity() { RELATO_ID = rid });
                        byPk.USER_ID = uid;
                        if (byPk.EMPRESA_ID > 0)
                            str = new RelatoBusiness().UpdateFromEmail(byPk, new LogEntity()
                            {
                                USER_ID = uid
                            });
                        else
                            str = "error";
                    }
                    return str;
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string AddRelato(
              int empresaId,
              string empresa,
              string tipoPublico,
              string animo,
              string tipoContato,
              string identificado,
              string nome,
              string genero,
              string cargo,
              string email,
              string telefone,
              string celular,
              string emailAnonimo,
              int produtoId,
              string produto,
              int unidadeId,
              string unidade,
              int areaId,
              string area,
              string possivelIdentificarPessoas,
              string den1Nome,
              string den1Participacao,
              string den1Relacao,
              string den1Email,
              string den1Cargo,
              string den1Telefone,
              string den1Cidade,
              string den1Estado,
              string den2Nome,
              string den2Participacao,
              string den2Relacao,
              string den2Email,
              string den2Cargo,
              string den2Telefone,
              string den2Cidade,
              string den2Estado,
              string den3Nome,
              string den3Participacao,
              string den3Relacao,
              string den3Email,
              string den3Cargo,
              string den3Telefone,
              string den3Cidade,
              string den3Estado,
              string den4Nome,
              string den4Participacao,
              string den4Relacao,
              string den4Email,
              string den4Cargo,
              string den4Telefone,
              string den4Cidade,
              string den4Estado,
              string den5Nome,
              string den5Participacao,
              string den5Relacao,
              string den5Email,
              string den5Cargo,
              string den5Telefone,
              string den5Cidade,
              string den5Estado,
              string relato,
              string relatoResumo,
              int natureza,
              int tipoNatureza,
              string tipoManifestacao,
              string criticidade,
              string histAtend,
              string histRevisor,
              string nmrProtocoloAnterior,
              string periodoEventoOcorreu,
              string existeRecorrencia,
              string comoFicouCiente,
              string perdaFinanceira,
              string sugestao,
              string quantoAoRelato,
              string anexoRelato,
              string anexo,
              string idioma,
              string flgFluxo,
              int relatoId,
              int userId,
              string evidencias,
              string comentarios,
              string histBwise,
              string histDenunciante,
              string descDetalhada,
              string tipoRelato,
              string guid)
            {
                try
                {
                    RelatoEntity objEntity = new RelatoEntity();
                    string empty = string.Empty;
                    objEntity.EMPRESA_ID = empresaId;
                    objEntity.EMPRESA = empresa;
                    objEntity.TIPO_PUBLICO = tipoPublico;
                    objEntity.ANIMO = animo;
                    objEntity.FORMA_CONTATO = tipoContato;
                    objEntity.IDENTIFICADO = identificado;
                    objEntity.NOME = nome;
                    objEntity.GENERO = genero;
                    objEntity.CARGO = cargo;
                    objEntity.EMAIL = email;
                    objEntity.TELEFONE = telefone;
                    objEntity.CELULAR = celular;
                    objEntity.EMAIL_ANONIMO = emailAnonimo;
                    objEntity.PRODUTO_ID = produtoId;
                    objEntity.PRODUTO = produtoId > 0 ? produto : "NENHUM";
                    objEntity.UNIDADE_ID = unidadeId;
                    objEntity.UNIDADE = unidadeId > 0 ? unidade : "NENHUM";
                    objEntity.AREA_ID = areaId;
                    objEntity.AREA = areaId > 0 ? area : "NENHUM";
                    objEntity.IDENTIFICAR_PESSOAS = possivelIdentificarPessoas;
                    objEntity.DEN1_NOME = den1Nome;
                    objEntity.DEN1_PARTICIPACAO = den1Participacao;
                    objEntity.DEN1_EMAIL = den1Email;
                    objEntity.DEN1_CARGO = den1Cargo;
                    objEntity.DEN1_RELACAO = den1Relacao;
                    objEntity.DEN1_TELEFONE = den1Telefone;
                    objEntity.DEN1_CIDADE = den1Cidade;
                    objEntity.DEN1_ESTADO = den1Estado;
                    objEntity.DEN2_NOME = den2Nome;
                    objEntity.DEN2_PARTICIPACAO = den2Participacao;
                    objEntity.DEN2_EMAIL = den2Email;
                    objEntity.DEN2_CARGO = den2Cargo;
                    objEntity.DEN2_RELACAO = den2Relacao;
                    objEntity.DEN2_TELEFONE = den2Telefone;
                    objEntity.DEN2_CIDADE = den2Cidade;
                    objEntity.DEN2_ESTADO = den2Estado;
                    objEntity.DEN3_NOME = den3Nome;
                    objEntity.DEN3_PARTICIPACAO = den3Participacao;
                    objEntity.DEN3_EMAIL = den3Email;
                    objEntity.DEN3_CARGO = den3Cargo;
                    objEntity.DEN3_RELACAO = den3Relacao;
                    objEntity.DEN3_TELEFONE = den3Telefone;
                    objEntity.DEN3_CIDADE = den3Cidade;
                    objEntity.DEN3_ESTADO = den3Estado;
                    objEntity.DEN4_NOME = den4Nome;
                    objEntity.DEN4_PARTICIPACAO = den4Participacao;
                    objEntity.DEN4_EMAIL = den4Email;
                    objEntity.DEN4_CARGO = den4Cargo;
                    objEntity.DEN4_RELACAO = den4Relacao;
                    objEntity.DEN4_TELEFONE = den4Telefone;
                    objEntity.DEN4_CIDADE = den4Cidade;
                    objEntity.DEN4_ESTADO = den4Estado;
                    objEntity.DEN5_NOME = den5Nome;
                    objEntity.DEN5_PARTICIPACAO = den5Participacao;
                    objEntity.DEN5_EMAIL = den5Email;
                    objEntity.DEN5_CARGO = den5Cargo;
                    objEntity.DEN5_RELACAO = den5Relacao;
                    objEntity.DEN5_TELEFONE = den5Telefone;
                    objEntity.DEN5_CIDADE = den5Cidade;
                    objEntity.DEN5_ESTADO = den5Estado;
                    objEntity.RELATO = relato;
                    objEntity.RELATO_RESUMO = relatoResumo;
                    objEntity.NATUREZA_ID = natureza;
                    objEntity.TIPONATUREZA_ID = tipoNatureza;
                    objEntity.TIPO_MANIFESTACAO = tipoManifestacao;
                    objEntity.CRITICIDADE = criticidade;
                    objEntity.HIST_ATEND = histAtend;
                    objEntity.NMR_PROTOCOLO_ANTERIOR = nmrProtocoloAnterior;
                    objEntity.PERIODO_EVENTO_OCORREU = periodoEventoOcorreu;
                    objEntity.EXISTE_RECORRENCIA = existeRecorrencia;
                    objEntity.COMO_FICOU_CIENTE = comoFicouCiente;
                    objEntity.PERDA_FINANCEIRA = perdaFinanceira;
                    objEntity.RELATO_SUGESTAO = sugestao;
                    objEntity.QUANTO_AO_RELATO = quantoAoRelato;
                    objEntity.IDIOMA = idioma;
                    objEntity.FLG_FLUXO = !string.IsNullOrEmpty(flgFluxo) ? flgFluxo : "R";
                    objEntity.RELATO_ID = relatoId;
                    objEntity.EVIDENCIAS = evidencias;
                    objEntity.COMENTARIOS = comentarios;
                    objEntity.DESCRICAO_DETALHADA = descDetalhada;
                    objEntity.TIPO_RELATO = tipoRelato;
                    objEntity.GUID = guid;
                    if (relatoId == 0)
                        return new RelatoBusiness().Add(objEntity, new LogEntity() { USER_ID = userId });
                    return new RelatoBusiness().Update(objEntity, new LogEntity() { USER_ID = userId }) ? "success" : "warning";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string RemoveRelato(int id)
            {
                try
                {
                    string empty = string.Empty;
                    return !new RelatoBusiness().Remove(new RelatoEntity() { RELATO_ID = id }) ? "alert" : "success";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string FindRelatoOutBwise()
            {
                try
                {
                    List<RelatoEntity> relatoOutBwise = new RelatoBusiness().FindRelatoOutBwise();
                    StringBuilder stringBuilder = new StringBuilder();
                    if (relatoOutBwise != null && relatoOutBwise.Count > 0)
                    {
                        stringBuilder.AppendFormat("\r\n                    <table class=\"table responsive table-header-style2\" cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n                    <caption><i class=\"fa fa-comments\"></i> Denúncias/Relatos <span class=\"info\">{0} registros encontrados</span></caption>\r\n                    <thead>\r\n                    <tr>\r\n                        <th class=\"with15 text-center\">Data</th>\r\n                         <th scope=\"col\">Empresa</th>\r\n                       <th scope=\"col\">Protocolo</th>\r\n                        <th scope=\"col\">Status</th>\r\n                        <th class=\"centeralign width5\" >Ações</th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>", (object)relatoOutBwise.FirstOrDefault<RelatoEntity>().COUNTER);
                        foreach (RelatoEntity relatoEntity in relatoOutBwise)
                            stringBuilder.AppendFormat("\r\n                    <tr style=\"display: table-row; opacity: 1;\" style=\"vertical-align\">\r\n                        <td class=\"width15 text-center\"><b>{1}</b></td>                    \r\n                        <td class=\"w150\"><b>{2}</b></td>                    \r\n                        <td class=\"w150\"><b>{4}</b></td>                    \r\n                        <td class=\"w150\"><b>{3}</b></td>                    \r\n                        <td class=\"centeralign width5\"><a class=\"btn\" href=\"#\" tooltip=\"tooltip\" data-title=\"Clique para reenviar para importação\" onclick=\"SendRelatoToBWise({0})\"><i class=\"fa fa-share fa-lg\"></i></a></td>\r\n                     </tr>\r\n                    ", (object)relatoEntity.RELATO_ID, relatoEntity.DT_INICIO > DateTime.MinValue ? (object)relatoEntity.DT_INICIO.ToString("dd/MM/yyyy") : (object)string.Empty, !string.IsNullOrEmpty(relatoEntity.EMPRESA) ? (object)relatoEntity.EMPRESA : (object)string.Empty, !string.IsNullOrEmpty(relatoEntity.STATUS) ? (object)relatoEntity.STATUS : (object)string.Empty, !string.IsNullOrEmpty(relatoEntity.NMR_PROTOCOLO) ? (object)relatoEntity.NMR_PROTOCOLO : (object)string.Empty);
                        stringBuilder.AppendFormat("</tbody><tfoot><td colspan=\"5\" class=\"\"><span class=\"info\">{0} registros encontrados</span><input type=\"hidden\" id=\"pager\" name=\"pager\" value=\"{0}\" /></td></tfoot></table>", (object)relatoOutBwise.FirstOrDefault<RelatoEntity>().COUNTER);
                    }
                    else
                        stringBuilder.AppendFormat("<h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "<h4 class=\"alert_error\">Ocorreu um erro.</h4>";
                }
            }

            public static string FindRelato(int index, int pagesize, int empresaId, string flgFluxo)
            {
                try
                {
                    RelatoBusiness relatoBusiness = new RelatoBusiness();
                    RelatoEntity relatoEntity1 = new RelatoEntity();
                    relatoEntity1.EMPRESA_ID = empresaId;
                    relatoEntity1.PAGEINDEX = index;
                    relatoEntity1.PAGESIZE = pagesize;
                    relatoEntity1.FLG_FLUXO = !string.IsNullOrEmpty(flgFluxo) ? flgFluxo : string.Empty;
                    RelatoEntity objEntity = relatoEntity1;
                    List<RelatoEntity> source = relatoBusiness.Find(objEntity);
                    StringBuilder stringBuilder = new StringBuilder();
                    if (source != null && source.Count > 0)
                    {
                        stringBuilder.AppendFormat("\r\n                    <table class=\"table responsive table-header-style2\" cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n                    <caption><i class=\"fa fa-comments\"></i> Denúncias/Relatos <span class=\"info\">{0} registros encontrados</span></caption>\r\n                    <thead>\r\n                    <tr>\r\n                        <th class=\"with15 text-center\">Data</th>\r\n                         <th scope=\"col\">Empresa</th>\r\n                       <th scope=\"col\">Protocolo</th>\r\n                        <th scope=\"col\">Status</th>\r\n                        <th class=\"centeralign width15\" colspan=\"2\">Ações</th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>", (object)source.FirstOrDefault<RelatoEntity>().COUNTER);
                        foreach (RelatoEntity relatoEntity2 in source)
                            stringBuilder.AppendFormat("\r\n                    <tr style=\"display: table-row; opacity: 1;\" style=\"vertical-align\">\r\n                        <td class=\"width15 text-center\"><b>{1}</b></td>                    \r\n                        <td class=\"w150\"><b>{2}</b></td>                    \r\n                        <td class=\"w150\"><b>{4}</b></td>                    \r\n                        <td class=\"w150\"><b>{3}</b></td>                    \r\n                        <td class=\"centeralign width5\"><a class=\"btn\"href=\"../relatos/editar.aspx?id={0}\" tooltip=\"tooltip\" data-title=\"Clique para editar\"><i class=\"fa fa-pencil fa-lg\"></i></a></td>\r\n                        <td class=\"centeralign width5\"><a class=\"btn\"href=\"#\" onclick=\"SHOW_RELATO_PREVIEW({0}); return false;\" tooltip=\"tooltip\" data-title=\"Clique para imprimir\"><i class=\"fa fa-print fa-lg\"></i></a></td>\r\n                    </tr>\r\n                    ", (object)relatoEntity2.RELATO_ID, relatoEntity2.DT_INICIO > DateTime.MinValue ? (object)relatoEntity2.DT_INICIO.ToString("dd/MM/yyyy") : (object)string.Empty, !string.IsNullOrEmpty(relatoEntity2.EMPRESA) ? (object)relatoEntity2.EMPRESA : (object)string.Empty, !string.IsNullOrEmpty(relatoEntity2.STATUS) ? (object)relatoEntity2.STATUS : (object)string.Empty, !string.IsNullOrEmpty(relatoEntity2.NMR_PROTOCOLO) ? (object)relatoEntity2.NMR_PROTOCOLO : (object)string.Empty);
                        stringBuilder.AppendFormat("</tbody><tfoot><td colspan=\"6\" class=\"\"><span class=\"info\">{0} registros encontrados</span><input type=\"hidden\" id=\"pager\" name=\"pager\" value=\"{0}\" /></td></tfoot></table>", (object)source.FirstOrDefault<RelatoEntity>().COUNTER);
                    }
                    else
                        stringBuilder.AppendFormat("<h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "<h4 class=\"alert_error\">Ocorreu um erro.</h4>";
                }
            }

            public static string LoadRelato(string perfil)
            {
                try
                {
                    string str = perfil == "1" ? "A" : (perfil == "2" ? "R" : string.Empty);
                    List<RelatoEntity> source = new RelatoBusiness().Find(new RelatoEntity() { FLG_FLUXO = str });
                    StringBuilder stringBuilder = new StringBuilder();
                    if (source != null && source.Count > 0)
                    {
                        stringBuilder.AppendFormat("\r\n                    <table class=\"table responsive table-header-style2 tablesorter\" cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n                    <caption><i class=\"fa fa-comments\"></i> Denúncias/Relatos <span class=\"info\">{0} registros encontrados</span></caption>\r\n                    <thead>\r\n                    <tr>\r\n                        <th class=\"with15 text-center\">Data</th>\r\n                        <th scope=\"col\">Empresa</th>\r\n                        <th scope=\"col\">Protocolo</th>\r\n                        <th scope=\"col\">Status</th>\r\n                        <th class=\"centeralign width15\" colspan=\"2\">Ações</th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>", (object)source.FirstOrDefault<RelatoEntity>().COUNTER);
                        foreach (RelatoEntity relatoEntity in source)
                            stringBuilder.AppendFormat("\r\n                    <tr style=\"display: table-row; opacity: 1;\" style=\"vertical-align\">\r\n                        <td class=\"width15 text-center\"><b>{1}</b></td>                    \r\n                     <td class=\"w150\"><b>{2}</b></td>                    \r\n                           <td class=\"w150\"><b>{4}</b></td>                    \r\n                        <td class=\"w150\"><b>{3}</b></td>                    \r\n                        <td class=\"centeralign width5\"><a class=\"btn\"href=\"../relatos/editar.aspx?id={0}\" tooltip=\"tooltip\" data-title=\"Clique para analisar\"><i class=\"fa fa-pencil fa-lg\"></i></a></td>\r\n                        <td class=\"centeralign width5\"><a class=\"btn\"href=\"#\" onclick=\"SHOW_RELATO_PREVIEW({0}); return false;\" tooltip=\"tooltip\" data-title=\"Clique para imprimir\"><i class=\"fa fa-print fa-lg\"></i></a></td>\r\n\r\n                    </tr>\r\n                    ", (object)relatoEntity.RELATO_ID, relatoEntity.DT_INICIO > DateTime.MinValue ? (object)relatoEntity.DT_INICIO.ToString("dd/MM/yyyy") : (object)string.Empty, !string.IsNullOrEmpty(relatoEntity.EMPRESA) ? (object)relatoEntity.EMPRESA : (object)string.Empty, !string.IsNullOrEmpty(relatoEntity.STATUS) ? (object)relatoEntity.STATUS : (object)string.Empty, !string.IsNullOrEmpty(relatoEntity.NMR_PROTOCOLO) ? (object)relatoEntity.NMR_PROTOCOLO : (object)string.Empty);
                        stringBuilder.AppendFormat("</tbody><tfoot><td colspan=\"6\" class=\"\"><span class=\"info\">{0} registros encontrados</span><input type=\"hidden\" id=\"pager\" name=\"pager\" value=\"{0}\" /></td></tfoot></table>", (object)source.FirstOrDefault<RelatoEntity>().COUNTER);
                    }
                    else
                        stringBuilder.AppendFormat("<h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "<h4 class=\"alert_error\">Ocorreu um erro.</h4>";
                }
            }

            public static string FindRelatoByPk(int id)
            {
                try
                {
                    RelatoEntity byPk = new RelatoBusiness().FindByPk(new RelatoEntity() { RELATO_ID = id });
                    StringBuilder stringBuilder = new StringBuilder();
                    new EmpresaBusiness().FindByPk(new EmpresaEntity()
                    {
                        EMPRESA_ID = byPk.EMPRESA_ID
                    });
                    if (byPk == null)
                        return (string)null;
                    stringBuilder.Append("{\"relato\":\"[{ ");
                    stringBuilder.AppendFormat("'data_tipo_publico':'{0}',", !string.IsNullOrEmpty(byPk.TIPO_PUBLICO) ? (object)byPk.TIPO_PUBLICO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_identificado':'{0}',", !string.IsNullOrEmpty(byPk.IDENTIFICADO) ? (object)byPk.IDENTIFICADO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_nome':'{0}',", !string.IsNullOrEmpty(byPk.NOME) ? (object)byPk.NOME : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_cargo':'{0}',", !string.IsNullOrEmpty(byPk.CARGO) ? (object)byPk.CARGO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_email':'{0}',", !string.IsNullOrEmpty(byPk.EMAIL) ? (object)byPk.EMAIL : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_email_anonimo':'{0}',", !string.IsNullOrEmpty(byPk.EMAIL_ANONIMO) ? (object)byPk.EMAIL_ANONIMO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_telefone':'{0}',", !string.IsNullOrEmpty(byPk.TELEFONE) ? (object)byPk.TELEFONE : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_celular':'{0}',", !string.IsNullOrEmpty(byPk.CELULAR) ? (object)byPk.CELULAR : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_empresa_id':'{0}',", (object)(byPk.EMPRESA_ID > 0 ? byPk.EMPRESA_ID : 0));
                    stringBuilder.AppendFormat("'data_unidade_id':'{0}',", (object)(byPk.UNIDADE_ID > 0 ? byPk.UNIDADE_ID : 0));
                    stringBuilder.AppendFormat("'data_produto_id':'{0}',", (object)(byPk.PRODUTO_ID > 0 ? byPk.PRODUTO_ID : 0));
                    stringBuilder.AppendFormat("'data_area_id':'{0}',", (object)(byPk.AREA_ID > 0 ? byPk.AREA_ID : 0));
                    stringBuilder.AppendFormat("'data_identificar_pessoas':'{0}',", !string.IsNullOrEmpty(byPk.IDENTIFICAR_PESSOAS) ? (object)byPk.IDENTIFICAR_PESSOAS : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den1_cargo':'{0}',", !string.IsNullOrEmpty(byPk.DEN1_CARGO) ? (object)byPk.DEN1_CARGO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den1_cidade':'{0}',", !string.IsNullOrEmpty(byPk.DEN1_CIDADE) ? (object)byPk.DEN1_CIDADE : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den1_email':'{0}',", !string.IsNullOrEmpty(byPk.DEN1_EMAIL) ? (object)byPk.DEN1_EMAIL : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den1_estado':'{0}',", !string.IsNullOrEmpty(byPk.DEN1_ESTADO) ? (object)byPk.DEN1_ESTADO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den1_nome':'{0}',", !string.IsNullOrEmpty(byPk.DEN1_NOME) ? (object)byPk.DEN1_NOME : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den1_participacao':'{0}',", !string.IsNullOrEmpty(byPk.DEN1_PARTICIPACAO) ? (object)byPk.DEN1_PARTICIPACAO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den1_relacao':'{0}',", !string.IsNullOrEmpty(byPk.DEN1_RELACAO) ? (object)byPk.DEN1_RELACAO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den1_telefone':'{0}',", !string.IsNullOrEmpty(byPk.DEN1_TELEFONE) ? (object)byPk.DEN1_TELEFONE : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den2_cargo':'{0}',", !string.IsNullOrEmpty(byPk.DEN2_CARGO) ? (object)byPk.DEN2_CARGO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den2_cidade':'{0}',", !string.IsNullOrEmpty(byPk.DEN2_CIDADE) ? (object)byPk.DEN2_CIDADE : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den2_email':'{0}',", !string.IsNullOrEmpty(byPk.DEN2_EMAIL) ? (object)byPk.DEN2_EMAIL : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den2_estado':'{0}',", !string.IsNullOrEmpty(byPk.DEN2_ESTADO) ? (object)byPk.DEN2_ESTADO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den2_nome':'{0}',", !string.IsNullOrEmpty(byPk.DEN2_NOME) ? (object)byPk.DEN2_NOME : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den2_participacao':'{0}',", !string.IsNullOrEmpty(byPk.DEN2_PARTICIPACAO) ? (object)byPk.DEN2_PARTICIPACAO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den2_relacao':'{0}',", !string.IsNullOrEmpty(byPk.DEN2_RELACAO) ? (object)byPk.DEN2_RELACAO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den2_telefone':'{0}',", !string.IsNullOrEmpty(byPk.DEN2_TELEFONE) ? (object)byPk.DEN2_TELEFONE : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den3_cargo':'{0}',", !string.IsNullOrEmpty(byPk.DEN3_CARGO) ? (object)byPk.DEN3_CARGO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den3_cidade':'{0}',", !string.IsNullOrEmpty(byPk.DEN3_CIDADE) ? (object)byPk.DEN3_CIDADE : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den3_email':'{0}',", !string.IsNullOrEmpty(byPk.DEN3_EMAIL) ? (object)byPk.DEN3_EMAIL : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den3_estado':'{0}',", !string.IsNullOrEmpty(byPk.DEN3_ESTADO) ? (object)byPk.DEN3_ESTADO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den3_nome':'{0}',", !string.IsNullOrEmpty(byPk.DEN3_NOME) ? (object)byPk.DEN3_NOME : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den3_participacao':'{0}',", !string.IsNullOrEmpty(byPk.DEN3_PARTICIPACAO) ? (object)byPk.DEN3_PARTICIPACAO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den3_relacao':'{0}',", !string.IsNullOrEmpty(byPk.DEN3_RELACAO) ? (object)byPk.DEN3_RELACAO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den3_telefone':'{0}',", !string.IsNullOrEmpty(byPk.DEN3_TELEFONE) ? (object)byPk.DEN3_TELEFONE : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den4_cargo':'{0}',", !string.IsNullOrEmpty(byPk.DEN4_CARGO) ? (object)byPk.DEN4_CARGO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den4_cidade':'{0}',", !string.IsNullOrEmpty(byPk.DEN4_CIDADE) ? (object)byPk.DEN4_CIDADE : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den4_email':'{0}',", !string.IsNullOrEmpty(byPk.DEN4_EMAIL) ? (object)byPk.DEN4_EMAIL : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den4_estado':'{0}',", !string.IsNullOrEmpty(byPk.DEN4_ESTADO) ? (object)byPk.DEN4_ESTADO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den4_nome':'{0}',", !string.IsNullOrEmpty(byPk.DEN4_NOME) ? (object)byPk.DEN4_NOME : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den4_participacao':'{0}',", !string.IsNullOrEmpty(byPk.DEN4_PARTICIPACAO) ? (object)byPk.DEN4_PARTICIPACAO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den4_relacao':'{0}',", !string.IsNullOrEmpty(byPk.DEN4_RELACAO) ? (object)byPk.DEN4_RELACAO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den4_telefone':'{0}',", !string.IsNullOrEmpty(byPk.DEN4_TELEFONE) ? (object)byPk.DEN4_TELEFONE : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den5_cargo':'{0}',", !string.IsNullOrEmpty(byPk.DEN5_CARGO) ? (object)byPk.DEN5_CARGO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den5_cidade':'{0}',", !string.IsNullOrEmpty(byPk.DEN5_CIDADE) ? (object)byPk.DEN5_CIDADE : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den5_email':'{0}',", !string.IsNullOrEmpty(byPk.DEN5_EMAIL) ? (object)byPk.DEN5_EMAIL : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den5_estado':'{0}',", !string.IsNullOrEmpty(byPk.DEN5_ESTADO) ? (object)byPk.DEN5_ESTADO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den5_nome':'{0}',", !string.IsNullOrEmpty(byPk.DEN5_NOME) ? (object)byPk.DEN5_NOME : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den5_participacao':'{0}',", !string.IsNullOrEmpty(byPk.DEN5_PARTICIPACAO) ? (object)byPk.DEN5_PARTICIPACAO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den5_relacao':'{0}',", !string.IsNullOrEmpty(byPk.DEN5_RELACAO) ? (object)byPk.DEN5_RELACAO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_den5_telefone':'{0}',", !string.IsNullOrEmpty(byPk.DEN5_TELEFONE) ? (object)byPk.DEN5_TELEFONE : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_tipo_manifestacao':'{0}',", !string.IsNullOrEmpty(byPk.TIPO_MANIFESTACAO) ? (object)byPk.TIPO_MANIFESTACAO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_dt_inicio':'{0}',", byPk.DT_INICIO > DateTime.MinValue ? (object)byPk.DT_INICIO.ToString("dd/MM/yyyy") : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_relato_id':'{0}',", (object)(byPk.RELATO_ID > 0 ? byPk.RELATO_ID : 0));
                    if (string.IsNullOrEmpty(byPk.EMAIL_ID))
                        stringBuilder.AppendFormat("'data_relato':'{0}',", !string.IsNullOrEmpty(byPk.RELATO) ? (object)byPk.RELATO.Replace("\r\n", " ").Replace("\"", "&quot;").Replace("\"", "&quot;") : (object)string.Empty);
                    else
                        stringBuilder.AppendFormat("'data_relato':'{0}',", (object)string.Empty);
                    stringBuilder.AppendFormat("'data_nmr_protocolo_anterior':'{0}',", !string.IsNullOrEmpty(byPk.NMR_PROTOCOLO_ANTERIOR) ? (object)byPk.NMR_PROTOCOLO_ANTERIOR : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_periodo_evento_ocorreu':'{0}',", !string.IsNullOrEmpty(byPk.PERIODO_EVENTO_OCORREU) ? (object)byPk.PERIODO_EVENTO_OCORREU : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_existe_recorrencia':'{0}',", !string.IsNullOrEmpty(byPk.EXISTE_RECORRENCIA) ? (object)byPk.EXISTE_RECORRENCIA : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_como_ficou_ciente':'{0}',", !string.IsNullOrEmpty(byPk.COMO_FICOU_CIENTE) ? (object)byPk.COMO_FICOU_CIENTE : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_perda_financeira':'{0}',", !string.IsNullOrEmpty(byPk.PERDA_FINANCEIRA) ? (object)byPk.PERDA_FINANCEIRA : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_relato_sugestao':'{0}',", !string.IsNullOrEmpty(byPk.RELATO_SUGESTAO) ? (object)byPk.RELATO_SUGESTAO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_quanto_ao_relato':'{0}',", !string.IsNullOrEmpty(byPk.QUANTO_AO_RELATO) ? (object)byPk.QUANTO_AO_RELATO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_idioma':'{0}',", !string.IsNullOrEmpty(byPk.IDIOMA) ? (object)byPk.IDIOMA : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_nmr_protocolo':'{0}',", !string.IsNullOrEmpty(byPk.NMR_PROTOCOLO) ? (object)byPk.NMR_PROTOCOLO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_status':'{0}',", !string.IsNullOrEmpty(byPk.STATUS) ? (object)byPk.STATUS : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_animo':'{0}',", !string.IsNullOrEmpty(byPk.ANIMO) ? (object)byPk.ANIMO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_forma_contato':'{0}',", !string.IsNullOrEmpty(byPk.FORMA_CONTATO) ? (object)byPk.FORMA_CONTATO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_genero':'{0}',", !string.IsNullOrEmpty(byPk.GENERO) ? (object)byPk.GENERO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_relato_resumo':'{0}',", !string.IsNullOrEmpty(byPk.RELATO_RESUMO) ? (object)byPk.RELATO_RESUMO.Replace("\r\n", " ") : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_criticidade':'{0}',", !string.IsNullOrEmpty(byPk.CRITICIDADE) ? (object)byPk.CRITICIDADE : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_natureza':'{0}',", (object)(byPk.NATUREZA_ID > 0 ? byPk.NATUREZA_ID : 0));
                    stringBuilder.AppendFormat("'data_tipo_natureza':'{0}',", (object)(byPk.TIPONATUREZA_ID > 0 ? byPk.TIPONATUREZA_ID : 0));
                    stringBuilder.AppendFormat("'data_dt_max':'{0}',", byPk.DT_MAX > DateTime.MinValue ? (object)byPk.DT_MAX.ToShortDateString() : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_dt_encerramento':'{0}',", byPk.DT_ENCERRAMENTO > DateTime.MinValue ? (object)byPk.DT_ENCERRAMENTO.ToShortDateString() : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_evidencias':'{0}',", !string.IsNullOrEmpty(byPk.EVIDENCIAS) ? (object)byPk.EVIDENCIAS.Replace("\r\n", " ").Replace("\"", "\"") : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_senhaacesso':'{0}',", !string.IsNullOrEmpty(byPk.SENHA_ACESSO) ? (object)byPk.SENHA_ACESSO : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_desc_detalhada':'{0}',", !string.IsNullOrEmpty(byPk.DESCRICAO_DETALHADA) ? (object)byPk.DESCRICAO_DETALHADA.Replace("\r\n", " ").Replace("\"", "\"") : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_tiporelato':'{0}',", !string.IsNullOrEmpty(byPk.TIPO_RELATO) ? (object)byPk.TIPO_RELATO.Replace("\r\n", " ").Replace("\"", "\"") : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_guid':'{0}',", !string.IsNullOrEmpty(byPk.GUID) ? (object)byPk.GUID : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_histatend':'{0}',", !string.IsNullOrEmpty(byPk.HIST_ATEND) ? (object)byPk.HIST_ATEND : (object)string.Empty);
                    stringBuilder.AppendFormat("'data_emailid':'{0}'", !string.IsNullOrEmpty(byPk.EMAIL_ID) ? (object)byPk.EMAIL_ID : (object)string.Empty);
                    stringBuilder.Append("}]\"}");
                    return stringBuilder.Replace("\n", " ").ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return string.Empty;
                }
            }

            public static string ViewRelatoByPk(int id, string idioma)
            {
                try
                {
                    RelatoEntity byPk1 = new RelatoBusiness().FindByPk(new RelatoEntity() { RELATO_ID = id });
                    StringBuilder stringBuilder1 = new StringBuilder();
                    bool flag = false;
                    if (byPk1 == null)
                        return (string)null;
                    EmpresaEntity byPk2 = new EmpresaBusiness().FindByPk(new EmpresaEntity() { EMPRESA_ID = byPk1.EMPRESA_ID });
                    if (byPk2 != null && !string.IsNullOrEmpty(byPk2.LOGO))
                        HtmlFactory.DownloadSftp(((IEnumerable<string>)byPk2.LOGO.Split('/')).Last<string>());
                    List<PesquisaSatisfacaoEntity> satisfacaoEntityList = new PesquisaSatisfacaoBusiness().Find(new PesquisaSatisfacaoEntity() { NMR_PROTOCOLO = byPk1.NMR_PROTOCOLO });
                    if (satisfacaoEntityList != null && satisfacaoEntityList.Count > 0)
                        flag = true;

                    string replacementString = String.Empty;//Environment.NewLine;

                    stringBuilder1.Append("{\"relato\":\"[{ ");
                    stringBuilder1.AppendFormat("'data_relato_id':'{0}',", (object)byPk1.RELATO_ID.ToString());
                    stringBuilder1.AppendFormat("'data_empresa_id':'{0}',", (object)(byPk1.EMPRESA_ID > 0 ? byPk1.EMPRESA_ID : 0));
                    if (!string.IsNullOrEmpty(byPk1.EMAIL_ID))
                        stringBuilder1.AppendFormat("'data_relato':'{0}',", (object)string.Empty);
                    else
                        //Fix Caracteres Especiais (R3)
                        stringBuilder1.AppendFormat("'data_relato':'{0}',", !string.IsNullOrEmpty(byPk1.RELATO) ? (object)byPk1.RELATO.Replace(@"\r\n", replacementString).Replace(@"\n", replacementString).Replace(@"\r", replacementString).Replace(@"\", @"\\\\").Replace(@"""", @"\""").Replace(@"'", @"\\'") : (object)string.Empty);
                    //stringBuilder1.AppendFormat("'data_relato':'{0}',", !string.IsNullOrEmpty(byPk1.RELATO) ? (object)byPk1.RELATO.Replace("\r\n", " ").Replace("\"", "&quot;").Replace("\"", "&quot;") : (object)string.Empty);
                    stringBuilder1.AppendFormat("'data_nmr_protocolo':'{0}',", !string.IsNullOrEmpty(byPk1.NMR_PROTOCOLO) ? (object)byPk1.NMR_PROTOCOLO : (object)string.Empty);
                    StringBuilder stringBuilder2 = stringBuilder1;
                    string str;
                    if (byPk2 == null || string.IsNullOrEmpty(byPk2.LOGO))
                        str = Common.App.GetValue("APP_NOIMAGE", typeof(string)).ToString();
                    else
                        str = Common.App.GetValue("APP_ROOT_IMG", typeof(string)).ToString() + ((IEnumerable<string>)byPk2.LOGO.Split('/')).Last<string>();
                    stringBuilder2.AppendFormat("'data_logo':'{0}',", (object)str);
                    stringBuilder1.AppendFormat("'data_inicio':'{0}',", byPk1.DT_INICIO > DateTime.MinValue ? (object)byPk1.DT_INICIO.ToString("ddd, dd/MMM/yyyy HH:mm", new CultureInfo(idioma)) : (object)string.Empty);
                    stringBuilder1.AppendFormat("'data_atualizacao':'{0}',", byPk1.DT_ATUALIZACAO > DateTime.MinValue ? (object)byPk1.DT_ATUALIZACAO.ToString("ddd, dd/MMM/yyyy HH:mm", new CultureInfo(idioma)) : (object)string.Empty);
                    stringBuilder1.AppendFormat("'data_status':'{0}',", !string.IsNullOrEmpty(byPk1.STATUS) ? (object)byPk1.STATUS : (object)string.Empty);
                    stringBuilder1.AppendFormat("'data_nomecanal':'{0}',", byPk2 == null || string.IsNullOrEmpty(byPk2.TITULO) ? (object)string.Empty : (object)byPk2.TITULO);
                    stringBuilder1.AppendFormat("'data_fluxo':'{0}',", !string.IsNullOrEmpty(byPk1.FLG_FLUXO) ? (object)byPk1.FLG_FLUXO : (object)string.Empty);
                    stringBuilder1.AppendFormat("'data_modelo':'{0}',", (object)(byPk2 == null || byPk2.MODELO_ID <= 0 ? 1 : byPk2.MODELO_ID));
                    stringBuilder1.AppendFormat("'data_resp':'{0}',", (object)flag);
                    stringBuilder1.AppendFormat("'data_guid':'{0}',", (object)byPk1.GUID);
                    stringBuilder1.AppendFormat("'data_emailid':'{0}',", (object)byPk1.EMAIL_ID);
                    stringBuilder1.AppendFormat("'data_resppesq':'{0}'", !string.IsNullOrEmpty(byPk1.FLG_RESP_PESQ) ? (object)byPk1.FLG_RESP_PESQ : (object)"N");
                    stringBuilder1.Append("}]\"}");
                    return stringBuilder1.Replace("\n", " ").ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return string.Empty;
                }
            }

            public static string FindRelatoByEmail(int index, int pagesize, int empresaId)
            {
                try
                {
                    RelatoBusiness relatoBusiness = new RelatoBusiness();
                    RelatoEntity relatoEntity1 = new RelatoEntity();
                    relatoEntity1.EMPRESA_ID = empresaId;
                    relatoEntity1.PAGEINDEX = index;
                    relatoEntity1.PAGESIZE = pagesize;
                    relatoEntity1.FLG_FLUXO = "M";
                    RelatoEntity objEntity = relatoEntity1;
                    List<RelatoEntity> source = relatoBusiness.Find(objEntity);
                    StringBuilder stringBuilder = new StringBuilder();
                    if (source != null && source.Count > 0)
                    {
                        stringBuilder.AppendFormat("\r\n                    <table class=\"table responsive table-header-style2\" cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n                    <caption><i class=\"fa fa-comments\"></i> Denúncias/Relatos <span class=\"info\">{0} registros encontrados</span></caption>\r\n                    <thead>\r\n                    <tr>\r\n                        <th class=\"with15 text-center\">Data</th>\r\n                         <th scope=\"col\">Empresa</th>\r\n                        <th class=\"centeralign width5\">Ações</th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>", (object)source.FirstOrDefault<RelatoEntity>().COUNTER);
                        foreach (RelatoEntity relatoEntity2 in source)
                            stringBuilder.AppendFormat("\r\n                    <tr style=\"display: table-row; opacity: 1;\" style=\"vertical-align\">\r\n                        <td class=\"width15 text-center\"><b>{1}</b></td>                    \r\n                        <td class=\"w150\"><b>{2}</b></td>                                                          \r\n                        <td class=\"centeralign  width5\"><a class=\"btn\" href=\"#\"  tooltip=\"tooltip\" data-title=\"Clique para visualizar\" data-id=\"{0}\"  onclick=\"SHOW_MODAL_PREVIEW({0} ,'ifrmail-{0}');return false;\"><i class=\"fa fa-eye fa-lg\"></i></a><iframe id=\"ifrmail-{0}\" style=\"display:none\">{3}</iframe></td>\r\n                        \r\n                    </tr>\r\n                    ", (object)relatoEntity2.RELATO_ID, relatoEntity2.DT_INICIO > DateTime.MinValue ? (object)relatoEntity2.DT_INICIO.ToString("dd/MM/yyyy hh:mm") : (object)string.Empty, !string.IsNullOrEmpty(relatoEntity2.EMPRESA) ? (object)relatoEntity2.EMPRESA : (object)string.Empty, (object)relatoEntity2.RELATO);
                        stringBuilder.AppendFormat("</tbody><tfoot><td colspan=\"6\" class=\"\"><span class=\"info\">{0} registros encontrados</span><input type=\"hidden\" id=\"pager\" name=\"pager\" value=\"{0}\" /></td></tfoot></table>", (object)source.FirstOrDefault<RelatoEntity>().COUNTER);
                    }
                    else
                        stringBuilder.AppendFormat("<h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "<h4 class=\"alert_error\">Ocorreu um erro.</h4>";
                }
            }

            public static string FindRelatoEmailFrame(int rid)
            {
                try
                {
                    RelatoEntity byPk = new RelatoBusiness().FindByPk(new RelatoEntity() { RELATO_ID = rid });
                    StringBuilder stringBuilder = new StringBuilder();
                    if (byPk != null)
                        stringBuilder.AppendFormat("<iframe id=\"ifrmail-{0}\" style=\"display:none\">{1}</iframe>", (object)byPk.RELATO_ID, (object)byPk.RELATO);
                    else
                        stringBuilder.AppendFormat("<iframe id=\"ifrmail-{0}\" style=\"display:none\"></iframe>", (object)byPk.RELATO_ID);
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "<h4 class=\"alert_error\">Ocorreu um erro.</h4>";
                }
            }

            public static string GetRelatoPreview(int relatoId)
            {
                try
                {
                    RelatoEntity byPk = new RelatoBusiness().FindByPk(new RelatoEntity() { RELATO_ID = relatoId });
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.AppendFormat("\r\n                        <div class=\"container-view-inner\" id=\"container-view-inner_{0}\" >\r\n                            <form id=\"printer-relato\">\r\n                                <div class=\"header-printer\">\r\n                                     <a href=\"#\" class=\"btn btn-primary pull-right\" onclick=\"PRINTER('printer-relato'); return false;\">Imprimir <i class=\"fa fa-print\"></i></a>\r\n                                   <img src = \"../resources/img/bkg/logo_deloitte.png\" width = \"100\" />\r\n                                    <h1 class=\"text-center\">Impressão de Relato</h1>\r\n\r\n                                </div>\r\n                                <div class=\"printer-section-envolvidos\">\r\n                                    <ul class=\"col-md-12 envolvidos\" style=\"border:0!important\">\r\n                                        <li class=\"col-md-3 col-xs-12 col-sm-6\"><label>Protocolo</label><label class=\"labelvalue\"> {1}</label></li>\r\n                                        <li class=\"col-md-3 col-xs-12 col-sm-6\"><label>Criticidade</label><label class=\"labelvalue\"> {2}</label></li>\r\n                                        <li class=\"col-md-3 col-xs-12 col-sm-6\"><label>Data Início</label><label class=\"labelvalue\"> {3}</label></li>\r\n                                        <li class=\"col-md-3 col-xs-12 col-sm-6\"><label>Data Encerramento</label><label class=\"labelvalue\"> {4}</label></li>\r\n                                        <li class=\"col-md-12\"><label>Status</label><label class=\"labelvalue\"> {5}</label></li>\r\n                                    </ul>\r\n                                </div>\r\n                                <div class=\"subheader-printer\">\r\n                                    <h1>Dados da Empresa</h1>\r\n                                </div>\r\n                                <div class=\"printer-section-envolvidos\">\r\n                                    <ul class=\"col-md-12 envolvidos\" style=\"border:0!important\">\r\n                                        <li class=\"col-md-6 col-xs-12 col-sm-6\"><label>Empresa</label><label class=\"labelvalue\">{6}</label></li>\r\n                                        <li class=\"col-md-6 col-xs-12 col-sm-6\"><label>Filial</label><label class=\"labelvalue\">{7}</label></li>\r\n                                        <li class=\"col-md-6 col-xs-12 col-sm-6\"><label>Unidade/Area</label><label class=\"labelvalue\">{8}</label></li>\r\n                                        <li class=\"col-md-6 col-xs-12 col-sm-6\"><label>Contatos</label><label class=\"labelvalue\"> {9}</label></li>\r\n                                    </ul>\r\n                                </div>\r\n                                <div class=\"subheader-printer\">\r\n                                     <h1>Dados do Denunciante</h1>\r\n                                </div>\r\n                                <div class=\"printer-section-envolvidos\">\r\n                                    <ul class=\"col-md-12 envolvidos\" style=\"border:0!important\">\r\n                                        <li class=\"col-md-6 col-xs-12 col-sm-6\"><label>Nome</label><label class=\"labelvalue\">{10}</label></li>\r\n                                        <li class=\"col-md-6 col-xs-12 col-sm-6\"><label>Gênero</label><label class=\"labelvalue\">{11}</label></li>\r\n                                        <li class=\"col-md-6 col-xs-12 col-sm-6\"><label>Cargo/Area</label><label class=\"labelvalue\">{12}</label></li>\r\n                                        <li class=\"col-md-6 col-xs-12 col-sm-6\"><label>Email</label><label class=\"labelvalue\">{13}</label></li>\r\n                                        <li class=\"col-md-6 col-xs-12 col-sm-6\"><label>Telefone</label><label class=\"labelvalue\">{14}</label></li>\r\n                                        <li class=\"col-md-6 col-xs-12 col-sm-6\"><label>Celular</label><label class=\"labelvalue\">{15}</label></li>\r\n                                    </ul>\r\n                                </div>\r\n                                <div class=\"subheader-printer\">\r\n                                    <h1>Dados do Relato</h1>\r\n                                </div>\r\n                                <div class=\"printer-section-envolvidos\">\r\n                                    <ul class=\"col-md-12 envolvidos\">\r\n                                        <li class=\"col-md-6 col-xs-12 col-sm-6\"><label>Tipo de Contato:</label><label class=\"labelvalue\">{16}</label></li>\r\n                                        <li class=\"col-md-6 col-xs-12 col-sm-6\"><label>Tipo de Publico:</label><label class=\"labelvalue\">{17}</label></li>\r\n                                        <li class=\"col-md-6 col-xs-12 col-sm-6\"><label>Forma de Contato:</label><label class=\"labelvalue\">{18}</label></li>\r\n                                        <li class=\"col-md-6 col-xs-12 col-sm-6\"><label>Tipo de Relato:</label><label class=\"labelvalue\">{19}</label></li>\r\n                                        <li class=\"col-md-6 col-xs-12 col-sm-6\"><label>Natureza:</label><label class=\"labelvalue\">{20}</label></li>\r\n                                        <li class=\"col-md-6 col-xs-12 col-sm-6\"><label>Tipo Natureza:</label><label class=\"labelvalue\">{21}</label></li>\r\n                                        <li class=\"col-md-6 col-xs-12 col-sm-6\"><label>Tipo Manifestação:</label><label class=\"labelvalue\">{22}</label></li>\r\n                                    </ul>\r\n                                </div>\r\n                                <div class=\"subheader-printer\">\r\n                                    <h1>Relato</h1>\r\n                                </div>\r\n                                <div class=\"printer-section text-justify\">{23}</div>\r\n                                <div class=\"subheader-printer\">\r\n                                    <h1>Envolvidos no Relato</h1>\r\n                                </div>\r\n                                <div class=\"printer-section-envolvidos\">\r\n                                    <ul class=\"col-md-12 envolvidos\">\r\n                                        <li class=\"col-lg-3 col-md-6 col-sm-6 col-xs-12\"><label>Nome</label><label class=\"labelvalue\">{24}</label></li>\r\n                                        <li class=\"col-lg-3 col-md-6 col-sm-6 col-xs-12\"><label>Email</label><label class=\"labelvalue\">{25}</label></li>\r\n                                        <li class=\"col-lg-3 col-md-6 col-sm-6 col-xs-12\"><label>Telefone</label><label class=\"labelvalue\">{26}</label></li>\r\n                                        <li class=\"col-lg-3 col-md-6 col-sm-6 col-xs-12\"><label>Cargo/Area</label><label class=\"labelvalue\">{27}</label></li>\r\n                                        <li class=\"col-lg-3 col-md-6 col-sm-6 col-xs-12\"><label>Participação no Incidente</label><label class=\"labelvalue\">{28}</label></li>\r\n                                        <li class=\"col-lg-3 col-md-6 col-sm-6 col-xs-12\"><label>Relação na empresa</label><label class=\"labelvalue\">{29}</label></li>\r\n                                        <li class=\"col-lg-3 col-md-6 col-sm-6 col-xs-12\"><label>Cidade</label><label class=\"labelvalue\">{30}</label></li>\r\n                                        <li class=\"col-lg-3 col-md-6 col-sm-6 col-xs-12\"><label>Estado</label><label class=\"labelvalue\">{31}</label></li>\r\n                                    </ul>\r\n                                    \r\n                                </div>\r\n                                <div class=\"subheader-printer\">\r\n                                    <h1>Dados Complementares do Relato</h1>\r\n                                </div>\r\n                                <div class=\"printer-section-envolvidos\">\r\n                                    <ul class=\"col-md-12 envolvidos\">\r\n                                        <li class=\"col-md-12 col-sm-12 col-xs-12\"><label>Qual a data ou período que o evento ocorreu?</label><label class=\"labelvalue\">{32}</label></li>\r\n                                        <li class=\"col-md-12 col-sm-12 col-xs-12\"><label>Existe recorrência desse evento ou sabe há quanto tempo o evento vem ocorrendo?</label><label class=\"labelvalue\">{33}</label></li>\r\n                                        <li  class=\"col-md-12 col-sm-12 col-xs-12\"><label>Como ficou ciente sobre a ocorrência deste evento?</label><label class=\"labelvalue\">{34}</label></li>\r\n                                        <li class=\"col-md-12 col-sm-12 col-xs-12\"><label>Se você tiver conhecimento de valores financeiros envolvidos, indique o valor</label><label class=\"labelvalue\">{35}</label></li>\r\n                                        <li  class=\"col-md-12 col-sm-12 col-xs-12\"><label>Se tiver alguma sugestão de como resolver o problema, descreva-a</label><label class=\"labelvalue\">{36}</label></li>\r\n                                        <li  class=\"col-md-12 col-sm-12 col-xs-12\"><label>Quanto ao conteúdo do seu relato, você</label><label class=\"labelvalue\">{37}</label></li>\r\n                                        <li class=\"col-md-12 col-sm-12 col-xs-12\"><label>Você sabe se existem evidências que comprovem o fato denunciado? em caso positivo, indique-as. quais e em que lugares podem ser encontradas evidências sobre o fato denunciado? existem documentos que comprovem esse fato? em caso positivo, onde podem ser encontrados? especifique da forma mais detalhada possível</label><label class=\"labelvalue\">{38}</label></li>\r\n                                        <li class=\"col-md-12 col-sm-12 col-xs-12\"><label>Já efetuou relato anterior associado a este mesmo assunto? se sim, informe o número de protocolo</label><label class=\"labelvalue\">{39}</label></li>\r\n\r\n\r\n                                    </ul>\r\n                                </div>\r\n                                <div class=\"subheader-printer\">\r\n                                    <p><span><i class=\"fa fa-copyright\"></i>\r\n                                        {40}. <b>SISGEDD<i class=\"fa fa-registered\"></i></b> - SISTEMA DE GESTÃO DE DENÚNCIAS DELOITTE</span>\r\n                                    </p>\r\n                                </div>\r\n\r\n                            </form>\r\n\r\n                    </div>", (object)byPk.RELATO_ID, (object)byPk.NMR_PROTOCOLO, (object)byPk.CRITICIDADE, (object)byPk.DT_INICIO, (object)byPk.DT_ENCERRAMENTO, (object)byPk.STATUS, (object)byPk.EMPRESA, (object)byPk.PRODUTO, (object)byPk.UNIDADE, (object)byPk.AREA, (object)byPk.NOME, (object)byPk.GENERO, (object)byPk.CARGO, (object)byPk.EMAIL, (object)byPk.TELEFONE, (object)byPk.CELULAR, (object)byPk.IDENTIFICADO, (object)byPk.TIPO_PUBLICO, (object)byPk.FORMA_CONTATO, (object)byPk.TIPO_RELATO, (object)byPk.NATUREZA, (object)byPk.TIPO_NATUREZA, (object)byPk.TIPO_MANIFESTACAO, (object)byPk.RELATO, (object)byPk.DEN1_NOME, (object)byPk.DEN1_EMAIL, (object)byPk.DEN1_TELEFONE, (object)byPk.DEN1_CARGO, (object)byPk.DEN1_PARTICIPACAO, (object)byPk.DEN1_RELACAO, (object)byPk.DEN1_CIDADE, (object)byPk.DEN1_ESTADO, (object)byPk.PERIODO_EVENTO_OCORREU, (object)byPk.EXISTE_RECORRENCIA, (object)byPk.COMO_FICOU_CIENTE, (object)byPk.PERDA_FINANCEIRA, (object)byPk.RELATO_SUGESTAO, (object)byPk.QUANTO_AO_RELATO, (object)byPk.EVIDENCIAS, (object)byPk.NMR_PROTOCOLO_ANTERIOR, (object)DateTime.Now.Year);
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "<h4 class=\"alert_error\">Ocorreu um erro.</h4>";
                }
            }

            public static string LoadUsers()
            {
                try
                {
                    UserBusiness userBusiness = new UserBusiness();
                    UserEntity userEntity1 = new UserEntity();
                    userEntity1.PAGEINDEX = 0;
                    userEntity1.PAGESIZE = 0;
                    userEntity1.PERFIL = "1";
                    userEntity1.FLG_STATUS = "A";
                    UserEntity objEntity = userEntity1;
                    List<UserEntity> userEntityList = userBusiness.Find(objEntity);
                    StringBuilder stringBuilder = new StringBuilder();
                    if (userEntityList.Count > 0)
                    {
                        stringBuilder.AppendFormat("<option value=\"0\">Selecione um usuário...</option>");
                        foreach (UserEntity userEntity2 in userEntityList)
                            stringBuilder.AppendFormat("<option value=\"{0}\">{1}</option>", (object)userEntity2.USER_ID, (object)userEntity2.NOME);
                    }
                    else
                        stringBuilder.Append("<option>Nenhum registro encontrado!</option>");
                    return stringBuilder.ToString();
                }
                catch
                {
                    Common.GetException(new Exception("Erro ao buscar usuarios"));
                    return "error";
                }
            }

            public static string FormataZero(string n, int q)
            {
                if (!string.IsNullOrEmpty(n))
                {
                    for (int index = 0; index < n.Length; ++index)
                    {
                        if (n.Length < q)
                            n = "0" + n;
                    }
                }
                return n;
            }

            public static string FormatCpf(string cpf, int underline)
            {
                string empty = string.Empty;
                string str;
                if (cpf.Trim().Length == 11)
                {
                    str = cpf.Substring(0, 3) + "." + cpf.Substring(3, 3) + "." + cpf.Substring(6, 3) + "-" + cpf.Substring(9, 2);
                    if (underline == 1)
                        str += " - ";
                }
                else
                    str = cpf;
                return str;
            }

            public static string SaveUser(
              int id,
              string nome,
              string email,
              string login,
              string senha,
              string perfil)
            {
                try
                {
                    UserEntity objEntity = new UserEntity() { USER_ID = id, NOME = nome.ToUpper(), EMAIL = email, LOGIN = login, PERFIL = perfil, SENHA = senha, FLG_STATUS = "A" };
                    string empty = string.Empty;
                    return id <= 0 ? (new UserBusiness().Add(objEntity) <= 0 ? "alert" : "success") : (!new UserBusiness().Update(objEntity) ? "alert" : "success");
                }
                catch
                {
                    Common.GetException(new Exception("Erro ao adicionar/atualizar usuário."));
                    return "error";
                }
            }

            public static string FindUser(int index, int pagesize, string nome)
            {
                try
                {
                    UserBusiness userBusiness = new UserBusiness();
                    UserEntity userEntity1 = new UserEntity();
                    userEntity1.NOME = nome;
                    userEntity1.PAGEINDEX = index;
                    userEntity1.PAGESIZE = pagesize;
                    UserEntity objEntity = userEntity1;
                    List<UserEntity> source = userBusiness.Find(objEntity);
                    StringBuilder stringBuilder = new StringBuilder();
                    if (source.Count > 0)
                    {
                        stringBuilder.AppendFormat("<table class=\"table responsive table-header-style2\" cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\"><caption><i class=\"fa fa-users\"></i>Usuários <span class=\"info\">{0}  registros encontrados</span></caption><thead>\r\n                        <tr style=\"display: table-row; opacity: 1;\">\r\n                        <th class=\"width10 leftalign\" scope=\"col\">Ult. Acesso</th>\r\n                        <th class=\"text-left\" scope=\"col\">Login - Nome</th>\r\n                        <th class=\"centeralign width20\" colspan=\"2\">Ação</th>\r\n                        </tr>\r\n                        </thead>\r\n                        <tbody>", (object)source.FirstOrDefault<UserEntity>().COUNTER);
                        foreach (UserEntity userEntity2 in source)
                        {
                            if (!string.IsNullOrEmpty(userEntity2.LOGIN) && userEntity2.LOGIN.ToUpper() != "SISTEMAS" && (userEntity2.LOGIN.ToUpper() != "ADMIN" && userEntity2.PERFIL != "3"))
                                stringBuilder.AppendFormat("<tr style=\"display: table-row; opacity: 1;\">\r\n                                                        <td class=\"width10 leftalign\">{2}</td>\r\n                                                        <td class=\"\"><i>{3}</i> - <b>{1}</b></td>\r\n                                                        <td class=\"centeralign width5\"><a class=\"btn\"href=\"../usuarios/editar.aspx?id={0}\" tooltip=\"tooltip\" data-title=\"Clique para editar\"><i class=\"fa fa-pencil fa-lg\"></i></a></td>\r\n                                                        <td class=\"centeralign width5\"><a class=\"btn\" href=\"#\"   title=\"Clique para deletar\" data-title=\"Clique para deletar\"   data-action=\"remove\" data-id=\"{0}\" rel=\"{0}\" tooltip=\"tooltip\"><i class=\"fa fa-trash-o fa-lg\"></i></a></td>\r\n                                                    </tr>", (object)userEntity2.USER_ID, !string.IsNullOrEmpty(userEntity2.NOME) ? (object)userEntity2.NOME : (object)string.Empty, userEntity2.DT_ULTIMO_ACESSO > DateTime.MinValue ? (object)userEntity2.DT_ULTIMO_ACESSO.ToString("dd/MM/yyyy HH:mm:ss") : (object)string.Empty, (object)userEntity2.LOGIN);
                        }
                        stringBuilder.AppendFormat("</tbody><tfoot><td colspan=\"4\" class=\"\"><span class=\"info\">{0} registros encontrados</span><input type=\"hidden\" id=\"pager\" name=\"pager\" value=\"{0}\" /></td></tfoot></table>", (object)source.FirstOrDefault<UserEntity>().COUNTER);
                    }
                    else
                        stringBuilder.Append("<h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>");
                    return stringBuilder.ToString();
                }
                catch
                {
                    Common.GetException(new Exception("Erro ao preencher o grid de usuarios"));
                    return "error";
                }
            }

            public static string FindUserByPk(int id)
            {
                try
                {
                    UserEntity byPk = new UserBusiness().FindByPk(new UserEntity() { USER_ID = id });
                    StringBuilder stringBuilder1 = new StringBuilder();
                    stringBuilder1.Append("{\"user\":\"[ ");
                    if (byPk != null)
                    {
                        stringBuilder1.Append("{");
                        StringBuilder stringBuilder2 = stringBuilder1;
                        object[] objArray = new object[7] { (object)byPk.USER_ID, (object)byPk.NOME, (object)byPk.LOGIN, (object)byPk.EMAIL, (object)byPk.PERFIL, (object)byPk.UF, null };
                        string str;
                        if (string.IsNullOrEmpty(byPk.PHOTO))
                            str = "../resources/img/bkg/no_user.png";
                        else
                            str = Common.App.GetValue("APP_ROOT_IMG", typeof(string)).ToString() + ((IEnumerable<string>)byPk.PHOTO.Split('/')).Last<string>();
                        objArray[6] = (object)str;
                        stringBuilder2.AppendFormat(" 'id':'{0}' ,'nome':'{1}','login':'{2}','email':'{3}','perfil':'{4}','uf':'{5}','photo':'{6}' ", objArray);
                        stringBuilder1.Append("}");
                    }
                    stringBuilder1.Append(" ]\"}");
                    return stringBuilder1.ToString();
                }
                catch
                {
                    Common.GetException(new Exception("Erro ao buscar usuarios"));
                    return "error";
                }
            }

            public static string RemoveUser(int id, int userid)
            {
                try
                {
                    UserBusiness userBusiness = new UserBusiness();
                    UserEntity userEntity = new UserEntity();
                    userEntity.USER_ID = id;
                    userEntity.Log = new LogEntity()
                    {
                        USER_ID = userid
                    };
                    UserEntity objEntity = userEntity;
                    return userBusiness.Remove(objEntity) ? "success" : "warning";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string RelResumoMensal(int empresaId, int mes)
            {
                List<ReportEntity> reportEntityList = new ReportBusiness().RelResumoMensal(mes, empresaId);
                StringBuilder stringBuilder = new StringBuilder();
                if (reportEntityList == null)
                    return (string)null;
                string str = string.Empty;
                stringBuilder.Append("{\"relatorio\":\"[ ");
                foreach (ReportEntity reportEntity in reportEntityList)
                {
                    str += "{";
                    str = str + "'status':'" + reportEntity.STATUS.ToString() + "','counter':'" + reportEntity.COUNTER.ToString() + "'";
                    str += "},";
                }
                stringBuilder.Append(str.Substring(0, str.Length - 1));
                stringBuilder.Append(" ]\"}");
                return stringBuilder.ToString();
            }

            public static string GetAttachment(ImapX.Message me)
            {
                try
                {
                    string str = string.Empty;
                    if (me.Attachments != null && ((IEnumerable<ImapX.Attachment>)me.Attachments).Count<ImapX.Attachment>() > 0)
                    {
                        ImapClient imapClient = new ImapClient();
                        imapClient.Connect("amemail.deloitte.com", 993, SslProtocols.Tls, true);
                        imapClient.Login(Common.App.GetValue("APP_MAILBOX_EML", typeof(string)).ToString(), Common.App.GetValue("APP_MAILBOX_PWD", typeof(string)).ToString());
                        ImapX.Message[] messageArray = imapClient.Folders["INBOX"].Search("ALL", MessageFetchMode.Attachments, 100);
                        if (messageArray != null && ((IEnumerable<ImapX.Message>)messageArray).Count<ImapX.Message>() > 0)
                        {
                            for (int index = 0; index < messageArray.Length; ++index)
                            {
                                if (messageArray[index].UId == me.UId)
                                {
                                    foreach (ImapX.Attachment attachment in messageArray[index].Attachments)
                                    {
                                        attachment.Download();
                                        string folder = HttpContext.Current.Server.MapPath("../resources/files/downloads/") + attachment.FileName.Replace("%20", "_").Replace(" ", "_");
                                        attachment.Save(folder, (string)null);
                                        HtmlFactory.DownloadSftp(attachment.FileName.Replace("%20", "_").Replace("%20", "_").Replace("%20", "_").Replace(" ", "_"));
                                        str = str + "<a href=\"../resources/files/downloads/" + attachment.FileName.Replace("%20", "_").Replace(" ", "_") + "\" onclick=\"window.open(this.href);return false;\" >" + attachment.FileName.Replace("%20", "_").Replace(" ", "_") + " </a><br/>";
                                    }
                                }
                            }
                        }
                    }
                    return str;
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static bool SaveAttachmentToRelato(ImapX.Message me, string guid)
            {
                try
                {
                    bool flag = false;
                    if (me.Attachments != null && ((IEnumerable<ImapX.Attachment>)me.Attachments).Count<ImapX.Attachment>() > 0)
                    {
                        ImapClient imapClient = new ImapClient();
                        imapClient.Connect("amemail.deloitte.com", 993, SslProtocols.Tls, true);
                        imapClient.Login(Common.App.GetValue("APP_MAILBOX_EML", typeof(string)).ToString(), Common.App.GetValue("APP_MAILBOX_PWD", typeof(string)).ToString());
                        ImapX.Message[] messageArray = imapClient.Folders["INBOX"].Search("ALL", MessageFetchMode.Attachments, 100);
                        if (messageArray != null && ((IEnumerable<ImapX.Message>)messageArray).Count<ImapX.Message>() > 0)
                        {
                            for (int index = 0; index < messageArray.Length; ++index)
                            {
                                if (messageArray[index].UId == me.UId)
                                {
                                    foreach (ImapX.Attachment attachment in messageArray[index].Attachments)
                                    {
                                        attachment.Download();
                                        string folder = HttpContext.Current.Server.MapPath("../resources/files/downloads/") + attachment.FileName.Replace("%20", "_").Replace(" ", "_");
                                        attachment.Save(folder, (string)null);
                                        HtmlFactory.DownloadSftp(attachment.FileName.Replace("%20", "_").Replace("%20", "_").Replace("%20", "_").Replace(" ", "_"));
                                        flag = new AnexoBusiness().Add(new AnexoEntity()
                                        {
                                            GUID = guid,
                                            NOME = attachment.FileName,
                                            URL = folder
                                        }) > 0;
                                    }
                                }
                            }
                        }
                    }
                    return flag;
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return false;
                }
            }

            public static string GetRelatoFromMailServer()
            {
                StringBuilder stringBuilder = new StringBuilder();
                try
                {
                    ImapClient imapClient = new ImapClient();
                    imapClient.Connect("amemail.deloitte.com", 993, SslProtocols.Tls, true);
                    imapClient.Login(Common.App.GetValue("APP_MAILBOX_EML", typeof(string)).ToString(), Common.App.GetValue("APP_MAILBOX_PWD", typeof(string)).ToString());
                    RelatoEntity objEntity1 = new RelatoEntity();
                    ImapX.Message[] messageArray = imapClient.Folders["INBOX"].Search("ALL", MessageFetchMode.Full, 100);
                    if (messageArray != null && ((IEnumerable<ImapX.Message>)messageArray).Count<ImapX.Message>() > 0)
                    {
                        for (int index = 0; index < messageArray.Length; ++index)
                        {
                            ImapX.Message message = messageArray[index];
                            string str1 = Guid.NewGuid().ToString();
                            EmpresaEntity empresaEntity = new EmpresaBusiness().Find(new EmpresaEntity() { EMAIL = message.From.Address.ToString() }).FirstOrDefault<EmpresaEntity>();
                            if (empresaEntity != null && !new RelatoBusiness().ExistRelatoByEmail(message.UId.ToString()))
                            {
                                string str2 = string.Empty;
                                if (message.Attachments != null)
                                {
                                    foreach (ImapX.Attachment attachment in message.Attachments)
                                    {
                                        if (attachment != null)
                                        {
                                            string empty1 = string.Empty;
                                            string empty2 = string.Empty;
                                            AnexoBusiness anexoBusiness = new AnexoBusiness();
                                            AnexoEntity objEntity2 = new AnexoEntity();
                                            attachment.Download();
                                            HtmlFactory.DownloadSftp(attachment.FileName.Replace("%20", "_").Replace("%20", "_").Replace("%20", "_").Replace(" ", "_"));
                                            string folder = HttpContext.Current.Server.MapPath("../resources/files/downloads/");
                                            string str3 = HttpContext.Current.Server.MapPath("../resources/files/downloads/") + attachment.FileName.Replace("%20", "_").Replace(" ", "_");
                                            attachment.Save(folder, (string)null);
                                            objEntity2.URL = str3;
                                            str2 = str2 + "<a href=\"../resources/files/downloads/" + attachment.FileName.Replace("%20", "_").Replace(" ", "_") + "\" onclick=\"window.open(this.href);return false;\" >" + attachment.FileName.Replace("%20", "_").Replace(" ", "_") + " </a><br/>";
                                            objEntity2.GUID = str1;
                                            objEntity2.DT_ANEXO = DateTime.Now;
                                            anexoBusiness.Add(objEntity2);
                                        }
                                    }
                                }
                                DateTime? date = message.Date;
                                DateTime dateTime1;
                                if (date.HasValue)
                                {
                                    date = message.Date;
                                    DateTime minValue = DateTime.MinValue;
                                    if ((date.HasValue ? (date.GetValueOrDefault() > minValue ? 1 : 0) : 0) != 0)
                                    {
                                        dateTime1 = Convert.ToDateTime(Convert.ToDateTime((object)message.Date).ToString("dd/MM/yyyy"));
                                        goto label_14;
                                    }
                                }
                                dateTime1 = DateTime.Now;
                                label_14:
                                DateTime dateTime2 = dateTime1;
                                objEntity1.DT_INICIO = dateTime2;
                                objEntity1.FLG_FLUXO = "M";
                                objEntity1.FORMA_CONTATO = "EMAIL";
                                objEntity1.EMPRESA_ID = empresaEntity.EMPRESA_ID;
                                objEntity1.EMPRESA = empresaEntity.NOME;
                                objEntity1.RELATO = string.Format("<b>Data: </b> {0}", (object)dateTime2) + (message.Subject != null ? "<br/><b>Titulo: </b>" + message.Subject.ToString() : string.Empty) + (message.From == null || message.From.DisplayName == null ? string.Empty : "<br/><b>Nome: </b>" + message.From.DisplayName.ToString()) + (message.From == null || message.From.Address == null ? string.Empty : "<br/><b>Email: </b>" + message.From.Address.ToString()) + "<br/>" + (message.Body == null || !message.Body.HasHtml ? (message.Body == null || !message.Body.HasText ? string.Empty : message.Body.Text) : message.Body.Html) + str2;
                                objEntity1.GUID = str1.ToString();
                                objEntity1.EMAIL_ID = message.UId.ToString();
                                new RelatoBusiness().AddFromEmail(objEntity1);
                            }
                        }
                    }
                    return HtmlFactory.AdminView.FindRelatoByEmail(0, 1000000, 0);
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "<h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>";
                }
            }

            public static string LoadEmail()
            {
                try
                {
                    StringBuilder stringBuilder1 = new StringBuilder();
                    List<EmailEntity> emailEntityList1 = new EmailBusiness().Find(new EmailEntity());
                    List<EmailEntity> emailEntityList2 = new List<EmailEntity>();
                    ImapClient imapClient = new ImapClient();
                    imapClient.Connect("amemail.deloitte.com", 993, SslProtocols.Tls, true);
                    imapClient.Login(Common.App.GetValue("APP_MAILBOX_EML", typeof(string)).ToString(), Common.App.GetValue("APP_MAILBOX_PWD", typeof(string)).ToString());
                    StringBuilder stringBuilder2 = new StringBuilder();
                    ImapX.Message[] messageArray = imapClient.Folders["INBOX"].Search("ALL", MessageFetchMode.Full, 100);
                    int num = 0;
                    if (((IEnumerable<ImapX.Message>)messageArray).Count<ImapX.Message>() > 0)
                    {
                        stringBuilder1.AppendFormat("\r\n                                    <table class=\"table responsive table-header-style2 tablesortermail\" cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n                                    <caption><i class=\"fa fa-envelope\"></i> Denúncias por email <span class=\"info\"> _COUNT_REPLACER_ registros encontrados</span></caption>\r\n                                    <thead>\r\n                                        <tr>\r\n                                            <th class=\"width15 text-center\">Data</th>\r\n                                            <th class=\"text-left\">Empresa</th>\r\n                                            <th class=\"centeralign width15\">Ações</th>\r\n                                        </tr>\r\n                                    </thead>\r\n                                    <tbody>");
                        DateTime? date;
                        DateTime dateTime;
                        foreach (ImapX.Message message in messageArray)
                        {
                            if (emailEntityList1 != null && emailEntityList1.Count > 0)
                            {
                                bool flag = false;
                                foreach (EmailEntity emailEntity in emailEntityList1)
                                {
                                    if (message.UId.ToString() == emailEntity.MESSAGE_ID)
                                        flag = true;
                                }
                                if (!flag)
                                {
                                    EmpresaEntity empresaEntity = new EmpresaBusiness().Find(new EmpresaEntity() { EMAIL = message.From.Address }).FirstOrDefault<EmpresaEntity>();
                                    if (empresaEntity != null)
                                    {
                                        ++num;
                                        StringBuilder stringBuilder3 = stringBuilder1;
                                        date = message.Date;
                                        string empty;
                                        if (date.HasValue)
                                        {
                                            date = message.Date;
                                            DateTime minValue = DateTime.MinValue;
                                            if ((date.HasValue ? (date.GetValueOrDefault() > minValue ? 1 : 0) : 0) != 0)
                                            {
                                                dateTime = Convert.ToDateTime((object)message.Date);
                                                empty = dateTime.ToString("dd/MM/yyyy");
                                                goto label_15;
                                            }
                                        }
                                        empty = string.Empty;
                                        label_15:
                                        string str1 = !string.IsNullOrEmpty(empresaEntity.NOME) ? empresaEntity.NOME : "NÃO IDENTIFICADO";
                                        string str2 = message.UId > 0L ? message.UId.ToString() : string.Empty;
                                        string str3 = empty;
                                        string str4 = str1;
                                        string str5 = str2;
                                        stringBuilder3.AppendFormat("\r\n                                            <tr style=\"display: table-row; opacity: 1;\" style=\"vertical-align:center\">\r\n                                                <td class=\"width15 text-center\" style=\"vertical-align:center\"><b>{0}</b></td>     \r\n                                                <td class=\"width70 text-left\" style=\"vertical-align:center\"><b>{1}</b></td>     \r\n                                                <td class=\"centeralign  width5\"><a class=\"btn\" href=\"#\"  tooltip=\"tooltip\" data-title=\"Clique para visualizar\" data-id=\"{2}\"  onclick=\"SHOW_MODAL_PREVIEW({2});return false;\"><i class=\"fa fa-eye fa-lg\"></i></a></td>\r\n                                            </tr>", (object)str3, (object)str4, (object)str5);
                                    }
                                }
                            }
                            else
                            {
                                EmpresaEntity empresaEntity = new EmpresaBusiness().Find(new EmpresaEntity() { EMAIL = message.From.Address }).FirstOrDefault<EmpresaEntity>();
                                if (empresaEntity != null)
                                {
                                    ++num;
                                    StringBuilder stringBuilder3 = stringBuilder1;
                                    date = message.Date;
                                    string empty;
                                    if (date.HasValue)
                                    {
                                        date = message.Date;
                                        dateTime = DateTime.MinValue;
                                        if ((date.HasValue ? (date.GetValueOrDefault() > dateTime ? 1 : 0) : 0) != 0)
                                        {
                                            dateTime = Convert.ToDateTime((object)message.Date);
                                            empty = dateTime.ToString("dd/MM/yyyy");
                                            goto label_21;
                                        }
                                    }
                                    empty = string.Empty;
                                    label_21:
                                    string str1 = !string.IsNullOrEmpty(empresaEntity.NOME) ? empresaEntity.NOME : "NÃO IDENTIFICADO";
                                    string str2 = message.UId > 0L ? message.UId.ToString() : string.Empty;
                                    string str3 = empty;
                                    string str4 = str1;
                                    string str5 = str2;
                                    stringBuilder3.AppendFormat("\r\n                                            <tr style=\"display: table-row; opacity: 1;\" style=\"vertical-align:center\">\r\n                                                <td class=\"width15 text-center\" style=\"vertical-align:center\"><b>{0}</b></td>     \r\n                                                <td class=\"width70 text-left\" style=\"vertical-align:center\"><b>{1}</b></td>     \r\n                                                <td class=\"centeralign  width5\"><a class=\"btn\" href=\"#\"  tooltip=\"tooltip\" data-title=\"Clique para visualizar\" data-id=\"{2}\"  onclick=\"SHOW_MODAL_PREVIEW({2});return false;\"><i class=\"fa fa-eye fa-lg\"></i></a></td>\r\n                                            </tr>", (object)str3, (object)str4, (object)str5);
                                }
                            }
                        }
                        stringBuilder1.AppendFormat("</tbody><tfoot><td colspan=\"3\" class=\"\"><span class=\"info\">{0} registros encontrados</span><input type=\"hidden\" id=\"pager\" name=\"pager\" value=\"{0}\" /></td></tfoot></table>", (object)num).Replace("_COUNT_REPLACER_", num.ToString());
                    }
                    else
                        stringBuilder1.Append("<h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>");
                    if (num > 0)
                        return stringBuilder1.ToString();
                    return "<h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "<h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>";
                }
            }

            public static RelatoEntity GetEmailByPk(long pk)
            {
                try
                {
                    RelatoEntity relatoEntity1 = new RelatoEntity();
                    relatoEntity1.FORMA_CONTATO = "EMAIL";
                    ImapClient imapClient = new ImapClient();
                    imapClient.Connect("amemail.deloitte.com", 993, SslProtocols.Tls, true);
                    imapClient.Login(Common.App.GetValue("APP_MAILBOX_EML", typeof(string)).ToString(), Common.App.GetValue("APP_MAILBOX_PWD", typeof(string)).ToString());
                    StringBuilder stringBuilder = new StringBuilder();
                    ImapX.Message[] messageArray = imapClient.Folders["INBOX"].Search("ALL", MessageFetchMode.Full, 100);
                    if (messageArray != null && ((IEnumerable<ImapX.Message>)messageArray).Count<ImapX.Message>() > 0)
                    {
                        for (int index = 0; index < messageArray.Length; ++index)
                        {
                            if (messageArray[index].UId == pk)
                            {
                                relatoEntity1.FLG_FLUXO = "R";
                                ImapX.Message me = messageArray[index];
                                relatoEntity1.FORMA_CONTATO = "EMAIL";
                                EmpresaEntity empresaEntity = new EmpresaBusiness().Find(new EmpresaEntity() { EMAIL = me.From.Address.ToString() }).FirstOrDefault<EmpresaEntity>();
                                if (empresaEntity != null)
                                {
                                    relatoEntity1.EMPRESA_ID = empresaEntity.EMPRESA_ID;
                                    relatoEntity1.EMPRESA = empresaEntity.NOME;
                                    RelatoEntity relatoEntity2 = relatoEntity1;
                                    string[] strArray = new string[7];
                                    DateTime? date = me.Date;
                                    DateTime dateTime = DateTime.MinValue;
                                    string str1;
                                    if ((date.HasValue ? (date.GetValueOrDefault() > dateTime ? 1 : 0) : 0) == 0)
                                    {
                                        str1 = string.Empty;
                                    }
                                    else
                                    {
                                        dateTime = Convert.ToDateTime((object)me.Date);
                                        str1 = "<b>Data: </b> " + dateTime.ToString("dd/MM/yyyy");
                                    }
                                    strArray[0] = str1;
                                    strArray[1] = me.Subject != null ? "<br/><b>Titulo: </b>" + me.Subject.ToString() : string.Empty;
                                    strArray[2] = me.From == null || me.From.DisplayName == null ? string.Empty : "<br/><b>Nome: </b>" + me.From.DisplayName.ToString();
                                    strArray[3] = me.From == null || me.From.Address == null ? string.Empty : "<br/><b>Email: </b>" + me.From.Address.ToString();
                                    strArray[4] = "<br/>";
                                    strArray[5] = me.Body == null || !me.Body.HasHtml ? (me.Body == null || !me.Body.HasText ? string.Empty : me.Body.Text) : me.Body.Html;
                                    strArray[6] = HtmlFactory.AdminView.GetAttachment(me);
                                    string str2 = string.Concat(strArray);
                                    relatoEntity2.RELATO = str2;
                                }
                            }
                        }
                    }
                    return relatoEntity1;
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return (RelatoEntity)null;
                }
            }

            public static string RemoveRelatoByEmail(int rid)
            {
                try
                {
                    string empty = string.Empty;
                    return rid > 0 ? (new RelatoBusiness().UpdateStatus(rid, "D") ? "success" : "warning") : "warning";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static byte[] GetFileStream(string file)
            {
                try
                {
                    FileStream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read);
                    BinaryReader binaryReader = new BinaryReader((Stream)fileStream);
                    byte[] numArray = binaryReader.ReadBytes(Convert.ToInt32(fileStream.Length));
                    binaryReader.Close();
                    fileStream.Close();
                    return numArray;
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return (byte[])null;
                }
            }

            public static string GetEmailPreview(long pk)
            {
                try
                {
                    StringBuilder stringBuilder1 = new StringBuilder();
                    ImapClient imapClient = new ImapClient();
                    imapClient.Connect("amemail.deloitte.com", 993, SslProtocols.Tls, true);
                    imapClient.Login(Common.App.GetValue("APP_MAILBOX_EML", typeof(string)).ToString(), Common.App.GetValue("APP_MAILBOX_PWD", typeof(string)).ToString());
                    ImapX.Message[] messageArray = imapClient.Folders["INBOX"].Search("ALL", MessageFetchMode.Full, 100);
                    if (messageArray != null && ((IEnumerable<ImapX.Message>)messageArray).Count<ImapX.Message>() > 0)
                    {
                        for (int index = 0; index < messageArray.Length; ++index)
                        {
                            if (messageArray[index].UId == pk)
                            {
                                ImapX.Message me = messageArray[index];
                                StringBuilder stringBuilder2 = stringBuilder1;
                                object[] objArray1 = new object[6];
                                DateTime? date = me.Date;
                                DateTime minValue = DateTime.MinValue;
                                string str = (date.HasValue ? (date.GetValueOrDefault() > minValue ? 1 : 0) : 0) != 0 ? Convert.ToDateTime((object)me.Date).ToString("dd/MM/yyyy") : string.Empty;
                                objArray1[0] = (object)str;
                                objArray1[1] = me.Subject != null ? (object)me.Subject.ToString() : (object)string.Empty;
                                objArray1[2] = me.From == null || me.From.DisplayName == null ? (object)string.Empty : (object)me.From.DisplayName;
                                objArray1[3] = me.From == null || me.From.Address == null ? (object)string.Empty : (object)me.From.Address;
                                objArray1[4] = me.Body == null || !me.Body.HasHtml ? (me.Body == null || !me.Body.HasText ? (object)string.Empty : (object)me.Body.Text) : (object)me.Body.Html;
                                objArray1[5] = (object)HtmlFactory.AdminView.GetAttachment(me);
                                object[] objArray2 = objArray1;
                                stringBuilder2.AppendFormat("<hr/>  <br/><b>Data: </b>{0}<br/><b>Titulo: </b>{1}<br/><b>De:</b> {2} - {3}<br/><hr/><br/><b>Email: </b>{4}<br/><hr/><b>Anexos:</b></br>{5}", objArray2);
                            }
                        }
                    }
                    return stringBuilder1.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "Nenhum conteudo para ser exibido";
                }
            }

            public static bool SendMailSsl(string to, string name, string msg)
            {
                try
                {
                    MailMessage message = new MailMessage();
                    message.From = new System.Net.Mail.MailAddress("naoresponder@deloitte.com");
                    message.To.Add(new System.Net.Mail.MailAddress(to));
                    message.Subject = "Protocolo de Atendimento";
                    message.Body = msg;
                    message.BodyEncoding = Encoding.UTF8;
                    message.Priority = MailPriority.Normal;
                    message.IsBodyHtml = true;
                    message.SubjectEncoding = Encoding.UTF8;
                    message.BodyEncoding = Encoding.UTF8;
                    AlternateView alternateViewFromString = AlternateView.CreateAlternateViewFromString(message.Body, (Encoding)null, "text/html");
                    message.AlternateViews.Add(alternateViewFromString);
                    SmtpClient smtpClient = new SmtpClient("amemail.deloitte.com", 587);
                    NetworkCredential networkCredential = new NetworkCredential();
                    networkCredential.UserName = Common.App.GetValue("APP_MAILHOST_USER", typeof(string)).ToString();
                    networkCredential.Password = Common.App.GetValue("APP_MAILHOST_PWD", typeof(string)).ToString();
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = (ICredentialsByHost)networkCredential;
                    smtpClient.EnableSsl = true;
                    smtpClient.Send(message);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            public static string LoadModelo()
            {
                try
                {
                    ModeloBusiness modeloBusiness = new ModeloBusiness();
                    ModeloEntity modeloEntity1 = new ModeloEntity();
                    modeloEntity1.PAGEINDEX = 0;
                    modeloEntity1.PAGESIZE = 0;
                    ModeloEntity objEntity = modeloEntity1;
                    List<ModeloEntity> modeloEntityList = modeloBusiness.Find(objEntity);
                    StringBuilder stringBuilder = new StringBuilder();
                    if (modeloEntityList.Count > 0)
                    {
                        stringBuilder.AppendFormat("<option value=\"\">SELECIONE UM MODELO...</option>");
                        foreach (ModeloEntity modeloEntity2 in modeloEntityList)
                            stringBuilder.AppendFormat("<option value=\"{0}\">{1}</option>", (object)modeloEntity2.MODELO_ID, (object)modeloEntity2.NOME);
                    }
                    else
                        stringBuilder.Append("<option>Nenhum registro encontrado!</option>");
                    return stringBuilder.ToString();
                }
                catch
                {
                    Common.GetException(new Exception("Erro ao buscar modelos"));
                    return "<option>Nenhum registro encontrado!</option>";
                }
            }

            public static string AddPesquisa(
              string protocolo,
              string pg1,
              string pg1B,
              string pg1A,
              string pg12A,
              string pg21,
              string pg21A,
              string pg3,
              string pg4,
              string pg41,
              string pg5,
              string pg51,
              string pg6,
              string pg61)
            {
                try
                {
                    PesquisaSatisfacaoEntity objEntity = new PesquisaSatisfacaoEntity() { PG1 = pg1, PG12A = pg12A, PG1A = pg1A, PG1B = pg1B, PG21 = pg21, PG21A = pg21A, PG3 = pg3, PG4 = pg4, PG41 = pg41, PG5 = pg5, PG51 = pg51, PG6 = pg6, PG61 = pg61, NMR_PROTOCOLO = protocolo };
                    string empty = string.Empty;
                    return new PesquisaSatisfacaoBusiness().Add(objEntity) <= 0 ? "alert" : "success";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string LoadTipoManifestacao()
            {
                try
                {
                    List<TipoManifestacaoEntity> manifestacaoEntityList = new TipoManifestacaoBusiness().Find(new TipoManifestacaoEntity());
                    StringBuilder stringBuilder = new StringBuilder();
                    if (manifestacaoEntityList.Count > 0)
                    {
                        foreach (TipoManifestacaoEntity manifestacaoEntity in manifestacaoEntityList)
                            stringBuilder.AppendFormat("<option value=\"{0}\">{1}</option>", (object)manifestacaoEntity.NOME, (object)manifestacaoEntity.NOME);
                    }
                    else
                        stringBuilder.Append("<option>Nenhum registro encontrado!</option>");
                    return stringBuilder.ToString();
                }
                catch
                {
                    Common.GetException(new Exception("Erro ao buscar tipomanifestacao"));
                    return "<option>Nenhum registro encontrado!</option>";
                }
            }

            public static string SaveTipoManifestacao(string nome, int id)
            {
                try
                {
                    TipoManifestacaoEntity objEntity = new TipoManifestacaoEntity() { TIPOMANIFESTACAO_ID = id, NOME = nome };
                    if (id > 0)
                        return new TipoManifestacaoBusiness().Update(objEntity) ? "success" : "alert";
                    return new TipoManifestacaoBusiness().Add(objEntity) > 0 ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string RemoveTipoManifestacao(int id)
            {
                try
                {
                    return new TipoManifestacaoBusiness().Remove(new TipoManifestacaoEntity() { TIPOMANIFESTACAO_ID = id }) ? "success" : "alert";
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string FindTipoManifestacao(int index, int pagesize, string nome)
            {
                try
                {
                    TipoManifestacaoBusiness manifestacaoBusiness = new TipoManifestacaoBusiness();
                    TipoManifestacaoEntity manifestacaoEntity1 = new TipoManifestacaoEntity();
                    manifestacaoEntity1.NOME = !string.IsNullOrEmpty(nome) ? nome : string.Empty;
                    manifestacaoEntity1.PAGEINDEX = index;
                    manifestacaoEntity1.PAGESIZE = pagesize;
                    TipoManifestacaoEntity objEntity = manifestacaoEntity1;
                    List<TipoManifestacaoEntity> source = manifestacaoBusiness.Find(objEntity);
                    StringBuilder stringBuilder = new StringBuilder();
                    if (source != null && source.Count > 0)
                    {
                        stringBuilder.AppendFormat("\r\n                    <table class=\"table responsive table-header-style2\" cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">\r\n                    <caption><i class=\"fa fa-building\"></i> Tipo Manifestacao <span class=\"info\">{0} registros encontrados</span></caption>\r\n                    <thead>\r\n                    <tr>\r\n                        \r\n                        <th >Nome</th>\r\n                        <th class=\"centeralign width10\" colspan=\"2\">Ações</th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>", (object)source.FirstOrDefault<TipoManifestacaoEntity>().COUNTER);
                        foreach (TipoManifestacaoEntity manifestacaoEntity2 in source)
                            stringBuilder.AppendFormat("\r\n                    <tr style=\"display: table-row; opacity: 1;\" >\r\n                        <td class=\"w150\"><b>{1}</b></td>   \r\n                        <td class=\"centeralign width5\"><a class=\"btn\"href=\"../tipomanifestacao/editar.aspx?id={0}\" tooltip=\"tooltip\" data-title=\"Clique para editar\"><i class=\"fa fa-pencil fa-lg\"></i></a></td>\r\n                        <td class=\"centeralign width5\"><a class=\"btn\" href=\"#\" data-action=\"remove\"  tooltip=\"tooltip\" data-title=\"Clique para remover\" data-id=\"{0}\"  ><i class=\"fa fa-trash fa-lg\"></i></a></td>\r\n                    </tr>\r\n                    ", (object)manifestacaoEntity2.TIPOMANIFESTACAO_ID, !string.IsNullOrEmpty(manifestacaoEntity2.NOME) ? (object)manifestacaoEntity2.NOME : (object)string.Empty);
                        stringBuilder.AppendFormat("</tbody><tfoot><td colspan=\"3\" class=\"\"><span class=\"info\">{0} registros encontrados</span><input type=\"hidden\" id=\"pager\" name=\"pager\" value=\"{0}\" /></td></tfoot></table>", (object)source.FirstOrDefault<TipoManifestacaoEntity>().COUNTER);
                    }
                    else
                        stringBuilder.AppendFormat("<h4 class=\"alert-no-result\">Nenhum registro encontrado!</h4>");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "<h4 class=\"alert_error\">Ocorreu um erro.</h4>";
                }
            }

            public static string FindTipoManifestacaoByPk(int id)
            {
                try
                {
                    TipoManifestacaoEntity byPk = new TipoManifestacaoBusiness().FindByPk(new TipoManifestacaoEntity() { TIPOMANIFESTACAO_ID = id });
                    StringBuilder stringBuilder = new StringBuilder();
                    if (byPk == null)
                        return (string)null;
                    stringBuilder.Append("{\"tipomanifestacao\":\"[{ ");
                    stringBuilder.AppendFormat("'data_id':'{0}','data_nome':'{1}'", (object)byPk.TIPOMANIFESTACAO_ID, !string.IsNullOrEmpty(byPk.NOME) ? (object)byPk.NOME : (object)string.Empty);
                    stringBuilder.Append("}]\"}");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return string.Empty;
                }
            }

            public static string loadTipoPublicoPorEmpresa(int empresaId, string _idioma)
            {
                try
                {
                    if (empresaId <= 0)
                        return "required";
                    List<TipoPublicoEntity> byEmpresa = new TipoPublicoBusiness().FindByEmpresa(new EmpresaBusiness().FindByPk(new EmpresaEntity() { EMPRESA_ID = empresaId }), _idioma);
                    StringBuilder stringBuilder = new StringBuilder();
                    if (!(_idioma == "fr-FR"))
                    {
                        if (!(_idioma == "en-US"))
                        {
                            if (_idioma == "es-ES")
                                stringBuilder.Append("<option value=\"0\" selected=\"selected\">Seleccionar tipo de audiencia...</option>");
                            else
                                stringBuilder.Append("<option value=\"0\" selected=\"selected\">Selecione o tipo de público...</option>");
                        }
                        else
                            stringBuilder.Append("<option value=\"0\" selected=\"selected\">Select audience type...</option>");
                    }
                    else
                        stringBuilder.Append("<option value=\"0\" selected=\"selected\">Sélectionnez le type de public...</option>");
                    if (byEmpresa.Count != 0)
                    {
                        foreach (TipoPublicoEntity tipoPublicoEntity in byEmpresa)
                            stringBuilder.AppendFormat("<option value=\"{0}\">{1}</option>", (object)tipoPublicoEntity.TIPOPUBLICO_ID, (object)tipoPublicoEntity.NOME);
                    }
                    else
                        stringBuilder.Append("<option value=\"0\">Nenuhum...</option>");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string loadTipoRelatoPorEmpresa(int empresaId, string _idioma)
            {
                try
                {
                    if (empresaId <= 0)
                        return "required";
                    List<TipoRelatoEntity> byEmpresa = new TipoRelatoBusiness().FindByEmpresa(new EmpresaBusiness().FindByPk(new EmpresaEntity() { EMPRESA_ID = empresaId }), _idioma);
                    StringBuilder stringBuilder = new StringBuilder();
                    if (!(_idioma == "fr-FR"))
                    {
                        if (!(_idioma == "en-US"))
                        {
                            if (_idioma == "es-ES")
                                stringBuilder.Append("<option value=\"0\" selected=\"selected\">Seleccionar tipo de informe...</option>");
                            else
                                stringBuilder.Append("<option value=\"0\" selected=\"selected\">Selecione o tipo de relato...</option>");
                        }
                        else
                            stringBuilder.Append("<option value=\"0\" selected=\"selected\">Select report type...</option>");
                    }
                    else
                        stringBuilder.Append("<option value=\"0\" selected=\"selected\">Sélectionnez le type de rapport...</option>");
                    if (byEmpresa.Count != 0)
                    {
                        foreach (TipoRelatoEntity tipoRelatoEntity in byEmpresa)
                            stringBuilder.AppendFormat("<option value=\"{0}\">{1}</option>", (object)tipoRelatoEntity.TIPORELATO_ID, (object)tipoRelatoEntity.NOME);
                    }
                    else
                        stringBuilder.Append("<option value=\"0\">Nenuhum...</option>");
                    return stringBuilder.ToString();
                }
                catch (Exception ex)
                {
                    Common.GetException(ex);
                    return "error";
                }
            }

            public static string EncryptString(string plainText, byte[] key, byte[] iv)
            {
                Aes aes = Aes.Create();
                aes.Mode = CipherMode.CBC;
                aes.Key = key;
                aes.IV = iv;
                MemoryStream memoryStream = new MemoryStream();
                ICryptoTransform encryptor = aes.CreateEncryptor();
                CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write);
                byte[] bytes = Encoding.ASCII.GetBytes(plainText);
                cryptoStream.Write(bytes, 0, bytes.Length);
                cryptoStream.FlushFinalBlock();
                byte[] array = memoryStream.ToArray();
                memoryStream.Close();
                cryptoStream.Close();
                return Convert.ToBase64String(array, 0, array.Length);
            }

            public static string DecryptString(string cipherText, byte[] key, byte[] iv)
            {
                Aes aes = Aes.Create();
                aes.Mode = CipherMode.CBC;
                aes.Key = key;
                aes.IV = iv;
                MemoryStream memoryStream = new MemoryStream();
                ICryptoTransform decryptor = aes.CreateDecryptor();
                CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Write);
                string empty = string.Empty;
                try
                {
                    byte[] buffer = Convert.FromBase64String(cipherText);
                    cryptoStream.Write(buffer, 0, buffer.Length);
                    cryptoStream.FlushFinalBlock();
                    byte[] array = memoryStream.ToArray();
                    return Encoding.ASCII.GetString(array, 0, array.Length);
                }
                finally
                {
                    memoryStream.Close();
                    cryptoStream.Close();
                }
            }

            private static string CryptoService(
              string HashValue,
              string ActionType,
              string ClientSecretKey)
            {
                string str = HashValue;
                byte[] hash = SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes(ClientSecretKey));
                byte[] iv = new byte[16];
                if (ActionType.ToUpper() == "E")
                    return HtmlFactory.AdminView.EncryptString(str, hash, iv);
                return HtmlFactory.AdminView.DecryptString(str, hash, iv);
            }

            public enum HistoricoType
            {
                Atendente,
                Revisor,
                Denunciante,
            }
        }
    }
}
