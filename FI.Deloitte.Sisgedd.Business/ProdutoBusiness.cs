﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Business.ProdutoBusiness
// Assembly: FI.Deloitte.Sisgedd.Business, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 170931A6-A2F0-40B5-B763-F38503D1EBDA
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Business.dll

using FI.Deloitte.Sisgedd.DataAccessLayer;
using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;

namespace FI.Deloitte.Sisgedd.Business
{
  public class ProdutoBusiness
  {
    private bool _dispose = true;
    private DataAccessLayerFactory _objDao;

    public ProdutoBusiness()
    {
      this._objDao = new DataAccessLayerFactory();
    }

    public ProdutoBusiness(ref DataAccessLayerFactory objDaoInstance)
    {
      this._objDao = objDaoInstance;
      this._dispose = false;
    }

    public int Add(ProdutoEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new ProdutoDAL().add(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Add(objEntity);
        return 0;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public bool Update(ProdutoEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new ProdutoDAL().update(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Update(objEntity);
        return false;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public bool Remove(ProdutoEntity objEntity)
    {
      try
      {
        if (this._dispose)
        {
          this._objDao.startConnection();
          this._objDao.hasTransaction();
        }
        AreaBusiness areaBusiness = new AreaBusiness(ref this._objDao);
        List<AreaEntity> areaEntityList = areaBusiness.Find(new AreaEntity() { PRODUTO_ID = objEntity.PRODUTO_ID });
        if (areaEntityList != null && areaEntityList.Count > 0)
        {
          foreach (AreaEntity objEntity1 in areaEntityList)
            areaBusiness.Remove(objEntity1);
        }
        int num = new ProdutoDAL().remove(objEntity, ref this._objDao) ? 1 : 0;
        if (this._dispose)
          this._objDao.commitTransaction();
        return (uint) num > 0U;
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Remove(objEntity);
        if (this._dispose)
          this._objDao.rollbackTransaction();
        return false;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public ProdutoEntity FindByPk(ProdutoEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new ProdutoDAL().findByPK(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.FindByPk(objEntity);
        return (ProdutoEntity) null;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }

    public List<ProdutoEntity> Find(ProdutoEntity objEntity)
    {
      try
      {
        this._objDao.startConnection();
        return new ProdutoDAL().find(objEntity, ref this._objDao);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
          return this.Find(objEntity);
        throw ex;
      }
      finally
      {
        if (this._dispose)
          this._objDao.Dispose();
      }
    }
  }
}
