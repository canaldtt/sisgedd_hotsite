﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("FI_BLL")]
[assembly: AssemblyDescription("A Fabrica Interactiva Desenvolvimento de Software possui todos os direitos de propriedade intelectual do Software. O Software é licenciado, não vendido.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Fabrica Interactiva Desenvolvimento de Software - www.fabricai.com.br")]
[assembly: AssemblyProduct("FI_BLL")]
[assembly: AssemblyCopyright("Copyright © Fabrica Interactiva 2016")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(true)]
[assembly: Guid("dde5d229-3c88-4813-8ca9-a90fe18993a9")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyVersion("1.0.0.0")]
