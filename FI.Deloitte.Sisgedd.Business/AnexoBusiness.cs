﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Business.AnexoBusiness
// Assembly: FI.Deloitte.Sisgedd.Business, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 170931A6-A2F0-40B5-B763-F38503D1EBDA
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Business.dll

using FI.Deloitte.Sisgedd.DataAccessLayer;
using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FI.Deloitte.Sisgedd.Business
{
    public class AnexoBusiness
    {
        private bool _dispose = true;
        private DataAccessLayerFactory _objDao;

        public AnexoBusiness()
        {
            this._objDao = new DataAccessLayerFactory();
        }

        public AnexoBusiness(string conexao, string provider)
        {
            this._objDao = new DataAccessLayerFactory(conexao, provider);
        }

        public AnexoBusiness(ref DataAccessLayerFactory objDaoInstance)
        {
            this._objDao = objDaoInstance;
            this._dispose = false;
        }

        public int Add(AnexoEntity objEntity)
        {
            try
            {
                this._objDao.startConnection();
                AnexoDAL anexoDal1 = new AnexoDAL();
                if (objEntity.STATUS)
                {
                    AnexoDAL anexoDal2 = anexoDal1;
                    AnexoEntity anexoEntity = new AnexoEntity();
                    anexoEntity.GUID = objEntity.GUID;
                    anexoEntity.STATUS = objEntity.STATUS;
                    DataAccessLayerFactory objDao = this._objDao;
                    AnexoEntity objEntity1 = anexoEntity;
                    DataAccessLayerFactory local = objDao;
                    List<AnexoEntity> source = anexoDal2.find(objEntity1, ref local);
                    if (source != null && source.Count<AnexoEntity>() > 0)
                    {
                        foreach (AnexoEntity objEntity2 in source)
                            anexoDal1.remove(objEntity2, ref this._objDao);
                    }
                }
                return anexoDal1.add(objEntity, ref this._objDao);
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.Add(objEntity);
                return 0;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public bool Update(AnexoEntity objEntity)
        {
            try
            {
                this._objDao.startConnection();
                return new AnexoDAL().update(objEntity, ref this._objDao);
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.Update(objEntity);
                return false;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public bool Remove(AnexoEntity objEntity)
        {
            try
            {
                this._objDao.startConnection();
                return new AnexoDAL().remove(objEntity, ref this._objDao);
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.Remove(objEntity);
                return false;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public AnexoEntity FindByPk(AnexoEntity objEntity)
        {
            try
            {
                this._objDao.startConnection();
                return new AnexoDAL().findByPK(objEntity, ref this._objDao);
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.FindByPk(objEntity);
                return (AnexoEntity)null;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public List<AnexoEntity> Find(AnexoEntity objEntity)
        {
            try
            {
                this._objDao.startConnection();
                return new AnexoDAL().find(objEntity, ref this._objDao);
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.Find(objEntity);
                throw ex;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }

        public List<AnexoEntity> FindAnexoFinal(AnexoEntity objEntity)
        {
            try
            {
                this._objDao.startConnection();
                return new AnexoDAL().find(objEntity, ref this._objDao);
            }
            catch (Exception ex)
            {
                Common.GetException(ex);
                if (ex.Message.Contains("Tempo limite expirou") || ex.Message.Contains("Timeout"))
                    return this.Find(objEntity);
                throw ex;
            }
            finally
            {
                if (this._dispose)
                    this._objDao.Dispose();
            }
        }
    }
}
