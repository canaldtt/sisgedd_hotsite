﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Business.Simulador
// Assembly: FI.Deloitte.Sisgedd.Business, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 170931A6-A2F0-40B5-B763-F38503D1EBDA
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Business.dll

namespace FI.Deloitte.Sisgedd.Business
{
  public class Simulador
  {
    public int Somar(int num1, int num2)
    {
      return num1 + num2;
    }

    public int Multiplicar(int num1, int num2)
    {
      return num1 * num2;
    }
  }
}
