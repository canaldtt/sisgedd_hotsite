﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Business.Common
// Assembly: FI.Deloitte.Sisgedd.Business, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 170931A6-A2F0-40B5-B763-F38503D1EBDA
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Business.dll

using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;

namespace FI.Deloitte.Sisgedd.Business
{
  public static class Common
  {
    public static AppSettingsReader App = new AppSettingsReader();

    public static string GetClient()
    {
      if (string.IsNullOrEmpty(Common.App.GetValue("APP_CLIENT_NAME", typeof (string)).ToString()))
        return "SISTEMAS WEB - FABRICA INTERACTIVA(www.fabricai.com.br)";
      return Common.App.GetValue("APP_CLIENT_NAME", typeof (string)).ToString();
    }

    public static string SetDots(string valueString, int maxLenght)
    {
      if (valueString.Length > maxLenght + 3)
        return valueString.Substring(0, maxLenght - 3) + "...";
      return valueString;
    }

    public static string GetIpAddress()
    {
      IPAddress[] hostAddresses = Dns.GetHostAddresses(Dns.GetHostName());
      if (string.IsNullOrEmpty(hostAddresses[0].ToString()))
        return string.Empty;
      return hostAddresses[0].ToString();
    }

    public static bool ExistFile(string urlFile)
    {
      bool flag = true;
      if (urlFile == Common.App.GetValue("APP_NOIMAGE", typeof (string)).ToString())
        flag = false;
      if (urlFile == Common.App.GetValue("APP_NOIMAGE", typeof (string)).ToString().Replace("~/", ""))
        flag = false;
      if (string.IsNullOrEmpty(urlFile))
        flag = false;
      return flag;
    }

    public static bool RemoveFile(string url)
    {
      try
      {
        if (url != Common.App.GetValue("imageNullOrEmpty", typeof (string)).ToString())
        {
          FileInfo fileInfo = new FileInfo((AppDomain.CurrentDomain.BaseDirectory + url).Replace("~/", "").Replace("\\", "/"));
          if (fileInfo != null && fileInfo.Exists)
            fileInfo.Delete();
        }
        return true;
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        return false;
      }
    }

    public static string Encrypt(string clearText)
    {
      string password = "MAKV2SPBNI99212";
      byte[] bytes = Encoding.Unicode.GetBytes(clearText);
      using (Aes aes = Aes.Create())
      {
        Rfc2898DeriveBytes rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, new byte[13]{ (byte) 73, (byte) 118, (byte) 97, (byte) 110, (byte) 32, (byte) 77, (byte) 101, (byte) 100, (byte) 118, (byte) 101, (byte) 100, (byte) 101, (byte) 118 });
        aes.Key = rfc2898DeriveBytes.GetBytes(32);
        aes.IV = rfc2898DeriveBytes.GetBytes(16);
        using (MemoryStream memoryStream = new MemoryStream())
        {
          using (CryptoStream cryptoStream = new CryptoStream((Stream) memoryStream, aes.CreateEncryptor(), CryptoStreamMode.Write))
          {
            cryptoStream.Write(bytes, 0, bytes.Length);
            cryptoStream.Close();
          }
          clearText = Convert.ToBase64String(memoryStream.ToArray());
        }
      }
      return clearText;
    }

    public static string Decrypt(string cipherText)
    {
      string password = "MAKV2SPBNI99212";
      cipherText = cipherText.Replace(" ", "+");
      byte[] buffer = Convert.FromBase64String(cipherText);
      using (Aes aes = Aes.Create())
      {
        Rfc2898DeriveBytes rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, new byte[13]{ (byte) 73, (byte) 118, (byte) 97, (byte) 110, (byte) 32, (byte) 77, (byte) 101, (byte) 100, (byte) 118, (byte) 101, (byte) 100, (byte) 101, (byte) 118 });
        aes.Key = rfc2898DeriveBytes.GetBytes(32);
        aes.IV = rfc2898DeriveBytes.GetBytes(16);
        using (MemoryStream memoryStream = new MemoryStream())
        {
          using (CryptoStream cryptoStream = new CryptoStream((Stream) memoryStream, aes.CreateDecryptor(), CryptoStreamMode.Write))
          {
            cryptoStream.Write(buffer, 0, buffer.Length);
            cryptoStream.Close();
          }
          cipherText = Encoding.Unicode.GetString(memoryStream.ToArray());
        }
      }
      return cipherText;
    }

    public static string GerarSenhaAleatoria(int tamanho)
    {
      Random random = new Random();
      return new string(Enumerable.Repeat<string>("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", tamanho).Select<string, char>((Func<string, char>) (s => s[random.Next(s.Length)])).ToArray<char>());
    }

    private static bool SendSmtpMail(MailMessage objMail)
    {
      try
      {
        SmtpClient smtpClient = new SmtpClient();
        NetworkCredential networkCredential = new NetworkCredential();
        networkCredential.UserName = "naoresponder@deloitte.com";
        networkCredential.Password = "dtt.2017";
        smtpClient.Host = "appmail.atrame.deloitte.com";
        smtpClient.Port = 25;
        smtpClient.UseDefaultCredentials = true;
        smtpClient.Credentials = (ICredentialsByHost) networkCredential;
        smtpClient.EnableSsl = true;
        smtpClient.Send(objMail);
        return true;
      }
      catch
      {
        return false;
      }
    }

    public static void GetException(Exception erro)
    {
      string str = string.Empty;
      if (erro != null)
      {
        if (erro.InnerException != null && !string.IsNullOrEmpty(erro.InnerException.Message))
          str = str + "<br> InnerInnerException: " + erro.InnerException.Message;
        if (!string.IsNullOrEmpty(erro.Message))
          str = str + "<br> Exception.Message: " + erro.Message;
      }
      MailMessage objMail = new MailMessage();
      objMail.From = new MailAddress("naoresponder@deloitte.com");
      objMail.To.Add(new MailAddress("dooramos@gmail.com"));
      objMail.Subject = string.Format("SISGEDD - Relatório de erros em: {0}", (object) DateTime.Now.ToString("dd/MM/yyyy hh:mm"));
      objMail.Body = str;
      objMail.BodyEncoding = Encoding.UTF8;
      objMail.Priority = MailPriority.Normal;
      objMail.IsBodyHtml = true;
      objMail.SubjectEncoding = Encoding.UTF8;
      objMail.BodyEncoding = Encoding.UTF8;
      objMail.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(objMail.Body, (Encoding) null, "text/html"));
      Common.SendSmtpMail(objMail);
    }

    public static bool SendMail(string from, string to, string subject, string message)
    {
      try
      {
        MailMessage message1 = new MailMessage();
        message1.From = new MailAddress(from);
        message1.To.Add(new MailAddress(to));
        message1.Subject = subject;
        message1.Body = message;
        message1.BodyEncoding = Encoding.UTF8;
        message1.Priority = MailPriority.Normal;
        message1.IsBodyHtml = true;
        message1.SubjectEncoding = Encoding.UTF8;
        message1.BodyEncoding = Encoding.UTF8;
        AlternateView alternateViewFromString = AlternateView.CreateAlternateViewFromString(message1.Body, (Encoding) null, "text/html");
        message1.AlternateViews.Add(alternateViewFromString);
        SmtpClient smtpClient = new SmtpClient();
        NetworkCredential networkCredential = new NetworkCredential();
        networkCredential.UserName = "naoresponder@deloitte.com";
        networkCredential.Password = "dtt.2017";
        smtpClient.Host = "appmail.atrame.deloitte.com";
        smtpClient.Port = 25;
        smtpClient.UseDefaultCredentials = true;
        smtpClient.Credentials = (ICredentialsByHost) networkCredential;
        smtpClient.EnableSsl = true;
        smtpClient.Send(message1);
        return true;
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        return false;
      }
    }

    public static bool SendMailProtocolo(string contactMail, string contactName, string msg)
    {
      try
      {
        MailMessage objMail = new MailMessage();
        objMail.From = new MailAddress(Common.App.GetValue("APP_MAIL_FROM", typeof (string)).ToString());
        objMail.To.Add(new MailAddress(contactMail));
        if (Common.App.GetValue("APP_MAIL_AUDIENCE", typeof (string)).ToString() == "1")
          objMail.Bcc.Add(new MailAddress(Common.App.GetValue("APP_MAIL_AUDIENCE_MAIL", typeof (string)).ToString()));
        objMail.Subject = "Protocolo de Atendimento";
        objMail.Body = msg;
        objMail.BodyEncoding = Encoding.UTF8;
        objMail.Priority = MailPriority.Normal;
        objMail.IsBodyHtml = true;
        objMail.SubjectEncoding = Encoding.UTF8;
        objMail.BodyEncoding = Encoding.UTF8;
        AlternateView alternateViewFromString = AlternateView.CreateAlternateViewFromString(objMail.Body, (Encoding) null, "text/html");
        objMail.AlternateViews.Add(alternateViewFromString);
        return Common.SendSmtpMail(objMail);
      }
      catch (Exception ex)
      {
        Common.GetException(ex);
        return false;
      }
    }
  }
}
