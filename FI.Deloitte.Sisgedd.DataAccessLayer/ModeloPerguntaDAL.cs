﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.ModeloPerguntaDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class ModeloPerguntaDAL
  {
    public int add(ModeloPerguntaEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_modelopergunta";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      command.Parameters.Add((object) objDao.createParameter("@MODELO_ID", DbType.Int32, (object) objEntity.MODELO_ID));
      if (!string.IsNullOrEmpty(objEntity.PERGUNTA))
        command.Parameters.Add((object) objDao.createParameter("@PERGUNTA", DbType.String, (object) objEntity.PERGUNTA));
      if (objEntity.NUMERO > 0)
        command.Parameters.Add((object) objDao.createParameter("@NUMERO", DbType.Int32, (object) objEntity.NUMERO));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool update(ModeloPerguntaEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_modelopergunta";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      command.Parameters.Add((object) objDao.createParameter("@MODELO_PERGUNTA_ID", DbType.Int32, (object) objEntity.MODELO_PERGUNTA_ID));
      if (objEntity.MODELO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@MODELO_ID", DbType.Int32, (object) objEntity.MODELO_ID));
      if (!string.IsNullOrEmpty(objEntity.PERGUNTA))
        command.Parameters.Add((object) objDao.createParameter("@PERGUNTA", DbType.String, (object) objEntity.PERGUNTA));
      if (objEntity.NUMERO > 0)
        command.Parameters.Add((object) objDao.createParameter("@NUMERO", DbType.Int32, (object) objEntity.NUMERO));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(ModeloPerguntaEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_modelopergunta";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      if (objEntity.MODELO_PERGUNTA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@MODELO_PERGUNTA_ID", DbType.Int32, (object) objEntity.MODELO_PERGUNTA_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<ModeloPerguntaEntity> find(
      ModeloPerguntaEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_modelopergunta";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.MODELO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@MODELO_ID", DbType.Int32, (object) objEntity.MODELO_ID));
      if (!string.IsNullOrEmpty(objEntity.PERGUNTA))
        command.Parameters.Add((object) objDao.createParameter("@PERGUNTA", DbType.String, (object) objEntity.PERGUNTA));
      if (objEntity.NUMERO > 0)
        command.Parameters.Add((object) objDao.createParameter("@NUMERO", DbType.Int32, (object) objEntity.NUMERO));
      return this.serialize(objDao.ExecuteReader(command));
    }

    public ModeloPerguntaEntity findByPK(
      ModeloPerguntaEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_modelopergunta";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.MODELO_PERGUNTA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@MODELO_PERGUNTA_ID", DbType.Int32, (object) objEntity.MODELO_PERGUNTA_ID));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<ModeloPerguntaEntity>();
    }

    private List<ModeloPerguntaEntity> serialize(IDataReader objReader)
    {
      List<ModeloPerguntaEntity> modeloPerguntaEntityList = new List<ModeloPerguntaEntity>();
      while (objReader.Read())
      {
        ModeloPerguntaEntity modeloPerguntaEntity = new ModeloPerguntaEntity();
        if (objReader["MODELO_PERGUNTA_ID"] != DBNull.Value)
          modeloPerguntaEntity.MODELO_PERGUNTA_ID = Convert.ToInt32(objReader["MODELO_PERGUNTA_ID"]);
        if (objReader["MODELO_ID"] != DBNull.Value)
          modeloPerguntaEntity.MODELO_ID = Convert.ToInt32(objReader["MODELO_ID"]);
        if (objReader["PERGUNTA"] != DBNull.Value)
          modeloPerguntaEntity.PERGUNTA = objReader["PERGUNTA"].ToString();
        if (objReader["NUMERO"] != DBNull.Value)
          modeloPerguntaEntity.NUMERO = Convert.ToInt32(objReader["NUMERO"]);
        if (objReader["COUNTER"] != DBNull.Value)
          modeloPerguntaEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        modeloPerguntaEntityList.Add(modeloPerguntaEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return modeloPerguntaEntityList;
    }
  }
}
