﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.DataAccessLayerFactory
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using System;
using System.Configuration;
using System.Data;
using System.Data.Common;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class DataAccessLayerFactory
  {
    private DbProviderFactory objProvider;
    private string connectionString;
    private IDbConnection objConn;
    private IDbTransaction objTransaction;

    public DataAccessLayerFactory(string connectionStringValue)
    {
      this.objProvider = DbProviderFactories.GetFactory(new AppSettingsReader().GetValue("provider", typeof (string)).ToString());
      this.connectionString = connectionStringValue;
      this.objConn = (IDbConnection) this.objProvider.CreateConnection();
    }

    public DataAccessLayerFactory(string connectionStringValue, string providerValue)
    {
      AppSettingsReader appSettingsReader = new AppSettingsReader();
      this.objProvider = DbProviderFactories.GetFactory(providerValue);
      this.connectionString = connectionStringValue;
      this.objConn = (IDbConnection) this.objProvider.CreateConnection();
    }

    public DataAccessLayerFactory()
    {
      AppSettingsReader appSettingsReader = new AppSettingsReader();
      this.objProvider = DbProviderFactories.GetFactory(appSettingsReader.GetValue("provider", typeof (string)).ToString());
      this.connectionString = appSettingsReader.GetValue("conexao", typeof (string)).ToString();
      this.objConn = (IDbConnection) this.objProvider.CreateConnection();
    }

    public IDbConnection Connection
    {
      get
      {
        return this.objConn;
      }
    }

    public bool ExecuteNonQuery(IDbCommand cmd)
    {
      IDbCommand dbCommand = cmd;
      dbCommand.Connection = this.objConn;
      if (this.objTransaction != null)
        dbCommand.Transaction = this.objTransaction;
      return Convert.ToBoolean(dbCommand.ExecuteNonQuery());
    }

    public double ExecuteScalar(IDbCommand cmd)
    {
      IDbCommand dbCommand = cmd;
      dbCommand.Connection = this.objConn;
      if (this.objTransaction != null)
        dbCommand.Transaction = this.objTransaction;
      return Convert.ToDouble(dbCommand.ExecuteScalar());
    }

    public IDataReader ExecuteReader(IDbCommand cmd)
    {
      IDbCommand dbCommand = cmd;
      dbCommand.Connection = this.objConn;
      if (this.objTransaction != null)
        dbCommand.Transaction = this.objTransaction;
      return dbCommand.ExecuteReader();
    }

    public object startConnection()
    {
      if (string.IsNullOrEmpty(this.objConn.ConnectionString))
        this.objConn.ConnectionString = this.connectionString;
      if (this.objConn.State == ConnectionState.Closed)
        this.objConn.Open();
      return (object) this.objConn;
    }

    public void hasTransaction()
    {
      this.beginTransaction();
    }

    public void closeConnection()
    {
      if (this.objConn.State != ConnectionState.Open)
        return;
      this.objConn.Close();
    }

    private IDbTransaction beginTransaction()
    {
      this.objTransaction = this.objConn.BeginTransaction();
      return this.objTransaction;
    }

    public void commitTransaction()
    {
      if (this.objTransaction == null)
        return;
      this.objTransaction.Commit();
    }

    public void rollbackTransaction()
    {
      this.objTransaction.Rollback();
    }

    public IDbCommand createCommand()
    {
      return (IDbCommand) this.objProvider.CreateCommand();
    }

    public IDbDataParameter createParameter()
    {
      return (IDbDataParameter) this.objProvider.CreateParameter();
    }

    public IDbDataParameter createParameter(string name, DbType type, object value)
    {
      return this.createParameter(name, type, value, ParameterDirection.Input);
    }

    public IDbDataParameter createParameter(
      string name,
      DbType type,
      ParameterDirection direction)
    {
      return this.createParameter(name, type, (object) null, direction);
    }

    public IDbDataParameter createParameter(
      string name,
      DbType type,
      object value,
      ParameterDirection direction)
    {
      DbParameter parameter = this.objProvider.CreateParameter();
      parameter.ParameterName = name;
      parameter.DbType = type;
      parameter.Value = value;
      parameter.Direction = direction;
      return (IDbDataParameter) parameter;
    }

    public void Dispose()
    {
      if (this.objConn.State == ConnectionState.Open)
        this.objConn.Close();
      this.objConn.Dispose();
    }
  }
}
