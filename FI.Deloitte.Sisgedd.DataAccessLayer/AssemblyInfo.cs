﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("FI_DAO")]
[assembly: AssemblyDescription("A Fabrica Interactiva Desenvolvimento de Software possui todos os direitos de propriedade intelectual do Software. O Software é licenciado, não vendido.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Fabrica Interactiva Desenvolvimento de Software - www.fabricai.com.br")]
[assembly: AssemblyProduct("FI_DAO")]
[assembly: AssemblyCopyright("Copyright © Fabrica Interactiva 2014")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(true)]
[assembly: Guid("991a3bed-37d1-45fc-a2dd-fe39b4c7627e")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyVersion("1.0.0.0")]
