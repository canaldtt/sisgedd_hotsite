﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.EmailDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class EmailDAL
  {
    public int add(EmailEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_email";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      if (objEntity.DT_ENVIO > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_ENVIO", DbType.DateTime, (object) objEntity.DT_ENVIO));
      if (!string.IsNullOrEmpty(objEntity.MESSAGE_ID))
        command.Parameters.Add((object) objDao.createParameter("@MESSAGE_ID", DbType.String, (object) objEntity.MESSAGE_ID));
      if (!string.IsNullOrEmpty(objEntity.EMAIL_TO))
        command.Parameters.Add((object) objDao.createParameter("@EMAIL_TO", DbType.String, (object) objEntity.EMAIL_TO));
      if (!string.IsNullOrEmpty(objEntity.EMAIL_FROM))
        command.Parameters.Add((object) objDao.createParameter("@EMAIL_FROM", DbType.String, (object) objEntity.EMAIL_FROM));
      if (!string.IsNullOrEmpty(objEntity.TITULO))
        command.Parameters.Add((object) objDao.createParameter("@TITULO", DbType.String, (object) objEntity.TITULO));
      if (!string.IsNullOrEmpty(objEntity.CONTEUDO))
        command.Parameters.Add((object) objDao.createParameter("@CONTEUDO", DbType.String, (object) objEntity.CONTEUDO));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool update(EmailEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_email";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      if (objEntity.DT_ENVIO > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_ENVIO", DbType.DateTime, (object) objEntity.DT_ENVIO));
      if (objEntity.EMAIL_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@EMAIL_ID", DbType.Int32, (object) objEntity.EMAIL_ID));
      if (!string.IsNullOrEmpty(objEntity.MESSAGE_ID))
        command.Parameters.Add((object) objDao.createParameter("@MESSAGE_ID", DbType.String, (object) objEntity.MESSAGE_ID));
      if (!string.IsNullOrEmpty(objEntity.STATUS))
        command.Parameters.Add((object) objDao.createParameter("@STATUS", DbType.String, (object) objEntity.STATUS));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(EmailEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_email";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      if (objEntity.EMAIL_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@EMAIL_ID", DbType.Int32, (object) objEntity.EMAIL_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<EmailEntity> find(
      EmailEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_email";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.DT_ENVIO > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_ENVIO", DbType.DateTime, (object) objEntity.DT_ENVIO));
      if (objEntity.EMAIL_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@EMAIL_ID", DbType.Int32, (object) objEntity.EMAIL_ID));
      if (!string.IsNullOrEmpty(objEntity.MESSAGE_ID))
        command.Parameters.Add((object) objDao.createParameter("@MESSAGE_ID", DbType.String, (object) objEntity.MESSAGE_ID));
      if (!string.IsNullOrEmpty(objEntity.STATUS))
        command.Parameters.Add((object) objDao.createParameter("@STATUS", DbType.String, (object) objEntity.STATUS));
      return this.serialize(objDao.ExecuteReader(command));
    }

    public EmailEntity findByPK(EmailEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_email";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.EMAIL_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@EMAIL_ID", DbType.Int32, (object) objEntity.EMAIL_ID));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<EmailEntity>();
    }

    private List<EmailEntity> serialize(IDataReader objReader)
    {
      List<EmailEntity> emailEntityList = new List<EmailEntity>();
      while (objReader.Read())
      {
        EmailEntity emailEntity = new EmailEntity();
        if (objReader["DT_ENVIO"] != DBNull.Value)
          emailEntity.DT_ENVIO = Convert.ToDateTime(objReader["DT_ENVIO"]);
        if (objReader["EMAIL_ID"] != DBNull.Value)
          emailEntity.EMAIL_ID = Convert.ToInt32(objReader["EMAIL_ID"]);
        if (objReader["MESSAGE_ID"] != DBNull.Value)
          emailEntity.MESSAGE_ID = objReader["MESSAGE_ID"].ToString();
        if (objReader["STATUS"] != DBNull.Value)
          emailEntity.STATUS = objReader["STATUS"].ToString();
        if (objReader["COUNTER"] != DBNull.Value)
          emailEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        emailEntityList.Add(emailEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return emailEntityList;
    }
  }
}
