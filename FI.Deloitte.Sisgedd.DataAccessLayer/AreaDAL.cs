﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.AreaDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class AreaDAL
  {
    public int add(AreaEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_area";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      if (objEntity.AREA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@AREA_ID", DbType.Int32, (object) objEntity.AREA_ID));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (objEntity.PRODUTO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@PRODUTO_ID", DbType.Int32, (object) objEntity.PRODUTO_ID));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool update(AreaEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_area";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      if (objEntity.AREA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@AREA_ID", DbType.Int32, (object) objEntity.AREA_ID));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (objEntity.PRODUTO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@PRODUTO_ID", DbType.Int32, (object) objEntity.PRODUTO_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(AreaEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_area";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      if (objEntity.AREA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@AREA_ID", DbType.Int32, (object) objEntity.AREA_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<AreaEntity> find(
      AreaEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_area";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.AREA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@AREA_ID", DbType.Int32, (object) objEntity.AREA_ID));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (objEntity.PRODUTO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@PRODUTO_ID", DbType.Int32, (object) objEntity.PRODUTO_ID));
      return this.serialize(objDao.ExecuteReader(command));
    }

    public AreaEntity findByPK(AreaEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_area";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.AREA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@AREA_ID", DbType.Int32, (object) objEntity.AREA_ID));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<AreaEntity>();
    }

    private List<AreaEntity> serialize(IDataReader objReader)
    {
      List<AreaEntity> areaEntityList = new List<AreaEntity>();
      while (objReader.Read())
      {
        AreaEntity areaEntity = new AreaEntity();
        if (objReader["AREA_ID"] != DBNull.Value)
          areaEntity.AREA_ID = Convert.ToInt32(objReader["AREA_ID"]);
        if (objReader["NOME"] != DBNull.Value)
          areaEntity.NOME = objReader["NOME"].ToString();
        if (objReader["PRODUTO_ID"] != DBNull.Value)
          areaEntity.PRODUTO_ID = Convert.ToInt32(objReader["PRODUTO_ID"]);
        if (objReader["COUNTER"] != DBNull.Value)
          areaEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        areaEntityList.Add(areaEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return areaEntityList;
    }
  }
}
