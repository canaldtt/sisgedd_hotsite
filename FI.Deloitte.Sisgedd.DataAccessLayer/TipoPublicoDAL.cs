﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.TipoPublicoDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class TipoPublicoDAL
  {
    public int add(TipoPublicoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tipopublico";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool update(TipoPublicoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tipopublico";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (objEntity.TIPOPUBLICO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TIPOPUBLICO_ID", DbType.Int32, (object) objEntity.TIPOPUBLICO_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(TipoPublicoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tipopublico";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      if (objEntity.TIPOPUBLICO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TIPOPUBLICO_ID", DbType.Int32, (object) objEntity.TIPOPUBLICO_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<TipoPublicoEntity> find(
      TipoPublicoEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tipopublico";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (objEntity.TIPOPUBLICO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TIPOPUBLICO_ID", DbType.Int32, (object) objEntity.TIPOPUBLICO_ID));
      return this.serialize(objDao.ExecuteReader(command));
    }

    public TipoPublicoEntity findByPK(
      TipoPublicoEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tipopublico";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.TIPOPUBLICO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TIPOPUBLICO_ID", DbType.Int32, (object) objEntity.TIPOPUBLICO_ID));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<TipoPublicoEntity>();
    }

    private List<TipoPublicoEntity> serialize(IDataReader objReader)
    {
      List<TipoPublicoEntity> tipoPublicoEntityList = new List<TipoPublicoEntity>();
      while (objReader.Read())
      {
        TipoPublicoEntity tipoPublicoEntity = new TipoPublicoEntity();
        if (objReader["NOME"] != DBNull.Value)
          tipoPublicoEntity.NOME = objReader["NOME"].ToString();
        if (objReader["TIPOPUBLICO_ID"] != DBNull.Value)
          tipoPublicoEntity.TIPOPUBLICO_ID = Convert.ToInt32(objReader["TIPOPUBLICO_ID"]);
        if (objReader["COUNTER"] != DBNull.Value)
          tipoPublicoEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        tipoPublicoEntityList.Add(tipoPublicoEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return tipoPublicoEntityList;
    }

    public List<TipoPublicoEntity> findByEmpresa(
      EmpresaEntity objEntity,
      string _idioma,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tipopublicoempresa";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (!string.IsNullOrEmpty(_idioma))
        command.Parameters.Add((object) objDao.createParameter("@IDIOMA", DbType.String, (object) _idioma));
      if (objEntity.EMPRESA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object) objEntity.EMPRESA_ID));
      return this.serialize(objDao.ExecuteReader(command));
    }
  }
}
