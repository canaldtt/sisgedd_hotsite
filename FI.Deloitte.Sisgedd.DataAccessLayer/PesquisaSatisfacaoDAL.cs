﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.PesquisaSatisfacaoDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class PesquisaSatisfacaoDAL
  {
    public int add(PesquisaSatisfacaoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_pesquisasatisfacao";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      if (!string.IsNullOrEmpty(objEntity.NMR_PROTOCOLO))
        command.Parameters.Add((object) objDao.createParameter("@NMR_PROTOCOLO", DbType.String, (object) objEntity.NMR_PROTOCOLO));
      if (!string.IsNullOrEmpty(objEntity.PG1))
        command.Parameters.Add((object) objDao.createParameter("@PG1", DbType.String, (object) objEntity.PG1));
      if (!string.IsNullOrEmpty(objEntity.PG12A))
        command.Parameters.Add((object) objDao.createParameter("@PG12A", DbType.String, (object) objEntity.PG12A));
      if (!string.IsNullOrEmpty(objEntity.PG1A))
        command.Parameters.Add((object) objDao.createParameter("@PG1A", DbType.String, (object) objEntity.PG1A));
      if (!string.IsNullOrEmpty(objEntity.PG1B))
        command.Parameters.Add((object) objDao.createParameter("@PG1B", DbType.String, (object) objEntity.PG1B));
      if (!string.IsNullOrEmpty(objEntity.PG21))
        command.Parameters.Add((object) objDao.createParameter("@PG21", DbType.String, (object) objEntity.PG21));
      if (!string.IsNullOrEmpty(objEntity.PG21A))
        command.Parameters.Add((object) objDao.createParameter("@PG21A", DbType.String, (object) objEntity.PG21A));
      if (!string.IsNullOrEmpty(objEntity.PG3))
        command.Parameters.Add((object) objDao.createParameter("@PG3", DbType.String, (object) objEntity.PG3));
      if (!string.IsNullOrEmpty(objEntity.PG4))
        command.Parameters.Add((object) objDao.createParameter("@PG4", DbType.String, (object) objEntity.PG4));
      if (!string.IsNullOrEmpty(objEntity.PG41))
        command.Parameters.Add((object) objDao.createParameter("@PG41", DbType.String, (object) objEntity.PG41));
      if (!string.IsNullOrEmpty(objEntity.PG5))
        command.Parameters.Add((object) objDao.createParameter("@PG5", DbType.String, (object) objEntity.PG5));
      if (!string.IsNullOrEmpty(objEntity.PG51))
        command.Parameters.Add((object) objDao.createParameter("@PG51", DbType.String, (object) objEntity.PG51));
      if (!string.IsNullOrEmpty(objEntity.PG6))
        command.Parameters.Add((object) objDao.createParameter("@PG6", DbType.String, (object) objEntity.PG6));
      if (!string.IsNullOrEmpty(objEntity.PG61))
        command.Parameters.Add((object) objDao.createParameter("@PG61", DbType.String, (object) objEntity.PG61));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool update(PesquisaSatisfacaoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_pesquisasatisfacao";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      command.Parameters.Add((object) objDao.createParameter("@PESQUISA_ID", DbType.Int32, (object) objEntity.PESQUISA_ID));
      if (!string.IsNullOrEmpty(objEntity.NMR_PROTOCOLO))
        command.Parameters.Add((object) objDao.createParameter("@NMR_PROTOCOLO", DbType.String, (object) objEntity.NMR_PROTOCOLO));
      if (!string.IsNullOrEmpty(objEntity.PG1))
        command.Parameters.Add((object) objDao.createParameter("@PG1", DbType.String, (object) objEntity.PG1));
      if (!string.IsNullOrEmpty(objEntity.PG12A))
        command.Parameters.Add((object) objDao.createParameter("@PG12A", DbType.String, (object) objEntity.PG12A));
      if (!string.IsNullOrEmpty(objEntity.PG1A))
        command.Parameters.Add((object) objDao.createParameter("@PG1A", DbType.String, (object) objEntity.PG1A));
      if (!string.IsNullOrEmpty(objEntity.PG1B))
        command.Parameters.Add((object) objDao.createParameter("@PG1B", DbType.String, (object) objEntity.PG1B));
      if (!string.IsNullOrEmpty(objEntity.PG21))
        command.Parameters.Add((object) objDao.createParameter("@PG21", DbType.String, (object) objEntity.PG21));
      if (!string.IsNullOrEmpty(objEntity.PG21A))
        command.Parameters.Add((object) objDao.createParameter("@PG21A", DbType.String, (object) objEntity.PG21A));
      if (!string.IsNullOrEmpty(objEntity.PG3))
        command.Parameters.Add((object) objDao.createParameter("@PG3", DbType.String, (object) objEntity.PG3));
      if (!string.IsNullOrEmpty(objEntity.PG4))
        command.Parameters.Add((object) objDao.createParameter("@PG4", DbType.String, (object) objEntity.PG4));
      if (!string.IsNullOrEmpty(objEntity.PG41))
        command.Parameters.Add((object) objDao.createParameter("@PG41", DbType.String, (object) objEntity.PG41));
      if (!string.IsNullOrEmpty(objEntity.PG5))
        command.Parameters.Add((object) objDao.createParameter("@PG5", DbType.String, (object) objEntity.PG5));
      if (!string.IsNullOrEmpty(objEntity.PG51))
        command.Parameters.Add((object) objDao.createParameter("@PG51", DbType.String, (object) objEntity.PG51));
      if (!string.IsNullOrEmpty(objEntity.PG6))
        command.Parameters.Add((object) objDao.createParameter("@PG6", DbType.String, (object) objEntity.PG6));
      if (!string.IsNullOrEmpty(objEntity.PG61))
        command.Parameters.Add((object) objDao.createParameter("@PG61", DbType.String, (object) objEntity.PG61));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(PesquisaSatisfacaoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_pesquisasatisfacao";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      if (objEntity.PESQUISA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@PESQUISA_ID", DbType.Int32, (object) objEntity.PESQUISA_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<PesquisaSatisfacaoEntity> find(
      PesquisaSatisfacaoEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_pesquisasatisfacao";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.PESQUISA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@PESQUISA_ID", DbType.Int32, (object) objEntity.PESQUISA_ID));
      if (!string.IsNullOrEmpty(objEntity.NMR_PROTOCOLO))
        command.Parameters.Add((object) objDao.createParameter("@NMR_PROTOCOLO", DbType.String, (object) objEntity.NMR_PROTOCOLO));
      return this.serialize(objDao.ExecuteReader(command));
    }

    public PesquisaSatisfacaoEntity findByPK(
      PesquisaSatisfacaoEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_pesquisasatisfacao";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.PESQUISA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@PESQUISA_ID", DbType.Int32, (object) objEntity.PESQUISA_ID));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<PesquisaSatisfacaoEntity>();
    }

    private List<PesquisaSatisfacaoEntity> serialize(
      IDataReader objReader)
    {
      List<PesquisaSatisfacaoEntity> satisfacaoEntityList = new List<PesquisaSatisfacaoEntity>();
      while (objReader.Read())
      {
        PesquisaSatisfacaoEntity satisfacaoEntity = new PesquisaSatisfacaoEntity();
        if (objReader["NMR_PROTOCOLO"] != DBNull.Value)
          satisfacaoEntity.NMR_PROTOCOLO = objReader["NMR_PROTOCOLO"].ToString();
        if (objReader["PESQUISA_ID"] != DBNull.Value)
          satisfacaoEntity.PESQUISA_ID = Convert.ToInt32(objReader["PESQUISA_ID"]);
        if (objReader["PG1"] != DBNull.Value)
          satisfacaoEntity.PG1 = objReader["PG1"].ToString();
        if (objReader["PG12A"] != DBNull.Value)
          satisfacaoEntity.PG12A = objReader["PG12A"].ToString();
        if (objReader["PG1A"] != DBNull.Value)
          satisfacaoEntity.PG1A = objReader["PG1A"].ToString();
        if (objReader["PG1B"] != DBNull.Value)
          satisfacaoEntity.PG1B = objReader["PG1B"].ToString();
        if (objReader["PG21"] != DBNull.Value)
          satisfacaoEntity.PG21 = objReader["PG21"].ToString();
        if (objReader["PG21A"] != DBNull.Value)
          satisfacaoEntity.PG21A = objReader["PG21A"].ToString();
        if (objReader["PG3"] != DBNull.Value)
          satisfacaoEntity.PG3 = objReader["PG3"].ToString();
        if (objReader["PG4"] != DBNull.Value)
          satisfacaoEntity.PG4 = objReader["PG4"].ToString();
        if (objReader["PG41"] != DBNull.Value)
          satisfacaoEntity.PG41 = objReader["PG41"].ToString();
        if (objReader["PG5"] != DBNull.Value)
          satisfacaoEntity.PG5 = objReader["PG5"].ToString();
        if (objReader["PG51"] != DBNull.Value)
          satisfacaoEntity.PG51 = objReader["PG51"].ToString();
        if (objReader["PG6"] != DBNull.Value)
          satisfacaoEntity.PG6 = objReader["PG6"].ToString();
        if (objReader["PG61"] != DBNull.Value)
          satisfacaoEntity.PG61 = objReader["PG61"].ToString();
        if (objReader["COUNTER"] != DBNull.Value)
          satisfacaoEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        satisfacaoEntityList.Add(satisfacaoEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return satisfacaoEntityList;
    }
  }
}
