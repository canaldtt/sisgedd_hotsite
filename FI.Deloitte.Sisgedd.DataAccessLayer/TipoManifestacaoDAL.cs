﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.TipoManifestacaoDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class TipoManifestacaoDAL
  {
    public int add(TipoManifestacaoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tipoManifestacao";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool update(TipoManifestacaoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tipoManifestacao";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (objEntity.TIPOMANIFESTACAO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TipoManifestacao_ID", DbType.Int32, (object) objEntity.TIPOMANIFESTACAO_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(TipoManifestacaoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tipoManifestacao";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      if (objEntity.TIPOMANIFESTACAO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TipoManifestacao_ID", DbType.Int32, (object) objEntity.TIPOMANIFESTACAO_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<TipoManifestacaoEntity> find(
      TipoManifestacaoEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tipoManifestacao";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (objEntity.TIPOMANIFESTACAO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TipoManifestacao_ID", DbType.Int32, (object) objEntity.TIPOMANIFESTACAO_ID));
      return this.serialize(objDao.ExecuteReader(command));
    }

    public TipoManifestacaoEntity findByPK(
      TipoManifestacaoEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tipoManifestacao";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.TIPOMANIFESTACAO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TipoManifestacao_ID", DbType.Int32, (object) objEntity.TIPOMANIFESTACAO_ID));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<TipoManifestacaoEntity>();
    }

    private List<TipoManifestacaoEntity> serialize(IDataReader objReader)
    {
      List<TipoManifestacaoEntity> manifestacaoEntityList = new List<TipoManifestacaoEntity>();
      while (objReader.Read())
      {
        TipoManifestacaoEntity manifestacaoEntity = new TipoManifestacaoEntity();
        if (objReader["NOME"] != DBNull.Value)
          manifestacaoEntity.NOME = objReader["NOME"].ToString();
        if (objReader["TipoManifestacao_ID"] != DBNull.Value)
          manifestacaoEntity.TIPOMANIFESTACAO_ID = Convert.ToInt32(objReader["TipoManifestacao_ID"]);
        if (objReader["COUNTER"] != DBNull.Value)
          manifestacaoEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        manifestacaoEntityList.Add(manifestacaoEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return manifestacaoEntityList;
    }
  }
}
