﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.SupportDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class SupportDAL
  {
    public int add(SupportEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_support";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      if (!string.IsNullOrEmpty(objEntity.TITLE))
        command.Parameters.Add((object) objDao.createParameter("@TITLE", DbType.String, (object) objEntity.TITLE));
      if (!string.IsNullOrEmpty(objEntity.COMMENT))
        command.Parameters.Add((object) objDao.createParameter("@COMMENT", DbType.String, (object) objEntity.COMMENT));
      if (!string.IsNullOrEmpty(objEntity.DESCRIPTION))
        command.Parameters.Add((object) objDao.createParameter("@DESCRIPTION", DbType.String, (object) objEntity.DESCRIPTION));
      if (objEntity.POSTEDDATE > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@POSTEDDATE", DbType.DateTime, (object) objEntity.POSTEDDATE));
      if (objEntity.CLOSEDDATE > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@CLOSEDDATE", DbType.DateTime, (object) objEntity.CLOSEDDATE));
      if (!string.IsNullOrEmpty(objEntity.STATUS))
        command.Parameters.Add((object) objDao.createParameter("@STATUS", DbType.String, (object) objEntity.STATUS));
      if (objEntity.SUPPORT_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@SUPPORT_ID", DbType.Int32, (object) objEntity.SUPPORT_ID));
      if (objEntity.USER_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@USER_ID", DbType.Int32, (object) objEntity.USER_ID));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool update(SupportEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_support";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      if (!string.IsNullOrEmpty(objEntity.TITLE))
        command.Parameters.Add((object) objDao.createParameter("@TITLE", DbType.String, (object) objEntity.TITLE));
      if (!string.IsNullOrEmpty(objEntity.COMMENT))
        command.Parameters.Add((object) objDao.createParameter("@COMMENT", DbType.String, (object) objEntity.COMMENT));
      if (!string.IsNullOrEmpty(objEntity.DESCRIPTION))
        command.Parameters.Add((object) objDao.createParameter("@DESCRIPTION", DbType.String, (object) objEntity.DESCRIPTION));
      if (objEntity.POSTEDDATE > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@POSTEDDATE", DbType.DateTime, (object) objEntity.POSTEDDATE));
      if (objEntity.CLOSEDDATE > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@CLOSEDDATE", DbType.DateTime, (object) objEntity.CLOSEDDATE));
      if (!string.IsNullOrEmpty(objEntity.STATUS))
        command.Parameters.Add((object) objDao.createParameter("@STATUS", DbType.String, (object) objEntity.STATUS));
      if (objEntity.SUPPORT_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@SUPPORT_ID", DbType.Int32, (object) objEntity.SUPPORT_ID));
      if (objEntity.USER_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@USER_ID", DbType.Int32, (object) objEntity.USER_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(SupportEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_support";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<SupportEntity> find(
      SupportEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_support";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.String, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.String, (object) objEntity.PAGESIZE));
      if (!string.IsNullOrEmpty(objEntity.TITLE))
        command.Parameters.Add((object) objDao.createParameter("@TITLE", DbType.String, (object) objEntity.TITLE));
      if (!string.IsNullOrEmpty(objEntity.COMMENT))
        command.Parameters.Add((object) objDao.createParameter("@COMMENT", DbType.String, (object) objEntity.COMMENT));
      if (!string.IsNullOrEmpty(objEntity.DESCRIPTION))
        command.Parameters.Add((object) objDao.createParameter("@DESCRIPTION", DbType.String, (object) objEntity.DESCRIPTION));
      if (objEntity.POSTEDDATE > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@POSTEDDATE", DbType.DateTime, (object) objEntity.POSTEDDATE));
      if (objEntity.CLOSEDDATE > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@CLOSEDDATE", DbType.DateTime, (object) objEntity.CLOSEDDATE));
      if (!string.IsNullOrEmpty(objEntity.STATUS))
        command.Parameters.Add((object) objDao.createParameter("@STATUS", DbType.String, (object) objEntity.STATUS));
      if (objEntity.SUPPORT_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@SUPPORT_ID", DbType.Int32, (object) objEntity.SUPPORT_ID));
      if (objEntity.USER_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@USER_ID", DbType.Int32, (object) objEntity.USER_ID));
      return this.serialize(objDao.ExecuteReader(command));
    }

    public SupportEntity findByPK(
      SupportEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_support";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@ID", DbType.Int32, (object) objEntity.SUPPORT_ID));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<SupportEntity>();
    }

    private List<SupportEntity> serialize(IDataReader objReader)
    {
      List<SupportEntity> supportEntityList = new List<SupportEntity>();
      while (objReader.Read())
      {
        SupportEntity supportEntity = new SupportEntity();
        if (objReader["TITLE"] != DBNull.Value)
          supportEntity.TITLE = objReader["TITLE"].ToString();
        if (objReader["COMMENT"] != DBNull.Value)
          supportEntity.COMMENT = objReader["COMMENT"].ToString();
        if (objReader["DESCRIPTION"] != DBNull.Value)
          supportEntity.DESCRIPTION = objReader["DESCRIPTION"].ToString();
        if (objReader["STATUS"] != DBNull.Value)
          supportEntity.STATUS = objReader["STATUS"].ToString();
        if (objReader["CLOSEDDATE"] != DBNull.Value)
          supportEntity.CLOSEDDATE = Convert.ToDateTime(objReader["CLOSEDDATE"]);
        if (objReader["POSTEDDATE"] != DBNull.Value)
          supportEntity.POSTEDDATE = Convert.ToDateTime(objReader["POSTEDDATE"]);
        if (objReader["USER_ID"] != DBNull.Value)
          supportEntity.USER_ID = Convert.ToInt32(objReader["USER_ID"]);
        if (objReader["SUPPORT_ID"] != DBNull.Value)
          supportEntity.SUPPORT_ID = Convert.ToInt32(objReader["SUPPORT_ID"]);
        if (objReader["COUNTER"] != DBNull.Value)
          supportEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        supportEntityList.Add(supportEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return supportEntityList;
    }
  }
}
