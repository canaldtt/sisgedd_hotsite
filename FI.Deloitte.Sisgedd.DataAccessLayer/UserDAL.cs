﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.UserDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class UserDAL
  {
    public int add(UserEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_user";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      if (!string.IsNullOrEmpty(objEntity.LOGIN))
        command.Parameters.Add((object) objDao.createParameter("@LOGIN", DbType.String, (object) objEntity.LOGIN));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (!string.IsNullOrEmpty(objEntity.SENHA))
        command.Parameters.Add((object) objDao.createParameter("@SENHA", DbType.String, (object) objEntity.SENHA));
      if (!string.IsNullOrEmpty(objEntity.EMAIL))
        command.Parameters.Add((object) objDao.createParameter("@EMAIL", DbType.String, (object) objEntity.EMAIL));
      if (!string.IsNullOrEmpty(objEntity.PERFIL))
        command.Parameters.Add((object) objDao.createParameter("@PERFIL", DbType.String, (object) objEntity.PERFIL));
      if (!string.IsNullOrEmpty(objEntity.FLG_STATUS))
        command.Parameters.Add((object) objDao.createParameter("@FLG_STATUS", DbType.String, (object) objEntity.FLG_STATUS));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool update(UserEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_user";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      if (!string.IsNullOrEmpty(objEntity.LOGIN))
        command.Parameters.Add((object) objDao.createParameter("@LOGIN", DbType.String, (object) objEntity.LOGIN));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (!string.IsNullOrEmpty(objEntity.SENHA))
        command.Parameters.Add((object) objDao.createParameter("@SENHA", DbType.String, (object) objEntity.SENHA));
      if (objEntity.ACESSO > 0)
        command.Parameters.Add((object) objDao.createParameter("@ACESSO", DbType.Int32, (object) objEntity.ACESSO));
      if (objEntity.DT_ULTIMO_ACESSO != DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_ULTIMO_ACESSO", DbType.DateTime, (object) objEntity.DT_ULTIMO_ACESSO));
      if (!string.IsNullOrEmpty(objEntity.EMAIL))
        command.Parameters.Add((object) objDao.createParameter("@EMAIL", DbType.String, (object) objEntity.EMAIL));
      if (!string.IsNullOrEmpty(objEntity.PERFIL))
        command.Parameters.Add((object) objDao.createParameter("@PERFIL", DbType.String, (object) objEntity.PERFIL));
      if (!string.IsNullOrEmpty(objEntity.FLG_STATUS))
        command.Parameters.Add((object) objDao.createParameter("@FLG_STATUS", DbType.String, (object) objEntity.FLG_STATUS));
      command.Parameters.Add((object) objDao.createParameter("@USER_ID", DbType.Int32, (object) objEntity.USER_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(UserEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_user";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      command.Parameters.Add((object) objDao.createParameter("@USER_ID", DbType.Int32, (object) objEntity.USER_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<UserEntity> find(
      UserEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_user";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.String, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.String, (object) objEntity.PAGESIZE));
      if (!string.IsNullOrEmpty(objEntity.LOGIN))
        command.Parameters.Add((object) objDao.createParameter("@LOGIN", DbType.String, (object) objEntity.LOGIN));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (!string.IsNullOrEmpty(objEntity.SENHA))
        command.Parameters.Add((object) objDao.createParameter("@SENHA", DbType.String, (object) objEntity.SENHA));
      if (!string.IsNullOrEmpty(objEntity.UF))
        command.Parameters.Add((object) objDao.createParameter("@UF", DbType.String, (object) objEntity.UF));
      if (!string.IsNullOrEmpty(objEntity.EMAIL))
        command.Parameters.Add((object) objDao.createParameter("@EMAIL", DbType.String, (object) objEntity.EMAIL));
      if (!string.IsNullOrEmpty(objEntity.FLG_STATUS))
        command.Parameters.Add((object) objDao.createParameter("@FLG_STATUS", DbType.String, (object) objEntity.FLG_STATUS));
      return this.serialize(objDao.ExecuteReader(command));
    }

    public UserEntity autenticate(UserEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      List<UserEntity> userEntityList = new List<UserEntity>();
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_user";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      if (!string.IsNullOrEmpty(objEntity.LOGIN))
        command.Parameters.Add((object) objDao.createParameter("@LOGIN", DbType.String, (object) objEntity.LOGIN));
      if (!string.IsNullOrEmpty(objEntity.EMAIL))
        command.Parameters.Add((object) objDao.createParameter("@EMAIL", DbType.String, (object) objEntity.EMAIL));
      command.Parameters.Add((object) objDao.createParameter("@SENHA", DbType.String, (object) objEntity.SENHA));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<UserEntity>();
    }

    public UserEntity autenticateByEmail(
      UserEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      List<UserEntity> userEntityList = new List<UserEntity>();
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_user";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@EMAIL", DbType.String, (object) objEntity.EMAIL));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<UserEntity>();
    }

    public UserEntity findByPK(UserEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_user";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@USER_ID", DbType.Int32, (object) objEntity.USER_ID));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<UserEntity>();
    }

    private List<UserEntity> serialize(IDataReader objReader)
    {
      List<UserEntity> userEntityList = new List<UserEntity>();
      while (objReader.Read())
      {
        UserEntity userEntity = new UserEntity();
        if (objReader["USER_ID"] != DBNull.Value)
          userEntity.USER_ID = Convert.ToInt32(objReader["USER_ID"]);
        if (objReader["NOME"] != DBNull.Value)
          userEntity.NOME = objReader["NOME"].ToString();
        if (objReader["LOGIN"] != DBNull.Value)
          userEntity.LOGIN = objReader["LOGIN"].ToString();
        if (objReader["SENHA"] != DBNull.Value)
          userEntity.SENHA = objReader["SENHA"].ToString();
        if (objReader["DT_ULTIMO_ACESSO"] != DBNull.Value)
          userEntity.DT_ULTIMO_ACESSO = Convert.ToDateTime(objReader["DT_ULTIMO_ACESSO"]);
        if (objReader["ACESSO"] != DBNull.Value)
          userEntity.ACESSO = Convert.ToInt32(objReader["ACESSO"]);
        if (objReader["COUNTER"] != DBNull.Value)
          userEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        if (objReader["EMAIL"] != DBNull.Value)
          userEntity.EMAIL = objReader["EMAIL"].ToString();
        if (objReader["PERFIL"] != DBNull.Value)
          userEntity.PERFIL = objReader["PERFIL"].ToString();
        if (objReader["FLG_STATUS"] != DBNull.Value)
          userEntity.FLG_STATUS = objReader["FLG_STATUS"].ToString();
        userEntityList.Add(userEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return userEntityList;
    }
  }
}
