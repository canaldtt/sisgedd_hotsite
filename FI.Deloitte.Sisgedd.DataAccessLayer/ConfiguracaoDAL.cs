﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.ConfiguracaoDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class ConfiguracaoDAL
  {
    public int add(ConfiguracaoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_configuracao";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      if (objEntity.CONFIGURACAO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@CONFIGURACAO_ID", DbType.Int32, (object) objEntity.CONFIGURACAO_ID));
      if (!string.IsNullOrEmpty(objEntity.CRITICIDADE))
        command.Parameters.Add((object) objDao.createParameter("@CRITICIDADE", DbType.String, (object) objEntity.CRITICIDADE));
      if (!string.IsNullOrEmpty(objEntity.INSTRUCAO))
        command.Parameters.Add((object) objDao.createParameter("@INSTRUCAO", DbType.String, (object) objEntity.INSTRUCAO));
      if (!string.IsNullOrEmpty(objEntity.TIPO_MANIFESTACAO))
        command.Parameters.Add((object) objDao.createParameter("@TIPO_MANIFESTACAO", DbType.String, (object) objEntity.TIPO_MANIFESTACAO));
      if (objEntity.EMPRESA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object) objEntity.EMPRESA_ID));
      if (objEntity.NATUREZA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@NATUREZA_ID", DbType.Int32, (object) objEntity.NATUREZA_ID));
      if (objEntity.PRAZO > 0)
        command.Parameters.Add((object) objDao.createParameter("@PRAZO", DbType.Int32, (object) objEntity.PRAZO));
      if (objEntity.TIPONATUREZA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TIPONATUREZA_ID", DbType.Int32, (object) objEntity.TIPONATUREZA_ID));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public int gerar(ConfiguracaoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_configuracaoempresa";
      command.Parameters.Add((object) objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object) objEntity.EMPRESA_ID));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool update(ConfiguracaoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_configuracao";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      if (objEntity.CONFIGURACAO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@CONFIGURACAO_ID", DbType.Int32, (object) objEntity.CONFIGURACAO_ID));
      if (!string.IsNullOrEmpty(objEntity.CRITICIDADE))
        command.Parameters.Add((object) objDao.createParameter("@CRITICIDADE", DbType.String, (object) objEntity.CRITICIDADE));
      if (!string.IsNullOrEmpty(objEntity.INSTRUCAO))
        command.Parameters.Add((object) objDao.createParameter("@INSTRUCAO", DbType.String, (object) objEntity.INSTRUCAO));
      if (!string.IsNullOrEmpty(objEntity.TIPO_MANIFESTACAO))
        command.Parameters.Add((object) objDao.createParameter("@TIPO_MANIFESTACAO", DbType.String, (object) objEntity.TIPO_MANIFESTACAO));
      if (objEntity.EMPRESA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object) objEntity.EMPRESA_ID));
      if (objEntity.NATUREZA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@NATUREZA_ID", DbType.Int32, (object) objEntity.NATUREZA_ID));
      if (objEntity.PRAZO > 0)
        command.Parameters.Add((object) objDao.createParameter("@PRAZO", DbType.Int32, (object) objEntity.PRAZO));
      if (objEntity.TIPONATUREZA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TIPONATUREZA_ID", DbType.Int32, (object) objEntity.TIPONATUREZA_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(ConfiguracaoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_configuracao";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      if (objEntity.CONFIGURACAO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@CONFIGURACAO_ID", DbType.Int32, (object) objEntity.CONFIGURACAO_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<ConfiguracaoEntity> find(
      ConfiguracaoEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_configuracao";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.CONFIGURACAO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@CONFIGURACAO_ID", DbType.Int32, (object) objEntity.CONFIGURACAO_ID));
      if (!string.IsNullOrEmpty(objEntity.CRITICIDADE))
        command.Parameters.Add((object) objDao.createParameter("@CRITICIDADE", DbType.String, (object) objEntity.CRITICIDADE));
      if (!string.IsNullOrEmpty(objEntity.TIPO_MANIFESTACAO))
        command.Parameters.Add((object) objDao.createParameter("@TIPO_MANIFESTACAO", DbType.String, (object) objEntity.TIPO_MANIFESTACAO));
      if (objEntity.EMPRESA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object) objEntity.EMPRESA_ID));
      if (objEntity.NATUREZA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@NATUREZA_ID", DbType.Int32, (object) objEntity.NATUREZA_ID));
      if (objEntity.PRAZO > 0)
        command.Parameters.Add((object) objDao.createParameter("@PRAZO", DbType.Int32, (object) objEntity.PRAZO));
      if (objEntity.TIPONATUREZA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TIPONATUREZA_ID", DbType.Int32, (object) objEntity.TIPONATUREZA_ID));
      return this.serialize(objDao.ExecuteReader(command));
    }

    public ConfiguracaoEntity findByPK(
      ConfiguracaoEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_configuracao";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.CONFIGURACAO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@CONFIGURACAO_ID", DbType.Int32, (object) objEntity.CONFIGURACAO_ID));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<ConfiguracaoEntity>();
    }

    private List<ConfiguracaoEntity> serialize(IDataReader objReader)
    {
      List<ConfiguracaoEntity> configuracaoEntityList = new List<ConfiguracaoEntity>();
      while (objReader.Read())
      {
        ConfiguracaoEntity configuracaoEntity = new ConfiguracaoEntity();
        if (objReader["CONFIGURACAO_ID"] != DBNull.Value)
          configuracaoEntity.CONFIGURACAO_ID = Convert.ToInt32(objReader["CONFIGURACAO_ID"]);
        if (objReader["CRITICIDADE"] != DBNull.Value)
          configuracaoEntity.CRITICIDADE = objReader["CRITICIDADE"].ToString();
        if (objReader["INSTRUCAO"] != DBNull.Value)
          configuracaoEntity.INSTRUCAO = objReader["INSTRUCAO"].ToString();
        if (objReader["TIPO_MANIFESTACAO"] != DBNull.Value)
          configuracaoEntity.TIPO_MANIFESTACAO = objReader["TIPO_MANIFESTACAO"].ToString();
        if (objReader["EMPRESA_ID"] != DBNull.Value)
          configuracaoEntity.EMPRESA_ID = Convert.ToInt32(objReader["EMPRESA_ID"]);
        if (objReader["NATUREZA_ID"] != DBNull.Value)
          configuracaoEntity.NATUREZA_ID = Convert.ToInt32(objReader["NATUREZA_ID"]);
        if (objReader["PRAZO"] != DBNull.Value)
          configuracaoEntity.PRAZO = Convert.ToInt32(objReader["PRAZO"]);
        if (objReader["TIPONATUREZA_ID"] != DBNull.Value)
          configuracaoEntity.TIPONATUREZA_ID = Convert.ToInt32(objReader["TIPONATUREZA_ID"]);
        if (objReader["COUNTER"] != DBNull.Value)
          configuracaoEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        configuracaoEntityList.Add(configuracaoEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return configuracaoEntityList;
    }
  }
}
