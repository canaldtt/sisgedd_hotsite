﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.ModeloEmpresaDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class ModeloEmpresaDAL
  {
    public int add(ModeloEmpresaEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_modeloempresa";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      command.Parameters.Add((object) objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object) objEntity.EMPRESA_ID));
      command.Parameters.Add((object) objDao.createParameter("@MODELO_ID", DbType.Int32, (object) objEntity.MODELO_ID));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool update(ModeloEmpresaEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_modeloempresa";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      command.Parameters.Add((object) objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object) objEntity.EMPRESA_ID));
      command.Parameters.Add((object) objDao.createParameter("@MODELO_ID", DbType.Int32, (object) objEntity.MODELO_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(ModeloEmpresaEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_modeloempresa";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      command.Parameters.Add((object) objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object) objEntity.EMPRESA_ID));
      command.Parameters.Add((object) objDao.createParameter("@MODELO_ID", DbType.Int32, (object) objEntity.MODELO_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<ModeloEmpresaEntity> find(
      ModeloEmpresaEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_modeloempresa";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.MODELO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@MODELO_ID", DbType.Int32, (object) objEntity.MODELO_ID));
      if (objEntity.EMPRESA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object) objEntity.EMPRESA_ID));
      return this.serialize(objDao.ExecuteReader(command));
    }

    public ModeloEmpresaEntity findByPK(
      ModeloEmpresaEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_modeloempresa";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      command.Parameters.Add((object) objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object) objEntity.EMPRESA_ID));
      command.Parameters.Add((object) objDao.createParameter("@MODELO_ID", DbType.Int32, (object) objEntity.MODELO_ID));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<ModeloEmpresaEntity>();
    }

    private List<ModeloEmpresaEntity> serialize(IDataReader objReader)
    {
      List<ModeloEmpresaEntity> modeloEmpresaEntityList = new List<ModeloEmpresaEntity>();
      while (objReader.Read())
      {
        ModeloEmpresaEntity modeloEmpresaEntity = new ModeloEmpresaEntity();
        if (objReader["EMPRESA_ID"] != DBNull.Value)
          modeloEmpresaEntity.EMPRESA_ID = Convert.ToInt32(objReader["EMPRESA_ID"]);
        if (objReader["MODELO_ID"] != DBNull.Value)
          modeloEmpresaEntity.MODELO_ID = Convert.ToInt32(objReader["MODELO_ID"]);
        if (objReader["COUNTER"] != DBNull.Value)
          modeloEmpresaEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        modeloEmpresaEntityList.Add(modeloEmpresaEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return modeloEmpresaEntityList;
    }
  }
}
