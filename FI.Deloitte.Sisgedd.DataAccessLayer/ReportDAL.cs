﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.ReportDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class ReportDAL
  {
    public List<ReportEntity> relResumoMensal(
      ReportEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.Text;
      command.CommandText = "SELECT status, count(status) as counter \r\n                                        FROM tbrelato \r\n                                        WHERE((DT_INICIO between '" + (object) objEntity.DT_PARAM_INICIO + "' and '" + (object) objEntity.DT_PARAM_FIM + "')) AND EMPRESA_ID=" + (object) objEntity.EMPRESA_ID + " GROUP BY status ORDER BY status";
      IDataReader dataReader = objDao.ExecuteReader(command);
      List<ReportEntity> reportEntityList = new List<ReportEntity>();
      while (dataReader.Read())
      {
        objEntity = new ReportEntity();
        if (dataReader["COUNTER"] != DBNull.Value)
          objEntity.COUNTER = Convert.ToInt32(dataReader["counter"]);
        if (dataReader["status"] != DBNull.Value)
          objEntity.STATUS = dataReader["status"].ToString();
        reportEntityList.Add(objEntity);
      }
      dataReader.Close();
      dataReader.Dispose();
      return reportEntityList;
    }
  }
}
