﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.ProdutoDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class ProdutoDAL
  {
    public int add(ProdutoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_produto";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (!string.IsNullOrEmpty(objEntity.IDIOMA))
        command.Parameters.Add((object) objDao.createParameter("@IDIOMA", DbType.String, (object) objEntity.IDIOMA));
      if (objEntity.PRODUTO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@PRODUTO_ID", DbType.Int32, (object) objEntity.PRODUTO_ID));
      if (objEntity.UNIDADE_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@UNIDADE_ID", DbType.Int32, (object) objEntity.UNIDADE_ID));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool update(ProdutoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_produto";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      if (!string.IsNullOrEmpty(objEntity.IDIOMA))
        command.Parameters.Add((object) objDao.createParameter("@IDIOMA", DbType.String, (object) objEntity.IDIOMA));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (objEntity.PRODUTO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@PRODUTO_ID", DbType.Int32, (object) objEntity.PRODUTO_ID));
      if (objEntity.UNIDADE_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@UNIDADE_ID", DbType.Int32, (object) objEntity.UNIDADE_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(ProdutoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_produto";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      if (objEntity.PRODUTO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@PRODUTO_ID", DbType.Int32, (object) objEntity.PRODUTO_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<ProdutoEntity> find(
      ProdutoEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_produto";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (!string.IsNullOrEmpty(objEntity.IDIOMA))
        command.Parameters.Add((object) objDao.createParameter("@IDIOMA", DbType.String, (object) objEntity.IDIOMA));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (objEntity.PRODUTO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@PRODUTO_ID", DbType.Int32, (object) objEntity.PRODUTO_ID));
      if (objEntity.UNIDADE_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@UNIDADE_ID", DbType.Int32, (object) objEntity.UNIDADE_ID));
      return this.Mapper(objDao.ExecuteReader(command));
    }

    public ProdutoEntity findByPK(
      ProdutoEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_produto";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      command.Parameters.Add((object) objDao.createParameter("@PRODUTO_ID", DbType.Int32, (object) objEntity.PRODUTO_ID));
      return this.Mapper(objDao.ExecuteReader(command)).FirstOrDefault<ProdutoEntity>();
    }

    private List<ProdutoEntity> Mapper(IDataReader objReader)
    {
      List<ProdutoEntity> produtoEntityList = new List<ProdutoEntity>();
      while (objReader.Read())
      {
        ProdutoEntity produtoEntity = new ProdutoEntity();
        if (objReader["NOME"] != DBNull.Value)
          produtoEntity.NOME = objReader["NOME"].ToString();
        if (objReader["IDIOMA"] != DBNull.Value)
          produtoEntity.IDIOMA = objReader["IDIOMA"].ToString();
        if (objReader["PRODUTO_ID"] != DBNull.Value)
          produtoEntity.PRODUTO_ID = Convert.ToInt32(objReader["PRODUTO_ID"]);
        if (objReader["UNIDADE_ID"] != DBNull.Value)
          produtoEntity.UNIDADE_ID = Convert.ToInt32(objReader["UNIDADE_ID"]);
        if (objReader["COUNTER"] != DBNull.Value)
          produtoEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        produtoEntityList.Add(produtoEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return produtoEntityList;
    }
  }
}
