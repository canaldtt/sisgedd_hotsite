﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.PesquisaRespDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class PesquisaRespDAL
  {
    public int add(PesquisaRespEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_pesquisaresp";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      if (objEntity.DT_RESPOSTA > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_RESPOSTA", DbType.DateTime, (object) objEntity.DT_RESPOSTA));
      if (!string.IsNullOrEmpty(objEntity.NMR_PROTOCOLO))
        command.Parameters.Add((object) objDao.createParameter("@NMR_PROTOCOLO", DbType.String, (object) objEntity.NMR_PROTOCOLO));
      if (objEntity.PESQUISA_RESP_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@PESQUISA_RESP_ID", DbType.Int32, (object) objEntity.PESQUISA_RESP_ID));
      if (!string.IsNullOrEmpty(objEntity.QUESTAO))
        command.Parameters.Add((object) objDao.createParameter("@QUESTAO", DbType.String, (object) objEntity.QUESTAO));
      if (!string.IsNullOrEmpty(objEntity.RESPOSTA))
        command.Parameters.Add((object) objDao.createParameter("@RESPOSTA", DbType.String, (object) objEntity.RESPOSTA));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool update(PesquisaRespEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_pesquisaresp";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      if (objEntity.DT_RESPOSTA > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_RESPOSTA", DbType.DateTime, (object) objEntity.DT_RESPOSTA));
      if (!string.IsNullOrEmpty(objEntity.NMR_PROTOCOLO))
        command.Parameters.Add((object) objDao.createParameter("@NMR_PROTOCOLO", DbType.String, (object) objEntity.NMR_PROTOCOLO));
      if (objEntity.PESQUISA_RESP_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@PESQUISA_RESP_ID", DbType.Int32, (object) objEntity.PESQUISA_RESP_ID));
      if (!string.IsNullOrEmpty(objEntity.QUESTAO))
        command.Parameters.Add((object) objDao.createParameter("@QUESTAO", DbType.String, (object) objEntity.QUESTAO));
      if (!string.IsNullOrEmpty(objEntity.RESPOSTA))
        command.Parameters.Add((object) objDao.createParameter("@RESPOSTA", DbType.String, (object) objEntity.RESPOSTA));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(PesquisaRespEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_pesquisaresp";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      if (objEntity.PESQUISA_RESP_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@PESQUISA_RESP_ID", DbType.Int32, (object) objEntity.PESQUISA_RESP_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<PesquisaRespEntity> find(
      PesquisaRespEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_pesquisaresp";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.DT_RESPOSTA > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_RESPOSTA", DbType.DateTime, (object) objEntity.DT_RESPOSTA));
      if (!string.IsNullOrEmpty(objEntity.NMR_PROTOCOLO))
        command.Parameters.Add((object) objDao.createParameter("@NMR_PROTOCOLO", DbType.String, (object) objEntity.NMR_PROTOCOLO));
      if (objEntity.PESQUISA_RESP_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@PESQUISA_RESP_ID", DbType.Int32, (object) objEntity.PESQUISA_RESP_ID));
      if (!string.IsNullOrEmpty(objEntity.QUESTAO))
        command.Parameters.Add((object) objDao.createParameter("@QUESTAO", DbType.String, (object) objEntity.QUESTAO));
      if (!string.IsNullOrEmpty(objEntity.RESPOSTA))
        command.Parameters.Add((object) objDao.createParameter("@RESPOSTA", DbType.String, (object) objEntity.RESPOSTA));
      return this.serialize(objDao.ExecuteReader(command));
    }

    public PesquisaRespEntity findByPK(
      PesquisaRespEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_pesquisaresp";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.PESQUISA_RESP_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@PESQUISA_RESP_ID", DbType.Int32, (object) objEntity.PESQUISA_RESP_ID));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<PesquisaRespEntity>();
    }

    private List<PesquisaRespEntity> serialize(IDataReader objReader)
    {
      List<PesquisaRespEntity> pesquisaRespEntityList = new List<PesquisaRespEntity>();
      while (objReader.Read())
      {
        PesquisaRespEntity pesquisaRespEntity = new PesquisaRespEntity();
        if (objReader["DT_RESPOSTA"] != DBNull.Value)
          pesquisaRespEntity.DT_RESPOSTA = Convert.ToDateTime(objReader["DT_RESPOSTA"]);
        if (objReader["NMR_PROTOCOLO"] != DBNull.Value)
          pesquisaRespEntity.NMR_PROTOCOLO = objReader["NMR_PROTOCOLO"].ToString();
        if (objReader["PESQUISA_RESP_ID"] != DBNull.Value)
          pesquisaRespEntity.PESQUISA_RESP_ID = Convert.ToInt32(objReader["PESQUISA_RESP_ID"]);
        if (objReader["QUESTAO"] != DBNull.Value)
          pesquisaRespEntity.QUESTAO = objReader["QUESTAO"].ToString();
        if (objReader["RESPOSTA"] != DBNull.Value)
          pesquisaRespEntity.RESPOSTA = objReader["RESPOSTA"].ToString();
        if (objReader["COUNTER"] != DBNull.Value)
          pesquisaRespEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        pesquisaRespEntityList.Add(pesquisaRespEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return pesquisaRespEntityList;
    }
  }
}
