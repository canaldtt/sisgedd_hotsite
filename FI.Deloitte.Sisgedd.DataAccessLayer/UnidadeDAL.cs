﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.UnidadeDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class UnidadeDAL
  {
    public int add(UnidadeEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_unidade";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      if (objEntity.EMPRESA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object) objEntity.EMPRESA_ID));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (!string.IsNullOrEmpty(objEntity.IDIOMA))
        command.Parameters.Add((object) objDao.createParameter("@IDIOMA", DbType.String, (object) objEntity.IDIOMA));
      if (objEntity.UNIDADE_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@UNIDADE_ID", DbType.Int32, (object) objEntity.UNIDADE_ID));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool update(UnidadeEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_unidade";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      if (objEntity.EMPRESA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object) objEntity.EMPRESA_ID));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (!string.IsNullOrEmpty(objEntity.IDIOMA))
        command.Parameters.Add((object) objDao.createParameter("@IDIOMA", DbType.String, (object) objEntity.IDIOMA));
      if (objEntity.UNIDADE_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@UNIDADE_ID", DbType.Int32, (object) objEntity.UNIDADE_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(UnidadeEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_unidade";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      if (objEntity.UNIDADE_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@UNIDADE_ID", DbType.Int32, (object) objEntity.UNIDADE_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<UnidadeEntity> find(
      UnidadeEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_unidade";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (!string.IsNullOrEmpty(objEntity.IDIOMA))
        command.Parameters.Add((object) objDao.createParameter("@IDIOMA", DbType.String, (object) objEntity.IDIOMA));
      if (objEntity.EMPRESA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object) objEntity.EMPRESA_ID));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (objEntity.UNIDADE_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@UNIDADE_ID", DbType.Int32, (object) objEntity.UNIDADE_ID));
      return this.Mapper(objDao.ExecuteReader(command));
    }

    public UnidadeEntity findByPK(
      UnidadeEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_unidade";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.EMPRESA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object) objEntity.EMPRESA_ID));
      return this.Mapper(objDao.ExecuteReader(command)).FirstOrDefault<UnidadeEntity>();
    }

    private List<UnidadeEntity> Mapper(IDataReader objReader)
    {
      List<UnidadeEntity> unidadeEntityList = new List<UnidadeEntity>();
      while (objReader.Read())
      {
        UnidadeEntity unidadeEntity = new UnidadeEntity();
        if (objReader["EMPRESA_ID"] != DBNull.Value)
          unidadeEntity.EMPRESA_ID = Convert.ToInt32(objReader["EMPRESA_ID"]);
        if (objReader["IDIOMA"] != DBNull.Value)
          unidadeEntity.IDIOMA = objReader["IDIOMA"].ToString();
        if (objReader["NOME"] != DBNull.Value)
          unidadeEntity.NOME = objReader["NOME"].ToString();
        if (objReader["UNIDADE_ID"] != DBNull.Value)
          unidadeEntity.UNIDADE_ID = Convert.ToInt32(objReader["UNIDADE_ID"]);
        if (objReader["COUNTER"] != DBNull.Value)
          unidadeEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        unidadeEntityList.Add(unidadeEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return unidadeEntityList;
    }
  }
}
