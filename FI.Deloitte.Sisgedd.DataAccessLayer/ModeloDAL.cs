﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.ModeloDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class ModeloDAL
  {
    public int add(ModeloEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_modelo";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool update(ModeloEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_modelo";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      command.Parameters.Add((object) objDao.createParameter("@MODELO_ID", DbType.Int32, (object) objEntity.MODELO_ID));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(ModeloEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_modelo";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      command.Parameters.Add((object) objDao.createParameter("@MODELO_ID", DbType.Int32, (object) objEntity.MODELO_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<ModeloEntity> find(
      ModeloEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_modelo";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      return this.serialize(objDao.ExecuteReader(command));
    }

    public ModeloEntity findByPK(
      ModeloEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_modelo";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      command.Parameters.Add((object) objDao.createParameter("@MODELO_ID", DbType.Int32, (object) objEntity.MODELO_ID));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<ModeloEntity>();
    }

    private List<ModeloEntity> serialize(IDataReader objReader)
    {
      List<ModeloEntity> modeloEntityList = new List<ModeloEntity>();
      while (objReader.Read())
      {
        ModeloEntity modeloEntity = new ModeloEntity();
        if (objReader["MODELO_ID"] != DBNull.Value)
          modeloEntity.MODELO_ID = Convert.ToInt32(objReader["MODELO_ID"]);
        if (objReader["NOME"] != DBNull.Value)
          modeloEntity.NOME = objReader["NOME"].ToString();
        if (objReader["COUNTER"] != DBNull.Value)
          modeloEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        modeloEntityList.Add(modeloEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return modeloEntityList;
    }
  }
}
