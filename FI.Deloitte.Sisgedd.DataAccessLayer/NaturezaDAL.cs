﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.NaturezaDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class NaturezaDAL
  {
    public int add(NaturezaEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_natureza";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      if (!string.IsNullOrEmpty(objEntity.DESC))
        command.Parameters.Add((object) objDao.createParameter("@DESC", DbType.String, (object) objEntity.DESC));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool update(NaturezaEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_natureza";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      if (!string.IsNullOrEmpty(objEntity.DESC))
        command.Parameters.Add((object) objDao.createParameter("@DESC", DbType.String, (object) objEntity.DESC));
      if (objEntity.NATUREZA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@NATUREZA_ID", DbType.Int32, (object) objEntity.NATUREZA_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(NaturezaEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_natureza";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      if (objEntity.NATUREZA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@NATUREZA_ID", DbType.Int32, (object) objEntity.NATUREZA_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<NaturezaEntity> find(
      NaturezaEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_natureza";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (!string.IsNullOrEmpty(objEntity.DESC))
        command.Parameters.Add((object) objDao.createParameter("@DESC", DbType.String, (object) objEntity.DESC));
      if (objEntity.NATUREZA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@NATUREZA_ID", DbType.Int32, (object) objEntity.NATUREZA_ID));
      return this.serialize(objDao.ExecuteReader(command));
    }

    public NaturezaEntity findByPK(
      NaturezaEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_natureza";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.NATUREZA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@NATUREZA_ID", DbType.Int32, (object) objEntity.NATUREZA_ID));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<NaturezaEntity>();
    }

    private List<NaturezaEntity> serialize(IDataReader objReader)
    {
      List<NaturezaEntity> naturezaEntityList = new List<NaturezaEntity>();
      while (objReader.Read())
      {
        NaturezaEntity naturezaEntity = new NaturezaEntity();
        if (objReader["DESC"] != DBNull.Value)
          naturezaEntity.DESC = objReader["DESC"].ToString();
        if (objReader["NATUREZA_ID"] != DBNull.Value)
          naturezaEntity.NATUREZA_ID = Convert.ToInt32(objReader["NATUREZA_ID"]);
        if (objReader["COUNTER"] != DBNull.Value)
          naturezaEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        naturezaEntityList.Add(naturezaEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return naturezaEntityList;
    }
  }
}
