﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.TipoRelatoDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class TipoRelatoDAL
  {
    public int add(TipoRelatoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tiporelato";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool update(TipoRelatoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tiporelato";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (objEntity.TIPORELATO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TIPORELATO_ID", DbType.Int32, (object) objEntity.TIPORELATO_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(TipoRelatoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tiporelato";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      if (objEntity.TIPORELATO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TIPORELATO_ID", DbType.Int32, (object) objEntity.TIPORELATO_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<TipoRelatoEntity> find(
      TipoRelatoEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tiporelato";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (objEntity.TIPORELATO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TIPORELATO_ID", DbType.Int32, (object) objEntity.TIPORELATO_ID));
      return this.serialize(objDao.ExecuteReader(command));
    }

    public TipoRelatoEntity findByPK(
      TipoRelatoEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tiporelato";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.TIPORELATO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TIPORELATO_ID", DbType.Int32, (object) objEntity.TIPORELATO_ID));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<TipoRelatoEntity>();
    }

    private List<TipoRelatoEntity> serialize(IDataReader objReader)
    {
      List<TipoRelatoEntity> tipoRelatoEntityList = new List<TipoRelatoEntity>();
      while (objReader.Read())
      {
        TipoRelatoEntity tipoRelatoEntity = new TipoRelatoEntity();
        if (objReader["NOME"] != DBNull.Value)
          tipoRelatoEntity.NOME = objReader["NOME"].ToString();
        if (objReader["TIPORELATO_ID"] != DBNull.Value)
          tipoRelatoEntity.TIPORELATO_ID = Convert.ToInt32(objReader["TIPORELATO_ID"]);
        if (objReader["COUNTER"] != DBNull.Value)
          tipoRelatoEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        tipoRelatoEntityList.Add(tipoRelatoEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return tipoRelatoEntityList;
    }

    public List<TipoRelatoEntity> findByEmpresa(
      EmpresaEntity objEntity,
      string _idioma,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tiporelatoempresa";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (!string.IsNullOrEmpty(_idioma))
        command.Parameters.Add((object) objDao.createParameter("@IDIOMA", DbType.String, (object) _idioma));
      if (objEntity.EMPRESA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object) objEntity.EMPRESA_ID));
      return this.serialize(objDao.ExecuteReader(command));
    }
  }
}
