﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.RelatoDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class RelatoDAL
  {
    public int add(RelatoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_relato";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      if (!string.IsNullOrEmpty(objEntity.ANIMO))
        command.Parameters.Add((object) objDao.createParameter("@ANIMO", DbType.String, (object) objEntity.ANIMO));
      if (objEntity.USER_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@USER_ID", DbType.Int32, (object) objEntity.USER_ID));
      if (!string.IsNullOrEmpty(objEntity.USER))
        command.Parameters.Add((object) objDao.createParameter("@USER", DbType.String, (object) objEntity.USER));
      if (!string.IsNullOrEmpty(objEntity.NOME_ATENDENTE))
        command.Parameters.Add((object) objDao.createParameter("@NOME_ATENDENTE", DbType.String, (object) objEntity.NOME_ATENDENTE));
      if (!string.IsNullOrEmpty(objEntity.CARGO))
        command.Parameters.Add((object) objDao.createParameter("@CARGO", DbType.String, (object) objEntity.CARGO));
      if (!string.IsNullOrEmpty(objEntity.CELULAR))
        command.Parameters.Add((object) objDao.createParameter("@CELULAR", DbType.String, (object) objEntity.CELULAR));
      if (!string.IsNullOrEmpty(objEntity.COMO_FICOU_CIENTE))
        command.Parameters.Add((object) objDao.createParameter("@COMO_FICOU_CIENTE", DbType.String, (object) objEntity.COMO_FICOU_CIENTE));
      if (!string.IsNullOrEmpty(objEntity.CRITICIDADE))
        command.Parameters.Add((object) objDao.createParameter("@CRITICIDADE", DbType.String, (object) objEntity.CRITICIDADE));
      if (!string.IsNullOrEmpty(objEntity.DEN1_CARGO))
        command.Parameters.Add((object) objDao.createParameter("@DEN1_CARGO", DbType.String, (object) objEntity.DEN1_CARGO));
      if (!string.IsNullOrEmpty(objEntity.DEN1_CIDADE))
        command.Parameters.Add((object) objDao.createParameter("@DEN1_CIDADE", DbType.String, (object) objEntity.DEN1_CIDADE));
      if (!string.IsNullOrEmpty(objEntity.DEN1_EMAIL))
        command.Parameters.Add((object) objDao.createParameter("@DEN1_EMAIL", DbType.String, (object) objEntity.DEN1_EMAIL));
      if (!string.IsNullOrEmpty(objEntity.DEN1_ESTADO))
        command.Parameters.Add((object) objDao.createParameter("@DEN1_ESTADO", DbType.String, (object) objEntity.DEN1_ESTADO));
      if (!string.IsNullOrEmpty(objEntity.DEN1_NOME))
        command.Parameters.Add((object) objDao.createParameter("@DEN1_NOME", DbType.String, (object) objEntity.DEN1_NOME));
      if (!string.IsNullOrEmpty(objEntity.DEN1_PARTICIPACAO))
        command.Parameters.Add((object) objDao.createParameter("@DEN1_PARTICIPACAO", DbType.String, (object) objEntity.DEN1_PARTICIPACAO));
      if (!string.IsNullOrEmpty(objEntity.DEN1_RELACAO))
        command.Parameters.Add((object) objDao.createParameter("@DEN1_RELACAO", DbType.String, (object) objEntity.DEN1_RELACAO));
      if (!string.IsNullOrEmpty(objEntity.DEN1_TELEFONE))
        command.Parameters.Add((object) objDao.createParameter("@DEN1_TELEFONE", DbType.String, (object) objEntity.DEN1_TELEFONE));
      if (!string.IsNullOrEmpty(objEntity.DEN2_CARGO))
        command.Parameters.Add((object) objDao.createParameter("@DEN2_CARGO", DbType.String, (object) objEntity.DEN2_CARGO));
      if (!string.IsNullOrEmpty(objEntity.DEN2_CIDADE))
        command.Parameters.Add((object) objDao.createParameter("@DEN2_CIDADE", DbType.String, (object) objEntity.DEN2_CIDADE));
      if (!string.IsNullOrEmpty(objEntity.DEN2_EMAIL))
        command.Parameters.Add((object) objDao.createParameter("@DEN2_EMAIL", DbType.String, (object) objEntity.DEN2_EMAIL));
      if (!string.IsNullOrEmpty(objEntity.DEN2_ESTADO))
        command.Parameters.Add((object) objDao.createParameter("@DEN2_ESTADO", DbType.String, (object) objEntity.DEN2_ESTADO));
      if (!string.IsNullOrEmpty(objEntity.DEN2_NOME))
        command.Parameters.Add((object) objDao.createParameter("@DEN2_NOME", DbType.String, (object) objEntity.DEN2_NOME));
      if (!string.IsNullOrEmpty(objEntity.DEN2_PARTICIPACAO))
        command.Parameters.Add((object) objDao.createParameter("@DEN2_PARTICIPACAO", DbType.String, (object) objEntity.DEN2_PARTICIPACAO));
      if (!string.IsNullOrEmpty(objEntity.DEN2_RELACAO))
        command.Parameters.Add((object) objDao.createParameter("@DEN2_RELACAO", DbType.String, (object) objEntity.DEN2_RELACAO));
      if (!string.IsNullOrEmpty(objEntity.DEN2_TELEFONE))
        command.Parameters.Add((object) objDao.createParameter("@DEN2_TELEFONE", DbType.String, (object) objEntity.DEN2_TELEFONE));
      if (!string.IsNullOrEmpty(objEntity.DEN3_CARGO))
        command.Parameters.Add((object) objDao.createParameter("@DEN3_CARGO", DbType.String, (object) objEntity.DEN3_CARGO));
      if (!string.IsNullOrEmpty(objEntity.DEN3_CIDADE))
        command.Parameters.Add((object) objDao.createParameter("@DEN3_CIDADE", DbType.String, (object) objEntity.DEN3_CIDADE));
      if (!string.IsNullOrEmpty(objEntity.DEN3_EMAIL))
        command.Parameters.Add((object) objDao.createParameter("@DEN3_EMAIL", DbType.String, (object) objEntity.DEN3_EMAIL));
      if (!string.IsNullOrEmpty(objEntity.DEN3_ESTADO))
        command.Parameters.Add((object) objDao.createParameter("@DEN3_ESTADO", DbType.String, (object) objEntity.DEN3_ESTADO));
      if (!string.IsNullOrEmpty(objEntity.DEN3_NOME))
        command.Parameters.Add((object) objDao.createParameter("@DEN3_NOME", DbType.String, (object) objEntity.DEN3_NOME));
      if (!string.IsNullOrEmpty(objEntity.DEN3_PARTICIPACAO))
        command.Parameters.Add((object) objDao.createParameter("@DEN3_PARTICIPACAO", DbType.String, (object) objEntity.DEN3_PARTICIPACAO));
      if (!string.IsNullOrEmpty(objEntity.DEN3_RELACAO))
        command.Parameters.Add((object) objDao.createParameter("@DEN3_RELACAO", DbType.String, (object) objEntity.DEN3_RELACAO));
      if (!string.IsNullOrEmpty(objEntity.DEN3_TELEFONE))
        command.Parameters.Add((object) objDao.createParameter("@DEN3_TELEFONE", DbType.String, (object) objEntity.DEN3_TELEFONE));
      if (!string.IsNullOrEmpty(objEntity.DEN4_CARGO))
        command.Parameters.Add((object) objDao.createParameter("@DEN4_CARGO", DbType.String, (object) objEntity.DEN4_CARGO));
      if (!string.IsNullOrEmpty(objEntity.DEN4_CIDADE))
        command.Parameters.Add((object) objDao.createParameter("@DEN4_CIDADE", DbType.String, (object) objEntity.DEN4_CIDADE));
      if (!string.IsNullOrEmpty(objEntity.DEN4_EMAIL))
        command.Parameters.Add((object) objDao.createParameter("@DEN4_EMAIL", DbType.String, (object) objEntity.DEN4_EMAIL));
      if (!string.IsNullOrEmpty(objEntity.DEN4_ESTADO))
        command.Parameters.Add((object) objDao.createParameter("@DEN4_ESTADO", DbType.String, (object) objEntity.DEN4_ESTADO));
      if (!string.IsNullOrEmpty(objEntity.DEN4_NOME))
        command.Parameters.Add((object) objDao.createParameter("@DEN4_NOME", DbType.String, (object) objEntity.DEN4_NOME));
      if (!string.IsNullOrEmpty(objEntity.DEN4_PARTICIPACAO))
        command.Parameters.Add((object) objDao.createParameter("@DEN4_PARTICIPACAO", DbType.String, (object) objEntity.DEN4_PARTICIPACAO));
      if (!string.IsNullOrEmpty(objEntity.DEN4_RELACAO))
        command.Parameters.Add((object) objDao.createParameter("@DEN4_RELACAO", DbType.String, (object) objEntity.DEN4_RELACAO));
      if (!string.IsNullOrEmpty(objEntity.DEN4_TELEFONE))
        command.Parameters.Add((object) objDao.createParameter("@DEN4_TELEFONE", DbType.String, (object) objEntity.DEN4_TELEFONE));
      if (!string.IsNullOrEmpty(objEntity.DEN5_CARGO))
        command.Parameters.Add((object) objDao.createParameter("@DEN5_CARGO", DbType.String, (object) objEntity.DEN5_CARGO));
      if (!string.IsNullOrEmpty(objEntity.DEN5_CIDADE))
        command.Parameters.Add((object) objDao.createParameter("@DEN5_CIDADE", DbType.String, (object) objEntity.DEN5_CIDADE));
      if (!string.IsNullOrEmpty(objEntity.DEN5_EMAIL))
        command.Parameters.Add((object) objDao.createParameter("@DEN5_EMAIL", DbType.String, (object) objEntity.DEN5_EMAIL));
      if (!string.IsNullOrEmpty(objEntity.DEN5_ESTADO))
        command.Parameters.Add((object) objDao.createParameter("@DEN5_ESTADO", DbType.String, (object) objEntity.DEN5_ESTADO));
      if (!string.IsNullOrEmpty(objEntity.DEN5_NOME))
        command.Parameters.Add((object) objDao.createParameter("@DEN5_NOME", DbType.String, (object) objEntity.DEN5_NOME));
      if (!string.IsNullOrEmpty(objEntity.DEN5_PARTICIPACAO))
        command.Parameters.Add((object) objDao.createParameter("@DEN5_PARTICIPACAO", DbType.String, (object) objEntity.DEN5_PARTICIPACAO));
      if (!string.IsNullOrEmpty(objEntity.DEN5_RELACAO))
        command.Parameters.Add((object) objDao.createParameter("@DEN5_RELACAO", DbType.String, (object) objEntity.DEN5_RELACAO));
      if (!string.IsNullOrEmpty(objEntity.DEN5_TELEFONE))
        command.Parameters.Add((object) objDao.createParameter("@DEN5_TELEFONE", DbType.String, (object) objEntity.DEN5_TELEFONE));
      if (objEntity.DT_ATUALIZACAO > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_ATUALIZACAO", DbType.DateTime, (object) objEntity.DT_ATUALIZACAO));
      if (objEntity.DT_ENCERRAMENTO > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_ENCERRAMENTO", DbType.DateTime, (object) objEntity.DT_ENCERRAMENTO));
      if (objEntity.DT_INICIO > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_INICIO", DbType.DateTime, (object) objEntity.DT_INICIO));
      if (objEntity.DT_MAX > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_MAX", DbType.DateTime, (object) objEntity.DT_MAX));
      if (!string.IsNullOrEmpty(objEntity.EMAIL))
        command.Parameters.Add((object) objDao.createParameter("@EMAIL", DbType.String, (object) objEntity.EMAIL));
      if (!string.IsNullOrEmpty(objEntity.EMAIL_ANONIMO))
        command.Parameters.Add((object) objDao.createParameter("@EMAIL_ANONIMO", DbType.String, (object) objEntity.EMAIL_ANONIMO));
      if (!string.IsNullOrEmpty(objEntity.EXISTE_RECORRENCIA))
        command.Parameters.Add((object) objDao.createParameter("@EXISTE_RECORRENCIA", DbType.String, (object) objEntity.EXISTE_RECORRENCIA));
      if (!string.IsNullOrEmpty(objEntity.FORMA_CONTATO))
        command.Parameters.Add((object) objDao.createParameter("@FORMA_CONTATO", DbType.String, (object) objEntity.FORMA_CONTATO));
      if (!string.IsNullOrEmpty(objEntity.GENERO))
        command.Parameters.Add((object) objDao.createParameter("@GENERO", DbType.String, (object) objEntity.GENERO));
      if (!string.IsNullOrEmpty(objEntity.IDENTIFICADO))
        command.Parameters.Add((object) objDao.createParameter("@IDENTIFICADO", DbType.String, (object) objEntity.IDENTIFICADO));
      if (!string.IsNullOrEmpty(objEntity.IDENTIFICAR_PESSOAS))
        command.Parameters.Add((object) objDao.createParameter("@IDENTIFICAR_PESSOAS", DbType.String, (object) objEntity.IDENTIFICAR_PESSOAS));
      if (!string.IsNullOrEmpty(objEntity.IDIOMA))
        command.Parameters.Add((object) objDao.createParameter("@IDIOMA", DbType.String, (object) objEntity.IDIOMA));
      if (!string.IsNullOrEmpty(objEntity.NATUREZA))
        command.Parameters.Add((object) objDao.createParameter("@NATUREZA", DbType.String, (object) objEntity.NATUREZA));
      if (!string.IsNullOrEmpty(objEntity.NMR_PROTOCOLO))
        command.Parameters.Add((object) objDao.createParameter("@NMR_PROTOCOLO", DbType.String, (object) objEntity.NMR_PROTOCOLO));
      if (!string.IsNullOrEmpty(objEntity.NMR_PROTOCOLO_ANTERIOR))
        command.Parameters.Add((object) objDao.createParameter("@NMR_PROTOCOLO_ANTERIOR", DbType.String, (object) objEntity.NMR_PROTOCOLO_ANTERIOR));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (!string.IsNullOrEmpty(objEntity.PERDA_FINANCEIRA))
        command.Parameters.Add((object) objDao.createParameter("@PERDA_FINANCEIRA", DbType.String, (object) objEntity.PERDA_FINANCEIRA));
      if (!string.IsNullOrEmpty(objEntity.PERIODO_EVENTO_OCORREU))
        command.Parameters.Add((object) objDao.createParameter("@PERIODO_EVENTO_OCORREU", DbType.String, (object) objEntity.PERIODO_EVENTO_OCORREU));
      if (!string.IsNullOrEmpty(objEntity.AREA))
        command.Parameters.Add((object) objDao.createParameter("@AREA", DbType.String, (object) objEntity.AREA));
      command.Parameters.Add((object) objDao.createParameter("@AREA_ID", DbType.Int32, (object) objEntity.AREA_ID));
      if (!string.IsNullOrEmpty(objEntity.EMPRESA))
        command.Parameters.Add((object) objDao.createParameter("@EMPRESA", DbType.String, (object) objEntity.EMPRESA));
      command.Parameters.Add((object) objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object) objEntity.EMPRESA_ID));
      if (!string.IsNullOrEmpty(objEntity.UNIDADE))
        command.Parameters.Add((object) objDao.createParameter("@UNIDADE", DbType.String, (object) objEntity.UNIDADE));
      command.Parameters.Add((object) objDao.createParameter("@UNIDADE_ID", DbType.Int32, (object) objEntity.UNIDADE_ID));
      if (!string.IsNullOrEmpty(objEntity.PRODUTO))
        command.Parameters.Add((object) objDao.createParameter("@PRODUTO", DbType.String, (object) objEntity.PRODUTO));
      command.Parameters.Add((object) objDao.createParameter("@PRODUTO_ID", DbType.Int32, (object) objEntity.PRODUTO_ID));
      if (!string.IsNullOrEmpty(objEntity.QUANTO_AO_RELATO))
        command.Parameters.Add((object) objDao.createParameter("@QUANTO_AO_RELATO", DbType.String, (object) objEntity.QUANTO_AO_RELATO));
      if (!string.IsNullOrEmpty(objEntity.RELATO))
        command.Parameters.Add((object) objDao.createParameter("@RELATO", DbType.String, (object) objEntity.RELATO));
      if (!string.IsNullOrEmpty(objEntity.RELATO_RESUMO))
        command.Parameters.Add((object) objDao.createParameter("@RELATO_RESUMO", DbType.String, (object) objEntity.RELATO_RESUMO));
      if (!string.IsNullOrEmpty(objEntity.RELATO_SUGESTAO))
        command.Parameters.Add((object) objDao.createParameter("@RELATO_SUGESTAO", DbType.String, (object) objEntity.RELATO_SUGESTAO));
      if (!string.IsNullOrEmpty(objEntity.SENHA_ACESSO))
        command.Parameters.Add((object) objDao.createParameter("@SENHA_ACESSO", DbType.String, (object) objEntity.SENHA_ACESSO));
      if (!string.IsNullOrEmpty(objEntity.STATUS))
        command.Parameters.Add((object) objDao.createParameter("@STATUS", DbType.String, (object) objEntity.STATUS));
      if (!string.IsNullOrEmpty(objEntity.TELEFONE))
        command.Parameters.Add((object) objDao.createParameter("@TELEFONE", DbType.String, (object) objEntity.TELEFONE));
      if (!string.IsNullOrEmpty(objEntity.TIPO_MANIFESTACAO))
        command.Parameters.Add((object) objDao.createParameter("@TIPO_MANIFESTACAO", DbType.String, (object) objEntity.TIPO_MANIFESTACAO));
      if (!string.IsNullOrEmpty(objEntity.TIPO_RELATO))
        command.Parameters.Add((object) objDao.createParameter("@TIPO_RELATO", DbType.String, (object) objEntity.TIPO_RELATO));
      if (!string.IsNullOrEmpty(objEntity.TIPO_NATUREZA))
        command.Parameters.Add((object) objDao.createParameter("@TIPO_NATUREZA", DbType.String, (object) objEntity.TIPO_NATUREZA));
      if (!string.IsNullOrEmpty(objEntity.TIPO_PUBLICO))
        command.Parameters.Add((object) objDao.createParameter("@TIPO_PUBLICO", DbType.String, (object) objEntity.TIPO_PUBLICO));
      if (!string.IsNullOrEmpty(objEntity.COMENTARIOS))
        command.Parameters.Add((object) objDao.createParameter("@COMENTARIOS", DbType.String, (object) objEntity.COMENTARIOS));
      if (objEntity.NATUREZA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@NATUREZA_ID", DbType.Int32, (object) objEntity.NATUREZA_ID));
      if (objEntity.TIPONATUREZA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TIPONATUREZA_ID", DbType.Int32, (object) objEntity.TIPONATUREZA_ID));
      if (!string.IsNullOrEmpty(objEntity.HIST_ATEND))
        command.Parameters.Add((object) objDao.createParameter("@HIST_ATENDIMENTO", DbType.String, (object) objEntity.HIST_ATEND));
      if (!string.IsNullOrEmpty(objEntity.FLG_FLUXO))
        command.Parameters.Add((object) objDao.createParameter("@FLG_FLUXO", DbType.String, (object) objEntity.FLG_FLUXO));
      if (!string.IsNullOrEmpty(objEntity.FLG_RESP_PESQ))
        command.Parameters.Add((object) objDao.createParameter("@FLG_RESP_PESQ", DbType.String, (object) objEntity.FLG_RESP_PESQ));
      if (objEntity.SEQ > 0)
        command.Parameters.Add((object) objDao.createParameter("@SEQ", DbType.Int32, (object) objEntity.SEQ));
      if (!string.IsNullOrEmpty(objEntity.EVIDENCIAS))
        command.Parameters.Add((object) objDao.createParameter("@EVIDENCIAS", DbType.String, (object) objEntity.EVIDENCIAS));
      if (!string.IsNullOrEmpty(objEntity.DESCRICAO_DETALHADA))
        command.Parameters.Add((object) objDao.createParameter("@DESC_DETALHADA", DbType.String, (object) objEntity.DESCRICAO_DETALHADA));
      if (!string.IsNullOrEmpty(objEntity.GUID))
        command.Parameters.Add((object) objDao.createParameter("@GUID", DbType.String, (object) objEntity.GUID));
      if (!string.IsNullOrEmpty(objEntity.EMAIL_ID))
        command.Parameters.Add((object) objDao.createParameter("@EMAIL_ID", DbType.String, (object) objEntity.EMAIL_ID));
      if (!string.IsNullOrEmpty(objEntity.RESPONSIBLE))
        command.Parameters.Add((object) objDao.createParameter("@RESPONSIBLE", DbType.String, (object) objEntity.RESPONSIBLE));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool SendRelatoToBWise(RelatoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_relato";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      command.Parameters.Add((object) objDao.createParameter("@RELATO_ID", DbType.Int32, (object) objEntity.RELATO_ID));
      command.Parameters.Add((object) objDao.createParameter("@FLG_FLUXO", DbType.String, (object) objEntity.FLG_FLUXO));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool update(RelatoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_relato";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      if (!string.IsNullOrEmpty(objEntity.ANIMO))
        command.Parameters.Add((object) objDao.createParameter("@ANIMO", DbType.String, (object) objEntity.ANIMO));
      if (objEntity.USER_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@USER_ID", DbType.Int32, (object) objEntity.USER_ID));
      if (!string.IsNullOrEmpty(objEntity.USER))
        command.Parameters.Add((object) objDao.createParameter("@USER", DbType.String, (object) objEntity.USER));
      if (!string.IsNullOrEmpty(objEntity.NOME_ATENDENTE))
        command.Parameters.Add((object) objDao.createParameter("@NOME_ATENDENTE", DbType.String, (object) objEntity.NOME_ATENDENTE));
      if (!string.IsNullOrEmpty(objEntity.CARGO))
        command.Parameters.Add((object) objDao.createParameter("@CARGO", DbType.String, (object) objEntity.CARGO));
      if (!string.IsNullOrEmpty(objEntity.CELULAR))
        command.Parameters.Add((object) objDao.createParameter("@CELULAR", DbType.String, (object) objEntity.CELULAR));
      if (!string.IsNullOrEmpty(objEntity.COMO_FICOU_CIENTE))
        command.Parameters.Add((object) objDao.createParameter("@COMO_FICOU_CIENTE", DbType.String, (object) objEntity.COMO_FICOU_CIENTE));
      if (!string.IsNullOrEmpty(objEntity.CRITICIDADE))
        command.Parameters.Add((object) objDao.createParameter("@CRITICIDADE", DbType.String, (object) objEntity.CRITICIDADE));
      if (!string.IsNullOrEmpty(objEntity.DEN1_CARGO))
        command.Parameters.Add((object) objDao.createParameter("@DEN1_CARGO", DbType.String, (object) objEntity.DEN1_CARGO));
      if (!string.IsNullOrEmpty(objEntity.DEN1_CIDADE))
        command.Parameters.Add((object) objDao.createParameter("@DEN1_CIDADE", DbType.String, (object) objEntity.DEN1_CIDADE));
      if (!string.IsNullOrEmpty(objEntity.DEN1_EMAIL))
        command.Parameters.Add((object) objDao.createParameter("@DEN1_EMAIL", DbType.String, (object) objEntity.DEN1_EMAIL));
      if (!string.IsNullOrEmpty(objEntity.DEN1_ESTADO))
        command.Parameters.Add((object) objDao.createParameter("@DEN1_ESTADO", DbType.String, (object) objEntity.DEN1_ESTADO));
      if (!string.IsNullOrEmpty(objEntity.DEN1_NOME))
        command.Parameters.Add((object) objDao.createParameter("@DEN1_NOME", DbType.String, (object) objEntity.DEN1_NOME));
      if (!string.IsNullOrEmpty(objEntity.DEN1_PARTICIPACAO))
        command.Parameters.Add((object) objDao.createParameter("@DEN1_PARTICIPACAO", DbType.String, (object) objEntity.DEN1_PARTICIPACAO));
      if (!string.IsNullOrEmpty(objEntity.DEN1_RELACAO))
        command.Parameters.Add((object) objDao.createParameter("@DEN1_RELACAO", DbType.String, (object) objEntity.DEN1_RELACAO));
      if (!string.IsNullOrEmpty(objEntity.DEN1_TELEFONE))
        command.Parameters.Add((object) objDao.createParameter("@DEN1_TELEFONE", DbType.String, (object) objEntity.DEN1_TELEFONE));
      if (!string.IsNullOrEmpty(objEntity.DEN2_CARGO))
        command.Parameters.Add((object) objDao.createParameter("@DEN2_CARGO", DbType.String, (object) objEntity.DEN2_CARGO));
      if (!string.IsNullOrEmpty(objEntity.DEN2_CIDADE))
        command.Parameters.Add((object) objDao.createParameter("@DEN2_CIDADE", DbType.String, (object) objEntity.DEN2_CIDADE));
      if (!string.IsNullOrEmpty(objEntity.DEN2_EMAIL))
        command.Parameters.Add((object) objDao.createParameter("@DEN2_EMAIL", DbType.String, (object) objEntity.DEN2_EMAIL));
      if (!string.IsNullOrEmpty(objEntity.DEN2_ESTADO))
        command.Parameters.Add((object) objDao.createParameter("@DEN2_ESTADO", DbType.String, (object) objEntity.DEN2_ESTADO));
      if (!string.IsNullOrEmpty(objEntity.DEN2_NOME))
        command.Parameters.Add((object) objDao.createParameter("@DEN2_NOME", DbType.String, (object) objEntity.DEN2_NOME));
      if (!string.IsNullOrEmpty(objEntity.DEN2_PARTICIPACAO))
        command.Parameters.Add((object) objDao.createParameter("@DEN2_PARTICIPACAO", DbType.String, (object) objEntity.DEN2_PARTICIPACAO));
      if (!string.IsNullOrEmpty(objEntity.DEN2_RELACAO))
        command.Parameters.Add((object) objDao.createParameter("@DEN2_RELACAO", DbType.String, (object) objEntity.DEN2_RELACAO));
      if (!string.IsNullOrEmpty(objEntity.DEN2_TELEFONE))
        command.Parameters.Add((object) objDao.createParameter("@DEN2_TELEFONE", DbType.String, (object) objEntity.DEN2_TELEFONE));
      if (!string.IsNullOrEmpty(objEntity.DEN3_CARGO))
        command.Parameters.Add((object) objDao.createParameter("@DEN3_CARGO", DbType.String, (object) objEntity.DEN3_CARGO));
      if (!string.IsNullOrEmpty(objEntity.DEN3_CIDADE))
        command.Parameters.Add((object) objDao.createParameter("@DEN3_CIDADE", DbType.String, (object) objEntity.DEN3_CIDADE));
      if (!string.IsNullOrEmpty(objEntity.DEN3_EMAIL))
        command.Parameters.Add((object) objDao.createParameter("@DEN3_EMAIL", DbType.String, (object) objEntity.DEN3_EMAIL));
      if (!string.IsNullOrEmpty(objEntity.DEN3_ESTADO))
        command.Parameters.Add((object) objDao.createParameter("@DEN3_ESTADO", DbType.String, (object) objEntity.DEN3_ESTADO));
      if (!string.IsNullOrEmpty(objEntity.DEN3_NOME))
        command.Parameters.Add((object) objDao.createParameter("@DEN3_NOME", DbType.String, (object) objEntity.DEN3_NOME));
      if (!string.IsNullOrEmpty(objEntity.DEN3_PARTICIPACAO))
        command.Parameters.Add((object) objDao.createParameter("@DEN3_PARTICIPACAO", DbType.String, (object) objEntity.DEN3_PARTICIPACAO));
      if (!string.IsNullOrEmpty(objEntity.DEN3_RELACAO))
        command.Parameters.Add((object) objDao.createParameter("@DEN3_RELACAO", DbType.String, (object) objEntity.DEN3_RELACAO));
      if (!string.IsNullOrEmpty(objEntity.DEN3_TELEFONE))
        command.Parameters.Add((object) objDao.createParameter("@DEN3_TELEFONE", DbType.String, (object) objEntity.DEN3_TELEFONE));
      if (!string.IsNullOrEmpty(objEntity.DEN4_CARGO))
        command.Parameters.Add((object) objDao.createParameter("@DEN4_CARGO", DbType.String, (object) objEntity.DEN4_CARGO));
      if (!string.IsNullOrEmpty(objEntity.DEN4_CIDADE))
        command.Parameters.Add((object) objDao.createParameter("@DEN4_CIDADE", DbType.String, (object) objEntity.DEN4_CIDADE));
      if (!string.IsNullOrEmpty(objEntity.DEN4_EMAIL))
        command.Parameters.Add((object) objDao.createParameter("@DEN4_EMAIL", DbType.String, (object) objEntity.DEN4_EMAIL));
      if (!string.IsNullOrEmpty(objEntity.DEN4_ESTADO))
        command.Parameters.Add((object) objDao.createParameter("@DEN4_ESTADO", DbType.String, (object) objEntity.DEN4_ESTADO));
      if (!string.IsNullOrEmpty(objEntity.DEN4_NOME))
        command.Parameters.Add((object) objDao.createParameter("@DEN4_NOME", DbType.String, (object) objEntity.DEN4_NOME));
      if (!string.IsNullOrEmpty(objEntity.DEN4_PARTICIPACAO))
        command.Parameters.Add((object) objDao.createParameter("@DEN4_PARTICIPACAO", DbType.String, (object) objEntity.DEN4_PARTICIPACAO));
      if (!string.IsNullOrEmpty(objEntity.DEN4_RELACAO))
        command.Parameters.Add((object) objDao.createParameter("@DEN4_RELACAO", DbType.String, (object) objEntity.DEN4_RELACAO));
      if (!string.IsNullOrEmpty(objEntity.DEN4_TELEFONE))
        command.Parameters.Add((object) objDao.createParameter("@DEN4_TELEFONE", DbType.String, (object) objEntity.DEN4_TELEFONE));
      if (!string.IsNullOrEmpty(objEntity.DEN5_CARGO))
        command.Parameters.Add((object) objDao.createParameter("@DEN5_CARGO", DbType.String, (object) objEntity.DEN5_CARGO));
      if (!string.IsNullOrEmpty(objEntity.DEN5_CIDADE))
        command.Parameters.Add((object) objDao.createParameter("@DEN5_CIDADE", DbType.String, (object) objEntity.DEN5_CIDADE));
      if (!string.IsNullOrEmpty(objEntity.DEN5_EMAIL))
        command.Parameters.Add((object) objDao.createParameter("@DEN5_EMAIL", DbType.String, (object) objEntity.DEN5_EMAIL));
      if (!string.IsNullOrEmpty(objEntity.DEN5_ESTADO))
        command.Parameters.Add((object) objDao.createParameter("@DEN5_ESTADO", DbType.String, (object) objEntity.DEN5_ESTADO));
      if (!string.IsNullOrEmpty(objEntity.DEN5_NOME))
        command.Parameters.Add((object) objDao.createParameter("@DEN5_NOME", DbType.String, (object) objEntity.DEN5_NOME));
      if (!string.IsNullOrEmpty(objEntity.DEN5_PARTICIPACAO))
        command.Parameters.Add((object) objDao.createParameter("@DEN5_PARTICIPACAO", DbType.String, (object) objEntity.DEN5_PARTICIPACAO));
      if (!string.IsNullOrEmpty(objEntity.DEN5_RELACAO))
        command.Parameters.Add((object) objDao.createParameter("@DEN5_RELACAO", DbType.String, (object) objEntity.DEN5_RELACAO));
      if (!string.IsNullOrEmpty(objEntity.DEN5_TELEFONE))
        command.Parameters.Add((object) objDao.createParameter("@DEN5_TELEFONE", DbType.String, (object) objEntity.DEN5_TELEFONE));
      if (objEntity.DT_ATUALIZACAO > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_ATUALIZACAO", DbType.DateTime, (object) objEntity.DT_ATUALIZACAO));
      if (objEntity.DT_ENCERRAMENTO > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_ENCERRAMENTO", DbType.DateTime, (object) objEntity.DT_ENCERRAMENTO));
      if (objEntity.DT_INICIO > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_INICIO", DbType.DateTime, (object) objEntity.DT_INICIO));
      if (objEntity.DT_MAX > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_MAX", DbType.DateTime, (object) objEntity.DT_MAX));
      if (!string.IsNullOrEmpty(objEntity.EMAIL))
        command.Parameters.Add((object) objDao.createParameter("@EMAIL", DbType.String, (object) objEntity.EMAIL));
      if (!string.IsNullOrEmpty(objEntity.EMAIL_ANONIMO))
        command.Parameters.Add((object) objDao.createParameter("@EMAIL_ANONIMO", DbType.String, (object) objEntity.EMAIL_ANONIMO));
      if (!string.IsNullOrEmpty(objEntity.EXISTE_RECORRENCIA))
        command.Parameters.Add((object) objDao.createParameter("@EXISTE_RECORRENCIA", DbType.String, (object) objEntity.EXISTE_RECORRENCIA));
      if (!string.IsNullOrEmpty(objEntity.FORMA_CONTATO))
        command.Parameters.Add((object) objDao.createParameter("@FORMA_CONTATO", DbType.String, (object) objEntity.FORMA_CONTATO));
      if (!string.IsNullOrEmpty(objEntity.GENERO))
        command.Parameters.Add((object) objDao.createParameter("@GENERO", DbType.String, (object) objEntity.GENERO));
      if (!string.IsNullOrEmpty(objEntity.IDENTIFICADO))
        command.Parameters.Add((object) objDao.createParameter("@IDENTIFICADO", DbType.String, (object) objEntity.IDENTIFICADO));
      if (!string.IsNullOrEmpty(objEntity.IDENTIFICAR_PESSOAS))
        command.Parameters.Add((object) objDao.createParameter("@IDENTIFICAR_PESSOAS", DbType.String, (object) objEntity.IDENTIFICAR_PESSOAS));
      if (!string.IsNullOrEmpty(objEntity.IDIOMA))
        command.Parameters.Add((object) objDao.createParameter("@IDIOMA", DbType.String, (object) objEntity.IDIOMA));
      if (!string.IsNullOrEmpty(objEntity.NATUREZA))
        command.Parameters.Add((object) objDao.createParameter("@NATUREZA", DbType.String, (object) objEntity.NATUREZA));
      if (!string.IsNullOrEmpty(objEntity.NMR_PROTOCOLO))
        command.Parameters.Add((object) objDao.createParameter("@NMR_PROTOCOLO", DbType.String, (object) objEntity.NMR_PROTOCOLO));
      if (!string.IsNullOrEmpty(objEntity.NMR_PROTOCOLO_ANTERIOR))
        command.Parameters.Add((object) objDao.createParameter("@NMR_PROTOCOLO_ANTERIOR", DbType.String, (object) objEntity.NMR_PROTOCOLO_ANTERIOR));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (!string.IsNullOrEmpty(objEntity.PERDA_FINANCEIRA))
        command.Parameters.Add((object) objDao.createParameter("@PERDA_FINANCEIRA", DbType.String, (object) objEntity.PERDA_FINANCEIRA));
      if (!string.IsNullOrEmpty(objEntity.PERIODO_EVENTO_OCORREU))
        command.Parameters.Add((object) objDao.createParameter("@PERIODO_EVENTO_OCORREU", DbType.String, (object) objEntity.PERIODO_EVENTO_OCORREU));
      if (!string.IsNullOrEmpty(objEntity.AREA))
        command.Parameters.Add((object) objDao.createParameter("@AREA", DbType.String, (object) objEntity.AREA));
      command.Parameters.Add((object) objDao.createParameter("@AREA_ID", DbType.Int32, (object) objEntity.AREA_ID));
      if (!string.IsNullOrEmpty(objEntity.EMPRESA))
        command.Parameters.Add((object) objDao.createParameter("@EMPRESA", DbType.String, (object) objEntity.EMPRESA));
      command.Parameters.Add((object) objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object) objEntity.EMPRESA_ID));
      if (!string.IsNullOrEmpty(objEntity.UNIDADE))
        command.Parameters.Add((object) objDao.createParameter("@UNIDADE", DbType.String, (object) objEntity.UNIDADE));
      command.Parameters.Add((object) objDao.createParameter("@UNIDADE_ID", DbType.Int32, (object) objEntity.UNIDADE_ID));
      if (!string.IsNullOrEmpty(objEntity.PRODUTO))
        command.Parameters.Add((object) objDao.createParameter("@PRODUTO", DbType.String, (object) objEntity.PRODUTO));
      command.Parameters.Add((object) objDao.createParameter("@PRODUTO_ID", DbType.Int32, (object) objEntity.PRODUTO_ID));
      if (!string.IsNullOrEmpty(objEntity.QUANTO_AO_RELATO))
        command.Parameters.Add((object) objDao.createParameter("@QUANTO_AO_RELATO", DbType.String, (object) objEntity.QUANTO_AO_RELATO));
      if (!string.IsNullOrEmpty(objEntity.RELATO))
        command.Parameters.Add((object) objDao.createParameter("@RELATO", DbType.String, (object) objEntity.RELATO));
      if (!string.IsNullOrEmpty(objEntity.RELATO_RESUMO))
        command.Parameters.Add((object) objDao.createParameter("@RELATO_RESUMO", DbType.String, (object) objEntity.RELATO_RESUMO));
      if (!string.IsNullOrEmpty(objEntity.RELATO_SUGESTAO))
        command.Parameters.Add((object) objDao.createParameter("@RELATO_SUGESTAO", DbType.String, (object) objEntity.RELATO_SUGESTAO));
      if (!string.IsNullOrEmpty(objEntity.SENHA_ACESSO))
        command.Parameters.Add((object) objDao.createParameter("@SENHA_ACESSO", DbType.String, (object) objEntity.SENHA_ACESSO));
      if (!string.IsNullOrEmpty(objEntity.STATUS))
        command.Parameters.Add((object) objDao.createParameter("@STATUS", DbType.String, (object) objEntity.STATUS));
      if (!string.IsNullOrEmpty(objEntity.TELEFONE))
        command.Parameters.Add((object) objDao.createParameter("@TELEFONE", DbType.String, (object) objEntity.TELEFONE));
      if (!string.IsNullOrEmpty(objEntity.TIPO_MANIFESTACAO))
        command.Parameters.Add((object) objDao.createParameter("@TIPO_MANIFESTACAO", DbType.String, (object) objEntity.TIPO_MANIFESTACAO));
      if (!string.IsNullOrEmpty(objEntity.TIPO_RELATO))
        command.Parameters.Add((object) objDao.createParameter("@TIPO_RELATO", DbType.String, (object) objEntity.TIPO_RELATO));
      if (!string.IsNullOrEmpty(objEntity.TIPO_NATUREZA))
        command.Parameters.Add((object) objDao.createParameter("@TIPO_NATUREZA", DbType.String, (object) objEntity.TIPO_NATUREZA));
      if (!string.IsNullOrEmpty(objEntity.TIPO_PUBLICO))
        command.Parameters.Add((object) objDao.createParameter("@TIPO_PUBLICO", DbType.String, (object) objEntity.TIPO_PUBLICO));
      if (!string.IsNullOrEmpty(objEntity.COMENTARIOS))
        command.Parameters.Add((object) objDao.createParameter("@COMENTARIOS", DbType.String, (object) objEntity.COMENTARIOS));
      if (objEntity.NATUREZA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@NATUREZA_ID", DbType.Int32, (object) objEntity.NATUREZA_ID));
      if (objEntity.TIPONATUREZA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TIPONATUREZA_ID", DbType.Int32, (object) objEntity.TIPONATUREZA_ID));
      if (!string.IsNullOrEmpty(objEntity.HIST_ATEND))
        command.Parameters.Add((object) objDao.createParameter("@HIST_ATENDIMENTO", DbType.String, (object) objEntity.HIST_ATEND));
      if (!string.IsNullOrEmpty(objEntity.FLG_FLUXO))
        command.Parameters.Add((object) objDao.createParameter("@FLG_FLUXO", DbType.String, (object) objEntity.FLG_FLUXO));
      if (!string.IsNullOrEmpty(objEntity.FLG_RESP_PESQ))
        command.Parameters.Add((object) objDao.createParameter("@FLG_RESP_PESQ", DbType.String, (object) objEntity.FLG_RESP_PESQ));
      if (objEntity.SEQ > 0)
        command.Parameters.Add((object) objDao.createParameter("@SEQ", DbType.Int32, (object) objEntity.SEQ));
      if (!string.IsNullOrEmpty(objEntity.EVIDENCIAS))
        command.Parameters.Add((object) objDao.createParameter("@EVIDENCIAS", DbType.String, (object) objEntity.EVIDENCIAS));
      if (!string.IsNullOrEmpty(objEntity.DESCRICAO_DETALHADA))
        command.Parameters.Add((object) objDao.createParameter("@DESC_DETALHADA", DbType.String, (object) objEntity.DESCRICAO_DETALHADA));
      if (!string.IsNullOrEmpty(objEntity.GUID))
        command.Parameters.Add((object) objDao.createParameter("@GUID", DbType.String, (object) objEntity.GUID));
      if (!string.IsNullOrEmpty(objEntity.EMAIL_ID))
        command.Parameters.Add((object) objDao.createParameter("@EMAIL_ID", DbType.String, (object) objEntity.EMAIL_ID));
      if (!string.IsNullOrEmpty(objEntity.RESPONSIBLE))
        command.Parameters.Add((object) objDao.createParameter("@RESPONSIBLE", DbType.String, (object) objEntity.RESPONSIBLE));
      command.Parameters.Add((object) objDao.createParameter("@RELATO_ID", DbType.Int32, (object) objEntity.RELATO_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(RelatoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_relato";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      if (objEntity.RELATO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@RELATO_ID", DbType.Int32, (object) objEntity.RELATO_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<RelatoEntity> FindRelatoOutBwise(ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_relato_outbwise";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      return this.serialize(objDao.ExecuteReader(command));
    }

    public List<RelatoEntity> find(
      RelatoEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_relato";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (!string.IsNullOrEmpty(objEntity.NOME_ATENDENTE))
        command.Parameters.Add((object) objDao.createParameter("@NOME_ATENDENTE", DbType.String, (object) objEntity.NOME_ATENDENTE));
      if (objEntity.EMPRESA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object) objEntity.EMPRESA_ID));
      if (!string.IsNullOrEmpty(objEntity.FORMA_CONTATO))
        command.Parameters.Add((object) objDao.createParameter("@FORMA_CONTATO", DbType.String, (object) objEntity.FORMA_CONTATO));
      if (!string.IsNullOrEmpty(objEntity.IDENTIFICADO))
        command.Parameters.Add((object) objDao.createParameter("@IDENTIFICADO", DbType.String, (object) objEntity.IDENTIFICADO));
      if (!string.IsNullOrEmpty(objEntity.IDIOMA))
        command.Parameters.Add((object) objDao.createParameter("@IDIOMA", DbType.String, (object) objEntity.IDIOMA));
      if (!string.IsNullOrEmpty(objEntity.NMR_PROTOCOLO))
        command.Parameters.Add((object) objDao.createParameter("@NMR_PROTOCOLO", DbType.String, (object) objEntity.NMR_PROTOCOLO));
      if (!string.IsNullOrEmpty(objEntity.NMR_PROTOCOLO_ANTERIOR))
        command.Parameters.Add((object) objDao.createParameter("@NMR_PROTOCOLO_ANTERIOR", DbType.String, (object) objEntity.NMR_PROTOCOLO_ANTERIOR));
      if (objEntity.PRODUTO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@PRODUTO_ID", DbType.Int32, (object) objEntity.PRODUTO_ID));
      if (objEntity.RELATO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@RELATO_ID", DbType.Int32, (object) objEntity.RELATO_ID));
      if (!string.IsNullOrEmpty(objEntity.SENHA_ACESSO))
        command.Parameters.Add((object) objDao.createParameter("@SENHA_ACESSO", DbType.String, (object) objEntity.SENHA_ACESSO));
      if (!string.IsNullOrEmpty(objEntity.STATUS))
        command.Parameters.Add((object) objDao.createParameter("@STATUS", DbType.String, (object) objEntity.STATUS));
      if (!string.IsNullOrEmpty(objEntity.TIPO_MANIFESTACAO))
        command.Parameters.Add((object) objDao.createParameter("@TIPO_MANIFESTACAO", DbType.String, (object) objEntity.TIPO_MANIFESTACAO));
      if (!string.IsNullOrEmpty(objEntity.TIPO_RELATO))
        command.Parameters.Add((object) objDao.createParameter("@TIPO_RELATO", DbType.String, (object) objEntity.TIPO_RELATO));
      if (!string.IsNullOrEmpty(objEntity.TIPO_NATUREZA))
        command.Parameters.Add((object) objDao.createParameter("@TIPO_NATUREZA", DbType.String, (object) objEntity.TIPO_NATUREZA));
      if (!string.IsNullOrEmpty(objEntity.TIPO_PUBLICO))
        command.Parameters.Add((object) objDao.createParameter("@TIPO_PUBLICO", DbType.String, (object) objEntity.TIPO_PUBLICO));
      if (objEntity.UNIDADE_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@UNIDADE_ID", DbType.Int32, (object) objEntity.UNIDADE_ID));
      if (objEntity.NATUREZA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@NATUREZA_ID", DbType.Int32, (object) objEntity.NATUREZA_ID));
      if (objEntity.TIPONATUREZA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TIPONATUREZA_ID", DbType.Int32, (object) objEntity.TIPONATUREZA_ID));
      if (!string.IsNullOrEmpty(objEntity.FLG_FLUXO))
        command.Parameters.Add((object) objDao.createParameter("@FLG_FLUXO", DbType.String, (object) objEntity.FLG_FLUXO));
      if (!string.IsNullOrEmpty(objEntity.FLG_RESP_PESQ))
        command.Parameters.Add((object) objDao.createParameter("@FLG_RESP_PESQ", DbType.String, (object) objEntity.FLG_RESP_PESQ));
      if (!string.IsNullOrEmpty(objEntity.GUID))
        command.Parameters.Add((object) objDao.createParameter("@GUID", DbType.String, (object) objEntity.GUID));
      if (!string.IsNullOrEmpty(objEntity.EMAIL_ID))
        command.Parameters.Add((object) objDao.createParameter("@EMAIL_ID", DbType.String, (object) objEntity.EMAIL_ID));
      return this.serialize(objDao.ExecuteReader(command));
    }

    public RelatoEntity findByPK(
      RelatoEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_relato";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.RELATO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@RELATO_ID", DbType.Int32, (object) objEntity.RELATO_ID));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<RelatoEntity>();
    }

    public RelatoEntity getRelatoByEmail(
      string _email_id,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.Text;
      command.CommandText = "SELECT *, 0 as counter from tbrelato where [EMAIL_ID] ='" + _email_id + "'";
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<RelatoEntity>();
    }

    public int getSequencial(int _empresa_id, ref DataAccessLayerFactory objDao)
    {
      int num = 0;
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.Text;
      command.CommandText = "select max(seq) as SEQ from tbrelato  where empresa_id=" + (object) _empresa_id;
      IDataReader dataReader = objDao.ExecuteReader(command);
      while (dataReader.Read())
      {
        if (dataReader["SEQ"] != DBNull.Value)
          num = Convert.ToInt32(dataReader["SEQ"]);
      }
      dataReader.Close();
      dataReader.Dispose();
      return num;
    }

    private List<RelatoEntity> serialize(IDataReader objReader)
    {
      List<RelatoEntity> relatoEntityList = new List<RelatoEntity>();
      while (objReader.Read())
      {
        RelatoEntity relatoEntity = new RelatoEntity();
        if (objReader["TIPO_PUBLICO"] != DBNull.Value)
          relatoEntity.TIPO_PUBLICO = objReader["TIPO_PUBLICO"].ToString();
        if (objReader["IDENTIFICADO"] != DBNull.Value)
          relatoEntity.IDENTIFICADO = objReader["IDENTIFICADO"].ToString();
        if (objReader["NOME"] != DBNull.Value)
          relatoEntity.NOME = objReader["NOME"].ToString();
        if (objReader["NOME_ATENDENTE"] != DBNull.Value)
          relatoEntity.NOME_ATENDENTE = objReader["NOME_ATENDENTE"].ToString();
        if (objReader["CARGO"] != DBNull.Value)
          relatoEntity.CARGO = objReader["CARGO"].ToString();
        if (objReader["EMAIL"] != DBNull.Value)
          relatoEntity.EMAIL = objReader["EMAIL"].ToString();
        if (objReader["EMAIL_ANONIMO"] != DBNull.Value)
          relatoEntity.EMAIL_ANONIMO = objReader["EMAIL_ANONIMO"].ToString();
        if (objReader["TELEFONE"] != DBNull.Value)
          relatoEntity.TELEFONE = objReader["TELEFONE"].ToString();
        if (objReader["CELULAR"] != DBNull.Value)
          relatoEntity.CELULAR = objReader["CELULAR"].ToString();
        if (objReader["EMPRESA"] != DBNull.Value)
          relatoEntity.EMPRESA = objReader["EMPRESA"].ToString();
        if (objReader["EMPRESA_ID"] != DBNull.Value)
          relatoEntity.EMPRESA_ID = Convert.ToInt32(objReader["EMPRESA_ID"]);
        if (objReader["UNIDADE"] != DBNull.Value)
          relatoEntity.UNIDADE = objReader["UNIDADE"].ToString();
        if (objReader["UNIDADE_ID"] != DBNull.Value)
          relatoEntity.UNIDADE_ID = Convert.ToInt32(objReader["UNIDADE_ID"]);
        if (objReader["PRODUTO"] != DBNull.Value)
          relatoEntity.PRODUTO = objReader["PRODUTO"].ToString();
        if (objReader["PRODUTO_ID"] != DBNull.Value)
          relatoEntity.PRODUTO_ID = Convert.ToInt32(objReader["PRODUTO_ID"]);
        if (objReader["AREA"] != DBNull.Value)
          relatoEntity.AREA = objReader["AREA"].ToString();
        if (objReader["AREA_ID"] != DBNull.Value)
          relatoEntity.AREA_ID = Convert.ToInt32(objReader["AREA_ID"]);
        if (objReader["USER_ID"] != DBNull.Value)
          relatoEntity.USER_ID = Convert.ToInt32(objReader["USER_ID"]);
        if (objReader["USER"] != DBNull.Value)
          relatoEntity.USER = objReader["USER"].ToString();
        if (objReader["IDENTIFICAR_PESSOAS"] != DBNull.Value)
          relatoEntity.IDENTIFICAR_PESSOAS = objReader["IDENTIFICAR_PESSOAS"].ToString();
        if (objReader["DEN1_CARGO"] != DBNull.Value)
          relatoEntity.DEN1_CARGO = objReader["DEN1_CARGO"].ToString();
        if (objReader["DEN1_CIDADE"] != DBNull.Value)
          relatoEntity.DEN1_CIDADE = objReader["DEN1_CIDADE"].ToString();
        if (objReader["DEN1_EMAIL"] != DBNull.Value)
          relatoEntity.DEN1_EMAIL = objReader["DEN1_EMAIL"].ToString();
        if (objReader["DEN1_ESTADO"] != DBNull.Value)
          relatoEntity.DEN1_ESTADO = objReader["DEN1_ESTADO"].ToString();
        if (objReader["DEN1_NOME"] != DBNull.Value)
          relatoEntity.DEN1_NOME = objReader["DEN1_NOME"].ToString();
        if (objReader["DEN1_PARTICIPACAO"] != DBNull.Value)
          relatoEntity.DEN1_PARTICIPACAO = objReader["DEN1_PARTICIPACAO"].ToString();
        if (objReader["DEN1_RELACAO"] != DBNull.Value)
          relatoEntity.DEN1_RELACAO = objReader["DEN1_RELACAO"].ToString();
        if (objReader["DEN1_TELEFONE"] != DBNull.Value)
          relatoEntity.DEN1_TELEFONE = objReader["DEN1_TELEFONE"].ToString();
        if (objReader["DEN2_CARGO"] != DBNull.Value)
          relatoEntity.DEN2_CARGO = objReader["DEN2_CARGO"].ToString();
        if (objReader["DEN2_CIDADE"] != DBNull.Value)
          relatoEntity.DEN2_CIDADE = objReader["DEN2_CIDADE"].ToString();
        if (objReader["DEN2_EMAIL"] != DBNull.Value)
          relatoEntity.DEN2_EMAIL = objReader["DEN2_EMAIL"].ToString();
        if (objReader["DEN2_ESTADO"] != DBNull.Value)
          relatoEntity.DEN2_ESTADO = objReader["DEN2_ESTADO"].ToString();
        if (objReader["DEN2_NOME"] != DBNull.Value)
          relatoEntity.DEN2_NOME = objReader["DEN2_NOME"].ToString();
        if (objReader["DEN2_PARTICIPACAO"] != DBNull.Value)
          relatoEntity.DEN2_PARTICIPACAO = objReader["DEN2_PARTICIPACAO"].ToString();
        if (objReader["DEN2_RELACAO"] != DBNull.Value)
          relatoEntity.DEN2_RELACAO = objReader["DEN2_RELACAO"].ToString();
        if (objReader["DEN2_TELEFONE"] != DBNull.Value)
          relatoEntity.DEN2_TELEFONE = objReader["DEN2_TELEFONE"].ToString();
        if (objReader["DEN3_CARGO"] != DBNull.Value)
          relatoEntity.DEN3_CARGO = objReader["DEN3_CARGO"].ToString();
        if (objReader["DEN3_CIDADE"] != DBNull.Value)
          relatoEntity.DEN3_CIDADE = objReader["DEN3_CIDADE"].ToString();
        if (objReader["DEN3_EMAIL"] != DBNull.Value)
          relatoEntity.DEN3_EMAIL = objReader["DEN3_EMAIL"].ToString();
        if (objReader["DEN3_ESTADO"] != DBNull.Value)
          relatoEntity.DEN3_ESTADO = objReader["DEN3_ESTADO"].ToString();
        if (objReader["DEN3_NOME"] != DBNull.Value)
          relatoEntity.DEN3_NOME = objReader["DEN3_NOME"].ToString();
        if (objReader["DEN3_PARTICIPACAO"] != DBNull.Value)
          relatoEntity.DEN3_PARTICIPACAO = objReader["DEN3_PARTICIPACAO"].ToString();
        if (objReader["DEN3_RELACAO"] != DBNull.Value)
          relatoEntity.DEN3_RELACAO = objReader["DEN3_RELACAO"].ToString();
        if (objReader["DEN3_TELEFONE"] != DBNull.Value)
          relatoEntity.DEN3_TELEFONE = objReader["DEN3_TELEFONE"].ToString();
        if (objReader["DEN4_CARGO"] != DBNull.Value)
          relatoEntity.DEN4_CARGO = objReader["DEN4_CARGO"].ToString();
        if (objReader["DEN4_CIDADE"] != DBNull.Value)
          relatoEntity.DEN4_CIDADE = objReader["DEN4_CIDADE"].ToString();
        if (objReader["DEN4_EMAIL"] != DBNull.Value)
          relatoEntity.DEN4_EMAIL = objReader["DEN4_EMAIL"].ToString();
        if (objReader["DEN4_ESTADO"] != DBNull.Value)
          relatoEntity.DEN4_ESTADO = objReader["DEN4_ESTADO"].ToString();
        if (objReader["DEN4_NOME"] != DBNull.Value)
          relatoEntity.DEN4_NOME = objReader["DEN4_NOME"].ToString();
        if (objReader["DEN4_PARTICIPACAO"] != DBNull.Value)
          relatoEntity.DEN4_PARTICIPACAO = objReader["DEN4_PARTICIPACAO"].ToString();
        if (objReader["DEN4_RELACAO"] != DBNull.Value)
          relatoEntity.DEN4_RELACAO = objReader["DEN4_RELACAO"].ToString();
        if (objReader["DEN4_TELEFONE"] != DBNull.Value)
          relatoEntity.DEN4_TELEFONE = objReader["DEN4_TELEFONE"].ToString();
        if (objReader["DEN5_CARGO"] != DBNull.Value)
          relatoEntity.DEN5_CARGO = objReader["DEN5_CARGO"].ToString();
        if (objReader["DEN5_CIDADE"] != DBNull.Value)
          relatoEntity.DEN5_CIDADE = objReader["DEN5_CIDADE"].ToString();
        if (objReader["DEN5_EMAIL"] != DBNull.Value)
          relatoEntity.DEN5_EMAIL = objReader["DEN5_EMAIL"].ToString();
        if (objReader["DEN5_ESTADO"] != DBNull.Value)
          relatoEntity.DEN5_ESTADO = objReader["DEN5_ESTADO"].ToString();
        if (objReader["DEN5_NOME"] != DBNull.Value)
          relatoEntity.DEN5_NOME = objReader["DEN5_NOME"].ToString();
        if (objReader["DEN5_PARTICIPACAO"] != DBNull.Value)
          relatoEntity.DEN5_PARTICIPACAO = objReader["DEN5_PARTICIPACAO"].ToString();
        if (objReader["DEN5_RELACAO"] != DBNull.Value)
          relatoEntity.DEN5_RELACAO = objReader["DEN5_RELACAO"].ToString();
        if (objReader["DEN5_TELEFONE"] != DBNull.Value)
          relatoEntity.DEN5_TELEFONE = objReader["DEN5_TELEFONE"].ToString();
        if (objReader["TIPO_MANIFESTACAO"] != DBNull.Value)
          relatoEntity.TIPO_MANIFESTACAO = objReader["TIPO_MANIFESTACAO"].ToString();
        if (objReader["TIPO_RELATO"] != DBNull.Value)
          relatoEntity.TIPO_RELATO = objReader["TIPO_RELATO"].ToString();
        if (objReader["DT_INICIO"] != DBNull.Value)
          relatoEntity.DT_INICIO = Convert.ToDateTime(objReader["DT_INICIO"]);
        if (objReader["RELATO_ID"] != DBNull.Value)
          relatoEntity.RELATO_ID = Convert.ToInt32(objReader["RELATO_ID"]);
        if (objReader["RELATO"] != DBNull.Value)
          relatoEntity.RELATO = objReader["RELATO"].ToString();
        if (objReader["NMR_PROTOCOLO_ANTERIOR"] != DBNull.Value)
          relatoEntity.NMR_PROTOCOLO_ANTERIOR = objReader["NMR_PROTOCOLO_ANTERIOR"].ToString();
        if (objReader["PERIODO_EVENTO_OCORREU"] != DBNull.Value)
          relatoEntity.PERIODO_EVENTO_OCORREU = objReader["PERIODO_EVENTO_OCORREU"].ToString();
        if (objReader["EXISTE_RECORRENCIA"] != DBNull.Value)
          relatoEntity.EXISTE_RECORRENCIA = objReader["EXISTE_RECORRENCIA"].ToString();
        if (objReader["COMO_FICOU_CIENTE"] != DBNull.Value)
          relatoEntity.COMO_FICOU_CIENTE = objReader["COMO_FICOU_CIENTE"].ToString();
        if (objReader["PERDA_FINANCEIRA"] != DBNull.Value)
          relatoEntity.PERDA_FINANCEIRA = objReader["PERDA_FINANCEIRA"].ToString();
        if (objReader["RELATO_SUGESTAO"] != DBNull.Value)
          relatoEntity.RELATO_SUGESTAO = objReader["RELATO_SUGESTAO"].ToString();
        if (objReader["QUANTO_AO_RELATO"] != DBNull.Value)
          relatoEntity.QUANTO_AO_RELATO = objReader["QUANTO_AO_RELATO"].ToString();
        if (objReader["IDIOMA"] != DBNull.Value)
          relatoEntity.IDIOMA = objReader["IDIOMA"].ToString();
        if (objReader["NMR_PROTOCOLO"] != DBNull.Value)
          relatoEntity.NMR_PROTOCOLO = objReader["NMR_PROTOCOLO"].ToString();
        if (objReader["SENHA_ACESSO"] != DBNull.Value)
          relatoEntity.SENHA_ACESSO = objReader["SENHA_ACESSO"].ToString();
        if (objReader["STATUS"] != DBNull.Value)
          relatoEntity.STATUS = objReader["STATUS"].ToString();
        if (objReader["ANIMO"] != DBNull.Value)
          relatoEntity.ANIMO = objReader["ANIMO"].ToString();
        if (objReader["FORMA_CONTATO"] != DBNull.Value)
          relatoEntity.FORMA_CONTATO = objReader["FORMA_CONTATO"].ToString();
        if (objReader["GENERO"] != DBNull.Value)
          relatoEntity.GENERO = objReader["GENERO"].ToString();
        if (objReader["RELATO_RESUMO"] != DBNull.Value)
          relatoEntity.RELATO_RESUMO = objReader["RELATO_RESUMO"].ToString();
        if (objReader["CRITICIDADE"] != DBNull.Value)
          relatoEntity.CRITICIDADE = objReader["CRITICIDADE"].ToString();
        if (objReader["NATUREZA"] != DBNull.Value)
          relatoEntity.NATUREZA = objReader["NATUREZA"].ToString();
        if (objReader["TIPO_NATUREZA"] != DBNull.Value)
          relatoEntity.TIPO_NATUREZA = objReader["TIPO_NATUREZA"].ToString();
        if (objReader["DT_MAX"] != DBNull.Value)
          relatoEntity.DT_MAX = Convert.ToDateTime(objReader["DT_MAX"]);
        if (objReader["COMENTARIOS"] != DBNull.Value)
          relatoEntity.COMENTARIOS = objReader["COMENTARIOS"].ToString();
        if (objReader["HIST_ATENDIMENTO"] != DBNull.Value)
          relatoEntity.HIST_ATEND = objReader["HIST_ATENDIMENTO"].ToString();
        if (objReader["NATUREZA_ID"] != DBNull.Value)
          relatoEntity.NATUREZA_ID = Convert.ToInt32(objReader["NATUREZA_ID"]);
        if (objReader["TIPONATUREZA_ID"] != DBNull.Value)
          relatoEntity.TIPONATUREZA_ID = Convert.ToInt32(objReader["TIPONATUREZA_ID"]);
        if (objReader["DT_ENCERRAMENTO"] != DBNull.Value)
          relatoEntity.DT_ENCERRAMENTO = Convert.ToDateTime(objReader["DT_ENCERRAMENTO"]);
        if (objReader["DT_ATUALIZACAO"] != DBNull.Value)
          relatoEntity.DT_ATUALIZACAO = Convert.ToDateTime(objReader["DT_ATUALIZACAO"]);
        if (objReader["COUNTER"] != DBNull.Value)
          relatoEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        if (objReader["FLG_FLUXO"] != DBNull.Value)
          relatoEntity.FLG_FLUXO = objReader["FLG_FLUXO"].ToString();
        if (objReader["FLG_RESP_PESQ"] != DBNull.Value)
          relatoEntity.FLG_RESP_PESQ = objReader["FLG_RESP_PESQ"].ToString();
        if (objReader["SEQ"] != DBNull.Value)
          relatoEntity.SEQ = Convert.ToInt32(objReader["SEQ"]);
        if (objReader["EVIDENCIAS"] != DBNull.Value)
          relatoEntity.EVIDENCIAS = objReader["EVIDENCIAS"].ToString();
        if (objReader["DESC_DETALHADA"] != DBNull.Value)
          relatoEntity.DESCRICAO_DETALHADA = objReader["DESC_DETALHADA"].ToString();
        if (objReader["GUID"] != DBNull.Value)
          relatoEntity.GUID = objReader["GUID"].ToString();
        if (objReader["EMAIL_ID"] != DBNull.Value)
          relatoEntity.EMAIL_ID = objReader["EMAIL_ID"].ToString();
        if (objReader["RESPONSIBLE"] != DBNull.Value)
          relatoEntity.RESPONSIBLE = objReader["RESPONSIBLE"].ToString();
        relatoEntityList.Add(relatoEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return relatoEntityList;
    }
  }
}
