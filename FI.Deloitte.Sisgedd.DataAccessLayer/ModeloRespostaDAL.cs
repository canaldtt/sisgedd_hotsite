﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.ModeloRespostaDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class ModeloRespostaDAL
  {
    public int add(ModeloRespostaEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_modeloresposta";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      if (objEntity.MODELO_PERGUNTA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@MODELO_PERGUNTA_ID", DbType.Int32, (object) objEntity.MODELO_PERGUNTA_ID));
      if (!string.IsNullOrEmpty(objEntity.NMR_PROTOCOLO))
        command.Parameters.Add((object) objDao.createParameter("@NMR_PROTOCOLO", DbType.String, (object) objEntity.NMR_PROTOCOLO));
      if (!string.IsNullOrEmpty(objEntity.RESPOSTA))
        command.Parameters.Add((object) objDao.createParameter("@RESPOSTA", DbType.String, (object) objEntity.RESPOSTA));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool update(ModeloRespostaEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_modeloresposta";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      command.Parameters.Add((object) objDao.createParameter("@MODELO_RESPOSTA_ID", DbType.Int32, (object) objEntity.MODELO_RESPOSTA_ID));
      if (objEntity.MODELO_PERGUNTA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@MODELO_PERGUNTA_ID", DbType.Int32, (object) objEntity.MODELO_PERGUNTA_ID));
      if (!string.IsNullOrEmpty(objEntity.NMR_PROTOCOLO))
        command.Parameters.Add((object) objDao.createParameter("@NMR_PROTOCOLO", DbType.String, (object) objEntity.NMR_PROTOCOLO));
      if (!string.IsNullOrEmpty(objEntity.RESPOSTA))
        command.Parameters.Add((object) objDao.createParameter("@RESPOSTA", DbType.String, (object) objEntity.RESPOSTA));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(ModeloRespostaEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_modeloresposta";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      if (objEntity.MODELO_PERGUNTA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@MODELO_PERGUNTA_ID", DbType.Int32, (object) objEntity.MODELO_PERGUNTA_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<ModeloRespostaEntity> find(
      ModeloRespostaEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_modeloresposta";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.MODELO_PERGUNTA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@MODELO_PERGUNTA_ID", DbType.Int32, (object) objEntity.MODELO_PERGUNTA_ID));
      if (!string.IsNullOrEmpty(objEntity.NMR_PROTOCOLO))
        command.Parameters.Add((object) objDao.createParameter("@NMR_PROTOCOLO", DbType.String, (object) objEntity.NMR_PROTOCOLO));
      if (!string.IsNullOrEmpty(objEntity.RESPOSTA))
        command.Parameters.Add((object) objDao.createParameter("@RESPOSTA", DbType.String, (object) objEntity.RESPOSTA));
      return this.serialize(objDao.ExecuteReader(command));
    }

    public ModeloRespostaEntity findByPK(
      ModeloRespostaEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_modeloresposta";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.MODELO_RESPOSTA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@MODELO_RESPOSTA_ID", DbType.Int32, (object) objEntity.MODELO_RESPOSTA_ID));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<ModeloRespostaEntity>();
    }

    private List<ModeloRespostaEntity> serialize(IDataReader objReader)
    {
      List<ModeloRespostaEntity> modeloRespostaEntityList = new List<ModeloRespostaEntity>();
      while (objReader.Read())
      {
        ModeloRespostaEntity modeloRespostaEntity = new ModeloRespostaEntity();
        if (objReader["MODELO_RESPOSTA_ID"] != DBNull.Value)
          modeloRespostaEntity.MODELO_RESPOSTA_ID = Convert.ToInt32(objReader["MODELO_RESPOSTA_ID"]);
        if (objReader["MODELO_PERGUNTA_ID"] != DBNull.Value)
          modeloRespostaEntity.MODELO_PERGUNTA_ID = Convert.ToInt32(objReader["MODELO_PERGUNTA_ID"]);
        if (objReader["NMR_PROTOCOLO"] != DBNull.Value)
          modeloRespostaEntity.NMR_PROTOCOLO = objReader["NMR_PROTOCOLO"].ToString();
        if (objReader["RESPOSTA"] != DBNull.Value)
          modeloRespostaEntity.RESPOSTA = objReader["RESPOSTA"].ToString();
        if (objReader["COUNTER"] != DBNull.Value)
          modeloRespostaEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        modeloRespostaEntityList.Add(modeloRespostaEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return modeloRespostaEntityList;
    }
  }
}
