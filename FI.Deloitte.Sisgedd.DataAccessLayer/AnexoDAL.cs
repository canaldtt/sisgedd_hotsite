﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.AnexoDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class AnexoDAL
  {
    public int add(AnexoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_anexo";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      if (objEntity.RELATO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@RELATO_ID", DbType.Int32, (object) objEntity.RELATO_ID));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (!string.IsNullOrEmpty(objEntity.GUID))
        command.Parameters.Add((object) objDao.createParameter("@GUID", DbType.String, (object) objEntity.GUID));
      if (!string.IsNullOrEmpty(objEntity.URL))
        command.Parameters.Add((object) objDao.createParameter("@URL", DbType.String, (object) objEntity.URL));
      if (!string.IsNullOrEmpty(objEntity.PROTOCOLO))
        command.Parameters.Add((object) objDao.createParameter("@PROTOCOLO", DbType.String, (object) objEntity.PROTOCOLO));
      if (objEntity.DT_ANEXO > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_ANEXO", DbType.DateTime, (object) objEntity.DT_ANEXO));
      command.Parameters.Add((object) objDao.createParameter("@STATUS", DbType.Boolean, (object) objEntity.STATUS));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool update(AnexoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_anexo";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      if (objEntity.ANEXO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@ANEXO_ID", DbType.Int32, (object) objEntity.ANEXO_ID));
      if (objEntity.RELATO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@RELATO_ID", DbType.Int32, (object) objEntity.RELATO_ID));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (!string.IsNullOrEmpty(objEntity.GUID))
        command.Parameters.Add((object) objDao.createParameter("@GUID", DbType.String, (object) objEntity.GUID));
      if (!string.IsNullOrEmpty(objEntity.URL))
        command.Parameters.Add((object) objDao.createParameter("@URL", DbType.String, (object) objEntity.URL));
      if (!string.IsNullOrEmpty(objEntity.PROTOCOLO))
        command.Parameters.Add((object) objDao.createParameter("@PROTOCOLO", DbType.String, (object) objEntity.PROTOCOLO));
      if (objEntity.DT_ANEXO > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_ANEXO", DbType.DateTime, (object) objEntity.DT_ANEXO));
      command.Parameters.Add((object) objDao.createParameter("@STATUS", DbType.Boolean, (object) objEntity.STATUS));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(AnexoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_anexo";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      if (objEntity.ANEXO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@ANEXO_ID", DbType.Int32, (object) objEntity.ANEXO_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<AnexoEntity> find(
      AnexoEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_anexo";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.RELATO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@RELATO_ID", DbType.Int32, (object) objEntity.RELATO_ID));
      if (!string.IsNullOrEmpty(objEntity.NOME))
        command.Parameters.Add((object) objDao.createParameter("@NOME", DbType.String, (object) objEntity.NOME));
      if (!string.IsNullOrEmpty(objEntity.URL))
        command.Parameters.Add((object) objDao.createParameter("@URL", DbType.String, (object) objEntity.URL));
      if (!string.IsNullOrEmpty(objEntity.PROTOCOLO))
        command.Parameters.Add((object) objDao.createParameter("@PROTOCOLO", DbType.String, (object) objEntity.PROTOCOLO));
      if (!string.IsNullOrEmpty(objEntity.GUID))
        command.Parameters.Add((object) objDao.createParameter("@GUID", DbType.String, (object) objEntity.GUID));
      if (objEntity.STATUS)
        command.Parameters.Add((object) objDao.createParameter("@STATUS", DbType.Boolean, (object) objEntity.STATUS));
      return this.serialize(objDao.ExecuteReader(command));
    }

    public AnexoEntity findByPK(AnexoEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_anexo";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.ANEXO_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@ANEXO_ID", DbType.Int32, (object) objEntity.ANEXO_ID));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<AnexoEntity>();
    }

    private List<AnexoEntity> serialize(IDataReader objReader)
    {
      List<AnexoEntity> anexoEntityList = new List<AnexoEntity>();
      while (objReader.Read())
      {
        AnexoEntity anexoEntity = new AnexoEntity();
        if (objReader["ANEXO_ID"] != DBNull.Value)
          anexoEntity.ANEXO_ID = Convert.ToInt32(objReader["ANEXO_ID"]);
        if (objReader["RELATO_ID"] != DBNull.Value)
          anexoEntity.RELATO_ID = Convert.ToInt32(objReader["RELATO_ID"]);
        if (objReader["NOME"] != DBNull.Value)
          anexoEntity.NOME = objReader["NOME"].ToString();
        if (objReader["URL"] != DBNull.Value)
          anexoEntity.URL = objReader["URL"].ToString();
        if (objReader["PROTOCOLO"] != DBNull.Value)
          anexoEntity.PROTOCOLO = objReader["PROTOCOLO"].ToString();
        if (objReader["DT_ANEXO"] != DBNull.Value)
          anexoEntity.DT_ANEXO = Convert.ToDateTime(objReader["DT_ANEXO"]);
        if (objReader["STATUS"] != DBNull.Value)
          anexoEntity.STATUS = Convert.ToBoolean(objReader["STATUS"]);
        if (objReader["GUID"] != DBNull.Value)
          anexoEntity.GUID = objReader["GUID"].ToString();
        if (objReader["COUNTER"] != DBNull.Value)
          anexoEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        anexoEntityList.Add(anexoEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return anexoEntityList;
    }
  }
}
