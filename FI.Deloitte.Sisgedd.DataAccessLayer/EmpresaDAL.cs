﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.EmpresaDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
    public class EmpresaDAL
    {
        public int add(EmpresaEntity objEntity, ref DataAccessLayerFactory objDao)
        {
            IDbCommand command = objDao.createCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "pr_empresa";
            command.Parameters.Add((object)objDao.createParameter("@ACTION", DbType.String, (object)"I"));
            if (objEntity.MODELO_ID > 0)
                command.Parameters.Add((object)objDao.createParameter("@MODELO_ID", DbType.Int32, (object)objEntity.MODELO_ID));
            if (!string.IsNullOrEmpty(objEntity.NOME))
                command.Parameters.Add((object)objDao.createParameter("@NOME", DbType.String, (object)objEntity.NOME));
            if (!string.IsNullOrEmpty(objEntity.TITULO))
                command.Parameters.Add((object)objDao.createParameter("@TITULO", DbType.String, (object)objEntity.TITULO));
            if (!string.IsNullOrEmpty(objEntity.TITULO_EN))
                command.Parameters.Add((object)objDao.createParameter("@TITULO_EN", DbType.String, (object)objEntity.TITULO_EN));
            if (!string.IsNullOrEmpty(objEntity.TITULO_ES))
                command.Parameters.Add((object)objDao.createParameter("@TITULO_ES", DbType.String, (object)objEntity.TITULO_ES));
            if (!string.IsNullOrEmpty(objEntity.TITULO_FR))
                command.Parameters.Add((object)objDao.createParameter("@TITULO_FR", DbType.String, (object)objEntity.TITULO_FR));
            if (!string.IsNullOrEmpty(objEntity.LOGO))
                command.Parameters.Add((object)objDao.createParameter("@LOGO", DbType.String, (object)objEntity.LOGO));
            if (!string.IsNullOrEmpty(objEntity.PREFIXO))
                command.Parameters.Add((object)objDao.createParameter("@PREFIXO", DbType.String, (object)objEntity.PREFIXO));
            if (!string.IsNullOrEmpty(objEntity.EXIBE_AREA))
                command.Parameters.Add((object)objDao.createParameter("@EXIBE_AREA", DbType.String, (object)objEntity.EXIBE_AREA));
            if (!string.IsNullOrEmpty(objEntity.FLG_ATIVA))
                command.Parameters.Add((object)objDao.createParameter("@FLG_ATIVA", DbType.String, (object)objEntity.FLG_ATIVA));
            if (!string.IsNullOrEmpty(objEntity.EXIBE_PRODUTO))
                command.Parameters.Add((object)objDao.createParameter("@EXIBE_PRODUTO", DbType.String, (object)objEntity.EXIBE_PRODUTO));
            if (!string.IsNullOrEmpty(objEntity.EXIBE_UNIDADE))
                command.Parameters.Add((object)objDao.createParameter("@EXIBE_UNIDADE", DbType.String, (object)objEntity.EXIBE_UNIDADE));
            if (!string.IsNullOrEmpty(objEntity.EMAIL))
                command.Parameters.Add((object)objDao.createParameter("@EMAIL", DbType.String, (object)objEntity.EMAIL));
            return Convert.ToInt32(objDao.ExecuteScalar(command));
        }

        public bool update(EmpresaEntity objEntity, ref DataAccessLayerFactory objDao)
        {
            IDbCommand command = objDao.createCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "pr_empresa";
            command.Parameters.Add((object)objDao.createParameter("@ACTION", DbType.String, (object)"U"));
            command.Parameters.Add((object)objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object)objEntity.EMPRESA_ID));
            if (!string.IsNullOrEmpty(objEntity.NOME))
                command.Parameters.Add((object)objDao.createParameter("@NOME", DbType.String, (object)objEntity.NOME));
            if (!string.IsNullOrEmpty(objEntity.TITULO))
                command.Parameters.Add((object)objDao.createParameter("@TITULO", DbType.String, (object)objEntity.TITULO));
            if (!string.IsNullOrEmpty(objEntity.TITULO_EN))
                command.Parameters.Add((object)objDao.createParameter("@TITULO_EN", DbType.String, (object)objEntity.TITULO_EN));
            if (!string.IsNullOrEmpty(objEntity.TITULO_ES))
                command.Parameters.Add((object)objDao.createParameter("@TITULO_ES", DbType.String, (object)objEntity.TITULO_ES));
            if (!string.IsNullOrEmpty(objEntity.TITULO_FR))
                command.Parameters.Add((object)objDao.createParameter("@TITULO_FR", DbType.String, (object)objEntity.TITULO_FR));
            if (!string.IsNullOrEmpty(objEntity.LOGO))
                command.Parameters.Add((object)objDao.createParameter("@LOGO", DbType.String, (object)objEntity.LOGO));
            if (!string.IsNullOrEmpty(objEntity.PREFIXO))
                command.Parameters.Add((object)objDao.createParameter("@PREFIXO", DbType.String, (object)objEntity.PREFIXO));
            if (!string.IsNullOrEmpty(objEntity.EXIBE_AREA))
                command.Parameters.Add((object)objDao.createParameter("@EXIBE_AREA", DbType.String, (object)objEntity.EXIBE_AREA));
            if (!string.IsNullOrEmpty(objEntity.FLG_ATIVA))
                command.Parameters.Add((object)objDao.createParameter("@FLG_ATIVA", DbType.String, (object)objEntity.FLG_ATIVA));
            if (!string.IsNullOrEmpty(objEntity.EXIBE_PRODUTO))
                command.Parameters.Add((object)objDao.createParameter("@EXIBE_PRODUTO", DbType.String, (object)objEntity.EXIBE_PRODUTO));
            if (!string.IsNullOrEmpty(objEntity.EXIBE_UNIDADE))
                command.Parameters.Add((object)objDao.createParameter("@EXIBE_UNIDADE", DbType.String, (object)objEntity.EXIBE_UNIDADE));
            if (!string.IsNullOrEmpty(objEntity.EMAIL))
                command.Parameters.Add((object)objDao.createParameter("@EMAIL", DbType.String, (object)objEntity.EMAIL));
            if (objEntity.MODELO_ID > 0)
                command.Parameters.Add((object)objDao.createParameter("@MODELO_ID", DbType.Int32, (object)objEntity.MODELO_ID));
            return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
        }

        public bool remove(EmpresaEntity objEntity, ref DataAccessLayerFactory objDao)
        {
            IDbCommand command = objDao.createCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "pr_empresa";
            command.Parameters.Add((object)objDao.createParameter("@ACTION", DbType.String, (object)"D"));
            if (objEntity.EMPRESA_ID > 0)
                command.Parameters.Add((object)objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object)objEntity.EMPRESA_ID));
            return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
        }

        public List<EmpresaEntity> find(
          EmpresaEntity objEntity,
          ref DataAccessLayerFactory objDao)
        {
            IDbCommand command = objDao.createCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "pr_empresa";
            command.Parameters.Add((object)objDao.createParameter("@ACTION", DbType.String, (object)"S"));
            command.Parameters.Add((object)objDao.createParameter("@PAGEINDEX", DbType.Int32, (object)objEntity.PAGEINDEX));
            command.Parameters.Add((object)objDao.createParameter("@PAGESIZE", DbType.Int32, (object)objEntity.PAGESIZE));
            if (objEntity.EMPRESA_ID > 0)
                command.Parameters.Add((object)objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object)objEntity.EMPRESA_ID));
            if (!string.IsNullOrEmpty(objEntity.NOME))
                command.Parameters.Add((object)objDao.createParameter("@NOME", DbType.String, (object)objEntity.NOME));
            if (!string.IsNullOrEmpty(objEntity.TITULO))
                command.Parameters.Add((object)objDao.createParameter("@TITULO", DbType.String, (object)objEntity.TITULO));
            if (!string.IsNullOrEmpty(objEntity.LOGO))
                command.Parameters.Add((object)objDao.createParameter("@LOGO", DbType.String, (object)objEntity.LOGO));
            if (!string.IsNullOrEmpty(objEntity.PREFIXO))
                command.Parameters.Add((object)objDao.createParameter("@PREFIXO", DbType.String, (object)objEntity.PREFIXO));
            if (!string.IsNullOrEmpty(objEntity.EXIBE_AREA))
                command.Parameters.Add((object)objDao.createParameter("@EXIBE_AREA", DbType.String, (object)objEntity.EXIBE_AREA));
            if (!string.IsNullOrEmpty(objEntity.FLG_ATIVA))
                command.Parameters.Add((object)objDao.createParameter("@FLG_ATIVA", DbType.String, (object)objEntity.FLG_ATIVA));
            if (!string.IsNullOrEmpty(objEntity.EXIBE_PRODUTO))
                command.Parameters.Add((object)objDao.createParameter("@EXIBE_PRODUTO", DbType.String, (object)objEntity.EXIBE_PRODUTO));
            if (!string.IsNullOrEmpty(objEntity.EXIBE_UNIDADE))
                command.Parameters.Add((object)objDao.createParameter("@EXIBE_UNIDADE", DbType.String, (object)objEntity.EXIBE_UNIDADE));
            if (!string.IsNullOrEmpty(objEntity.EMAIL))
                command.Parameters.Add((object)objDao.createParameter("@EMAIL", DbType.String, (object)objEntity.EMAIL));
            if (objEntity.MODELO_ID > 0)
                command.Parameters.Add((object)objDao.createParameter("@MODELO_ID", DbType.Int32, (object)objEntity.MODELO_ID));
            return this.Mapper(objDao.ExecuteReader(command));
        }

        public EmpresaEntity findByPK(
          EmpresaEntity objEntity,
          ref DataAccessLayerFactory objDao)
        {
            IDbCommand command = objDao.createCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "pr_empresa";
            command.Parameters.Add((object)objDao.createParameter("@ACTION", DbType.String, (object)"S"));
            command.Parameters.Add((object)objDao.createParameter("@PAGEINDEX", DbType.Int32, (object)objEntity.PAGEINDEX));
            command.Parameters.Add((object)objDao.createParameter("@PAGESIZE", DbType.Int32, (object)objEntity.PAGESIZE));
            command.Parameters.Add((object)objDao.createParameter("@EMPRESA_ID", DbType.Int32, (object)objEntity.EMPRESA_ID));
            return this.Mapper(objDao.ExecuteReader(command)).FirstOrDefault<EmpresaEntity>();
        }

        private List<EmpresaEntity> Mapper(IDataReader objReader)
        {
            List<EmpresaEntity> empresaEntityList = new List<EmpresaEntity>();
            while (objReader.Read())
            {
                EmpresaEntity empresaEntity = new EmpresaEntity();
                if (objReader["EMPRESA_ID"] != DBNull.Value)
                    empresaEntity.EMPRESA_ID = Convert.ToInt32(objReader["EMPRESA_ID"]);
                if (objReader["NOME"] != DBNull.Value)
                    empresaEntity.NOME = objReader["NOME"].ToString();
                if (objReader["TITULO"] != DBNull.Value)
                    empresaEntity.TITULO = objReader["TITULO"].ToString();
                if (objReader["TITULO_EN"] != DBNull.Value)
                    empresaEntity.TITULO_EN = objReader["TITULO_EN"].ToString();
                if (objReader["TITULO_ES"] != DBNull.Value)
                    empresaEntity.TITULO_ES = objReader["TITULO_ES"].ToString();
                if (objReader["TITULO_FR"] != DBNull.Value)
                    empresaEntity.TITULO_FR = objReader["TITULO_FR"].ToString();
                if (objReader["LOGO"] != DBNull.Value)
                    empresaEntity.LOGO = objReader["LOGO"].ToString();
                if (objReader["EXIBE_UNIDADE"] != DBNull.Value)
                    empresaEntity.EXIBE_UNIDADE = objReader["EXIBE_UNIDADE"].ToString();
                if (objReader["EXIBE_PRODUTO"] != DBNull.Value)
                    empresaEntity.EXIBE_PRODUTO = objReader["EXIBE_PRODUTO"].ToString();
                if (objReader["FLG_ATIVA"] != DBNull.Value)
                    empresaEntity.FLG_ATIVA = objReader["FLG_ATIVA"].ToString();
                if (objReader["EXIBE_AREA"] != DBNull.Value)
                    empresaEntity.EXIBE_AREA = objReader["EXIBE_AREA"].ToString();
                if (objReader["PREFIXO"] != DBNull.Value)
                    empresaEntity.PREFIXO = objReader["PREFIXO"].ToString();
                if (objReader["EMAIL"] != DBNull.Value)
                    empresaEntity.EMAIL = objReader["EMAIL"].ToString();
                if (objReader["MODELO_ID"] != DBNull.Value)
                    empresaEntity.MODELO_ID = Convert.ToInt32(objReader["MODELO_ID"]);
                if (objReader["COUNTER"] != DBNull.Value)
                    empresaEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
                empresaEntityList.Add(empresaEntity);
            }
            objReader.Close();
            objReader.Dispose();
            return empresaEntityList;
        }
    }
}
