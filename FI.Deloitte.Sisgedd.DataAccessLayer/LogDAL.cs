﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.LogDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class LogDAL
  {
    public int add(LogEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_log";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      if (!string.IsNullOrEmpty(objEntity.Mensagem))
        command.Parameters.Add((object) objDao.createParameter("@ACAO", DbType.String, (object) objEntity.Mensagem));
      if (objEntity.DT_LOG > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_LOG", DbType.DateTime, (object) objEntity.DT_LOG));
      if (objEntity.USER_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@USER_ID", DbType.Int32, (object) objEntity.USER_ID));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool update(LogEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_log";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      if (!string.IsNullOrEmpty(objEntity.Mensagem))
        command.Parameters.Add((object) objDao.createParameter("@ACAO", DbType.String, (object) objEntity.Mensagem));
      if (objEntity.DT_LOG > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_LOG", DbType.DateTime, (object) objEntity.DT_LOG));
      if (objEntity.USER_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@USER_ID", DbType.Int32, (object) objEntity.USER_ID));
      command.Parameters.Add((object) objDao.createParameter("@ID", DbType.Int32, (object) objEntity.LOG_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(LogEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_log";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      command.Parameters.Add((object) objDao.createParameter("@ID", DbType.Int32, (object) objEntity.LOG_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<LogEntity> find(LogEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_log";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.String, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.String, (object) objEntity.PAGESIZE));
      if (!string.IsNullOrEmpty(objEntity.Mensagem))
        command.Parameters.Add((object) objDao.createParameter("@ACAO", DbType.String, (object) objEntity.Mensagem));
      if (objEntity.DT_LOG_INICIO > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_LOG_INICIO", DbType.DateTime, (object) objEntity.DT_LOG_INICIO));
      if (objEntity.DT_LOG_FIM > DateTime.MinValue)
        command.Parameters.Add((object) objDao.createParameter("@DT_LOG_FIM", DbType.DateTime, (object) objEntity.DT_LOG_FIM));
      if (objEntity.USER_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@USER_ID", DbType.Int32, (object) objEntity.USER_ID));
      return this.serialize(objDao.ExecuteReader(command));
    }

    public LogEntity findByPK(LogEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      List<LogEntity> logEntityList = new List<LogEntity>();
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_log";
      command.Parameters.Add((object) objDao.createParameter("@ID", DbType.Int32, (object) objEntity.LOG_ID));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<LogEntity>();
    }

    private List<LogEntity> serialize(IDataReader objReader)
    {
      List<LogEntity> logEntityList = new List<LogEntity>();
      while (objReader.Read())
      {
        LogEntity logEntity = new LogEntity();
        if (objReader["ACAO"] != DBNull.Value)
          logEntity.Mensagem = objReader["ACAO"].ToString();
        if (objReader["DT_LOG"] != DBNull.Value)
          logEntity.DT_LOG = Convert.ToDateTime(objReader["DT_LOG"]);
        if (objReader["ID"] != DBNull.Value)
          logEntity.LOG_ID = int.Parse(objReader["ID"].ToString());
        if (objReader["USER_ID"] != DBNull.Value)
          logEntity.USER_ID = int.Parse(objReader["USER_ID"].ToString());
        if (objReader["COUNTER"] != DBNull.Value)
          logEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        logEntityList.Add(logEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return logEntityList;
    }
  }
}
