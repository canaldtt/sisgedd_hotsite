﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.DataAccessLayer.TipoNaturezaDAL
// Assembly: FI.Deloitte.Sisgedd.DataAccessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B2D2368A-8FFB-417F-9DF5-1D3FBE411FC0
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.DataAccessLayer.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
  public class TipoNaturezaDAL
  {
    public int add(TipoNaturezaEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tiponatureza";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "I"));
      if (!string.IsNullOrEmpty(objEntity.DESC))
        command.Parameters.Add((object) objDao.createParameter("@DESC", DbType.String, (object) objEntity.DESC));
      return Convert.ToInt32(objDao.ExecuteScalar(command));
    }

    public bool update(TipoNaturezaEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tiponatureza";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "U"));
      if (!string.IsNullOrEmpty(objEntity.DESC))
        command.Parameters.Add((object) objDao.createParameter("@DESC", DbType.String, (object) objEntity.DESC));
      if (objEntity.TIPONATUREZA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TIPONATUREZA_ID", DbType.Int32, (object) objEntity.TIPONATUREZA_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public bool remove(TipoNaturezaEntity objEntity, ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tiponatureza";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "D"));
      if (objEntity.TIPONATUREZA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TIPONATUREZA_ID", DbType.Int32, (object) objEntity.TIPONATUREZA_ID));
      return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
    }

    public List<TipoNaturezaEntity> find(
      TipoNaturezaEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tiponatureza";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (!string.IsNullOrEmpty(objEntity.DESC))
        command.Parameters.Add((object) objDao.createParameter("@DESC", DbType.String, (object) objEntity.DESC));
      if (objEntity.TIPONATUREZA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TIPONATUREZA_ID", DbType.Int32, (object) objEntity.TIPONATUREZA_ID));
      return this.serialize(objDao.ExecuteReader(command));
    }

    public TipoNaturezaEntity findByPK(
      TipoNaturezaEntity objEntity,
      ref DataAccessLayerFactory objDao)
    {
      IDbCommand command = objDao.createCommand();
      command.CommandType = CommandType.StoredProcedure;
      command.CommandText = "pr_tiponatureza";
      command.Parameters.Add((object) objDao.createParameter("@ACTION", DbType.String, (object) "S"));
      command.Parameters.Add((object) objDao.createParameter("@PAGEINDEX", DbType.Int32, (object) objEntity.PAGEINDEX));
      command.Parameters.Add((object) objDao.createParameter("@PAGESIZE", DbType.Int32, (object) objEntity.PAGESIZE));
      if (objEntity.TIPONATUREZA_ID > 0)
        command.Parameters.Add((object) objDao.createParameter("@TIPONATUREZA_ID", DbType.Int32, (object) objEntity.TIPONATUREZA_ID));
      return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<TipoNaturezaEntity>();
    }

    private List<TipoNaturezaEntity> serialize(IDataReader objReader)
    {
      List<TipoNaturezaEntity> tipoNaturezaEntityList = new List<TipoNaturezaEntity>();
      while (objReader.Read())
      {
        TipoNaturezaEntity tipoNaturezaEntity = new TipoNaturezaEntity();
        if (objReader["DESC"] != DBNull.Value)
          tipoNaturezaEntity.DESC = objReader["DESC"].ToString();
        if (objReader["TIPONATUREZA_ID"] != DBNull.Value)
          tipoNaturezaEntity.TIPONATUREZA_ID = Convert.ToInt32(objReader["TIPONATUREZA_ID"]);
        if (objReader["COUNTER"] != DBNull.Value)
          tipoNaturezaEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
        tipoNaturezaEntityList.Add(tipoNaturezaEntity);
      }
      objReader.Close();
      objReader.Dispose();
      return tipoNaturezaEntityList;
    }
  }
}
