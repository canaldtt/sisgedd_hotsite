﻿using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FI.Deloitte.Sisgedd.DataAccessLayer
{
    public class HistoricoDAL
    {
        public int add(HistoricoEntity objEntity, ref DataAccessLayerFactory objDao)
        {
            IDbCommand command = objDao.createCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "pr_historico";
            command.Parameters.Add((object)objDao.createParameter("@ACTION", DbType.String, (object)"I"));
            if (!string.IsNullOrEmpty(objEntity.AUTOR))
                command.Parameters.Add((object)objDao.createParameter("@AUTOR", DbType.String, (object)objEntity.AUTOR));
            if (!string.IsNullOrEmpty(objEntity.COMENTARIO))
                command.Parameters.Add((object)objDao.createParameter("@COMENTARIO", DbType.String, (object)objEntity.COMENTARIO));
            if (!string.IsNullOrEmpty(objEntity.NMR_PROTOCOLO))
                command.Parameters.Add((object)objDao.createParameter("@NMR_PROTOCOLO", DbType.String, (object)objEntity.NMR_PROTOCOLO));
            if (objEntity.RELATO_ID > 0)
                command.Parameters.Add((object)objDao.createParameter("@RELATO_ID", DbType.Int32, (object)objEntity.RELATO_ID));
            if (objEntity.TIPO > 0)
                command.Parameters.Add((object)objDao.createParameter("@TIPO", DbType.Int32, (object)objEntity.TIPO));
            return Convert.ToInt32(objDao.ExecuteScalar(command));
        }

        public bool update(HistoricoEntity objEntity, ref DataAccessLayerFactory objDao)
        {
            IDbCommand command = objDao.createCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "pr_historico";
            command.Parameters.Add((object)objDao.createParameter("@ACTION", DbType.String, (object)"U"));

            command.Parameters.Add((object)objDao.createParameter("@HISTORICO_ID", DbType.Int32, (object)objEntity.HISTORICO_ID));

            if (!string.IsNullOrEmpty(objEntity.AUTOR))
                command.Parameters.Add((object)objDao.createParameter("@AUTOR", DbType.String, (object)objEntity.AUTOR));
            if (!string.IsNullOrEmpty(objEntity.COMENTARIO))
                command.Parameters.Add((object)objDao.createParameter("@COMENTARIO", DbType.String, (object)objEntity.COMENTARIO));
            if (!string.IsNullOrEmpty(objEntity.NMR_PROTOCOLO))
                command.Parameters.Add((object)objDao.createParameter("@NMR_PROTOCOLO", DbType.String, (object)objEntity.NMR_PROTOCOLO));
            if (objEntity.RELATO_ID > 0)
                command.Parameters.Add((object)objDao.createParameter("@RELATO_ID", DbType.Int32, (object)objEntity.RELATO_ID));
            if (objEntity.TIPO > 0)
                command.Parameters.Add((object)objDao.createParameter("@TIPO", DbType.Int32, (object)objEntity.TIPO));
            return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
        }

        public bool remove(HistoricoEntity objEntity, ref DataAccessLayerFactory objDao)
        {
            IDbCommand command = objDao.createCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "pr_historico";
            command.Parameters.Add((object)objDao.createParameter("@ACTION", DbType.String, (object)"D"));
            command.Parameters.Add((object)objDao.createParameter("@HISTORICO_ID", DbType.Int32, (object)objEntity.HISTORICO_ID));
            return Convert.ToBoolean(objDao.ExecuteNonQuery(command));
        }

        public List<HistoricoEntity> findBWise(
          string protocolo,
          ref DataAccessLayerFactory objDao)
        {
            IDbCommand command = objDao.createCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "pr_bwise_sisgedd";
            command.Parameters.Add((object)objDao.createParameter("@NMR_PROTOCOLO", DbType.String, (object)protocolo));
            IDataReader dataReader = objDao.ExecuteReader(command);
            List<HistoricoEntity> historicoEntityList = new List<HistoricoEntity>();
            while (dataReader.Read())
            {
                HistoricoEntity historicoEntity = new HistoricoEntity();
                if (dataReader["COMENTARIOS"] != DBNull.Value)
                    historicoEntity.COMENTARIO = dataReader["COMENTARIOS"].ToString();
                historicoEntity.COUNTER = 1;
                historicoEntityList.Add(historicoEntity);
            }
            dataReader.Close();
            dataReader.Dispose();
            return historicoEntityList;
        }

        public List<HistoricoEntity> find(
          HistoricoEntity objEntity,
          ref DataAccessLayerFactory objDao)
        {
            IDbCommand command = objDao.createCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "pr_historico";
            command.Parameters.Add((object)objDao.createParameter("@ACTION", DbType.String, (object)"S"));
            command.Parameters.Add((object)objDao.createParameter("@PAGEINDEX", DbType.Int32, (object)objEntity.PAGEINDEX));
            command.Parameters.Add((object)objDao.createParameter("@PAGESIZE", DbType.Int32, (object)objEntity.PAGESIZE));
            if (!string.IsNullOrEmpty(objEntity.AUTOR))
                command.Parameters.Add((object)objDao.createParameter("@AUTOR", DbType.String, (object)objEntity.AUTOR));
            if (objEntity.RELATO_ID > 0)
                command.Parameters.Add((object)objDao.createParameter("@RELATO_ID", DbType.Int32, (object)objEntity.RELATO_ID));
            if (objEntity.TIPO > 0)
                command.Parameters.Add((object)objDao.createParameter("@TIPO", DbType.Int32, (object)objEntity.TIPO));
            return this.serialize(objDao.ExecuteReader(command));
        }

        public HistoricoEntity findByPK(
          HistoricoEntity objEntity,
          ref DataAccessLayerFactory objDao)
        {
            IDbCommand command = objDao.createCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "pr_historico";
            command.Parameters.Add((object)objDao.createParameter("@ACTION", DbType.String, (object)"S"));
            command.Parameters.Add((object)objDao.createParameter("@PAGEINDEX", DbType.Int32, (object)objEntity.PAGEINDEX));
            command.Parameters.Add((object)objDao.createParameter("@PAGESIZE", DbType.Int32, (object)objEntity.PAGESIZE));
            command.Parameters.Add((object)objDao.createParameter("@HISTORICO_ID", DbType.Int32, (object)objEntity.HISTORICO_ID));
            return this.serialize(objDao.ExecuteReader(command)).FirstOrDefault<HistoricoEntity>();
        }

        /// <summary>
        /// Método para serializar resultados do DB
        /// </summary>
        /// <param name="objReader"></param>
        /// <returns></returns>
        private List<HistoricoEntity> serialize(IDataReader objReader)
        {
            List<HistoricoEntity> historicoEntityList = new List<HistoricoEntity>();
            while (objReader.Read())
            {
                HistoricoEntity historicoEntity = new HistoricoEntity();
                if (objReader["HISTORICO_ID"] != DBNull.Value)
                    historicoEntity.HISTORICO_ID = Convert.ToInt32(objReader["HISTORICO_ID"]);
                if (objReader["RELATO_ID"] != DBNull.Value)
                    historicoEntity.RELATO_ID = Convert.ToInt32(objReader["RELATO_ID"]);
                if (objReader["AUTOR"] != DBNull.Value)
                    historicoEntity.AUTOR = objReader["AUTOR"].ToString();
                if (objReader["TIPO"] != DBNull.Value)
                    historicoEntity.TIPO = Convert.ToInt32(objReader["TIPO"]);
                if (objReader["DT_COMENTARIO"] != DBNull.Value)
                    historicoEntity.DT_COMENTARIO = Convert.ToDateTime(objReader["DT_COMENTARIO"]);
                if (objReader["NMR_PROTOCOLO"] != DBNull.Value)
                    historicoEntity.NMR_PROTOCOLO = objReader["NMR_PROTOCOLO"].ToString();
                if (objReader["COMENTARIO"] != DBNull.Value)
                    historicoEntity.COMENTARIO = objReader["COMENTARIO"].ToString();
                if (objReader["COUNTER"] != DBNull.Value)
                    historicoEntity.COUNTER = Convert.ToInt32(objReader["COUNTER"]);
                historicoEntityList.Add(historicoEntity);
            }
            objReader.Close();
            objReader.Dispose();
            return historicoEntityList;
        }
    }
}
