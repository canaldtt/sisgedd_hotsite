﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relato-m2.aspx.cs" Inherits="FI.Deloitte.Sisgedd.View.relato_m2" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<meta charset="utf-8" />
	 <meta name="theme-color" content="#ffffff"/>  
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>SISGEDD® HOTSITE - SISTEMA DE GESTÃO DE DENÚNCIAS DELOITTE</title>
	<script type="text/javascript" src="resources/js/jquery.min.js"></script> 
	<script type="text/javascript" src="resources/js/jquery.maskedinput.min.js"></script>     
   <link rel="stylesheet" href="resources/css/default.css"/>
	<script>jQuery(document).ready(function () { INIT_M2(); });</script>
</head>
<body>
	<form id="form1" runat="server" name="form1" class="form-horizontal i-validate"  enctype="multipart/form-data" novalidate="novalidate" >
	<div class="container">
		<div class="col-md-12">
			<div class="row header"  >
				<img src="resources/img/logo_deloitte_header.png" class="pull-right img-responsive" />
				<img src="resources/img/logo_deloitte_header.png" class="pull-left img-responsive"  style="max-height: 120px;" id="logo-cliente"/>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<h3 class="h3-title" id="nome-canal"> <%= Resources.Relato.lblTituloPrincipal %><br /></h3>
				</div>
			</div>
		<div class="row">
			<h3 class="h3-subtitle"> <%= Resources.Relato.lblInfo01 %> <i class="fa fa-question-circle-o" onmouseover="SHOWDIV('div-inst1');" onmouseout="HIDEDIV('div-inst1');" ></i><div id="div-inst1" class="showdiv-baloon"><p> <%= Resources.Relato.lblInfo02 %> </p></div></h3>
			<div class="form-group">
			   <div class="col-md-12 info-intro">
					  <%= Resources.Relato.Intro %>
				</div>
			   
			</div>    
		   <div class="form-group">                  
					<div class="col-md-12">
						<label   style="margin-right:20px" class="sub"><%= Resources.Relato.lblEscolha %> <i class="fa fa-question-circle-o" onmouseover="SHOWDIV('div-inst21');" onmouseout="HIDEDIV('div-inst21');" ></i><div id="div-inst21" class="showdiv-baloon"><p><%= Resources.Relato.lblInfo05 %></p></div></label>
					
						<label class="radio-inline">
							<input type="radio" name="identificar" id="identificadoN" value="N" checked="checked" /> <%= Resources.Relato.rblAnonimo %>
						</label>  
						<label class="radio-inline">
							<input type="radio" name="identificar" id="identificadoI" value="I"/>  <%= Resources.Relato.rblIdentificado %>
						</label>
						<label class="radio-inline">
							<input type="radio" name="identificar" id="identificadoS" value="S"/>  <%= Resources.Relato.rblSigiloso %>
						</label>
					</div>
		   </div>
		   <div id="identificado-container" style="display:none">
			<div class="form-group" >
				<div class="col-md-12">                    
					<label for="den-txt-nome">* <%= Resources.Relato.lblNome %></label>
					<input type="text" id="den-txt-nome" name="den-txt-nome"  class="form-control" placeholder=" <%= Resources.Relato.lblNome %>"  />
				</div>
			</div>
						<div class="form-group" >
			   <div class="col-md-12">
					<div class="col-md-12">
						<label for="den-ddl-identificado"> <%= Resources.Relato.lblGenero %></label>
					</div>
				   <div class="col-md-12">
						<label class="radio-inline">
						  <input type="radio" name="genero" id="generoM" value="Masculino" checked="checked" />  <%= Resources.Relato.rblMasculino %>
						</label>
						<label class="radio-inline">
						  <input type="radio" name="genero" id="generoF" value="Feminino"/> <%= Resources.Relato.rblFeminino %>
						</label>
					</div>
				</div>  
			</div>    
			<div class="form-group" >
				<div class="col-md-6">                    
					<label for="den-txt-cargo"> <%= Resources.Relato.lblCargo %></label>
					<input type="text" id="den-txt-cargo" name="den-txt-cargo"  class="form-control" placeholder=" <%= Resources.Relato.lblCargo %>"  />
				</div>
				<div class="col-md-6">                                  
					<label for="den-txt-email" > <%= Resources.Relato.lblEmail %></label>
					<input type="email" id="den-txt-email" name="den-txt-email"  class="form-control email" placeholder="<%= Resources.Relato.lblEmail %>"  />
				</div>
			</div>
			<div class="form-group" >
				<div class="col-md-6">                                  
					<label for="den-txt-tel1" > <%= Resources.Relato.lblTelefone %></label>
					<input type="text" id="den-txt-tel1" class="form-control fone"   placeholder="<%= Resources.Relato.lblTelefone %>"  maxlength="30"      />
				</div>
				<div class="col-md-6">                                  
					<label for="den-txt-tel2" > <%= Resources.Relato.lblCelular %></label>
					<input type="text" id="den-txt-tel2" class="form-control fone "   placeholder="<%= Resources.Relato.lblCelular %>"  maxlength="20"      />
				</div>
			</div> 
			 <div class="form-group">
				<div class="col-md-12">
					<label for="den-ddl-tpp"> <%= Resources.Relato.lblTipoPublico %> <i class="fa fa-question-circle-o" onmouseover="SHOWDIV('div-inst2');" onmouseout="HIDEDIV('div-inst2');" ></i><div id="div-inst2" class="showdiv-baloon"><p><%= Resources.Relato.lblInfo019 %></p></div></label>
					<select class="form-control" id="den-ddl-tpp"> 
						</select> 
				</div> 
		   </div>    


			</div>
			<div id="identificado-containerN" >
			<div class="form-group" >
				<div class="col-md-6">                                  
					<label for="den-txt-email" > <%= Resources.Relato.lblEmailAnonimo %></label>
					<input type="email" id="den-txt-emailanonimo" name="den-txt-emailanonimo"  class="form-control email" placeholder="<%= Resources.Relato.lblEmailAnonimo %>"   />
				</div>
			</div>
			
			</div>
			<hr />    
			<div class="form-group">
			   <div class="col-md-12">
					<label > <%= Resources.Relato.lblInforme %> <i class="fa fa-question-circle-o" onmouseover="SHOWDIV('div-inst3');" onmouseout="HIDEDIV('div-inst3');" ></i><div id="div-inst3" class="showdiv-baloon"><p><%= Resources.Relato.lblMsgSelecioneLocal %></p></div> </label>
				</div>       
			</div>
			<div class="form-group">
				<!-- UNIDADE -->
				<div class="col-md-6"  id="exibeu">
					<label for="den-ddl-unidade"> <%= Resources.Relato.lblFilial %></label>
					<select class="form-control" id="den-ddl-unidade"></select> 
				</div> 
				<!-- PRODUTO-->
				<div class="col-md-3" id="exibep">
					<label for="den-ddl-area"> <%= Resources.Relato.lblUnidade %></label>
					<select class="form-control" id="den-ddl-produto"><option value="0"> <%= Resources.Relato.lblComboSelecioneFilial %></option></select> 
				</div>   
				<!-- AREA -->
				<div class="col-md-3" id="exibea">
					<label for="den-ddl-area"> <%= Resources.Relato.lblArea %></label>
					<select class="form-control" id="den-ddl-area"><option value="0"><%= Resources.Relato.lblComboSelecioneUnidade %></option></select> 
				</div>   
			</div>       
			<hr />
			<h3 class="h3-subtitle"> <%= Resources.Relato.subInformacoesSobreRelato %> <i class="fa fa-question-circle-o" onmouseover="SHOWDIV('div-inst5');" onmouseout="HIDEDIV('div-inst5');" ></i><div id="div-inst5" class="showdiv-baloon"><p> <%= Resources.Relato.lblMsgPorFavorInforme %> </p></div></h3>
			<div class="form-group">
			   <div class="col-md-12">
					<div class="col-md-12">
						<label > <%= Resources.Relato.lblEhPossivel %></label>
					</div>
				   <div class="col-md-12">
						<label class="radio-inline">
						  <input type="radio" name="identificado" id="den_identificado1_s" value="S"/>  <%= Resources.Relato.rblSim %>
						</label>
						<label class="radio-inline">
						  <input type="radio" name="identificado" id="den_identificado1_n" value="S" checked="checked" /> <%= Resources.Relato.rblNao %>
						</label>
					</div>
				</div>  
			</div> 
			<!-- identificado 1 -->
			<div id="den_identificado1">            
			<h3 class="h3-subtitle"><%= Resources.Relato.lblPessoaIdentificadaRelato %> <i class="fa fa-question-circle-o" onmouseover="SHOWDIV('div-inst6');" onmouseout="HIDEDIV('div-inst6');" ></i><div id="div-inst6" class="showdiv-baloon"><p><%= Resources.Relato.lblMsgPorFavorInforme %></p></div></h3>
			<div class="form-group">
				<div class="col-md-6">                    
					<label for="den1-txt-nome"><%= Resources.Relato.lblNome %></label>
					<input type="text" id="den1-txt-nome" name="den1-txt-nome"  class="form-control" placeholder="<%= Resources.Relato.lblNome %>"  />
				</div>
				<div class="col-md-3">                                  
					<label for="den1-txt-email" ><%= Resources.Relato.lblEmail %></label>
					<input type="email" id="den1-txt-email" name="den1-txt-email"  class="form-control email" placeholder="<%= Resources.Relato.lblEmail %>"  />
				</div>
				<div class="col-md-3">                                                                
					<label for="den1-txt-tel" ><%= Resources.Relato.lblTelefone %></label>
					<input type="text" id="den1-txt-tel" class="form-control fone "   placeholder="<%= Resources.Relato.lblTelefone %>"       />
				</div>
			</div>   
			<div class="form-group">
				<div class="col-md-6">  
					<label for="den1-ddl-participacao"><%= Resources.Relato.lblParticipacao %></label>               
					<select class="form-control" id="den1-ddl-participacao">
						<option value="0" selected="selected">Selecione...</option>
						<option value="Sujeito"><%= Resources.Relato.lblSujeito %></option>
						<option value="Testemunha"><%= Resources.Relato.lblTestemunha %></option>
						<option value="Outros"><%= Resources.Relato.lblOutros %></option>
					</select> 
				</div>
				<div class="col-md-6">                
					<label for="den1-ddl-relacaoempresa"><%= Resources.Relato.lblRelacaoNaEmpresa %></label>              
					<select class="form-control" id="den1-ddl-relacaoempresa">
					   
					</select> 
				</div>
			</div>   
			<div class="form-group">
				<div class="col-md-4">                
					<label for="den1-txt-cargo" ><%= Resources.Relato.lblCargo %></label>
					<input type="text" id="den1-txt-cargo" class="form-control"   placeholder="<%= Resources.Relato.lblCargo %>" />
				</div>
				<div class="col-md-4">                
					<label for="den1-txt-cidade" ><%= Resources.Relato.lblCidade %></label>
					<input type="text" id="den1-txt-cidade" class="form-control"   placeholder="<%= Resources.Relato.lblCidade %>" />
				</div>
				<div class="col-md-4">                
					<label for="den1-txt-estado" ><%= Resources.Relato.lblEstado %></label>
					<input type="text" id="den1-txt-estado" class="form-control"   placeholder="<%= Resources.Relato.lblEstado %>" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-4 text-left">
					<a href="#" class="add"  add-form="true" data-form="den_identificado2"><i class="fa fa-user-plus"></i> <%= Resources.Relato.lblAdicionarOutro %></a>
				</div>
				<div class="col-md-4">&nbsp;</div>
				<div class="col-md-4 text-right">
					&nbsp;
				</div>
			</div>
				<br class="clearfix" />
			</div>
			<!-- identificado 2 -->
			<div id="den_identificado2">
			<h3 class="h3-subtitle"><%= Resources.Relato.lblPessoaIdentificadaRelato %>  <i class="fa fa-question-circle-o" onmouseover="SHOWDIV('div-inst6');" onmouseout="HIDEDIV('div-inst6');" ></i><div id="div-inst6" class="showdiv-baloon"><p><%= Resources.Relato.lblMsgPorFavorInforme %></p></div></h3>
			<div class="form-group">
				<div class="col-md-6">                    
					<label for="den2-txt-nome"><%= Resources.Relato.lblNome %> </label>
					<input type="text" id="den2-txt-nome" name="den2-txt-nome"  class="form-control" placeholder="<%= Resources.Relato.lblNome %>"  />
				</div>
				<div class="col-md-3">                                  
					<label for="den2-txt-email" ><%= Resources.Relato.lblEmail %> </label>
					<input type="email" id="den2-txt-email" name="den2-txt-email"  class="form-control" placeholder="<%= Resources.Relato.lblEmail %>"  />
				</div>
				 
				<div class="col-md-3">                                  
					<label for="den2-txt-tel" ><%= Resources.Relato.lblTelefone %> </label>
					<input type="text" id="den2-txt-tel" class="form-control fone "   placeholder="Telefone..."  maxlength="20"      />
				</div>
			</div>   
			<div class="form-group">
				<div class="col-md-6">  
					<label for="den2-ddl-participacao"><%= Resources.Relato.lblParticipacao %> </label>               
					<select class="form-control" id="den2-ddl-participacao">
						<option value="0" selected="selected">Selecione...</option>
					   <option value="Sujeito"><%= Resources.Relato.lblSujeito %></option>
						<option value="Testemunha"><%= Resources.Relato.lblTestemunha %></option>
						<option value="Outros"><%= Resources.Relato.lblOutros %></option>
					  </select> 
				</div>
				<div class="col-md-6">                
					<label for="den2-ddl-relacaoempresa"><%= Resources.Relato.lblRelacaoNaEmpresa %> </label>              
					<select class="form-control" id="den2-ddl-relacaoempresa">
						
					</select> 
				</div>
			</div>   
			<div class="form-group">
				<div class="col-md-4">                
					<label for="den2-txt-cargo" ><%= Resources.Relato.lblCargo %> </label>
					<input type="text" id="den2-txt-cargo" class="form-control"   placeholder="<%= Resources.Relato.lblCargo %>" />
				</div>
				<div class="col-md-4">                
					<label for="den2-txt-cidade" ><%= Resources.Relato.lblCidade %> </label>
					<input type="text" id="den2-txt-cidade" class="form-control"   placeholder="<%= Resources.Relato.lblCidade %>" />
				</div>
				<div class="col-md-4">                
					<label for="den2-txt-estado" ><%= Resources.Relato.lblEstado %> </label>
					<input type="text" id="den2-txt-estado" class="form-control"   placeholder="<%= Resources.Relato.lblEstado %>" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-4 text-left">
					<a href="#" class="add"  add-form="true" data-form="den_identificado3"><i class="fa fa-user-plus"></i> <%= Resources.Relato.lblAdicionarOutro %></a>
				</div>
				<div class="col-md-4">&nbsp;</div>
				<div class="col-md-4 text-right">
					<a href="#" class="add"  remove-form="true" data-form="2"><i class="fa fa-user-times"></i> <%= Resources.Relato.lblRemoverOutro %></a>
				</div>
			</div>
				
				<br class="clearfix" />
			</div>
			 <!-- identificado 3 -->
			<div id="den_identificado3">
			<h3 class="h3-subtitle"><%= Resources.Relato.lblPessoaIdentificadaRelato %> <i class="fa fa-question-circle-o" onmouseover="SHOWDIV('div-inst6');" onmouseout="HIDEDIV('div-inst6');" ></i><div id="div-inst6" class="showdiv-baloon"><p><%= Resources.Relato.lblMsgPorFavorInforme %></p></div></h3>
			<div class="form-group">
				<div class="col-md-6">                    
					<label for="den3-txt-nome"><%= Resources.Relato.lblNome %></label>
					<input type="text" id="den3-txt-nome" name="den3-txt-nome"  class="form-control" placeholder="<%= Resources.Relato.lblNome %>"  />
				</div>
				<div class="col-md-3">                                  
					<label for="den3-txt-email" ><%= Resources.Relato.lblEmail %></label>
					<input type="email" id="den3-txt-email" name="den3-txt-email"  class="form-control  "   placeholder="<%= Resources.Relato.lblEmail %>"  />
				</div>
				<div class="col-md-3">                                  
						
					<label for="den3-txt-tel" ><%= Resources.Relato.lblTelefone %></label>
					<input type="text" id="den3-txt-tel" class="form-control fone "   placeholder="<%= Resources.Relato.lblTelefone %>"  maxlength="20"      />
				</div>
			</div>   
			<div class="form-group">
				<div class="col-md-6">  
					<label for="den3-ddl-participacao"><%= Resources.Relato.lblParticipacao %></label>               
					<select class="form-control" id="den3-ddl-participacao">
						<option value="0" selected="selected">Selecione...</option>
					  <option value="Sujeito"><%= Resources.Relato.lblSujeito %></option>
						<option value="Testemunha"><%= Resources.Relato.lblTestemunha %></option>
						<option value="Outros"><%= Resources.Relato.lblOutros %></option>
					  </select> 
				</div>
				<div class="col-md-6">                
					<label for="den3-ddl-relacaoempresa"><%= Resources.Relato.lblRelacaoNaEmpresa %></label>              
					<select class="form-control" id="den3-ddl-relacaoempresa">
						
					</select> 
				</div>
			</div>   
			<div class="form-group">
				<div class="col-md-4">                
					<label for="den3-txt-cargo" ><%= Resources.Relato.lblCargo %></label>
					<input type="text" id="den3-txt-cargo" class="form-control"   placeholder="<%= Resources.Relato.lblCargo %>" />
				</div>
				<div class="col-md-4">                
					<label for="den3-txt-cidade" ><%= Resources.Relato.lblCidade %></label>
					<input type="text" id="den3-txt-cidade" class="form-control"   placeholder="<%= Resources.Relato.lblCidade %>" />
				</div>
				<div class="col-md-4">                
					<label for="den3-txt-estado" ><%= Resources.Relato.lblEstado %></label>
					<input type="text" id="den3-txt-estado" class="form-control"   placeholder="<%= Resources.Relato.lblEstado %>" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-4 text-left">
					<a href="#" class="add"  add-form="true" data-form="den_identificado4"><i class="fa fa-user-plus"></i> <%= Resources.Relato.lblAdicionarOutro %></a>
				</div>
				<div class="col-md-4">&nbsp;</div>
				<div class="col-md-4 text-right">
					<a href="#" class="add"  remove-form="true" data-form="3"><i class="fa fa-user-times"></i> <%= Resources.Relato.lblRemoverOutro %></a>
				</div>
			</div>
				
				<br class="clearfix" />
			</div>
			<!-- identificado 4 -->
			<div id="den_identificado4">
			<h3 class="h3-subtitle"><%= Resources.Relato.lblPessoaIdentificadaRelato %> <i class="fa fa-question-circle-o" onmouseover="SHOWDIV('div-inst6');" onmouseout="HIDEDIV('div-inst6');" ></i><div id="div-inst6" class="showdiv-baloon"><p><%= Resources.Relato.lblMsgPorFavorInforme %></p></div></h3>
			<div class="form-group">
				<div class="col-md-6">                    
					<label for="den4-txt-nome"><%= Resources.Relato.lblNome %></label>
					<input type="text" id="den4-txt-nome" name="den4-txt-nome"  class="form-control" placeholder="<%= Resources.Relato.lblNome %>"  />
				</div>
				<div class="col-md-3">                                  
					<label for="den4-txt-email" ><%= Resources.Relato.lblEmail %></label>
					<input type="email" id="den4-txt-email" name="den4-txt-email"  class="form-control "  placeholder="<%= Resources.Relato.lblEmail %>"  />
				</div>
				<div class="col-md-3">                                  
					<label for="den4-txt-tel" ><%= Resources.Relato.lblTelefone %></label>
					<input type="text" id="den4-txt-tel" class="form-control fone "   placeholder="<%= Resources.Relato.lblTelefone %>"  maxlength="20"      />
				</div>
			</div>   
			<div class="form-group">
				<div class="col-md-6">  
					<label for="den4-ddl-participacao"><%= Resources.Relato.lblParticipacao %></label>               
					<select class="form-control" id="den4-ddl-participacao">
						<option value="0" selected="selected">Selecione...</option>
					  <option value="Sujeito"><%= Resources.Relato.lblSujeito %></option>
						<option value="Testemunha"><%= Resources.Relato.lblTestemunha %></option>
						<option value="Outros"><%= Resources.Relato.lblOutros %></option>
					  </select> 
				</div>
				<div class="col-md-6">                
					<label for="den4-ddl-relacaoempresa"><%= Resources.Relato.lblRelacaoNaEmpresa %></label>              
					<select class="form-control" id="den4-ddl-relacaoempresa">
					  
					</select> 
				</div>
			</div>   
			<div class="form-group">
				<div class="col-md-4">                
					<label for="den4-txt-cargo" ><%= Resources.Relato.lblCargo %></label>
					<input type="text" id="den4-txt-cargo" class="form-control"   placeholder="<%= Resources.Relato.lblCargo %>" />
				</div>
				<div class="col-md-4">                
					<label for="den4-txt-cidade" ><%= Resources.Relato.lblCidade %></label>
					<input type="text" id="den4-txt-cidade" class="form-control"   placeholder="<%= Resources.Relato.lblCidade %>" />
				</div>
				<div class="col-md-4">                
					<label for="den4-txt-estado" ><%= Resources.Relato.lblEstado %></label>
					<input type="text" id="den4-txt-estado" class="form-control"   placeholder="<%= Resources.Relato.lblEstado %>" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-4 text-left">
					<a href="#" class="add" add-form="true" data-form="den_identificado5"><i class="fa fa-user-plus"></i> <%= Resources.Relato.lblAdicionarOutro %></a>
				</div>
				<div class="col-md-4">&nbsp;</div>
				<div class="col-md-4 text-right">
					<a href="#" class="add"  remove-form="true" data-form="4"><i class="fa fa-user-times"></i> <%= Resources.Relato.lblRemoverOutro %></a>
				</div>
			</div>
				
				<br class="clearfix" />
			</div>
			<!-- identificado 5 -->
			<div id="den_identificado5">
			<h3 class="h3-subtitle"><%= Resources.Relato.lblPessoaIdentificadaRelato %> <i class="fa fa-question-circle-o" onmouseover="SHOWDIV('div-inst6');" onmouseout="HIDEDIV('div-inst6');" ></i><div id="div-inst6" class="showdiv-baloon"><p><%= Resources.Relato.lblMsgPorFavorInforme %></p></div></h3>
			<div class="form-group">
				<div class="col-md-6">                    
					<label for="den5-txt-nome"><%= Resources.Relato.lblNome %></label>
					<input type="text" id="den5-txt-nome" name="den5-txt-nome"  class="form-control" placeholder="<%= Resources.Relato.lblNome %>"  />
				</div>
				<div class="col-md-3">                                  
					<label for="den5-txt-email" ><%= Resources.Relato.lblEmail %></label>
					<input type="email" id="den5-txt-email" name="den5-txt-email"  class="form-control "  placeholder="<%= Resources.Relato.lblEmail %>"  />
				</div>
				<div class="col-md-3">                                   
					<label for="den5-txt-tel" ><%= Resources.Relato.lblTelefone %></label>
					<input type="text" id="den5-txt-tel" class="form-control fone "   placeholder="<%= Resources.Relato.lblTelefone %>"  maxlength="20"      />
				</div>
			</div>   
			<div class="form-group">
				<div class="col-md-6">  
					<label for="den5-ddl-participacao"><%= Resources.Relato.lblParticipacao %></label>               
					<select class="form-control" id="den5-ddl-participacao">
						<option value="0" selected="selected">Selecione...</option>
					  <option value="Sujeito"><%= Resources.Relato.lblSujeito %></option>
						<option value="Testemunha"><%= Resources.Relato.lblTestemunha %></option>
						<option value="Outros"><%= Resources.Relato.lblOutros %></option>
					  </select> 
				</div>
				<div class="col-md-6">                
					<label for="den5-ddl-relacaoempresa"><%= Resources.Relato.lblRelacaoNaEmpresa %></label>              
					<select class="form-control" id="den5-ddl-relacaoempresa">
						
					</select> 
				</div>
			</div>   
			<div class="form-group">
				<div class="col-md-4">                
					<label for="den5-txt-cargo" ><%= Resources.Relato.lblCargo %></label>
					<input type="text" id="den5-txt-cargo" class="form-control"   placeholder="<%= Resources.Relato.lblCargo %>" />
				</div>
				<div class="col-md-4">                
					<label for="den5-txt-cidade" ><%= Resources.Relato.lblCidade %></label>
					<input type="text" id="den5-txt-cidade" class="form-control"   placeholder="<%= Resources.Relato.lblCidade %>" />
				</div>
				<div class="col-md-4">                
					<label for="den5-txt-estado" ><%= Resources.Relato.lblEstado %></label>
					<input type="text" id="den5-txt-estado" class="form-control"   placeholder="<%= Resources.Relato.lblEstado %>" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-4 text-left">
				  &nbsp;
				</div>
				<div class="col-md-4">&nbsp;</div>
				<div class="col-md-4 text-right">
					<a href="#" class="add"  remove-form="true" data-form="5"><i class="fa fa-user-times"></i> <%= Resources.Relato.lblRemoverOutro %></a>
				</div>
			</div>
				
				<br class="clearfix" />
			</div>
			  <hr />   
			<div class="form-group">
				<div class="col-md-12">  
<label for="den-ddl-tiporelato">
							<%= Resources.Relato.lblTipoRelato %> 
							<i class="fa fa-question-circle-o" onmouseover="SHOWDIV('div-inst77')" onmouseout="HIDEDIV('div-inst77')" ></i>
							<div id="div-inst77" class="showdiv-baloon">
								<p><%= Resources.Relato.lblQueTipoDeRelatoSeEnquadra %></p>

							</div>

					</label>              
					<select id="den-ddl-tiporelato" class="form-control">
						<% if (Request.QueryString["id"].ToString() != "1045") {%>
						<option value="0"><%= Resources.Relato.lblSelecione %></option>                       
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo1 %>"><%= Resources.Relato.lblComboTipoRelatoInfo1 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo2 %>"><%= Resources.Relato.lblComboTipoRelatoInfo2 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo3 %>"><%= Resources.Relato.lblComboTipoRelatoInfo3 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo4 %>"><%= Resources.Relato.lblComboTipoRelatoInfo4 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo5 %>"><%= Resources.Relato.lblComboTipoRelatoInfo5 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo6 %>"><%= Resources.Relato.lblComboTipoRelatoInfo6 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo7 %>"><%= Resources.Relato.lblComboTipoRelatoInfo7 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo8 %>"><%= Resources.Relato.lblComboTipoRelatoInfo8 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo9 %>"><%= Resources.Relato.lblComboTipoRelatoInfo9 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo10 %>"><%= Resources.Relato.lblComboTipoRelatoInfo10 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo11 %>"><%= Resources.Relato.lblComboTipoRelatoInfo11 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo12 %>"><%= Resources.Relato.lblComboTipoRelatoInfo12 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo13 %>"><%= Resources.Relato.lblComboTipoRelatoInfo13 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo14 %>"><%= Resources.Relato.lblComboTipoRelatoInfo14 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo15 %>"><%= Resources.Relato.lblComboTipoRelatoInfo15 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo16 %>"><%= Resources.Relato.lblComboTipoRelatoInfo16 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo17 %>"><%= Resources.Relato.lblComboTipoRelatoInfo17 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo18 %>"><%= Resources.Relato.lblComboTipoRelatoInfo18 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo19 %>"><%= Resources.Relato.lblComboTipoRelatoInfo19 %></option>
					</select>                
					<%}%>
					<% if (Request.QueryString["id"].ToString() == "1045") {%>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo1085_1 %>"><%= Resources.Relato.lblComboTipoRelatoInfo1085_1 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo1085_2 %>"><%= Resources.Relato.lblComboTipoRelatoInfo1085_2 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo1085_3 %>"><%= Resources.Relato.lblComboTipoRelatoInfo1085_3 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo1085_4 %>"><%= Resources.Relato.lblComboTipoRelatoInfo1085_4 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo1085_5 %>"><%= Resources.Relato.lblComboTipoRelatoInfo1085_5 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo1085_6 %>"><%= Resources.Relato.lblComboTipoRelatoInfo1085_6 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo1085_7 %>"><%= Resources.Relato.lblComboTipoRelatoInfo1085_7 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo1085_8 %>"><%= Resources.Relato.lblComboTipoRelatoInfo1085_8 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo1085_9 %>"><%= Resources.Relato.lblComboTipoRelatoInfo1085_9 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo1085_10 %>"><%= Resources.Relato.lblComboTipoRelatoInfo1085_10 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo1085_11 %>"><%= Resources.Relato.lblComboTipoRelatoInfo1085_11 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo1085_12 %>"><%= Resources.Relato.lblComboTipoRelatoInfo1085_12 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo1085_13 %>"><%= Resources.Relato.lblComboTipoRelatoInfo1085_13 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo1085_14 %>"><%= Resources.Relato.lblComboTipoRelatoInfo1085_14 %></option>
						<option value="<%= Resources.Relato.lblComboTipoRelatoInfo1085_15 %>"><%= Resources.Relato.lblComboTipoRelatoInfo1085_15 %></option>

					<%}%>
					</div>
			</div>
			<div class="form-group"  >
				<div class="col-md-12">                    
					<label for="den-txt-desc">* <%= Resources.Relato.lblRelato %> <i class="fa fa-question-circle-o" onmouseover="SHOWDIV('div-inst7');" onmouseout="HIDEDIV('div-inst7');" ></i><div id="div-inst7" class="showdiv-baloon"><p><%= Resources.Relato.lblDescrevaDetalhadamente %></p></div></label>
					<textarea id="den-txt-desc"  rows="6" name="den-txt-desc"  class="form-control required" required="required" placeholder="<%= Resources.Relato.lblDescrevaSeuRelato %>" ></textarea>
				</div>                                
			</div>
			<div class="hidden">
			<hr /> 
		   <label for="den-txt-desc"><%= Resources.Relato.subInformacoesSobreRelato %> <i class="fa fa-question-circle-o" onmouseover="SHOWDIV('div-inst8');" onmouseout="HIDEDIV('div-inst8');" ></i><div id="div-inst8" class="showdiv-baloon"><p><%= Resources.Relato.lblQuantoMaisDetalhes %></p></div></label>
			<div class="form-group" > 
				<div class="col-md-12">
					<label for="den-txt-dataocorreu"><%= Resources.Relato.lblDataEvento %></label>
					<input type="text" id="den-txt-dataocorreu" name="den-txt-dataocorreu"  class="form-control" placeholder="<%= Resources.Relato.lblDataEvento %>"  />
				</div>
			</div>             
			<div class="form-group" > 
				<div class="col-md-12">
					<label for="den-txt-recorrencia"><%= Resources.Relato.lblRecorrencia %></label>
					<input type="text" id="den-txt-recorrencia" name="den-txt-recorrencia"  class="form-control" placeholder="<%= Resources.Relato.lblRecorrencia %>"  />
				</div>
			</div>             
			<div class="form-group" > 
				<div class="col-md-12">
					<label for="den-txt-comosoube"><%= Resources.Relato.lblComoFicouCiente %></label>
					<input type="text" id="den-txt-comosoube" name="den-txt-comosoube"  class="form-control" placeholder="<%= Resources.Relato.lblComoFicouCiente %>"  />
				</div>
			</div>             
			<div class="form-group" > 
				<div class="col-md-12"> 
					<label for="den-txt-perdafinanceira"><%= Resources.Relato.lblSeTiverConhecimento %></label>
					<input type="text" id="den-txt-perdafinanceira" name="den-txt-perdafinanceira"  class="form-control" placeholder="<%= Resources.Relato.lblSeTiverConhecimento %>"  />
				</div>
			</div>             
			<div class="form-group" > 
				<div class="col-md-12"> 
					<label for="den-txt-sugestao"><%= Resources.Relato.lblSeTiverSugestao %></label>
					<textarea id="den-txt-sugestao" name="den-txt-sugestao"  class="form-control" placeholder="<%= Resources.Relato.lblSeTiverSugestao %>"  rows="3" ></textarea>
				</div>
			</div>             
			<div class="form-group">
				<div class="col-md-12">
					<label for="den-ddl-quantoaorelato"><%= Resources.Relato.lblQuantoAoRelato %></label>
					<select id="den-ddl-quantoaorelato" name="den-ddl-quantoaorelato" class="form-control">
						<option value="0"><%= Resources.Relato.lblSelecione %></option>
						<option value="<%= Resources.Relato.lblQuantoAoRelatoInfo1 %>"><%= Resources.Relato.lblQuantoAoRelatoInfo1 %></option>
						<option value="<%= Resources.Relato.lblQuantoAoRelatoInfo2 %>"><%= Resources.Relato.lblQuantoAoRelatoInfo2 %></option>
						<option value="<%= Resources.Relato.lblQuantoAoRelatoInfo3 %>"><%= Resources.Relato.lblQuantoAoRelatoInfo3 %></option>
						<option value="<%= Resources.Relato.lblOutros %>"><%= Resources.Relato.lblOutros %></option>
					</select>
				</div>                 
			</div>  
			 <div class="form-group" > 
				<div class="col-md-12"> 
					<label for="den-txt-evidencia"><%= Resources.Relato.lblEvidencias %></label>
					<textarea id="den-txt-evidencia" name="den-txt-evidencia"  class="form-control" placeholder="<%= Resources.Relato.lblEvidencias %>"  rows="3" ></textarea>
				</div>
			</div>             
			 <div class="form-group" > 
				<div class="col-md-12"> 
					<label for="den-txt-protocoloanterior"><%= Resources.Relato.lblProtocoloAnterior %></label>
					<input type="text" id="den-txt-protocoloanterior" name="den-txt-protocoloanterior"  class="form-control" placeholder="..."  />
				</div>
			</div>               
		   </div>
			  <div class="form-group">
				   <div class="col-md-12" id="anexos-container">
						 
					</div>
					<div class="col-md-12">    
					   
						
						<label for="file-uploader2"><%= Resources.Relato.lblAnexo %></label>
						<div id="file-uploader2" ></div>
					</div>
				</div>   
			<hr /> 
			<div class="form-group">
					<div class="col-md-12 text-center">     
						<div class="col-md-6 col-md-push-3">    
							<input type="hidden" id="den-txt-idioma"        />
							<input type="hidden" id="den-txt-empresaid"        />
							 <input type="hidden" id="den-txt-guid"  /> 
							  <a id="den-btn-save" class="btn btn-block btn-lg btn-success"><i class="fa fa-check"></i> <%= Resources.Relato.btnEnviarRelato %></a>     
						</div>
					</div>
					<div class="col-md-12">            
						
					</div>
				</div>
		</div>
		<div class="row">
			 <footer><label for="footer"><%= Resources.Relato.lblFooter %></label>
			</footer>
		</div>
			</div>
	</div>
		
		<div class="modal fade" id="modal-protocolo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Aten&ccedil;&atilde;o!</h4>
			</div>
			<div class="modal-body" id="modal-protocolo-body" style="background:#333">
			   
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->    
<div class="modal fade" id="mini-modal" tabindex="-1" role="dialog" aria-labelledby="mymini" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body" id="mini-modal-protocolo-body" style="background:#fff">
				<h4 class="alert-no-result"><i class="fa fa-spinner fa-pulse"></i> Enviando arquivo...</h4>

			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->    
<div class="modal fade" id="modal-confirm-anexoremove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content alert-danger">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabelanexo"><i class="fa fa-warning"></i> Aten&ccedil;&atilde;o!</h4>
			</div>
			<div class="modal-body">
				<p>Essa operação NÃO poderá ser desfeita.<br/> Confirma a exclusão deste anexo? </p>
			</div>
			<div class="modal-footer">
				<div class="col-md-6 "> 
					<button type="button" class="btn btn-success btn-lg btn-block" id="btn-modal-anexoremove-confirm"><i class="fa fa-check"></i> Sim</button>
				</div>
				<div class="col-md-6 "> 
					<button type="button" class="btn btn-inverse btn-lg btn-block" id="btn-modal-anexoremove-close" data-dismiss="modal"><i class="fa fa-times"></i> N&atilde;o</button>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->   
	</form>

	<script type="text/javascript" src="resources/js/bootstrap.min.js" ></script>
	 <script type="text/javascript" src="resources/js/fileuploader.js"></script> 
	 <script type="text/javascript" src="resources/js/jquery.plugin.min.js"></script> 
	  <script type="text/javascript" src="resources/js/jquery-ui.min.js"></script>    
	 <script type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
	 <script type="text/javascript" src="resources/js/init.js"></script>
</body>
</html>
