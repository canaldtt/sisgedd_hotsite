﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="FI.Deloitte.Sisgedd.View.login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head >
<meta charset="charset=iso-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<title>SISGEDD® HOTSITE - SISTEMA DE GESTÃO DE DENÚNCIAS DELOITTE</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <link rel="manifest" href="resources/img/ico/manifest.json"/>
    <meta name="msapplication-TileColor" content="#ffffff"/>
    <meta name="msapplication-TileImage" content="resources/img/ico/ms-icon-144x144.png"/>
    <meta name="theme-color" content="#ffffff"/>  

<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="SISGEDD® HOTSITE - SISTEMA DE GESTÃO DE DENÚNCIAS DELOITTE| Desenvolvido por Canal de Denúncias - Deloitte"/>
<meta name="author" content="Canal de Denúncias Deloitte - etica.deloitte.com.br"/>    
<link rel="stylesheet" href="resources/css/default.css" type="text/css" />

<script type="text/javascript" src="resources/js/jquery.min.js"></script>     
<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script> <!--NAO PODE SER DEPOIS DO BOOTSTRAP-->
<script type="text/javascript" src="resources/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="resources/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="resources/js/bootstrap-datetimepicker.pt-BR.js" charset="UTF-8"></script>
<script type="text/javascript" src="resources/js/jquery.placeholder.min.js"></script>
<script type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="resources/js/init.js"></script>
</head>
<body class="loginpage" > <div class="header"></div>
<form id="form1" runat="server"></form>
<div id="mainwrapper" class="mainwrapper"> 
<div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="text-center login-title">
                    <img src="resources/img/logologin.png" alt="">
                    <br>
                    <label style="font-family: 'Open Sans', Verdana, Helvetica, sans-serif;color:#c0c0c0";>Acompanhamento de Denúncia</label>
                </h2>   
            </div>  
            <div class="modal-body">    
                <form id="frm-login"  name="frm-login" class="form-horizontal col-md-12 center-block" novalidate="novalidate">
                    <div class="form-group">
                        <label style="color: #c0c0c0">PROTOCOLO</label>
                        <input type="text" id="user-txt-login" name="user-txt-login" class="form-control required" required="required" placeholder="Informe o protocolo"/> 
                    </div>
                    <div class="form-group">
                        <label style="color: #c0c0c0">SENHA</label>
                      <input type="password" id="user-txt-senha" name="user-txt-senha" class="form-control required" required="required" placeholder="Informe a senha"/> 
                    </div>
                    <div class="form-group">
                        <a href="#" id="login-btn-OK" class="btn btn-lg btn-block"><i class="fa fa-sign-in"></i> Entrar</a>  
                    </div>
                    <div class="form-group">   
                        <a href="#" class="link" data-toggle="modal" data-target="#loginPolitica">Política de Privacidade</a>
                        <br/>
                        <a href="#" class="link" data-toggle="modal" data-target="#loginEsqueci">Esqueceu sua senha?</a>
                    </div>
                    <input type="hidden" id="user-txt-eid" />
                </form>
                
             </div> 
            <br class="clearfix"    />
        </div>

    </div>
</div>
        <%--Política de privacidade (Body)--%>
 <div id="loginPolitica" class="modal fade" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel1">Política de Privacidade</h4>
      </div>
      <div class="modal-body">
        <p>O <b>SISGEDD® HOTSITE</b> - Sistema de Gestão de Denúncias da Deloitte é de uso exclusivo de usuários cadastrados.</p>
        <p> Este sistema foi idealizado e desenvolvido pela equipe de Canal de Denúncias <b>Deloitte</b> tendo seu conteúdo protegido e liberado apenas para usuários previamente cadastrados e autorizados a utilizar o mesmo. Caso não possua acesso ou permissão para uso, favor encerrar a navegação e mudar de página.</p>
        <p> Este conteúdo está de acordo com as normas de segurança do W3C e requer autenticação para uso. Qualquer tentativa de violação de acesso, seu IP será gravado e levado às autoridades para as devidas medidas cabíveis, certo da compreensão e bom senso de todos, agradecemos.</p>
        <p>Atenciosamente, <br /><br />Canal de Denúncias <br /><b>Deloitte</b></p> 
 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK, Entendi</button>
        
      </div>
    </div>
  </div>
</div>       
    <%--Esqueci a senha(Body)--%>
 <div id="loginEsqueci" class="modal fade" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel2" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel1">Esqueci minha Senha</h4>
      </div>
      <div class="modal-body">
          <p>O <b>SISGEDD® HOTSITE</b> - Sistema de Gestão de Denúncias da Deloitte é de uso exclusivo de usuários com registros de denúncias previamente registradas via 0800, HOTSITE ou E-Mail.</p>
        <p>Para recuperação da senha gerada para consulta no registro da denúnicia via sistema é através do contato com nossa central de 0800. Com o <b>Número de protocolo</b> em mãos, entre em contato com a central solicitando a recuperação da senha.</p>
        <p>Lembrando que este conteúdo é liberado apenas para usuários que realizaram previamente o registro de denúncias através do HOTSITE. Caso não possua acesso ou permissão para uso, favor encerrar a navegação e mudar de página.</p>
        <p>Atenciosamente, <br /><br />Canal de Denúncias <br /><b>Deloitte</b></p> 
       </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK, Entendi</button>
        
      </div>
    </div>
  </div>
</div>      
    
    

 </div>
    </body>
</html>