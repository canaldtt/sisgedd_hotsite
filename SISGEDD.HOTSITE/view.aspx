﻿<%@ Page Language="C#"  Culture="pt-BR" UICulture="pt-BR"  AutoEventWireup="true" CodeBehind="view.aspx.cs" Inherits="FI.Deloitte.Sisgedd.View.view" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<meta charset="utf-8" />
	<link rel="shortcut icon" type="image/x-icon" href="resources/img/ico/favicon.ico" />    
	<link rel="manifest" href="resources/img/ico/manifest.json"/>
	<meta name="msapplication-TileColor" content="#ffffff"/>
	<meta name="msapplication-TileImage" content="../resources/img/ico/ms-icon-144x144.png"/>
	<meta name="theme-color" content="#ffffff"/>  
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>SISGEDD® HOTSITE - SISTEMA DE GESTÃO DE DENÚNCIAS DELOITTE</title>
	<script type="text/javascript" src="resources/js/jquery.min.js"></script>     
   <link rel="stylesheet" href="resources/css/default.css"/>
	<script>jQuery(document).ready(function () { VIEW_RELATO(); });</script>
</head>
<body><div class="loading modal" style="z-index:9999; border:1px solid red" id="divloading"><p>Carregando...</p></div>
	<form id="form1" runat="server" class="form-horizontal"  enctype="multipart/form-data" novalidate="novalidate" >
	<div class="container">
		<div class="col-md-12">
		<div class="row header"  >
			<img src="resources/img/logo_deloitte_header.png" class="pull-right img-responsive" />
			<img src="resources/img/logo_deloitte_header.png" class="pull-left img-responsive"  style="max-height: 120px;" id="logo-cliente"/>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<h3 class="h3-title"  id="nome-canal"><br /></h3>
			</div>
		</div>
		<div class="row">
			 <div class="form-group"  >
				<div class="col-md-6">                    
					<label><%= Resources.View.lblProtocolo %> <span id="protocolo"></span> </label>                    
				</div>                 
				 <div class="col-md-6">  
					 <label for="den-ddl-status"><%= Resources.View.lblStatusRelato %>  <span id="den-ddl-status" class="status-container" ></span></label>				   
				 </div>               
			</div> 
			<hr />    
			<div class="form-group">
				<div class="col-md-6">  
					<label for="den-ddl-datainicio"><%= Resources.View.lblDataAberturaRelato %>  <span id="den-ddl-datainicio" class="status-container" ></span></label>               
				   
				</div>
				<div class="col-md-6">  
					<label for="den-ddl-dataatualizacao"><%= Resources.View.lblUltimaAtualizacaoRelato %>   <span id="den-ddl-dataatualizacao" class="status-container" ></span></label>               
				   
				</div>
			</div> 
			<hr />
			<div class="form-group"  >
				<div class="col-md-12">                    
					<label for="den-txt-desc"><%= Resources.View.lblDescricaoRelato %></label>
					<div id="den-txt-desc" class="mensagem-container" ></div>
					<div id="relato-emailcontainer" class="div-emailcontainer" style="display:none"></div>

				</div>                                
			</div>
			<hr />    
		   <div class="form-group"  >
			  
				<div class="col-md-12">                    
					<label for="den-txt-desc" id="den-lbl-parecer"><%= Resources.View.lblHistoricoRelato %></label>
					<div id="historico-container"  ></div>
				</div>                                
			</div>
			<hr /> 
			<div class="form-group"  >
				<div class="col-md-12">                    
					<label for="den-txt-desc" id="den-lbl-resposta"><%= Resources.View.lblRespostaRelato %></label>
					<div id="resposta-container"  ></div>
				</div>                                
			</div>
			<hr />  
		   <div class="form-group "  >
			   <div class="col-md-12"><label id="den-lbl-dtparc"></label></div>
				<div class="col-md-12  pesq-sat">                    
					<label for="den-txt-replica"><%= Resources.View.lblInfoAdicionais %></label>
					<textarea id="den-txt-replica"  rows="10" name="den-txt-replica" class="form-control" ></textarea>
				</div>                                
			</div>
			<hr />   
			  <div class="form-group pesq-sat">
				   <div class="col-md-12" id="anexos-container">
						 
					</div>
					<div class="col-md-12">    
					   
						
						<label for="file-uploader2"><%= Resources.Relato.lblAnexo %></label>
						<div id="file-uploader2" ></div>
						<a href="#" class="link" data-toggle="modal" data-target="#anexoPermitido"><i class="fa fa-question-circle"></i> Arquivos suportados</a>
					</div>
				</div>    
				<div class="col-md-12  pesq-sat">  
						<br />
						<br /><hr /> 
				</div>
				<div class="form-group">
					<div class="col-md-12 text-center">     
						<div class="col-md-6 col-md-push-3">    
							<input type="hidden" id="txt-relato-id"        />
							<input type="hidden" id="den-txt-idioma"        />
							<input type="hidden" id="den-txt-empresaid"        />
							<input type="hidden" id="den-txt-guid"  /> 

							  <a id="den-btn-save" class="btn btn-block btn-lg btn-success"><i class="fa fa-check"></i> <%= Resources.View.btnEnviar %></a>     
							<a id="den-btn-pesq" class="btn btn-block btn-lg btn-success" style="display:none"> <%= Resources.View.lblPesquisaSatisfacao %></a>     
						</div>
					</div>
					<div class="col-md-12">            
					</div>
				</div>
		</div>
		<div class="row">
			<footer></footer>
		</div>
	</div> 
</div>
</form>
	<div class="modal fade" id="modal-pesquisa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel" style="color:#333!important;text-transform:uppercase; text-indent:5px; font-weight:bold; font-size:16px"><i class="fa fa-comment"></i>  <%= Resources.View.lblPesquisaSatisfacao %></h4>
			</div>
			<div class="modal-body" id="modal-pesquisa-body1" style="background:#fff">
				  <div class="form-group">
					<div class="col-md-12">
						<div class="col-md-12">
							<label for="cust_PergSatisfacao1_1"><%= Resources.View.lblPerg11 %></label>
						</div>
						<div class="col-md-12">
							<label class="radio">
							  <input type="radio" name="cust_PergSatisfacao1_1" id="11_A"  value="Sim" /><%= Resources.Relato.rblSim %>
							</label>
							<label class="radio">
							  <input type="radio" name="cust_PergSatisfacao1_1" id="11_B" value="Não" /><%= Resources.Relato.rblNao %>
							</label>
							 <label class="radio">
							  <input type="radio" name="cust_PergSatisfacao1_1" id="11_C" value="Não quis responder a pesquisa " /><%= Resources.View.rblNaoResponder %>
							</label>
							 
						</div>
						<div class="col-md-12" id="Q1_NAO" >
							<label for="cust_PergSatisfacao1_1b"><%= Resources.View.lblPerg11b %></label>
							<input type="text" class="form-control" id="cust_PergSatisfacao1_1b" /><br /><br />
						</div>
					</div>  
				</div>    <!-- 1 -->
				<div class="form-group" id="Q1_SIM">
					<div class="col-md-12">
						<div class="col-md-12">
							<label for="cust_PergSatisfacao1_1a"><%= Resources.View.lblPerg11a %></label>
						</div>
						<div class="col-md-12">
							<label class="radio">
							  <input type="radio" name="cust_PergSatisfacao1_1a" id="1A_A"  value="Sim" /> <%= Resources.Relato.rblSim %>
							</label>
							<label class="radio">
							  <input type="radio" name="cust_PergSatisfacao1_1a" id="1A_B" value="Não" /><%= Resources.Relato.rblNao %>
							</label>
						</div>
						<div class="col-md-12" id="Q1A_NAO">
							<label for="cust_PergSatisfacao1_2a"><%= Resources.View.lblPerg12a %></label>
							<input type="text" class="form-control" id="cust_PergSatisfacao1_2a" /><br /><br />
						</div>
					</div>  
				</div>    <!-- 1 a-->
				 <div class="form-group" id="Q2_container">
					<div class="col-md-12">
						<div class="col-md-12">
							<label for="cust_PergSatisfacao2_1"><%= Resources.View.lblPerg21 %></label>
						</div>
						<div class="col-md-12">
							<label class="radio">
							  <input type="radio" name="cust_PergSatisfacao2_1" id="21_A"  value="SIM" /><%= Resources.Relato.rblSim %>
							</label>
							<label class="radio">
							  <input type="radio" name="cust_PergSatisfacao2_1" id="21_B"  value="NÃO" /><%= Resources.Relato.rblNao %>
							</label>
						</div>
						<div class="col-md-12" id="Q2_NAO">
							<label for="cust_PergSatisfacao1_2a"><%= Resources.View.lblPerg22 %></label>
							<input type="text" class="form-control" id="cust_PergSatisfacao2_2" /><br /><br />
						</div>
					</div>  
				</div>  <!-- 2 -->
				 <div class="form-group"  id="Q3_container">
					<div class="col-md-12">
						<div class="col-md-12">
							<label for="cust_PergSatisfacao3"><%= Resources.View.lblPerg3 %></label>
						</div>
						<div class="col-md-12">
							<label class="radio">
							  <input type="radio" name="cust_PergSatisfacao3" id="24_A" value="1 - Muito Insatisfeito" /> <%= Resources.View.Perg3Opt1 %> 
							</label>
							<label class="radio">
							  <input type="radio" name="cust_PergSatisfacao3" id="24_B" value="2 - Insatisfeito"/> <%= Resources.View.Perg3Opt2 %>
							</label>
							<label class="radio">
							  <input type="radio" name="cust_PergSatisfacao3" id="24_C" value="3 - Indiferente"/><%= Resources.View.Perg3Opt3 %>
							</label>
							<label class="radio">
							  <input type="radio" name="cust_PergSatisfacao3" id="24_D" value="4 - Satisfeito" /><%= Resources.View.Perg3Opt4 %>
							</label>
							<label class="radio">
							  <input type="radio" name="cust_PergSatisfacao3" id="24_E" value="5 - Muito Satisfeito" /><%= Resources.View.Perg3Opt5 %>
							</label>
						</div>
					</div>  
				</div>  <!-- 3 -->
				<br class="clearfix" />
			</div>
			 <div class="modal-body" id="modal-pesquisa-body2" style="background:#fff">
				<div class="form-group">
					<div class="col-md-12">
						<div class="col-md-12">
							<label for="den-ddl-identificado"><%= Resources.View.lblPerg4 %></label>
						</div>
						<div class="col-md-12">
							<label class="radio">
							  <input type="radio" name="cust_PergSatisfacao4" id="41_A" value="Bom"/> <%= Resources.View.Perg4Opt1 %>
							</label>
							<label class="radio">
							  <input type="radio" name="cust_PergSatisfacao4" id="41_B" value="Excelente" /><%= Resources.View.Perg4Opt2 %>
							</label>
							<label class="radio">
							  <input type="radio" name="cust_PergSatisfacao4" id="41_C" value="Regular" /><%= Resources.View.Perg4Opt3 %>
							</label>
							<label class="radio">
							  <input type="radio" name="cust_PergSatisfacao4" id="41_D"  value="Ruim"/><%= Resources.View.Perg4Opt4 %>
							</label>
						</div> 
						<div class="col-md-12" id="Q4_NAO">
							  <label for="q2_nao_pq"><%= Resources.View.lblPerg41 %></label>
							  <input type="text" class="form-control" id="cust_PergSatisfacao4_1" /><br /><br />
						  </div>
					</div>  
				</div>    
				<div class="form-group">
					<div class="col-md-12">
						<div class="col-md-12">
							<label for="den-ddl-identificado"><%= Resources.View.lblPerg5 %></label>
						</div>
						<div class="col-md-12">
							<label class="radio">
							  <input type="radio" name="cust_PergSatisfacao5" id="5_A" value="Sim"/> <%= Resources.Relato.rblSim %>
							</label>
							<label class="radio">
							  <input type="radio" name="cust_PergSatisfacao5" id="5_B" value="Não"/><%= Resources.Relato.rblNao %>
							</label>
						</div>
						  <div class="col-md-12" id="Q5_NAO">
							  <label for="q2_nao_pq"><%= Resources.View.lblPerg51 %></label>
							  <input type="text" class="form-control" id="cust_PergSatisfacao5_1" /><br /><br />
						  </div>
					</div>  
				</div>    
				 <div class="form-group">
					<div class="col-md-12">
						<div class="col-md-12">
							<label for="den-ddl-identificado"><%= Resources.View.lblPerg6 %></label>
						</div>
						<div class="col-md-12">
							<label class="radio">
							  <input type="radio" name="cust_PergSatisfacao6" id="6_A" value="Sim"/> <%= Resources.Relato.rblSim %>
							</label>
							<label class="radio">
							  <input type="radio" name="cust_PergSatisfacao6" id="6_B" value="Não"/><%= Resources.Relato.rblNao %>
							</label>
						</div>
						<div class="col-md-12" id="Q6_NAO">
							<label for="q3_nao_pq"><%= Resources.View.lblPerg61 %></label>
							<input type="text" class="form-control" id="cust_PergSatisfacao6_1" /><br /><br />
						</div>
					</div>   
				</div>    <br class="clearfix" />
		   </div>
		   <div class="modal-footer">
				<div class="col-md-12 text-center">
					<a href="#" class="btn btn-success" id="den-btn-svp" ><i class="fa fa-check"></i> <%= Resources.View.btnEnviar %></a>

				</div>
			</div>
			<br class="clearfix" />
		</div><!-- /.modal-content --><br class="clearfix" />
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->    
	<div class="modal fade" id="mini-modal" tabindex="-1" role="dialog" aria-labelledby="mymini" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body" id="mini-modal-protocolo-body" style="background:#fff">
				<h4 class="alert-no-result"><i class="fa fa-spinner fa-pulse"></i> <%= Resources.Relato.lblEnviando %></h4>

			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->    
<div class="modal fade" id="modal-loading" tabindex="-1" role="dialog" aria-labelledby="myminimodalloading" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content" style="background:none!important; border:none!important">
			<div class="modal-body">
				<h4 class="alert-no-result"><i class="fa fa-spinner fa-pulse"></i> <%= Resources.Relato.lblCarregando %></h4>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->  
	

<%--Arquivos Suportados(Body)--%>
<div id="anexoPermitido" class="modal fade" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel2" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h2 class="modal-title" id="myModalLabel1"><i class="fa fa-question-circle"></i> Arquivos suportados</h2>
			</div>
			<div class="modal-body">
				<h4><p>A inclusão de arquivos em anexo será aceita apenas nos seguintes formatos:</p></h4><br/><br/>
				<h4><p><b>Áudio:</b> .wav, .ac3, .ogg, .aac, .wma, .mp3, .opus</p></h4><br/>
				<h4><p><b>Imagem/Foto:</b> .png, .jpeg, .jpg, .gif, .bmp</p></h4><br/>
				<h4><p><b>Documento/Texto:</b> .pdf, .ppt, .xls, .doc, .docx, .txt</p></h4><br/>
				<h4><p><b>Vídeo:</b> .avi, .mpeg, .mov, .mkv, .mp4, .m4a</p></h4><br/>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">OK, Entendi</button>
		
			</div>
		</div>
	</div>
</div>     


<div class="modal fade" id="modal-confirm-anexoremove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content alert-danger">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabelanexo"><i class="fa fa-warning"></i>  <%= Resources.Relato.lblMsgTitAnexo %></h4>
			</div>
			<div class="modal-body">
				<p> <%= Resources.Relato.lblMsgConfirma1 %><br/>  <%= Resources.Relato.lblMsgConfirma2 %> </p>
			</div>
			<div class="modal-footer">
				<div class="col-md-6 "> 
					<button type="button" class="btn btn-success btn-lg btn-block" id="btn-modal-anexoremove-confirm"><i class="fa fa-check"></i>  <%= Resources.Relato.rblSim %></button>
				</div>
				<div class="col-md-6 "> 
					<button type="button" class="btn btn-inverse btn-lg btn-block" id="btn-modal-anexoremove-close" data-dismiss="modal"><i class="fa fa-times"></i>  <%= Resources.Relato.rblNao %></button>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->   
	<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script> 
	<script type="text/javascript" src="resources/js/bootstrap.min.js" ></script>
	 <script type="text/javascript" src="resources/js/fileuploader.js"></script> 
	 <script type="text/javascript" src="resources/js/jquery.plugin.min.js"></script> 
	<script type="text/javascript" src="resources/js/init.js"></script>
</body>
</html>
