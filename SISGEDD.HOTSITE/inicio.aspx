﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="FI.Deloitte.Sisgedd.View.inicio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <link rel="shortcut icon" type="image/x-icon" href="resources/img/ico/favicon.ico" />    
    <link rel="manifest" href="resources/img/ico/manifest.json"/>
    <meta name="msapplication-TileColor" content="#ffffff"/>
    <meta name="msapplication-TileImage" content="resources/img/ico/ms-icon-144x144.png"/>
    <meta name="theme-color" content="#ffffff"/>  
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>SISGEDD® HOTSITE - SISTEMA DE GESTÃO DE DENÚNCIAS DELOITTE</title>
    <script type="text/javascript" src="resources/js/jquery.min.js"></script>     
   <link rel="stylesheet" href="resources/css/default.css"/>
  <script>
       INIT_PRE = function () {
          INIT_PLUGINS();
          LOAD_EMPRESA('den-ddl-empresa');
          jQuery(document).undelegate('#den-btn-save', 'click').delegate('#den-btn-save', 'click', function () { SET_AMBIENTE(); });
     };
      SET_AMBIENTE = function () {
          var _id = jQuery('#den-ddl-empresa option:selected').val() != '0' ? jQuery('#den-ddl-empresa option:selected').val() : 1;
          console.log(jQuery('#den-ddl-idioma option:selected').val());
          var _lang = jQuery('#den-ddl-idioma option:selected').val() != '0' ? jQuery('#den-ddl-idioma option:selected').val() : 1;

          switch(_lang) {
          case "1":
              _lang = "pt-BR";
              break;
          case "2":
              _lang = "en-US";
              break;
          case "3":
              _lang = "es-ES";
              break;
          case "4":
              _lang = "fr-FR";
              break;
          default :
              _lang = "pt-BR";
              break;
          }
          var _ambiente = "relato.aspx?id=" + _id + "&language=" + _lang;
          window.location.href = _ambiente;
      };
  </script>
</head>
<body>
    <form id="form1" runat="server" class="form-horizontal"  enctype="multipart/form-data" novalidate="novalidate" >
    <div class="container">
        <div class="col-md-12">
            <div class="row header"  >
                
                <img src="resources/img/logo_deloitte_header.png" class="pull-left img-responsive"  style="max-height: 120px;" />
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="h3-title" id="nome-canal">AMBIENTE DE TESTES E TREINAMENTOS<br /></h3>
                </div>
            </div>
        <div class="row">
            <h3 class="h3-subtitle">Identificação do cliente </h3>
            <div class="form-group">
                <div class="col-md-12">
                    <label for="den-ddl-empresa">Empresa </label>
                    <select class="form-control required" required="required" id="den-ddl-empresa" tabindex="1"> 
                    </select> 
                </div> 
           </div>    
            <hr />
            <div class="form-group">
               <div class="col-md-12">
                    
                        <label for="den-ddl-idioma">Escolha o idioma</label>
                        <select class="form-control" id="den-ddl-idioma" tabindex="2"> 
                            <option value="1">Português (PT-BR)</option>
                            <option value="2">Inglês (EN-US)</option>
                            <option value="3">Espanhol (ES)</option>
                            <option value="4">Francês (FR)</option>
                    </select> 
 
                    </div>
                   
                
            </div>    
            <div class="col-md-12">  
                <br />
                <br />
                <hr /> 
                <br />
            </div>
            <div class="form-group">
                <div class="col-md-12 text-center">     
                    <div class="col-md-6 ">    
                        <input type="hidden" id="den-txt-empresaid"        />
                        <a id="den-btn-save" class="btn btn-block btn-lg btn-default " tabindex="3"><i class="fa fa-laptop"></i>Registre seu Relato</a> 
                        <br/>    
                    </div>
                    <div class="col-md-6 ">    
                        <a id="den-btn-acompanhe" href="login.aspx" class="btn btn-block btn-lg btn-success" tabindex="4"><i class="fa fa-search"></i>Acompanhe seu Relato</a>     
                    </div>
                </div>
                    <div class="col-md-12">            
                        
                    </div>
                </div>
        </div>
        <div class="row">
            <footer>
                   
            </footer>
        </div>
            </div>
    </div>
        
    </form>

    <script type="text/javascript" src="resources/js/bootstrap.min.js" ></script>
     <script type="text/javascript" src="resources/js/fileuploader.js"></script> 
     <script type="text/javascript" src="resources/js/jquery.plugin.min.js"></script> 
      <script type="text/javascript" src="resources/js/jquery-ui.min.js"></script>    

     <script type="text/javascript" src="resources/js/init.js"></script>  
    <script>jQuery(document).ready(function () { INIT_PRE(); });</script>
</body>
</html>
