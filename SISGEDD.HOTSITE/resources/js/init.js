jQuery.noConflict();
jQuery.extend({ getUrlVars: function () { var vars = [], hash; var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&'); for (var i = 0; i < hashes.length; i++) { hash = hashes[i].split('='); vars.push(hash[0]); vars[hash[0]] = hash[1]; } return vars; }, getUrlVar: function (name) { return jQuery.getUrlVars()[name]; } });
jQuery(function (jQuery) { jQuery.datepicker.regional['pt-BR'] = { closeText: 'Fechar', prevText: '&#x3c;Anterior', nextText: 'Pr&oacute;ximo&#x3e;', currentText: 'Hoje', monthNames: ['Janeiro', 'Fevereiro', 'Mar&ccedil;o', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'], monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'], dayNames: ['Domingo', 'Segunda-feira', 'Ter&ccedil;a-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sabado'], dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'], dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'], weekHeader: 'Sm', dateFormat: 'dd/mm/yy', firstDay: 0, isRTL: false, showMonthAfterYear: false, yearSuffix: '' }; jQuery.datepicker.setDefaults(jQuery.datepicker.regional['pt-BR']); });
jQuery.fn.removeNot = function (settings) { var $this = jQuery(this); var defaults = { pattern: /[^0-9]/, replacement: '' }; settings = jQuery.extend(defaults, settings); $this.keyup(function () { var new_value = $this.val().replace(settings.pattern, settings.replacement); $this.val(new_value); }); return $this; };
jQuery(document).ready(function () {
	INIT_PLUGINS();
	//jQuery("body").bind("ajaxSend", function (e, xhr, settings) { INIT_PLUGINS(); }).bind("ajaxComplete", function (e, xhr, settings) { INIT_PLUGINS(); }).bind("ajaxError", function (e, xhr, settings, thrownError) { INIT_PLUGINS(); });
});
INIT_PLUGINS = function () {   
	jQuery(function () {
		var _idioma = jQuery.getUrlVar('language');

		if (_idioma === 'pt-BR') {
			if (jQuery('.fone').length > 0) { jQuery('.fone').mask("(99) 9999-9999?9", { placeholder: " " }); }
		}
		else {
				jQuery('.fone').removeClass('.fone');
		 
		}
		if (_idioma === 'pt-BR') {
			if (jQuery('.celular').length > 0) { jQuery('.celular').mask("(99) 9999?9-9999", { placeholder: " " }); }
		}
		else {
				jQuery('.celular').removeClass('.celular');
		 
		}
		if (jQuery('.datemask').length > 0) { jQuery('.datemask').mask("99/99/9999", { placeholder: " " }); }

		jQuery('den-txt-tel1').on('paste', function (event) {
  if (event.originalEvent.clipboardData.getData('Text').match(/[^\d]/)) {
	event.preventDefault();
  }
});

		jQuery("den-txt-tel1").on("keypress",function(event){
				if(event.which <= 48 || event.which >=57){
					return false;
				}
			});



	 });
};
SET_LANGUAGE = function (_lang) {
	var _opt = '';
	var _upl = '';
	switch (_lang) {
		case "pt-BR":
			_opt = '<option value="Anônimo">Selecione...</option>' +
				'<option value="Colaborador">Colaborador</option>' +
				'<option value="Ex-Colaborador">Ex-Colaborador</option>' +
				'<option value="Cliente">Cliente</option>' +
				'<option value="Fornecedor">Fornecedor</option>' +
				'<option value="Parceiro">Parceiro</option>' +
				'<option value="Prestador de Serviço">Prestador de Serviço</option>'+
				'<option value ="Comunidade">Comunidade</option>'+
				'<option value = "Terceiro">Terceiro</option> ';
				if(jQuery.getUrlVar('id') == "1085") //Deloitte //QGMI
				{
				 _opt = '<option value="Anônimo">Selecione...</option>' +
				'<option value="Cliente">Cliente</option>' +
				'<option value="Colaborador">Colaborador</option>' +
				'<option value="Comunidade">Comunidade</option>' +
				'<option value="Ex-Colaborador">Ex-Colaborador</option>' +
				'<option value="Fornecedor">Fornecedor</option>' +
				'<option value="Parceiro">Parceiro</option>'+
				'<option value ="Outro">Outro</option> ';	
					//console.log('naka');
				}
			_upl = 'Enviar arquivo';
			
			break;
		case "fr-FR":
			_opt ='<option value="Anonyme">Sélectionner...</option><option value = "Employé">Employé</option><option value="Ex-Employé">Ex-Employé</option><option value="Client">Client</option><option value="Fournisseur">Fournisseur</option><option value="Partner">Partner</option><option value="Fournisseur de service">Fournisseur de service</option><option value="Communauté">Communauté</option><option value="Autre">Autre</option>';
			if(jQuery.getUrlVar('id') == "1085") //Deloitte //QGMI
				{
				 _opt = '<option value="Anonyme">Sélectionner...</option>' +
				'<option value="Client">Client</option>' +
				'<option value="Communauté">Communauté</option>' +
				'<option value="Employé">Employé</option>' +
				'<option value="Ex-Employé">Ex-Employé</option>' +
				'<option value="Fournisseur">Fournisseur</option>' +
				'<option value="Partner">Partner</option>'+
				'<option value ="Autre">Autre</option> ';	
					//console.log('naka');
				}
				
			_upl = 'Envoyer l’archive';
			jQuery('.fone').removeClass('.fone');
			break;
		case "en-US":
			_opt = '<option value="Anonymous">Select...</option><option value = "Employee">Employee</option><option value="Ex-Employee">Ex-Employee</option><option value="Client">Client</option><option value="Provider">Provider</option><option value="Partner">Partner</option><option value="Service provider">Service provider</option><option value="Community">Community</option><option value="Other">Other</option>';
			if(jQuery.getUrlVar('id') == "1085") //Deloitte //QGMI
				{
				 _opt = '<option value="Anonymous">Select...</option>' +
				'<option value="Client">Client</option>' +
				'<option value="Community">Community</option>' +
				'<option value="Employee">Employee</option>' +
				'<option value="Ex-Employee">Ex-Employee</option>' +
				'<option value="Partner">Partner</option>' +
				'<option value="Provider">Provider</option>'+
				'<option value ="Other">Other</option> ';	
					//console.log('naka');
				}
				
			_upl = 'Choose a file...';
			jQuery('.fone').removeClass('.fone');
			break;
		case "es-ES":
			_opt = '<option value="Anónimo">Seleccione...</option><option value = "Empleado">Empleado</option><option value="Ex-Empleado">Ex-Empleado</option><option value="Cliente">Cliente</option><option value="Proveedor">Proveedor</option><option value="Socio">Socio</option><option value="Prestador de servicio">Prestador de servicio</option><option value="Comunidad">Comunidad</option><option value="Otro">Otro</option>';
			if(jQuery.getUrlVar('id') == "1085") //Deloitte //QGMI
				{
				 _opt = '<option value="Anónimo">Seleccione...</option>' +
				'<option value="Cliente">Cliente</option>' +
				'<option value="Comunidad">Comunidad</option>' +
				'<option value="Empleado">Empleado</option>' +
				'<option value="Ex-Empleado">Ex-Empleado</option>' +
				'<option value="Proveedor">Proveedor</option>' +
				'<option value="Socio">Socio</option>'+
				'<option value ="Otro">Otro</option> ';	
					//console.log('naka');
				}
			_upl = 'Enviar archivo...';
			jQuery('.fone').removeClass('.fone');
			break;
		default:
			_opt = '<option value="Anônimo">Selecione...</option><option value = "Colaborador">Colaborador</option><option value="Ex-Colaborador">Ex-Colaborador</option><option value="Cliente">Cliente</option><option value="Fornecedor">Fornecedor</option><option value="Parceiro">Parceiro</option><option value="Prestador de Serviço">Prestador de Serviço</option><option value="Comunidade">Comunidade</option><option value="Terceiro">Terceiro</option>';
			if(jQuery.getUrlVar('id') == "1085") //Deloitte //QGMI
			{
			 _opt = '<option value="Anônimo">Selecione...</option>' +
			'<option value="Cliente">Cliente</option>' +
			'<option value="Colaborador">Colaborador</option>' +
			'<option value="Comunidade">Comunidade</option>' +
			'<option value="Ex-Colaborador">Ex-Colaborador</option>' +
			'<option value="Fornecedor">Fornecedor</option>' +
			'<option value="Parceiro">Parceiro</option>'+
			'<option value ="Outro">Outro</option> ';	
				//console.log('naka');
				}
			_upl = 'Enviar arquivo';
			break;
	}
	jQuery('#den-ddl-tpp,#den1-ddl-relacaoempresa,#den2-ddl-relacaoempresa,#den3-ddl-relacaoempresa,#den4-ddl-relacaoempresa,#den5-ddl-relacaoempresa').html('').html(_opt);
   
	jQuery('#uploadercontainer').html('<i class="fa fa-upload"></i> ' + _upl);

};


//Descrição: Tratamento de Caracteres Especiais para sintaxe JSON (AJAX)
//Methods: GET/POST
//Author: Mihai
PARSER_JSON = function (str) {
	return str
		.replace("&quot;", "\"")
		.replace(/[\b]/g, '\\b')
		.replace(/[\f]/g, '\\f')
		.replace(/[\r]/g, '\\r')
		.replace(/[\t]/g, '\\t')
		.replace(/[\\"']/g, '\\$&');
};


//Descrição: Função jQuery para tratamento na entrada dos campos com caracteres especiais, evitando cross-scripting (XSS)
//Vulnerabilidade: Cross-site Scripting Stored XSS (2.3)
//Method: POST
//Author: Mihai
ESCAPE_HTML = function (string) {
	var entityMap = {
		"&": "&amp;",
		"<": "&lt;",
		">": "&gt;",
		"\"": "&quot;",
		"'": "&#39;",
		"/": "&#x2F;"
	};
	return String(string).replace(/[&<>"'\/]/g, function (s) {
		return entityMap[s];
	});
};

//Descrição: Função jQuery para tratamento na saída dos campos com caracteres especiais, evitando cross-scripting (XSS)
//Vulnerabilidade: Cross-site Scripting Stored XSS (2.3)
//Method: GET
//Author: Mihai
UNESCAPE_HTML = function (_string) {
	var entityMap = {
		"&amp;": "&",
		"&lt;": "<",
		"&gt;": ">",
		"&quot;": "\"",
		"&#39;": "'",
		"&#x2F;": "/"
	};
	return String(_string).replace(/[&<>"'\/]/g, function (s) {
		return entityMap[s];
	});
};

CLEAR = function (formname) {
	jQuery('#' + formname).find("input[type=text],input.i-spinner,input[type=hidden],input[type=email],input[type=datetime],input[type=password],textarea").each(function () { if (jQuery(this).attr('id') !== "pager") { jQuery(this).val(''); } }); jQuery('select option[value=0]').prop('selected', true);  jQuery('select option[value=UF]').prop('selected', true); jQuery('select option[value=00000]').prop('selected', true);  jQuery("input[type=checkbox]").each(function () { jQuery(this).prop("checked", false); });
	jQuery('#' + formname).find('select').each(function () { jQuery("#" + jQuery(this).attr('id') + " option:eq(0)").val(); });
	jQuery("input, div, label").removeClass('success, has-error, valid');
	
	jQuery('#anexos-container').html('');
	var g = GUID();
	jQuery("#den-txt-guid").val(g);

};
SETUP_UPLOADER = function (_type, _id) {
	//if (_type == 1) { if (jQuery('#' + _id).length == 1) { var uploader = new qq.FileUploader({ element: document.getElementById(_id), action: '../hotsite/FileUpload.ashx', allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'], debug: true, onComplete: function (id, fileName, responseJSON) { if (responseJSON.success) { jQuery("#img-preview_" + _id).attr("src", responseJSON.path); } } }); } };
	//if (_type == 2) { if (jQuery('#' + _id).length == 1) { var uploader = new qq.FileUploader({ element: document.getElementById(_id), action: '../hotsite/FileUpload.ashx', debug: true, onSubmit: function (id, fileName) { jQuery('#mini-modal').modal({ show: true, backdrop: 'static' }); }, onComplete: function (id, fileName, responseJSON) { jQuery('#mini-modal').modal('hide'); if (responseJSON.success) { ADD_ANEXO(responseJSON.path, responseJSON.name); } } }); } };
	//if (_type == 3) { if (jQuery('#' + _id).length == 1) { var uploader = new qq.FileUploader({ element: document.getElementById(_id), action: '../hotsite/FileUpload.ashx', debug: true, onSubmit: function (id, fileName) { jQuery('#mini-modal').modal({ show: true, backdrop: 'static' }); }, onComplete: function (id, fileName, responseJSON) { jQuery('#mini-modal').modal('hide'); if (responseJSON.success) { ADD_ANEXO(responseJSON.path, responseJSON.name); } } }); } };

	if (_type == 1) { if (jQuery('#' + _id).length == 1) { var uploader = new qq.FileUploader({ element: document.getElementById(_id), action: '../hotsite/FileUpload.ashx', allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'], debug: true, onComplete: function (id, fileName, responseJSON) { if (responseJSON.success) { jQuery("#img-preview_" + _id).attr("src", responseJSON.path); } else { SHOW_MESSAGEBOX('anexoinvalido'); jQuery('.qq-upload-file').hide(); }} }); } };
	if (_type == 2) { if (jQuery('#' + _id).length == 1) { var uploader = new qq.FileUploader({ element: document.getElementById(_id), action: '../hotsite/FileUpload.ashx', debug: true, onSubmit: function (id, fileName) { jQuery('#mini-modal').modal({ show: true, backdrop: 'static' }); }, onComplete: function (id, fileName, responseJSON) { jQuery('#mini-modal').modal('hide'); if (responseJSON.success) { ADD_ANEXO(responseJSON.path, responseJSON.name); } else { SHOW_MESSAGEBOX('anexoinvalido'); jQuery('.qq-upload-file').hide(); } } }); } };
	if (_type == 3) { if (jQuery('#' + _id).length == 1) { var uploader = new qq.FileUploader({ element: document.getElementById(_id), action: '../hotsite/FileUpload.ashx', debug: true, onSubmit: function (id, fileName) { jQuery('#mini-modal').modal({ show: true, backdrop: 'static' }); }, onComplete: function (id, fileName, responseJSON) { jQuery('#mini-modal').modal('hide'); if (responseJSON.success) { ADD_ANEXO(responseJSON.path, responseJSON.name); } else { SHOW_MESSAGEBOX('anexoinvalido'); jQuery('.qq-upload-file').hide(); } } }); } };
};
SHOW_MESSAGEBOX = function (_info) {
	var _msg = '';

	switch (_info) {
		case "updated": case "removed": case "success": _msg = '<div class="alert alert-success alert-fixed-top "   id="messagebox"><i class="fa fa-check-circle"></i>      <p>As informa&ccedil;&otilde;es adicionais foram enviadas com sucesso! </p><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a></div>'; break;
		case "warning": case "alert": _msg = '<div class="alert alert-warning alert-fixed-top "   id="messagebox"><i class="fa fa-exclamation-circle"></i><p><strong>     Aten&ccedil;&atilde;o! </strong> N&atilde;o foi poss&iacute;vel realizar a opera&ccedil;&atilde;o!</p><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a></div>'; break;
		case "error": _msg = '<div class="alert alert-error alert-fixed-top   "   id="messagebox"><i class="fa fa-times-circle"></i>      <p><strong>     Aten&ccedil;&atilde;o! </strong> Verifique os campos e tente novamente. </p><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a></div>'; break;
		case "required": _msg = '<div class="alert alert-warning alert-fixed-top "  id="messagebox"><i class="fa fa-info-circle"></i>       <p><strong>    Aten&ccedil;&atilde;o! </strong> Verifique se todos os campos foram preenchidos corretamente!</p><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a></div>'; break;
		case "exist": _msg = '<div class="alert alert-warning alert-fixed-top "  id="messagebox"><i class="fa fa-info-circle"></i>       <p><strong>    Aten&ccedil;&atilde;o! </strong> Filiado j&aacute; cadastrado! Tente pesquisar e editar o registro.</p><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a></div>'; break;

		case "anexoinvalido": _msg = '<div class="alert alert-error alert-fixed-top   "   id="messagebox"><i class="fa fa-times-circle"></i>      <p><strong>     Aten&ccedil;&atilde;o! </strong> Formato de arquivo não-suportado! (Verificar arquivos suportados) </p><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a></div>'; break;
	}

	jQuery('#messagebox').remove();
	jQuery(_msg.toString()).insertBefore('.header');
	jQuery('#messagebox').fadeIn('fast');
	setTimeout(function () { jQuery('.alert, .alert-success, .alert-fixed-top').fadeOut('slow').remove(); }, 5000);
};
REPLACE_ALL = function (find, replace, str) { return str.replace(new RegExp(find, 'g'), replace); };
REMOVE_FORM = function (_id) {
 
	 if (_id !== '1') {
		 jQuery('#den_identificado' + _id).hide();
	 }
	 jQuery('#den_identificado' + _id + ' input[type=text]').val('');
	 jQuery('#den_identificado' + _id + ' input[type=email]').val('');
	 jQuery('#den' + _id + '-ddl-participacao').val('Sujeito');
	 jQuery('#den' + _id + '-ddl-relacaoempresa').val('Colaborador');
 };
SHOWDIV = function (_div) { jQuery('#' + _div).show(); };
HIDEDIV = function (_div) { jQuery('#' + _div).hide(); };
INIT = function () {
	var _id = jQuery.getUrlVar('id');
	var _idioma = jQuery.getUrlVar('language');

	//Hotfix: CANAL DE DENÚNCIA - GRUPO HABIBS (3142)
	if (_id === '3142') {
		jQuery("#lbl-filial").text("Nível 1");
		jQuery("#lbl-produto").text("Nível 2");
		jQuery("#lbl-area").text("Nível 3");
	}

	//Hotfix: CANAL DE ÉTICA E CONDUTA DO IGH (1108)
	if (_id === '1108') {
		jQuery("#exibea").hide();
		jQuery("#lbl-filial").text("Cidade");
		jQuery("#lbl-produto").text("Unidade / Contrato");
		jQuery("#exibep").removeClass("col-md-3");
		jQuery("#exibep").addClass("col-md-6");
	}


	jQuery('#den-txt-idioma').val(_idioma);
	INIT_PLUGINS();
	SETUP_UPLOADER(2, 'file-uploader2');
	jQuery(document).undelegate('input[name=identificar]', 'click').delegate('input[name=identificar]', 'click', function () {
		var _vl = jQuery(this).val();
		jQuery('#den-txt-email,#den-txt-emailanonimo').removeAttr("required").removeClass("required");
		jQuery('#identificado-container').show();
		jQuery('#identificado-containerN').hide();
		if (_vl === 'N') {
			jQuery('#identificado-container').hide();
			jQuery('#identificado-containerN').show();
		}
	});
	jQuery(document).undelegate('#den_identificado1_s', 'click').delegate('#den_identificado1_s', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#den_identificado1').show(); } else { jQuery('#den_identificado1').hide(); } });
	jQuery(document).undelegate('#den_identificado1_n', 'click').delegate('#den_identificado1_n', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#den_identificado1,#den_identificado2,#den_identificado3,#den_identificado4,#den_identificado5').hide(); } else { jQuery('#identificado-container').show(); } });
	jQuery(document).undelegate('a[remove-form]', 'click').delegate('a[remove-form]', 'click', function (e) { e.preventDefault(); REMOVE_FORM(jQuery(this).attr('data-form')); });
	jQuery(document).undelegate('a[add-form]', 'click').delegate('a[add-form]', 'click', function (e) { e.preventDefault(); jQuery('#' + jQuery(this).attr('data-form')).show() });
	jQuery('#den_identificado1,#den_identificado2,#den_identificado3,#den_identificado4,#den_identificado5').hide();
	jQuery(document).undelegate('#den-ddl-unidade', 'change').delegate('#den-ddl-unidade', 'change', function (e)
	{
		e.preventDefault();
		LOAD_PRODUTO('den-ddl-produto', jQuery('#den-ddl-unidade option:selected').val());
	});
	jQuery(document).undelegate('#den-ddl-produto', 'change').delegate('#den-ddl-produto', 'change', function (e)
	{
		e.preventDefault();
		LOAD_AREA('den-ddl-area', jQuery('#den-ddl-produto option:selected').val());
	});

	jQuery('#den-txt-empresaid').val(_id);
	jQuery(document).undelegate('#den-btn-save', 'click').delegate('#den-btn-save', 'click', function () { SAVE_DENUNCIA_SITE(); });
	FIND_EMPRESA(_id);
	var g = GUID();
	jQuery("#den-txt-guid").val(g);
	SET_LANGUAGE(_idioma);
};
INIT_M1 = function () {
	var _id = jQuery.getUrlVar('id');
	var _idioma = jQuery.getUrlVar('language');

	jQuery('#den-txt-idioma').val(_idioma);
	INIT_PLUGINS();
	SETUP_UPLOADER(2, 'file-uploader2');
	jQuery(document).undelegate('input[name=identificar]', 'click').delegate('input[name=identificar]', 'click', function () {
		var _vl = jQuery(this).val();
		jQuery('#den-txt-email,#den-txt-emailanonimo').removeAttr("required").removeClass("required");
		jQuery('#identificado-container').show();
		jQuery('#identificado-containerN').hide();
		if (_vl === 'N') {
			jQuery('#identificado-container').hide();
			jQuery('#identificado-containerN').show();
		}
	});
	jQuery(document).undelegate('#den_identificado1_s', 'click').delegate('#den_identificado1_s', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#den_identificado1').show(); } else { jQuery('#den_identificado1').hide(); } });
	jQuery(document).undelegate('#den_identificado1_n', 'click').delegate('#den_identificado1_n', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#den_identificado1,#den_identificado2,#den_identificado3,#den_identificado4,#den_identificado5').hide(); } else { jQuery('#identificado-container').show(); } });
	jQuery(document).undelegate('a[remove-form]', 'click').delegate('a[remove-form]', 'click', function (e) { e.preventDefault(); REMOVE_FORM(jQuery(this).attr('data-form')); });
	jQuery(document).undelegate('a[add-form]', 'click').delegate('a[add-form]', 'click', function (e) { e.preventDefault(); jQuery('#' + jQuery(this).attr('data-form')).show() });
	jQuery('#den_identificado1,#den_identificado2,#den_identificado3,#den_identificado4,#den_identificado5').hide();
	jQuery(document).undelegate('#den-ddl-unidade', 'change').delegate('#den-ddl-unidade', 'change', function (e) { e.preventDefault(); LOAD_PRODUTO('den-ddl-produto', jQuery('#den-ddl-unidade option:selected').val()); });
	jQuery(document).undelegate('#den-ddl-produto', 'change').delegate('#den-ddl-produto', 'change', function (e) { e.preventDefault(); LOAD_AREA('den-ddl-area', jQuery('#den-ddl-produto option:selected').val()); });

	jQuery('#den-txt-empresaid').val(_id);
	jQuery(document).undelegate('#den-btn-save', 'click').delegate('#den-btn-save', 'click', function () { SAVE_DENUNCIA_SITE(); });
	FIND_EMPRESA(_id);
	var g = GUID();
	jQuery("#den-txt-guid").val(g);
	SET_LANGUAGE(_idioma);
};

INIT_M2 = function () {
	var _id = jQuery.getUrlVar('id');
	var _idioma = jQuery.getUrlVar('language');

	jQuery('#den-txt-idioma').val(_idioma);
	INIT_PLUGINS();
	SETUP_UPLOADER(2, 'file-uploader2');
	jQuery(document).undelegate('input[name=identificar]', 'click').delegate('input[name=identificar]', 'click', function () {
		var _vl = jQuery(this).val();
		jQuery('#den-txt-email,#den-txt-emailanonimo').removeAttr("required").removeClass("required");
		jQuery('#identificado-container').show();
		jQuery('#identificado-containerN').hide();
		if (_vl === 'N') {
			jQuery('#identificado-container').hide();
			jQuery('#identificado-containerN').show();
		}
	});
	jQuery(document).undelegate('#den_identificado1_s', 'click').delegate('#den_identificado1_s', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#den_identificado1').show(); } else { jQuery('#den_identificado1').hide(); } });
	jQuery(document).undelegate('#den_identificado1_n', 'click').delegate('#den_identificado1_n', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#den_identificado1,#den_identificado2,#den_identificado3,#den_identificado4,#den_identificado5').hide(); } else { jQuery('#identificado-container').show(); } });
	jQuery(document).undelegate('a[remove-form]', 'click').delegate('a[remove-form]', 'click', function (e) { e.preventDefault(); REMOVE_FORM(jQuery(this).attr('data-form')); });
	jQuery(document).undelegate('a[add-form]', 'click').delegate('a[add-form]', 'click', function (e) { e.preventDefault(); jQuery('#' + jQuery(this).attr('data-form')).show() });
	jQuery('#den_identificado1,#den_identificado2,#den_identificado3,#den_identificado4,#den_identificado5').hide();
	jQuery(document).undelegate('#den-ddl-unidade', 'change').delegate('#den-ddl-unidade', 'change', function (e) { e.preventDefault(); LOAD_PRODUTO('den-ddl-produto', jQuery('#den-ddl-unidade option:selected').val()); });
	jQuery(document).undelegate('#den-ddl-produto', 'change').delegate('#den-ddl-produto', 'change', function (e) { e.preventDefault(); LOAD_AREA('den-ddl-area', jQuery('#den-ddl-produto option:selected').val()); });

	jQuery('#den-txt-empresaid').val(_id);
	jQuery(document).undelegate('#den-btn-save', 'click').delegate('#den-btn-save', 'click', function () { SAVE_DENUNCIA_SITE(); });
	FIND_EMPRESA(_id);
	var g = GUID();
	jQuery("#den-txt-guid").val(g);
	jQuery('#identificadoI').click();
	//jQuery('#identificadoN,#identificadoS,#identificadoI').attr('disabled','disabled');
	SET_LANGUAGE(_idioma);
	jQuery('label[class=radio-inline]:eq(0)').hide();
	jQuery('input[name=identificar]:eq(0)').hide();

	jQuery('label[class=radio-inline]:eq(2)').hide();
	jQuery('input[name=identificar]:eq(2)').hide();
};
INIT_M3 = function () {
	var _id = jQuery.getUrlVar('id');
	var _idioma = jQuery.getUrlVar('language');

	jQuery('#den-txt-idioma').val(_idioma);
	INIT_PLUGINS();
	SETUP_UPLOADER(2, 'file-uploader2');
	jQuery(document).undelegate('input[name=identificar]', 'click').delegate('input[name=identificar]', 'click', function () {
		var _vl = jQuery(this).val();
		jQuery('#den-txt-email,#den-txt-emailanonimo').removeAttr("required").removeClass("required");
		jQuery('#identificado-container').show();
		jQuery('#identificado-containerN').hide();
		if (_vl === 'N') {
			jQuery('#identificado-container').hide();
			jQuery('#identificado-containerN').show();
		}
	});
	jQuery(document).undelegate('#den_identificado1_s', 'click').delegate('#den_identificado1_s', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#den_identificado1').show(); } else { jQuery('#den_identificado1').hide(); } });
	jQuery(document).undelegate('#den_identificado1_n', 'click').delegate('#den_identificado1_n', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#den_identificado1,#den_identificado2,#den_identificado3,#den_identificado4,#den_identificado5').hide(); } else { jQuery('#identificado-container').show(); } });
	jQuery(document).undelegate('a[remove-form]', 'click').delegate('a[remove-form]', 'click', function (e) { e.preventDefault(); REMOVE_FORM(jQuery(this).attr('data-form')); });
	jQuery(document).undelegate('a[add-form]', 'click').delegate('a[add-form]', 'click', function (e) { e.preventDefault(); jQuery('#' + jQuery(this).attr('data-form')).show() });
	jQuery('#den_identificado1,#den_identificado2,#den_identificado3,#den_identificado4,#den_identificado5').hide();
	jQuery(document).undelegate('#den-ddl-unidade', 'change').delegate('#den-ddl-unidade', 'change', function (e) {
		e.preventDefault();
		LOAD_PRODUTO('den-ddl-produto', jQuery('#den-ddl-unidade option:selected').val());
	});
	jQuery(document).undelegate('#den-ddl-produto', 'change').delegate('#den-ddl-produto', 'change', function (e) {
		e.preventDefault();
		LOAD_AREA('den-ddl-area', jQuery('#den-ddl-produto option:selected').val());
	});

	jQuery('#den-txt-empresaid').val(_id);
	jQuery(document).undelegate('#den-btn-save', 'click').delegate('#den-btn-save', 'click', function () { SAVE_DENUNCIA_SITE(); });
	FIND_EMPRESA(_id);
	var g = GUID();
	jQuery("#den-txt-guid").val(g);
	var _opt = '<option value="Agência Reguladora">Agência Reguladora</option>' +
		'<option value = "Cliente (Pessoa Física)" > Cliente(Pessoa Física)</option>' +
		'<option value="Cliente (Pessoa Jurídica)">Cliente (Pessoa Jurídica)</option>' +
		'<option value="Comunidade">Comunidade</option>' +
		'<option value="Conselhos de Classe">Conselhos de Classe</option>' +
		'<option value="Convênio / Empregador">Convênio / Empregador</option>' +
		'<option value="Correspondente Bancário">Correspondente Bancário</option>' +
		'<option value="Fornecedor">Fornecedor </option>' +
		'<option value="Funcionário">Funcionário</option>' +
		'<option value="Ex-Funcionário">Ex-Funcionário</option>' +
		'<option value="Parceiro Comercial">Parceiro Comercial</option>' +
		'<option value="Setor Público">Setor Público</option>' +
		'<option value="Sindicatos">Sindicatos</option>' +
		'<option value="Supervisores do SFN (Bacen, CVM, Susep e Previc)">Supervisores do SFN (Bacen, CVM, Susep e Previc)</option>' +
		'<option value="Terceirizados / Prestadores de Serviço">Terceirizados / Prestadores de Serviço</option>' +
		'<option value="Não Identificado / Outros">Não Identificado / Outros</option>';
	jQuery('#den-ddl-tpp,#den1-ddl-relacaoempresa,#den2-ddl-relacaoempresa,#den3-ddl-relacaoempresa,#den4-ddl-relacaoempresa,#den5-ddl-relacaoempresa').html('').html(_opt);

};

// Função: viewRelatoByPK
// Method: GET
// _id: ID do Relato
VIEW_RELATO = function (_id)
 {
	jQuery('#divloading').show();
	 INIT_PLUGINS();
	 SETUP_UPLOADER(2, 'file-uploader2');
	 var _idioma = jQuery.getUrlVar('language');

	 jQuery('#modal-loading').modal('show');
	 jQuery("#den-btn-pesq").hide();
	 jQuery("#den-btn-reab").hide();

	 jQuery.ajax({
		 type: "POST", 
		 url: 'services.aspx/viewRelatoByPK', 
		 data: "{'_id':'" + _id + "','_idioma':'" + _idioma + "'}",
		 contentType: "application/json; charset=utf-8",
		 dataType: "json", async: true, cache: false,
		 success: function (msg)
		 {
			 if (msg.d !== null && msg.d !== '' && msg.d !== undefined)
			 {
				 var _obj = JSON.parse(msg.d.toString());
				 var _a = eval(_obj.relato);   
				 
				 jQuery("#txt-relato-id").val(_id);
				 jQuery("#den-txt-guid").val('').val(_a[0].data_guid);
				 jQuery("#nome-canal").html('').html(_a[0].data_nomecanal);
				 jQuery("#logo-cliente").removeAttr('src').attr('src', _a[0].data_logo);
				 jQuery("#protocolo").html('').html(_a[0].data_nmr_protocolo);

				 jQuery("#den-ddl-status").html('').html(_a[0].data_status);
				 jQuery("#den-ddl-datainicio").html('').html(_a[0].data_inicio);
				 jQuery("#den-ddl-dataatualizacao").html('').html(_a[0].data_atualizacao);


				 PARSER_JSON(UNESCAPE_HTML(jQuery("#den-txt-desc").html('').html(_a[0].data_relato)));

				 if (_a[0].data_fluxo === 'F' && _a[0].data_resppesq!=='S') {
					 jQuery('.pesq-sat').hide();
					 if (_a[0].data_resp === "False" || _a[0].data_resp === "false") { jQuery("#den-btn-pesq").show(); } else { jQuery("#den-btn-pesq").hide(); }
					 jQuery("#den-btn-save").hide();
					 jQuery(document).undelegate('#den-btn-pesq', 'click').delegate('#den-btn-pesq', 'click', function () { SHOW_MODAL_PESQUISA(_a[0].data_modelo); });
				 }

				 else {
					 jQuery("#den-btn-save").show();
					 jQuery('.pesq-resp').hide();
				 }
				 if (_a[0].data_emailid !== null && _a[0].data_emailid !== '') {
					 FIND_RELATO_EMAIL_FRAME(_id);
				 }
				 FIND_HISTORICO(_id, 2, 'historico-container');
				 FIND_RESPOSTA_BWISE(_a[0].data_nmr_protocolo, 'resposta-container');
				 FIND_ANEXOS(_a[0].data_guid);
			 }
		 }, complete: function () { INIT_PLUGINS(); jQuery('#divloading').hide(); }
	 });     
	 jQuery(document).undelegate('#den-btn-save', 'click').delegate('#den-btn-save', 'click', function () { UPDATE_DENUNCIA_SITE(); });
	 jQuery(document).undelegate('#den-btn-svp', 'click').delegate('#den-btn-svp', 'click', function () { SAVE_PESQUISA(); });
};

// Função: findRelatoEmailFrame
// Method: GET
// _id: ID do Relato
FIND_RELATO_EMAIL_FRAME = function (_id) {
	jQuery.ajax({
		type: "POST",
		url: 'services.aspx/findRelatoEmailFrame', data: "{'_rid':'" + _id + "'}", contentType: "application/json; charset=utf-8",
		dataType: "json", async: true, cache: false,
		success: function (msg) {
			jQuery('#relato-emailcontainer').html(msg.d);
			var _h = jQuery('#ifrmail-' + _id).text();
			jQuery('#relato-emailcontainer').html('').html(_h).show();
			jQuery('#den-txt-desc').hide();
		}
	});
	//

};

// Função: findHistorico
// Method: GET
// _id: ID do Relato
// _tip: Tipo da interação com o Relato
// _container: Nome do container(Table HTML)
FIND_HISTORICO = function (_rid,_tip, _container) {
	jQuery('#divloading').show();
	 return jQuery.ajax({
		 type: "POST", url: 'services.aspx/findHistorico',
		 data: "{'_relato_id':'" + _rid + "','_tipo':'" + _tip + "'}",
		 contentType: "application/json; charset=utf-8",
		 dataType: "json", async: true, cache: false,
		 success: function(msg) {
			  jQuery('#' + _container).html('').html(msg.d);
			  jQuery('#divloading').hide();
		 },
		 error: function(msg) {
			  SHOW_MESSAGEBOX('error'); jQuery('#divloading').hide();
		 }
	 }).promise();
 };


FIND_RESPOSTA_BWISE = function (_protocolo, _container) {
	jQuery('#divloading').show();
	 return jQuery.ajax({
		 type: "POST", url: 'services.aspx/findResposta',
		 data: "{'_protocolo':'" + _protocolo + "'}",
		 contentType: "application/json; charset=utf-8",
		 dataType: "json", async: true, cache: false,
		 success: function (msg) { jQuery('#' + _container).html('').html(msg.d); jQuery('#divloading').hide(); },
		 error: function (msg) { SHOW_MESSAGEBOX('error'); jQuery('#divloading').hide(); }
	 }).promise();
 };

SAVE_HISTORICO = function (_rid,_tip,_com,_aut) {
	 //1=historico atendente/revisor,2=historico revisor/denunciante, 3=historico revisor/bwise
	 return  jQuery.ajax({
		 type: "POST", url: 'services.aspx/saveHistorico',
		 data: "{'_rid':'" + _rid + "','_tip':'" + _tip + "','_aut':'" + _aut + "','_com':'" + _com + "'}",
		 contentType: "application/json; charset=utf-8", dataType: "json", async: true, cache: false,
		 success: function (msg) {
			 if (msg.d === 'success') { FIND_HISTORICO(_rid, 2, 'historico-container'); CLEAR('form1'); }
			 else { SHOW_MESSAGEBOX(msg.d); }
		 },
		 error: function (msg) {
			 SHOW_MESSAGEBOX('error');
			 jQuery('body').animate({ scrollTop: '0' }, 100, 'swing');
		 }
	 }).promise();
};

SET_LOADING = function () {
	var _idioma = jQuery.getUrlVar('language');
	var _msgloading = 'Carregando...';
	switch (_idioma) {
		case 'pt-BR':
			_msgloading = 'Carregando...';
			break;
		case 'en-US':
			_msgloading = 'Loading...';
			break;
		case 'fr-FR':
			_msgloading = 'Wait...';
			break;
		case 'es-ES':
			_msgloading = 'Aguarde...';
			break;
	}
	return _msgloading;
};

LOAD_EMPRESA = function (_ddl) {
	var _msg = SET_LOADING();
	jQuery('#' + _ddl).html('<option>'+_msg+'</option>');
	jQuery.ajax({
		type: "POST", url: 'services.aspx/loadEmpresa',
		data: "{}", contentType: "application/json; charset=utf-8",
		dataType: "json", async: true, cache: false,
		success: function (msg)
		{
			if (msg.d !== '<option value="0" selected="selected">Outros...</option>')
			{
				jQuery('#' + _ddl).html('').append(msg.d);
			}
			else {
				jQuery('#' + _ddl).html('').html(msg.d);
			}
		},
		error: function (msg) { SHOW_MESSAGEBOX('error'); }
	});
};

LOAD_PRODUTO = function (_ddl, _eid) {
	var _msg = SET_LOADING();
	jQuery('#' + _ddl).html('<option>' + _msg + '</option>');
	var _firstOption = "Selecione...";
	var _idi = jQuery.getUrlVar('language');
	if (_idi === undefined || _idi === null) {
		_idi = '';
	}
	else {
		switch (_idi) {
			case "pt-BR":
				_firstOption = "Selecione...";
				break;
			case "en-US":
				_firstOption = "Select...";
				break;
			case "es-ES":
				_firstOption = "Seleccione...";
				break;
			case "fr-FR":
				_firstOption = "Sélectionner...";
				break;
		}
	}
	jQuery.ajax({
		type: "POST", url: 'services.aspx/loadProduto',
		data: "{'_unidade_id':'" + _eid + "'}", contentType: "application/json; charset=utf-8",
		dataType: "json", async: true, cache: false,
		success: function (msg) {
			if (msg.d !== '<option value="0" selected="selected">Outros...</option>')
			{
				jQuery('#' + _ddl).html('').html('<option value="0">' + _firstOption + '</option>').append(msg.d);
			}
			else
			{
				jQuery('#' + _ddl).html('').html(msg.d);
			}
		},
		error: function (msg) { SHOW_MESSAGEBOX('error'); }
	});
};

LOAD_UNIDADE = function (_ddl, _eid, _idi) {
	
	var _firstOption = "Selecione...";
	if (_idi === undefined || _idi === null) {
		_idi = '';
	}
	else {
		switch (_idi) {
			case "pt-BR":
				_firstOption = "Selecione...";
				break;
			case "en-US":
				_firstOption = "Select...";
				break;
			case "es-ES":
				_firstOption = "Seleccione...";
				break;
			case "fr-FR":
				_firstOption = "Sélectionner...";
				break;
		}
	}
	var _msg = SET_LOADING();
	jQuery('#' + _ddl).html('<option>' + _msg + '</option>');
	jQuery.ajax({
		type: "POST", url: 'services.aspx/loadUnidade',
		data: "{'_empresa_id':'" + _eid + "','_idioma':'" + _idi + "'}",
		contentType: "application/json; charset=utf-8",
		dataType: "json", async: true, cache: false,
		success: function (msg) {
			if (msg.d !== '<option value="0" selected="selected">Outros...</option>')
			{
				jQuery('#' + _ddl).html('').html('<option value="0">'+_firstOption+'</option>').append(msg.d);
			}
			else
			{
				jQuery('#' + _ddl).html('').html(msg.d);
			}
		},
		error: function (msg) { SHOW_MESSAGEBOX('error'); }
	});
};

LOAD_AREA = function (_ddl, _eid) {
	var _idi = jQuery.getUrlVar('language');
	var _firstOption = "Selecione...";
	if (_idi === undefined || _idi === null) {
		_idi = '';
	}
	else {
		switch (_idi) {
			case "pt-BR":
				_firstOption = "Selecione...";
				break;
			case "en-US":
				_firstOption = "Select...";
				break;
			case "es-ES":
				_firstOption = "Seleccione...";
				break;
			case "fr-FR":
				_firstOption = "Sélectionner...";
				break;
		}
	}
	jQuery('#' + _ddl).html('<option>Carregando...</option>');
	jQuery.ajax({
		type: "POST", url: 'services.aspx/loadArea',
		data: "{'_produto_id':'" + _eid + "'}", contentType: "application/json; charset=utf-8",
		dataType: "json", async: true, cache: false,
		success: function (msg) {
			if (msg.d !== '<option value="0" selected="selected">Outros...</option>')
			{
				jQuery('#' + _ddl).html('').html('<option value="0">' + _firstOption +'</option>').append(msg.d);
			}
			else {
				jQuery('#' + _ddl).html('').html(msg.d);
			}
		},
		error: function (msg) { SHOW_MESSAGEBOX('error'); }
	});
};

FIND_EMPRESA = function (_id) {
	var _idioma = jQuery.getUrlVar('language');
	
	if (_idioma === undefined || _idioma === null) {
		_idioma = '';
	
	}
	jQuery.ajax({
		type: "POST", url: 'services.aspx/findEmpresaByPK', data: "{'_id':'" + _id + "'}",
		contentType: "application/json; charset=utf-8",
		dataType: "json", async: true, cache: false,
		success: function (msg) {// 
			if (msg.d !== null && msg.d !== '' && msg.d !== undefined) {
				var _obj = JSON.parse(msg.d.toString());
				var _a = eval(_obj.empresa);

				var _nomecanal = '';
				switch (_idioma) {
					case 'es-ES':
						_nomecanal = _a[0].data_tituloes;
						break;
					case 'en-US':
						_nomecanal = _a[0].data_tituloen;
						break;
					case 'pt-BR':
						_nomecanal = _a[0].data_titulocanal;
						break;
					case 'fr-FR':
						_nomecanal = _a[0].data_titulofr;
						break;
					default :
						_nomecanal = _a[0].data_titulocanal;
						break;
				}
				jQuery("#den-ddl-empresa").val(_a[0].data_empresaid).attr('disabled', 'disabled');
				jQuery("#nome-canal").html('').html(_nomecanal);
				jQuery("#exibee").show(_a[0].data_exibee === 'S');
				jQuery("#exibeu").show(_a[0].data_exibeu === 'S');
				jQuery("#exibea").show(_a[0].data_exibea === 'S');
				jQuery("#exibep").show(_a[0].data_exibep === 'S');

				jQuery("#den-txt-empresaid").val('').val(_id);
				jQuery("#logo-cliente").removeAttr('src').attr('src', _a[0].data_logo);
				LOAD_UNIDADE('den-ddl-unidade', _id, _idioma);
	
				//TESTE_DEN();
				LOAD_TIPORELATO_ALL(_id);
				LOAD_TIPOPUBLICO_ALL(_id);
			}
		}, complete: function () {
		   INIT_PLUGINS();
		}
	});
};

// Descrição: Salva a denúncia (Formulário)
// Função AJAX: addRelatoSite
// Method: POST
SAVE_DENUNCIA_SITE = function () {
	jQuery('#mini-modal').modal({ show: true, backdrop: 'static' });
	jQuery('#den-btn-save').html('<i class="fa fa-spinner fa-pulse"></i> Por favor, Aguarde...').attr('disabled', 'disabled');
	var _identificado = jQuery('input[name=identificar]:checked').val();

	var _tipo_publico = jQuery('#den-ddl-tpp option:selected').val() !== undefined && jQuery('#den-ddl-tpp option:selected').val() !== '0' ? PARSER_JSON(ESCAPE_HTML((jQuery('#den-ddl-tpp option:selected').text()))) : '0' ;
	var _nome = PARSER_JSON(ESCAPE_HTML(jQuery('#den-txt-nome').val()));
	var _cargo = PARSER_JSON(ESCAPE_HTML(jQuery('#den-txt-cargo').val()));
	var _email = PARSER_JSON(ESCAPE_HTML(jQuery('#den-txt-email').val()));

	var _celular = jQuery('#den-txt-tel2').val();
	var _telefone = jQuery('#den-txt-tel1').val();

	//Hotfix Mihai: Gênero = Não-Identificado (Anônimo)
	var _genero = jQuery('input[name=genero]:checked').val() !== undefined && jQuery('input[name=identificar]:checked').val() !== 'N'  ? jQuery('input[name=genero]:checked').val() : 'Não Identificado';

	var _emailanonimo = PARSER_JSON(ESCAPE_HTML(jQuery('#den-txt-emailanonimo').val()));
	var _empresa_id = jQuery('#den-txt-empresaid').val();
	/*10*/

	var _unidade_id = jQuery('#den-ddl-unidade option:selected').val() !== undefined && jQuery('#den-ddl-unidade option:selected').val() !== '0' ? jQuery('#den-ddl-unidade option:selected').val() : '0';
	var _unidade = jQuery('#den-ddl-unidade option:selected').val() !== undefined && jQuery('#den-ddl-unidade option:selected').val() !== '0' ? PARSER_JSON(ESCAPE_HTML(jQuery('#den-ddl-unidade option:selected').text())) : '';
	var _produto_id = jQuery('#den-ddl-produto option:selected').val() !== undefined && jQuery('#den-ddl-produto option:selected').val() !== '0' ? jQuery('#den-ddl-produto option:selected').val() : '0';
	var _produto = jQuery('#den-ddl-produto option:selected').val() !== undefined && jQuery('#den-ddl-produto option:selected').val() !== '0' ? PARSER_JSON(ESCAPE_HTML(jQuery('#den-ddl-produto option:selected').text())) : '';
	var _area_id = jQuery('#den-ddl-area option:selected').val() !== undefined && jQuery('#den-ddl-area option:selected').val() !== '0' ? jQuery('#den-ddl-area option:selected').val() : '0';
	var _area = jQuery('#den-ddl-area option:selected').val() !== undefined && jQuery('#den-ddl-area option:selected').val() !== '0' ? PARSER_JSON(ESCAPE_HTML(jQuery('#den-ddl-area option:selected').text())) : '';
	
	var _relato = PARSER_JSON(ESCAPE_HTML(jQuery('#den-txt-desc').val()));

	var _tiporelato = jQuery('#den-ddl-tiporelato option:selected').val() !== undefined && jQuery('#den-ddl-tiporelato option:selected').val() !== '0' ? PARSER_JSON(ESCAPE_HTML(jQuery('#den-ddl-tiporelato option:selected').text())) : '0' ;
	var _possivel_identificar_pessoas = jQuery('input[name=identificado]:checked').val();
	var _den1_nome = PARSER_JSON(jQuery('#den1-txt-nome').val());
	/*20*/ 

	var _den1_participacao = jQuery('#den1-ddl-participacao option:selected').val() !== undefined && jQuery('#den1-ddl-participacao option:selected').val() !== '0' ? PARSER_JSON(ESCAPE_HTML(jQuery('#den1-ddl-participacao option:selected').text())) : '0' ;
	var _den1_relacao = jQuery('#den1-ddl-relacaoempresa option:selected').val() !== undefined && jQuery('#den1-ddl-relacaoempresa option:selected').val() !== '0' ? PARSER_JSON(ESCAPE_HTML(jQuery('#den1-ddl-relacaoempresa option:selected').text())) : '0' ;
	var _den1_email = PARSER_JSON(ESCAPE_HTML(jQuery('#den1-txt-email').val()));
	var _den1_cargo = PARSER_JSON(ESCAPE_HTML(jQuery('#den1-txt-cargo').val()));
	var _den1_telefone = jQuery('#den1-txt-tel').val();
	var _den1_cidade = PARSER_JSON(ESCAPE_HTML(jQuery('#den1-txt-cidade').val()));
	var _den1_estado = PARSER_JSON(ESCAPE_HTML(jQuery('#den1-txt-estado').val()));
	var _den2_nome = PARSER_JSON(ESCAPE_HTML(jQuery('#den2-txt-nome').val()));
	var _den2_participacao = jQuery('#den2-ddl-participacao option:selected').val()  !== undefined && jQuery('#den2-ddl-participacao option:selected').val() !== '0' ? PARSER_JSON(ESCAPE_HTML(jQuery('#den2-ddl-participacao option:selected').text())) : '0' ;
	var _den2_relacao = jQuery('#den2-ddl-relacaoempresa option:selected').val() !== undefined && jQuery('#den2-ddl-relacaoempresa option:selected').val() !== '0' ? PARSER_JSON(ESCAPE_HTML(jQuery('#den2-ddl-relacaoempresa option:selected').text())) : '0' ;
	/*30*/ 

	var _den2_email = PARSER_JSON(ESCAPE_HTML(jQuery('#den2-txt-email').val()));
	var _den2_cargo = PARSER_JSON(ESCAPE_HTML(jQuery('#den2-txt-cargo').val()));
	var _den2_telefone = jQuery('#den2-txt-tel').val();
	var _den2_cidade = PARSER_JSON(ESCAPE_HTML(jQuery('#den2-txt-cidade').val()));
	var _den2_estado = PARSER_JSON(ESCAPE_HTML(jQuery('#den2-txt-estado').val()));
	var _den3_nome = PARSER_JSON(ESCAPE_HTML(jQuery('#den3-txt-nome').val()));
	var _den3_participacao = jQuery('#den3-ddl-participacao option:selected').val()  !== undefined && jQuery('#den3-ddl-participacao option:selected').val() !== '0' ? PARSER_JSON(ESCAPE_HTML(jQuery('#den3-ddl-participacao option:selected').text())) : '0' ;
	var _den3_relacao = jQuery('#den3-ddl-relacaoempresa option:selected').val() !== undefined && jQuery('#den3-ddl-relacaoempresa option:selected').val() !== '0' ? PARSER_JSON(ESCAPE_HTML(jQuery('#den3-ddl-relacaoempresa option:selected').text())) : '0' ;
	var _den3_email = PARSER_JSON(ESCAPE_HTML(jQuery('#den3-txt-email').val()));
	var _den3_cargo = PARSER_JSON(ESCAPE_HTML(jQuery('#den3-txt-cargo').val()));
	/*40*/  

	var _den3_telefone = PARSER_JSON(ESCAPE_HTML(jQuery('#den3-txt-tel').val()));
	var _den3_cidade = PARSER_JSON(ESCAPE_HTML(jQuery('#den3-txt-cidade').val()));
	var _den3_estado = PARSER_JSON(ESCAPE_HTML(jQuery('#den3-txt-estado').val()));
	var _den4_nome = PARSER_JSON(ESCAPE_HTML(jQuery('#den4-txt-nome').val()));
	var _den4_participacao = jQuery('#den4-ddl-participacao option:selected').val()  !== undefined && jQuery('#den4-ddl-participacao option:selected').val() !== '0' ? PARSER_JSON(ESCAPE_HTML(jQuery('#den4-ddl-participacao option:selected').text())) : '0' ;
	var _den4_relacao = jQuery('#den4-ddl-relacaoempresa option:selected').val() !== undefined && jQuery('#den4-ddl-relacaoempresa option:selected').val() !== '0' ? PARSER_JSON(ESCAPE_HTML(jQuery('#den4-ddl-relacaoempresa option:selected').text())) : '0' ;
	var _den4_email = PARSER_JSON(ESCAPE_HTML( jQuery('#den4-txt-email').val()));
	var _den4_cargo = PARSER_JSON(ESCAPE_HTML(jQuery('#den4-txt-cargo').val()));
	var _den4_telefone = jQuery('#den4-txt-tel').val();
	var _den4_cidade = PARSER_JSON(ESCAPE_HTML(jQuery('#den4-txt-cidade').val()));
	/*50*/ 

	var _den4_estado = PARSER_JSON(ESCAPE_HTML(jQuery('#den4-txt-estado').val()));
	var _den5_nome = PARSER_JSON(ESCAPE_HTML(jQuery('#den5-txt-nome').val()));
	var _den5_participacao = jQuery('#den5-ddl-participacao option:selected').val()  !== undefined && jQuery('#den1-ddl-participacao option:selected').val() !== '0' ? PARSER_JSON(ESCAPE_HTML(jQuery('#den5-ddl-participacao option:selected').text())) : '0' ;
	var _den5_relacao = jQuery('#den5-ddl-relacaoempresa option:selected').val() !== undefined && jQuery('#den5-ddl-relacaoempresa option:selected').val() !== '0' ? PARSER_JSON(ESCAPE_HTML(jQuery('#den5-ddl-relacaoempresa option:selected').text())) : '0' ;
	var _den5_email = PARSER_JSON(ESCAPE_HTML(jQuery('#den5-txt-email').val()));
	var _den5_cargo = PARSER_JSON(ESCAPE_HTML(jQuery('#den5-txt-cargo').val()));
	var _den5_telefone = jQuery('#den5-txt-tel').val();
	var _den5_cidade = PARSER_JSON(ESCAPE_HTML(jQuery('#den5-txt-cidade').val()));
	var _den5_estado = PARSER_JSON(ESCAPE_HTML(jQuery('#den5-txt-estado').val()));
	var _evidencias = PARSER_JSON(ESCAPE_HTML(jQuery('#den-txt-evidencia').val()));
	/*60*/ 

	var _nmr_protocolo_anterior = PARSER_JSON(ESCAPE_HTML(jQuery('#den-txt-protocoloanterior').val()));
	var _periodo_evento_ocorreu = PARSER_JSON(ESCAPE_HTML(jQuery('#den-txt-dataocorreu').val()));
	var _existe_recorrencia = PARSER_JSON(ESCAPE_HTML(jQuery('#den-txt-recorrencia').val()));

	var _como_ficou_ciente = PARSER_JSON(ESCAPE_HTML(jQuery('#den-txt-comosoube').val()));


	var _perda_financeira = PARSER_JSON(ESCAPE_HTML(jQuery('#den-txt-perdafinanceira').val()));
	
	var _quantoaorelato = jQuery('#den-ddl-quantoaorelato option:selected').val() !== '0' ? PARSER_JSON(ESCAPE_HTML(jQuery('#den-ddl-quantoaorelato option:selected').val())) : '';
	//console.log('_quantoaorelato -> '+_quantoaorelato);

	var _sugestao = PARSER_JSON(ESCAPE_HTML(jQuery('#den-txt-sugestao').val()));
	var _tipomanifestacao = '';
	var _idioma = jQuery('#den-txt-idioma').val();
	var _guid = jQuery('#den-txt-guid').val();

	/*70*/

	var _anexos = '';
	var _validate_form = true;

	 if (jQuery('form#form1').valid() && _validate_form === true) {
		jQuery.ajax({
			type: "POST", 
			url: 'services.aspx/addRelatoSite',
			data: "{'"

			+ "_IDENTIFICADO':'"                    + _identificado + "','"
			+ "_TIPO_PUBLICO':'"                    + _tipo_publico + "','"
			+ "_NOME':'"                            + _nome + "','"
			+ "_CARGO':'"                           + _cargo + "','"
			+ "_EMAIL':'"                           + _email + "','"
			+ "_CELULAR':'"                         + _celular + "','"
			+ "_TELEFONE':'"                        + _telefone + "','"
			+ "_GENERO':'"                          + _genero + "','"
			+ "_EMAIL_ANONIMO':'"                   + _emailanonimo + "','"
			+ "_EMPRESA_ID':'"                      + _empresa_id + "','" //10

			+ "_UNIDADE_ID':'"                      + _unidade_id + "','"
			+ "_UNIDADE':'"                         + _unidade + "','"
			+ "_PRODUTO_ID':'"                      + _produto_id + "','"
			+ "_PRODUTO':'"                         + _produto + "','"
			+ "_AREA_ID':'"                         + _area_id + "','"
			+ "_AREA':'"                            + _area + "','"
			+ "_RELATO':'"                          + _relato + "','"
			+ "_TIPO_RELATO':'"                     + _tiporelato + "','" 
			+ "_POSSIVEL_IDENTIFICAR_PESSOAS':'"    + _possivel_identificar_pessoas + "','"
			+ "_DEN1_NOME':'"                       + _den1_nome + "','" //20

			+ "_DEN1_PARTICIPACAO':'"               + _den1_participacao + "','"
			+ "_DEN1_RELACAO':'"                    + _den1_relacao + "','"
			+ "_DEN1_EMAIL':'"                      + _den1_email + "','"
			+ "_DEN1_CARGO':'"                      + _den1_cargo + "','"
			+ "_DEN1_TELEFONE':'"                   + _den1_telefone + "','"
			+ "_DEN1_CIDADE':'"                     + _den1_cidade + "','"
			+ "_DEN1_ESTADO':'"                     + _den1_estado + "','"
			+ "_DEN2_NOME':'"                       + _den2_nome + "','"
			+ "_DEN2_PARTICIPACAO':'"               + _den2_participacao + "','"
			+ "_DEN2_RELACAO':'"                    + _den2_relacao + "','" //30

			+ "_DEN2_EMAIL':'"                      + _den2_email + "','"
			+ "_DEN2_CARGO':'"                      + _den2_cargo + "','"
			+ "_DEN2_TELEFONE':'"                   + _den2_telefone + "','"
			+ "_DEN2_CIDADE':'"                     + _den2_cidade + "','"
			+ "_DEN2_ESTADO':'"                     + _den2_estado + "','"
			+ "_DEN3_NOME':'"                       + _den3_nome + "','"
			+ "_DEN3_PARTICIPACAO':'"               + _den3_participacao + "','"
			+ "_DEN3_RELACAO':'"                    + _den3_relacao + "','"
			+ "_DEN3_EMAIL':'"                      + _den3_email + "','"
			+ "_DEN3_CARGO':'"                      + _den3_cargo + "','" //40

			+ "_DEN3_TELEFONE':'"                   + _den3_telefone + "','"
			+ "_DEN3_CIDADE':'"                     + _den3_cidade + "','"
			+ "_DEN3_ESTADO':'"                     + _den3_estado + "','"
			+ "_DEN4_NOME':'"                       + _den4_nome + "','"
			+ "_DEN4_PARTICIPACAO':'"               + _den4_participacao + "','"
			+ "_DEN4_RELACAO':'"                    + _den4_relacao + "','"
			+ "_DEN4_EMAIL':'"                      + _den4_email + "','"
			+ "_DEN4_CARGO':'"                      + _den4_cargo + "','"
			+ "_DEN4_TELEFONE':'"                   + _den4_telefone + "','"
			+ "_DEN4_CIDADE':'"                     + _den4_cidade + "','" //50

			+ "_DEN4_ESTADO':'"                     + _den4_estado + "','"
			+ "_DEN5_NOME':'"                       + _den5_nome + "','"
			+ "_DEN5_PARTICIPACAO':'"               + _den5_participacao + "','"
			+ "_DEN5_RELACAO':'"                    + _den5_relacao + "','"
			+ "_DEN5_EMAIL':'"                      + _den5_email + "','"
			+ "_DEN5_CARGO':'"                      + _den5_cargo + "','"
			+ "_DEN5_TELEFONE':'"                   + _den5_telefone + "','"
			+ "_DEN5_CIDADE':'"                     + _den5_cidade + "','"
			+ "_DEN5_ESTADO':'"                     + _den5_estado + "','"
			+ "_EVIDENCIAS':'"                      + _evidencias + "','"//60

			+ "_NMR_PROTOCOLO_ANTERIOR':'"          + _nmr_protocolo_anterior + "','"
			+ "_PERIODO_EVENTO_OCORREU':'"          + _periodo_evento_ocorreu + "','"
			+ "_EXISTE_RECORRENCIA':'"              + _existe_recorrencia + "','"
			+ "_COMO_FICOU_CIENTE':'"               + _como_ficou_ciente + "','"
			+ "_PERDA_FINANCEIRA':'"                + _perda_financeira + "','"
			+ "_QUANTO_AO_RELATO':'"                + _quantoaorelato + "','"
			+ "_SUGESTAO':'"                        + _sugestao + "','"
			+ "_TIPO_MANIFESTACAO':'"               + _tipomanifestacao + "','"
			+ "_IDIOMA':'"                          + _idioma + "','"         
			+ "_GUID':'"                            + _guid + "'"//70
			+ "}",
		  contentType: "application/json; charset=utf-8", 
			dataType: "json", 
			async: true, 
			cache: false,
			success: function (msg) {
				if (msg.d !== 'error' && msg.d !== 'warning' && msg.d !== 'alert') {
					CLEAR('form1');
					SHOW_PROTOCOLO(msg.d);INIT();
				}
				SHOW_MESSAGEBOX(msg.d);
				jQuery('body').animate({ scrollTop: '0' }, 100, 'swing');
				jQuery('#den-btn-save').html('<i class="fa fa-check"></i> Salvar').removeAttr('disabled');
				jQuery('#mini-modal').modal('hide');
			},
			error: function (msg) {
				jQuery('body').animate({ scrollTop: '0' }, 100, 'swing');
				SHOW_MESSAGEBOX('error'); jQuery('body').animate({ scrollTop: '0' }, 100, 'swing');
				jQuery('#den-btn-save').html('<i class="fa fa-check"></i> Salvar').removeAttr('disabled');
				jQuery('#mini-modal').modal('hide');
			}
		}); 
	}
	else {
		SHOW_MESSAGEBOX('required'); jQuery('body').animate({ scrollTop: '0' }, 100, 'swing');
		jQuery('#den-btn-save').html('<i class="fa fa-check"></i> Salvar').removeAttr('disabled');
		jQuery('#mini-modal').modal('hide');
	}
};

// Descrição: Atualiza a denúncia (Acompanhamento)
// Method: addRelatoSite
// Method: POST
UPDATE_DENUNCIA_SITE = function () {
	jQuery('#mini-modal').modal({ show: true, backdrop: 'static' });
	jQuery('#den-btn-save').html('<i class="fa fa-spinner fa-pulse"></i> Aguarde...').attr('disabled', 'disabled');
  var _relato_id = jQuery('#txt-relato-id').val();
  var _comentario = PARSER_JSON(ESCAPE_HTML(jQuery('#den-txt-replica').val()));

  jQuery.ajax({
	 type: "POST", url: 'services.aspx/updateRelatoSite',
	 data: "{'" + "_relato_id':'" + _relato_id + "','" + "_comentarios':'" + _comentario + "'" + "}",
	 contentType: "application/json; charset=utf-8",
	 dataType: "json",
	 async: true,
	 cache: false,     
	 success: function () { 
		 SHOW_MESSAGEBOX('success');
		 jQuery('body').animate({ scrollTop: '0' }, 100, 'swing');
		 jQuery('#mini-modal').modal('hide');
		 setTimeout(function () { location.href = window.location.href; }, 5000);
		 //
	 },
	  error:function () {
			  SHOW_MESSAGEBOX('error');
			  jQuery('#mini-modal').modal('hide');
		  }
 });
};
SHOW_PROTOCOLO = function (_txt) {
	jQuery('#modal-protocolo').modal({ show: true, backdrop: 'static' });
	jQuery('#modal-protocolo-body').html('').html(_txt);
	CLEAR('form1');
	INIT();
};

SPLASH_PESQUISA = function (_txt) {
	jQuery('#modal-pesquisa-respondida').modal({ show: true, backdrop: 'static' });
	jQuery('#modal-pesquisa-respondida-body').html('').html(_txt);
	jQuery(document).undelegate('#den-btn-okay', 'click').delegate('#den-btn-okay', 'click', function (e) { e.preventDefault(); jQuery('#modal-pesquisa-respondida').modal('hide'); location.reload(true);});
	CLEAR('form1');
	//setTimeout(function () { location.href = window.location.href; }, 5000);
	INIT();
};

SHOW_MODAL_PESQUISA = function (_modelo) {
	 
	jQuery('#modal-pesquisa').modal({ show: true, backdrop: 'static' });
	jQuery('#modal-pesquisa-body1,#modal-pesquisa-body2,#Q1_SIM,#Q1_NAO,#Q1A_NAO,#Q2_NAO,#Q3_NAO,#Q4_NAO,#Q5_NAO,#Q6_NAO').hide();
	jQuery('#modal-pesquisa-body' + _modelo).show();
	jQuery(document).undelegate('#11_C', 'click').delegate('#11_C', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#Q1_SIM,#Q1_NAO,#Q1A_NAO,#Q2_NAO,#Q3_NAO,#Q4_NAO,#Q5_NAO,#Q6_NAO,#Q3_container,#Q2_container').hide(); } });
	jQuery(document).undelegate('#11_B', 'click').delegate('#11_B', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#Q1_NAO,#Q2_container,#Q3_container').show(); jQuery('#Q1_SIM').hide(); } /*else { jQuery('#Q1_NAO').hide(); }*/ jQuery('#cust_PergSatisfacao1_1b').val(''); });
	jQuery(document).undelegate('#11_A', 'click').delegate('#11_A', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#Q1_NAO').hide(); jQuery('#Q1_SIM,#Q2_container,#Q3_container').show(); } /*else { jQuery('#Q1_NAO').show(); }*/ jQuery('#cust_PergSatisfacao1_1b').val(''); });
	jQuery(document).undelegate('#1A_B', 'click').delegate('#1A_B', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#Q1A_NAO').show(); } else { jQuery('#Q1A_NAO').hide(); } jQuery('#cust_PergSatisfacao1_2a').val(''); });
	jQuery(document).undelegate('#1A_A', 'click').delegate('#1A_A', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#Q1A_NAO').hide(); } else { jQuery('#Q1A_NAO').show(); } jQuery('#cust_PergSatisfacao1_2a').val(''); });
	jQuery(document).undelegate('#21_B', 'click').delegate('#21_B', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#Q2_NAO').show(); } else { jQuery('#Q2_NAO').hide(); } jQuery('#cust_PergSatisfacao2_2').val(''); });
	jQuery(document).undelegate('#21_A', 'click').delegate('#21_A', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#Q2_NAO').hide(); } else { jQuery('#Q2_NAO').show(); } jQuery('#cust_PergSatisfacao2_2').val(''); });
	jQuery(document).undelegate('#41_D', 'click').delegate('#41_D', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#Q4_NAO').show(); } else { jQuery('#Q4_NAO').hide(); } jQuery('#cust_PergSatisfacao4_1').val(''); });
	jQuery(document).undelegate('#41_A,#41_B,#41_C', 'click').delegate('#41_A,#41_B,#41_C', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#Q4_NAO').hide(); } else { jQuery('#Q4_NAO').show(); } jQuery('#cust_PergSatisfacao4_1').val(''); });
	jQuery(document).undelegate('#5_B', 'click').delegate('#5_B', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#Q5_NAO').show(); } else { jQuery('#Q5_NAO').hide(); } jQuery('#cust_PergSatisfacao5_1').val(''); });
	jQuery(document).undelegate('#5_A', 'click').delegate('#5_A', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#Q5_NAO').hide(); } else { jQuery('#Q5_NAO').show(); } jQuery('#cust_PergSatisfacao5_1').val(''); });
	jQuery(document).undelegate('#6_B', 'click').delegate('#6_B', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#Q6_NAO').show(); } else { jQuery('#Q6_NAO').hide(); } jQuery('#cust_PergSatisfacao6_1').val(''); });
	jQuery(document).undelegate('#6_A', 'click').delegate('#6_A', 'click', function () { if (jQuery(this).has(':checked')) { jQuery('#Q6_NAO').hide(); } else { jQuery('#Q6_NAO').show(); } jQuery('#cust_PergSatisfacao6_1').val(''); });
	jQuery(document).undelegate('#den-btn-svp', 'click').delegate('#den-btn-svp', 'click', function (e) { e.preventDefault(); SAVE_PESQUISA(); });

};
SAVE_PESQUISA = function () {
	var _protocolo = jQuery('#protocolo').html();

   /*modelo 2*/
	var _pg1 = jQuery('input:radio[name=cust_PergSatisfacao1_1]:checked').val()!==undefined?jQuery('input:radio[name=cust_PergSatisfacao1_1]:checked').val():'';
	var _pg1b = jQuery('#cust_PergSatisfacao1_1b').val()!==undefined?jQuery('#cust_PergSatisfacao1_1b').val():'';
	var _pg1a = jQuery('input:radio[name=cust_PergSatisfacao1_1a]:checked').val()!==undefined?jQuery('input:radio[name=cust_PergSatisfacao1_1a]:checked').val():'';
	var _pg12a = jQuery('#cust_PergSatisfacao1_2a').val()!==undefined?jQuery('#cust_PergSatisfacao1_2a').val():'';
	var _pg21 = jQuery('input:radio[name=cust_PergSatisfacao2_1]:checked').val()!==undefined?jQuery('input:radio[name=cust_PergSatisfacao2_1]:checked').val():'';
	var _pg21a = jQuery('#cust_PergSatisfacao2_2').val()!==undefined?jQuery('#cust_PergSatisfacao2_2').val():'';
	var _pg3 = jQuery('input:radio[name=cust_PergSatisfacao3]:checked').val()!==undefined?jQuery('input:radio[name=cust_PergSatisfacao3]:checked').val():'';

	/*modelo 1*/
	var _pg4 = jQuery('input:radio[name=cust_PergSatisfacao4]:checked').val()!==undefined?jQuery('input:radio[name=cust_PergSatisfacao4]:checked').val():'';
	var _pg41 = jQuery('#cust_PergSatisfacao4_1').val()!==undefined?jQuery('#cust_PergSatisfacao4_1').val():'';
	var _pg5 = jQuery('input:radio[name=cust_PergSatisfacao5]:checked').val()!==undefined?jQuery('input:radio[name=cust_PergSatisfacao5]:checked').val():'';
	var _pg51 = jQuery('#cust_PergSatisfacao5_1').val()!==undefined?jQuery('#cust_PergSatisfacao5_1').val():'';
	var _pg6 = jQuery('input:radio[name=cust_PergSatisfacao6]:checked').val()!==undefined?jQuery('input:radio[name=cust_PergSatisfacao6]:checked').val():'';
	var _pg61 = jQuery('#cust_PergSatisfacao6_1').val()!==undefined?jQuery('#cust_PergSatisfacao6_1').val():'';
	if (_protocolo === '' || _protocolo === undefined) {
		SHOW_MESSAGEBOX('error');
	}
	else {
		var _relato_id = jQuery('#txt-relato-id').val();

		jQuery.ajax({
			type: "POST", url: 'services.aspx/addPesquisa',
			data: "{'"
			/*modelo 2*/
			+ "_pg1':'" + _pg1 + "','"
			+ "_pg1b':'" + _pg1b + "','"
			+ "_pg1a':'" + _pg1a + "','"
			+ "_pg12a':'" + _pg12a + "','"
			+ "_pg21':'" + _pg21 + "','"
			+ "_pg21a':'" + _pg21a + "','"
			+ "_pg3':'" + _pg3 + "','"

			/*modelo 1*/
			+ "_pg4':'" + _pg4 + "','"
			+ "_pg41':'" + _pg41 + "','"
			+ "_pg5':'" + _pg5 + "','"
			+ "_pg51':'" + _pg51 + "','"
			+ "_pg6':'" + _pg6 + "','"
			+ "_pg61':'" + _pg61 + "','"

			+ "_protocolo':'" + _protocolo + "'"
			 + "}",
			contentType: "application/json; charset=utf-8", dataType: "json", async: true, cache: false,
			success: function (msg) {
				if (msg.d !== 'error' && msg.d !== 'warning' && msg.d !== 'alert') {
					jQuery('#den-btn-pesq').hide(); jQuery('#modal-pesquisa').modal('hide');
					CLEAR('form1');
					SPLASH_PESQUISA(msg.d);INIT();
					//ATUALIZA_FLAG_FLUXO('X'); //atualiza o FLG_FLUXO PARA 'X' (DATAFEED FAZ ESSA ATUALIZAÇÃO)
				}
				SHOW_MESSAGEBOX(msg.d);
				jQuery('body').animate({ scrollTop: '0' }, 1000, 'swing');
			},
			error: function (msg) {
				jQuery('body').animate({ scrollTop: '0' }, 1000, 'swing');
				SHOW_MESSAGEBOX('error');
			   

			}
		});
	}
};
INIT_LOGIN = function () {
	jQuery("#user-txt-login,#user-txt-senha").removeAttr('disabled');
	var _eid = jQuery.getUrlVar('eid');
	
	jQuery("#user-txt-eid").val(_eid);
	jQuery(document).undelegate('#login-btn-OK', 'click').delegate('#login-btn-OK', 'click', function (e) {
		e.preventDefault();
		LOGIN_V1();
	});
};
LOGIN_V1 = function () {
	var _usr = jQuery("#user-txt-login").val();
	var _pwd = jQuery("#user-txt-senha").val();
	//console.log("{'_usr':'" + _usr + "','_pwd':'" + _pwd + "'}");

	if (jQuery("#frm-login").valid()) {
		jQuery("#login-btn-OK").html('<i class="fa fa-spinner fa-pulse"></i> Aguarde...');
		jQuery("#user-txt-login,#user-txt-senha,#login-btn-OK").attr('disabled', 'disabled');
		jQuery.ajax({
			type: "POST", url: 'services.aspx/loginSite',

			data: "{'_usr':'" + _usr + "','_pwd':'" + _pwd + "'}",
contentType: "application/json; charset=utf-8",
			dataType: "json",
			async: true,
			cache: false,
			success: function (msg) {
				if (msg.d === 'success') { location.href = 'languageRoute.aspx'; }
				else
				{
					SHOW_MESSAGEBOX('warning');
					jQuery('body').animate({ scrollTop: '0' }, 100, 'swing');
					jQuery("#user-txt-login,#user-txt-senha,#login-btn-OK").removeAttr('disabled');
					jQuery("#login-btn-OK").html('<i class="fa fa-sign-in" ></i> Entrar');
				};
			},
			error: function (msg) {
				SHOW_MESSAGEBOX('error');
				jQuery("#login-btn-OK").html('<i class="fa fa-sign-in" ></i> Entrar'); jQuery("#user-txt-login,#user-txt-senha,#login-btn-OK").removeAttr('disabled');
			}
		});
	} else { SHOW_MESSAGEBOX('required'); jQuery("#user-txt-login,#user-txt-senha,#login-btn-OK").removeAttr('disabled'); }

}
	DO_LOGIN = function () {   
		var _usr = jQuery("#user-txt-login").val();
		var _pwd = jQuery("#user-txt-senha").val();
		var _eid = jQuery("#user-txt-eid").val();

		if (jQuery("#frm-login").valid()&&_eid!==null&&_eid!=='') {       
			jQuery("#login-btn-OK").html('<i class="fa fa-spinner fa-pulse"></i> Aguarde...');
			jQuery("#user-txt-login,#user-txt-senha,#login-btn-OK").attr('disabled', 'disabled');
			//console.log("{'_usr':'" + _usr + "','_pwd':'" + _pwd + "','_eid':" + _eid + "}");
			jQuery.ajax({
				type: "POST", url: '../services.aspx/loginBySite',

				data: "{'_usr':'" + _usr + "','_pwd':'" + _pwd + "','_eid':" + _eid + "}",
		


				contentType: "application/json; charset=utf-8",
				dataType: "json",
				async: true,
				cache: false,
				success: function (msg)
				{
					if (msg.d === 'success') { location.href = 'languageRoute.aspx'; }
					else
					{
						SHOW_MESSAGEBOX('warning');
						jQuery('body').animate({ scrollTop: '0' }, 100, 'swing');
						jQuery("#user-txt-login,#user-txt-senha,#login-btn-OK").removeAttr('disabled');
						jQuery("#login-btn-OK").html('<i class="fa fa-sign-in" ></i> Entrar');
					};
				},
				error: function (msg) {
					SHOW_MESSAGEBOX('error');
					jQuery("#login-btn-OK").html('<i class="fa fa-sign-in" ></i> Entrar'); jQuery("#user-txt-login,#user-txt-senha,#login-btn-OK").removeAttr('disabled');
				}
			});
		} else { SHOW_MESSAGEBOX('required'); jQuery("#user-txt-login,#user-txt-senha,#login-btn-OK").removeAttr('disabled'); }
	};
	NO_HISTORY = function () { var backlen = history.length; history.go(-backlen); };
	ADD_ANEXO = function (_anexo, _nome) {
	
		var _guid = jQuery('#den-txt-guid').val();

		// alert(_anexo + "-" + _nome + "-" + _guid);
		if (_guid === undefined || _guid === "") {  return;

		} else {
			jQuery.ajax({
				type: "POST",
				url: 'services.aspx/addAnexo',
				data: "{'nome':'" + _nome + "','url':'" + _anexo + "','guid':'" + _guid + "'}",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				async: true,
				cache: false,
				success: function (msg) {
			 
					if (msg.d === "success") {
						FIND_ANEXOS(_guid);
					} else {
						SHOW_MESSAGEBOX(msg.d);
					}
				},
				error: function (msg) {
			   
					SHOW_MESSAGEBOX('error');
					jQuery('body').animate({ scrollTop: '0' }, 100, 'swing');
				}
			});
		}

	};
	REMOVE_ANEXO = function (aid) {
		var uid =  '0';
		var guid = jQuery('#den-txt-guid').val();
 
		jQuery.ajax({
			type: "POST",
			url: 'services.aspx/removeAnexo',
			data: "{'aid':'" + aid + "','uid':'" + uid + "'}",
			contentType: "application/json; charset=utf-8",
			dataType: "json", async: true, cache: false,
			success: function (msg) { SHOW_MESSAGEBOX(msg.d); if (msg.d === 'success') { FIND_ANEXOS(guid); } },
			error: function () { SHOW_MESSAGEBOX('error'); }
		});

	};

	//REABRIR_RELATO = function (_relato_id) {
	//	var uid =  '0';
	//	var guid = jQuery('#den-txt-guid').val();
 
	//	jQuery.ajax({
	//		type: "POST",
	//		url: 'services.aspx/reabrirRelato',
	//		data: "{'_relato_id':'" + _relato_id + "'}",
	//		contentType: "application/json; charset=utf-8",
	//		dataType: "json", async: true, cache: false,
	//		success: function (msg) { SHOW_MESSAGEBOX(msg.d); if (msg.d === 'success') {
	//			 VIEW_RELATO(_relato_id); 
	//			 jQuery('#modal-confirm-reabrerelato').modal('hide');
	//			 jQuery('.pesq-sat').show();
	//		} },
	//		error: function () { SHOW_MESSAGEBOX('error'); }
	//	});
	//};

	REABRIR_RELATO = function () {
		jQuery('#modal-confirm-reabrerelato').modal('hide');
		jQuery("#den-btn-save").show();
		jQuery("#den-btn-pesq").hide();
		jQuery("#den-btn-reab").hide();
		jQuery('.pesq-resp').hide();
		jQuery('.pesq-sat').show();
	};

	FIND_ANEXOS = function (guid) {
		jQuery('#divloading').show();
		jQuery("#anexos-container").html("").show();
		if (guid !== undefined && guid !== null && guid !== '') {
			jQuery.ajax({
				type: "POST", url: "services.aspx/findAnexo",
				data: "{'_guid':'" + guid + "'}",
				contentType: "application/json; charset=utf-8", dataType: "json", async: true, cache: false,
				success: function (msg) {
					jQuery('#anexos-container').html("").html(msg.d);
					jQuery('#divloading').hide();
					jQuery('#modal-loading').modal('hide');
				},
				error: function (msg) { SHOW_MESSAGEBOX("error"); jQuery('#divloading').hide(); },
				complete: function () { INIT_PLUGINS(); jQuery('#modal-loading').modal('hide'); }
			});
		}
	};
	GUID = function () {
		function s4() { return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1); }
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	}
	SHOW_MODAL_ANEXO_REMOVE = function (_a) {
		jQuery('#modal-confirm-anexoremove').modal({ show: true });
		jQuery(document).undelegate('#btn-modal-anexoremove-confirm', 'click').delegate('#btn-modal-anexoremove-confirm', 'click', function (e) {
			e.preventDefault();
			REMOVE_ANEXO(_a); 
			jQuery('#btn-modal-anexoremove-close').click();
		});
	};

	SHOW_MODAL_REABRIR_RELATO = function (_id) {
		jQuery('#modal-confirm-reabrerelato').modal({ show: true });
		jQuery(document).undelegate('#btn-modal-reabrerelato-confirm', 'click').delegate('#btn-modal-reabrerelato-confirm', 'click', function (e) {
			e.preventDefault();
			REABRIR_RELATO();
			jQuery('#btn-modal-anexoremove-close').click();
		});
	};


//-----------------------------------------------------------------------//
//== T I P O  R E L A T O ==//
LOAD_TIPORELATO_ALL = function (_eid) {
	var _idioma = jQuery.getUrlVar('language');
	jQuery('#den-ddl-tiporelato').html('<option>Carregando...</option>');
	return  jQuery.ajax({
		type: "POST", 
		url: 'services.aspx/loadTipoRelatoPorEmpresa',
		data: "{'_empresa_id':'" + _eid + "','_idioma':'" + _idioma + "'}",
		contentType: "application/json; charset=utf-8",
		dataType: "json", async: true, cache: false,
		success: function (msg) { jQuery('#den-ddl-tiporelato').html('').html(msg.d); },

		error: function (msg) { SHOW_MESSAGEBOX('error'); }
	}).promise();
};


//-----------------------------------------------------------------------//
//== T I P O  P U B L I C O ==//
LOAD_TIPOPUBLICO_ALL = function (_eid) {
	var _idioma = jQuery.getUrlVar('language');
	jQuery('#den-ddl-tpp').html('<option value="0">Carregando...</option>');
	jQuery('#den1-ddl-relacaoempresa').html('<option value="0">Carregando...</option>');
	jQuery('#den2-ddl-relacaoempresa').html('<option value="0">Carregando...</option>');
	jQuery('#den3-ddl-relacaoempresa').html('<option value="0">Carregando...</option>');
	jQuery('#den4-ddl-relacaoempresa').html('<option value="0">Carregando...</option>');
	jQuery('#den5-ddl-relacaoempresa').html('<option value="0">Carregando...</option>');
	return  jQuery.ajax({
		type: "POST", 
		url: 'services.aspx/loadTipoPublicoPorEmpresa',
		data: "{'_empresa_id':'" + _eid + "','_idioma':'" + _idioma + "'}",
		contentType: "application/json; charset=utf-8",
		dataType: "json", async: true, cache: false,
		success: function(msg) {
			jQuery('#den-ddl-tpp').html('').html(msg.d);

			jQuery('#den1-ddl-relacaoempresa').html('').html(msg.d);
			jQuery('#den2-ddl-relacaoempresa').html('').html(msg.d);
			jQuery('#den3-ddl-relacaoempresa').html('').html(msg.d);
			jQuery('#den4-ddl-relacaoempresa').html('').html(msg.d);
			jQuery('#den5-ddl-relacaoempresa').html('').html(msg.d);
		},

		error: function (msg) { SHOW_MESSAGEBOX('error'); }
	}).promise();
};
