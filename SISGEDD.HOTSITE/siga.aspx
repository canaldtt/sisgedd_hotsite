﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="FI.Deloitte.Sisgedd.View.login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head >
<meta charset="charset=iso-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<title>SISGEDD® HOTSITE - SISTEMA DE GESTÃO DE DENÚNCIAS DELOITTE</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <link rel="manifest" href="resources/img/ico/manifest.json"/>
    <meta name="msapplication-TileColor" content="#ffffff"/>
    <meta name="msapplication-TileImage" content="resources/img/ico/ms-icon-144x144.png"/>
    <meta name="theme-color" content="#ffffff"/>  

<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="SISGEDD® HOTSITE - SISTEMA DE GESTÃO DE DENÚNCIAS DELOITTE| Desenvolvido por Canal de Denúncias - Deloitte"/>
<meta name="author" content="Canal de Denúncias Deloitte - etica.deloitte.com.br"/>    
<link rel="stylesheet" href="resources/css/default.css" type="text/css" />

<script type="text/javascript" src="resources/js/jquery.min.js"></script>     
<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script> <!--NAO PODE SER DEPOIS DO BOOTSTRAP-->
<script type="text/javascript" src="resources/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="resources/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="resources/js/bootstrap-datetimepicker.pt-BR.js" charset="UTF-8"></script>
<script type="text/javascript" src="resources/js/jquery.placeholder.min.js"></script>
<script type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="resources/js/init.js"></script>
</head>
<body class="loginpage" > <div class="header"></div>
<form id="form1" runat="server"></form>
<div id="mainwrapper" class="mainwrapper"> 
<div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="text-center login-title">
                    <img src="resources/img/logologin.png" alt="">
                    <br>
                    Login
                </h2>
            </div>
            <div class="modal-body">    
                <form id="frm-login"  name="frm-login" class="form-horizontal col-md-12 center-block" novalidate="novalidate">
                    <div class="form-group">
                        <input type="text" id="user-txt-login" name="user-txt-login" class="form-control required" required="required" placeholder="Ingrese el protocolo"/> 
                    </div>
                    <div class="form-group">
                      <input type="password" id="user-txt-senha" name="user-txt-senha" class="form-control required" required="required" placeholder="Ingrese contraseña"/> 
                    </div>
                    <div class="form-group">
                        <a href="#" id="login-btn-OK" class="btn btn-lg btn-block"><i class="fa fa-sign-in"></i> Entrar</a>  
                    </div>
                     
                     
                    <div class="form-group">   
                        <a href="#" class="link" data-toggle="modal" data-target="#loginPolitica">Política de Privacidad</a>
                    </div>
                    <input type="hidden" id="user-txt-eid" />
                </form>
                
             </div> 
            <br class="clearfix"    />
        </div>

    </div>
</div>
 </div>
    </body>
</html>