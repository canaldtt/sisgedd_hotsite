﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="404.aspx.cs" Inherits="FI.Deloitte.Sisgedd.View._404" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head >
<meta charset="charset=iso-8859-1" />
 
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>SISGEDD® HOTSITE - SISTEMA DE GESTÃO DE DENÚNCIAS DELOITTE</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="shortcut icon" type="image/x-icon" href="resources/img/ico/favicon.ico" />    
    <link rel="manifest" href="resources/img/ico/manifest.json"/>
    <meta name="msapplication-TileColor" content="#ffffff"/>
    <meta name="msapplication-TileImage" content="../resources/img/ico/ms-icon-144x144.png"/>
    <meta name="theme-color" content="#ffffff"/>  

<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="SISGEDD® HOTSITE - SISTEMA DE GESTÃO DE DENÚNCIAS DELOITTE "/>
<meta name="author" content="Canal de Denúncias Deloitte - etica.deloitte.com.br"/>    
<link rel="stylesheet" href="resources/css/default.css" type="text/css" />
<!--[if lte IE 8]>
<script src="resources/js/excanvas.min.js"></script>
<![endif]-->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>
<![endif]-->
<script type="text/javascript" src="resources/js/jquery.min.js"></script>     
<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script> <!--NAO PODE SER DEPOIS DO BOOTSTRAP-->
<script type="text/javascript" src="resources/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="resources/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="resources/js/bootstrap-datetimepicker.pt-BR.js" charset="UTF-8"></script>
<script type="text/javascript" src="resources/js/jquery.placeholder.min.js"></script>
<script type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="resources/js/init.js"></script>
</head>
<body class="loginpage" > 
<form id="form1" runat="server"></form>
<div id="mainwrapper" class="mainwrapper"> 
<div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="text-center" style="font-family: 'Open Sans', Verdana, Helvetica, sans-serif;color:#c0c0c0">
                    <br/>
                    <img src="resources/img/logo.png" alt="" />
                    <br/>
                    <br/>
                    <i class="fa fa-exclamation-circle"></i> 
                    <br/> Página inexistente
                    <br/>(404)
                </h2>
            </div>
            <div class="modal-body">    
                <form  class="form-horizontal col-md-12 center-block" >
                    <div class="form-group text-center">   
                        <a href="login.aspx" class="btn btn-lg" ><i class="fa fa-reply"></i> Voltar ao início</a>
                        <br/>
                        
                    </div>
                </form>
             </div> <br class="clearfix" />
        </div>
    </div>
</div>
</div>
      

    </body>
</html>