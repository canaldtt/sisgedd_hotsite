﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.View.services
// Assembly: FI.Deloitte.Sisgedd.View, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6B99F16B-A178-43AF-B750-60F1A7792D8A
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.View.dll

using FI.Deloitte.Sisgedd.Business;
using System;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace FI.Deloitte.Sisgedd.View
{
    public class services : Page
    {
        protected HtmlForm form1;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        [WebMethod]
        public static string loadNatureza()
        {
            return HtmlFactory.AdminView.LoadNatureza();
        }

        [WebMethod]
        public static string loadTipoNatureza()
        {
            return HtmlFactory.AdminView.LoadTipoNatureza();
        }

        [WebMethod]
        public static string loadEmpresa(int _id)
        {
            return HtmlFactory.AdminView.LoadEmpresa(_id);
        }

        [WebMethod]
        public static string findEmpresa(int _index, int _pagesize, string _nome)
        {
            return HtmlFactory.AdminView.FindEmpresa(_index, _pagesize, _nome);
        }

        [WebMethod]
        public static string findEmpresaByPK(int _id)
        {
            return HtmlFactory.AdminView.FindEmpresaByPk(_id);
        }

        [WebMethod]
        public static string loadEmpresa()
        {
            return HtmlFactory.AdminView.LoadEmpresa();
        }

        [WebMethod]
        public static string loadUnidade(int _empresa_id, string _idioma)
        {
            return HtmlFactory.AdminView.LoadUnidade(_empresa_id, _idioma);
        }

        [WebMethod]
        public static string findUnidade(
          int _index,
          int _pagesize,
          string _nome,
          string _idioma,
          int _empresa_id)
        {
            return HtmlFactory.AdminView.FindUnidade(_index, _pagesize, _nome, _idioma, _empresa_id);
        }

        [WebMethod]
        public static string findUnidadeByPK(int _id)
        {
            return HtmlFactory.AdminView.FindUnidadeByPk(_id);
        }

        [WebMethod]
        public static string loadProduto(int _unidade_id)
        {
            return HtmlFactory.AdminView.LoadProduto(_unidade_id);
        }

        [WebMethod]
        public static string findProduto(int _index, int _pagesize, string _nome, int _unidade_id)
        {
            return HtmlFactory.AdminView.FindProduto(_index, _pagesize, _nome, _unidade_id);
        }

        [WebMethod]
        public static string findProdutoByPK(int _id)
        {
            return HtmlFactory.AdminView.FindProdutoByPk(_id);
        }

        [WebMethod]
        public static string findResposta(string _protocolo)
        {
            return HtmlFactory.AdminView.FindHistoricoBWise(_protocolo);
        }


        [WebMethod]
        public static string loadArea(int _produto_id)
        {
            return HtmlFactory.AdminView.LoadArea(_produto_id);
        }

        [WebMethod]
        public static string findArea(int _index, int _pagesize, string _nome, int _produto_id)
        {
            return HtmlFactory.AdminView.FindArea(_index, _pagesize, _nome, _produto_id);
        }

        [WebMethod]
        public static string findAreaByPK(int _id)
        {
            return HtmlFactory.AdminView.FindAreaByPk(_id);
        }

        [WebMethod]
        public static string AddAnexo(string guid, string nome, string url)
        {
            return HtmlFactory.AdminView.AddAnexo(nome, url, guid);
        }

        [WebMethod]
        public static string removeAnexo(int aid, int uid)
        {
            return HtmlFactory.AdminView.RemoveAnexo(aid, uid);
        }

        [WebMethod]
        public static string findAnexo(string _guid)
        {
            return HtmlFactory.AdminView.FindAnexoAcompanhe(_guid);
        }

        [WebMethod]
        public static string addRelatoSite(
          string _IDENTIFICADO,
          string _TIPO_PUBLICO,
          string _NOME,
          string _CARGO,
          string _EMAIL,
          string _CELULAR,
          string _TELEFONE,
          string _GENERO,
          string _EMAIL_ANONIMO,
          int _EMPRESA_ID,
          int _UNIDADE_ID,
          string _UNIDADE,
          int _PRODUTO_ID,
          string _PRODUTO,
          int _AREA_ID,
          string _AREA,
          string _RELATO,
          string _TIPO_RELATO,
          string _POSSIVEL_IDENTIFICAR_PESSOAS,
          string _DEN1_NOME,
          string _DEN1_PARTICIPACAO,
          string _DEN1_RELACAO,
          string _DEN1_EMAIL,
          string _DEN1_CARGO,
          string _DEN1_TELEFONE,
          string _DEN1_CIDADE,
          string _DEN1_ESTADO,
          string _DEN2_NOME,
          string _DEN2_PARTICIPACAO,
          string _DEN2_RELACAO,
          string _DEN2_EMAIL,
          string _DEN2_CARGO,
          string _DEN2_TELEFONE,
          string _DEN2_CIDADE,
          string _DEN2_ESTADO,
          string _DEN3_NOME,
          string _DEN3_PARTICIPACAO,
          string _DEN3_RELACAO,
          string _DEN3_EMAIL,
          string _DEN3_CARGO,
          string _DEN3_TELEFONE,
          string _DEN3_CIDADE,
          string _DEN3_ESTADO,
          string _DEN4_NOME,
          string _DEN4_PARTICIPACAO,
          string _DEN4_RELACAO,
          string _DEN4_EMAIL,
          string _DEN4_CARGO,
          string _DEN4_TELEFONE,
          string _DEN4_CIDADE,
          string _DEN4_ESTADO,
          string _DEN5_NOME,
          string _DEN5_PARTICIPACAO,
          string _DEN5_RELACAO,
          string _DEN5_EMAIL,
          string _DEN5_CARGO,
          string _DEN5_TELEFONE,
          string _DEN5_CIDADE,
          string _DEN5_ESTADO,
          string _EVIDENCIAS,
          string _NMR_PROTOCOLO_ANTERIOR,
          string _PERIODO_EVENTO_OCORREU,
          string _EXISTE_RECORRENCIA,
          string _COMO_FICOU_CIENTE,
          string _PERDA_FINANCEIRA,
          string _QUANTO_AO_RELATO,
          string _SUGESTAO,
          string _TIPO_MANIFESTACAO,
          string _IDIOMA,
          string _GUID)
        {
            return HtmlFactory.AdminView.AddRelatoSite(_IDENTIFICADO, _TIPO_PUBLICO, _NOME, _CARGO, _EMAIL, _CELULAR, _TELEFONE, _GENERO, _EMAIL_ANONIMO, _EMPRESA_ID, _UNIDADE_ID, _UNIDADE, _PRODUTO_ID, _PRODUTO, _AREA_ID, _AREA, _RELATO, _TIPO_RELATO, _POSSIVEL_IDENTIFICAR_PESSOAS, _DEN1_NOME, _DEN1_PARTICIPACAO, _DEN1_RELACAO, _DEN1_EMAIL, _DEN1_CARGO, _DEN1_TELEFONE, _DEN1_CIDADE, _DEN1_ESTADO, _DEN2_NOME, _DEN2_PARTICIPACAO, _DEN2_RELACAO, _DEN2_EMAIL, _DEN2_CARGO, _DEN2_TELEFONE, _DEN2_CIDADE, _DEN2_ESTADO, _DEN3_NOME, _DEN3_PARTICIPACAO, _DEN3_RELACAO, _DEN3_EMAIL, _DEN3_CARGO, _DEN3_TELEFONE, _DEN3_CIDADE, _DEN3_ESTADO, _DEN4_NOME, _DEN4_PARTICIPACAO, _DEN4_RELACAO, _DEN4_EMAIL, _DEN4_CARGO, _DEN4_TELEFONE, _DEN4_CIDADE, _DEN4_ESTADO, _DEN5_NOME, _DEN5_PARTICIPACAO, _DEN5_RELACAO, _DEN5_EMAIL, _DEN5_CARGO, _DEN5_TELEFONE, _DEN5_CIDADE, _DEN5_ESTADO, _EVIDENCIAS, _NMR_PROTOCOLO_ANTERIOR, _PERIODO_EVENTO_OCORREU, _EXISTE_RECORRENCIA, _COMO_FICOU_CIENTE, _PERDA_FINANCEIRA, _QUANTO_AO_RELATO, _SUGESTAO, _TIPO_MANIFESTACAO, _IDIOMA, _GUID);
        }

        [WebMethod]
        public static string updateRelatoSite(int _relato_id, string _comentarios)
        {
            return HtmlFactory.AdminView.UpdateRelatoSite(_relato_id, _comentarios);
        }

        [WebMethod]
        public static string saveHistorico(int _rid, int _tip, string _aut, string _com)
        {
            return HtmlFactory.AdminView.SaveHistorico(_tip, _aut, _com, _rid);
        }

        [WebMethod]
        public static string findHistorico(int _relato_id, int _tipo)
        {
            return HtmlFactory.AdminView.FindHistorico(0, 1000, _relato_id, _tipo);
        }

        [WebMethod]
        public static string viewRelatoByPK(int _id, string _idioma)
        {
            return HtmlFactory.AdminView.ViewRelatoByPk(_id, _idioma);
        }

        [WebMethod]
        public static string findRelatoEmailFrame(int _rid)
        {
            return HtmlFactory.AdminView.FindRelatoEmailFrame(_rid);
        }

        [WebMethod]
        public static string findRelatoByPK(int _id)
        {
            return HtmlFactory.AdminView.FindRelatoByPk(_id);
        }

        [WebMethod]
        public static string findRelato(
          int _index,
          int _pagesize,
          int _canal_id,
          int _empresa_id,
          string _flg_fluxo)
        {
            return HtmlFactory.AdminView.FindRelato(_index, _pagesize, _empresa_id, _flg_fluxo);
        }

        [WebMethod]
        public static string loadRelato(string _perfil)
        {
            return HtmlFactory.AdminView.LoadRelato(_perfil);
        }

        [WebMethod]
        public static string addPesquisa(
          string _protocolo,
          string _pg1,
          string _pg1b,
          string _pg1a,
          string _pg12a,
          string _pg21,
          string _pg21a,
          string _pg3,
          string _pg4,
          string _pg41,
          string _pg5,
          string _pg51,
          string _pg6,
          string _pg61)
        {
            return HtmlFactory.AdminView.AddPesquisa(_protocolo, _pg1, _pg1b, _pg1a, _pg12a, _pg21, _pg21a, _pg3, _pg4, _pg41, _pg5, _pg51, _pg6, _pg61);
        }

        [WebMethod]
        public static string loginSite(string _usr, string _pwd)
        {
            return HtmlFactory.AdminView.LoginSite(_usr, _pwd);
        }

        [WebMethod]
        public static string loginBySite(string _usr, string _pwd, string _eid)
        {
            return HtmlFactory.AdminView.LogInSite(_usr, _pwd, _eid);
        }

        [WebMethod(EnableSession = true)]
        public static string logout(int _id)
        {
            return HtmlFactory.AdminView.LogOut(_id);
        }

        [WebMethod]
        public static string loadTipoRelatoPorEmpresa(int _empresa_id, string _idioma)
        {
            return HtmlFactory.AdminView.loadTipoRelatoPorEmpresa(_empresa_id, _idioma);
        }

        [WebMethod]
        public static string loadTipoPublicoPorEmpresa(int _empresa_id, string _idioma)
        {
            return HtmlFactory.AdminView.loadTipoPublicoPorEmpresa(_empresa_id, _idioma);
        }
    }
}
