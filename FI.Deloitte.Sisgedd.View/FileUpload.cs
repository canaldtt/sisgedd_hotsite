﻿using FI.Deloitte.Sisgedd.Business;
using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace FI.Deloitte.Sisgedd.View
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class FileUpload : IHttpHandler
    {
        private string filename = HttpContext.Current.Request.Headers["X-File-Name"];
        public static readonly string[] VALID_EXTENSIONS = new string[24]
        {
            "pdf",
            "ppt",
            "xls",
            "xlsx",
            "doc",
            "docx",
            "txt",
            "png",
            "jpeg",
            "jpg",
            "gif",
            "bmp",
            "avi",
            "mpeg",
            "mov",
            "mkv",
            "mp4",
            "wav",
            "ac3",
            "ogg",
            "aac",
            "wma",
            "mp3",
            "opus"
        };


        private const string rootPath = "C:\\inetpub\\wwwroot\\sites\\anexos";
        private const string webPath = "https://etica.deloitte.com.br//anexos//";

        public string saveAS { get; set; }

        public void ProcessRequest(HttpContext context)
        {
            string extension = filename.Substring(filename.LastIndexOf('.') + 1);

            if (!VALID_EXTENSIONS.Contains(extension.ToLower()))
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("{success:false}");
            }
            else
            {
                string str1 = DateTime.Now.ToString().Replace('/', '_').Replace('/', '_').Replace(' ', '_').Replace(":", "_").Trim().Replace(".", "_");
                if (!Directory.Exists("C:\\inetpub\\wwwroot\\sites\\anexos"))
                    Directory.CreateDirectory("C:\\inetpub\\wwwroot\\sites\\anexos");
                if (this.filename == null)
                {
                    try
                    {
                        HttpPostedFile file = context.Request.Files[0];
                        this.filename = file.FileName.Replace('/', '_').Replace('%', '_').Replace(' ', '_').Replace(" ", "_").Trim();
                        string str2 = str1 + "_" + this.filename;
                        file.SaveAs("C:\\inetpub\\wwwroot\\sites\\anexos\\" + str2);
                        this.saveAS = "C:\\inetpub\\wwwroot\\sites\\anexos\\" + str2.Replace("/", "\\");
                        context.Response.Write("{success:true, name:\"" + str2 + "\", path:\"https://etica.deloitte.com.br//anexos///" + str2 + "\"}");
                    }
                    catch (Exception ex)
                    {
                        Common.GetException(ex);
                        context.Response.Write("{success:false}");
                    }
                    finally
                    {
                        if (HtmlFactory.UploadSftp(this.saveAS, 1))
                        {
                            FileInfo fileInfo = new FileInfo(this.saveAS);
                            if (fileInfo.Exists)
                                fileInfo.Delete();
                        }
                    }
                }
                else
                {
                    string str2 = str1 + "_" + this.filename;
                    FileStream fileStream = new FileStream("C:\\inetpub\\wwwroot\\sites\\anexos\\" + str2.Replace('/', '_').Replace('%', '_').Replace(' ', '_').Replace(" ", "_").Trim(), FileMode.OpenOrCreate);
                    try
                    {
                        HttpContext.Current.Request.InputStream.CopyTo((Stream)fileStream);
                        this.saveAS = "C:\\inetpub\\wwwroot\\sites\\anexos\\" + str2.Replace('/', '_').Replace('%', '_').Replace(' ', '_').Replace(" ", "_").Trim();
                        string str3 = str2.Replace('/', '_').Replace('%', '_').Replace(' ', '_').Replace(" ", "_").Trim() + "\",path:\"https://etica.deloitte.com.br//anexos//" + str2.Replace('/', '_').Replace('%', '_').Replace(' ', '_').Replace(" ", "_").Trim();
                        context.Response.Write("{success:true, name:\"" + str3 + "\"}");
                    }
                    catch (Exception ex)
                    {
                        Common.GetException(ex);
                        context.Response.Write("{success:false}");
                    }
                    finally
                    {
                        fileStream.Close();
                    }
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private void LogRequest(string Log)
        {
            StreamWriter streamWriter = new StreamWriter(HttpContext.Current.Server.MapPath("~/resources/files/") + "Log.txt", true);
            streamWriter.WriteLine(Log);
            streamWriter.Flush();
            streamWriter.Close();
        }
    }
}
