﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace FI.Deloitte.Sisgedd.View
{
    public class inicio : Page
    {
        protected HtmlForm form1;

        protected void Page_Load(object sender, EventArgs e)
        {
            string url;

            if (Request.Path == "/inicio.aspx")
            {
                url = "~/login.aspx";
                this.Response.Redirect(url, true);
            }

        }
    }
}
