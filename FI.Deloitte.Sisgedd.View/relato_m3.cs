﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.View.relato_m3
// Assembly: FI.Deloitte.Sisgedd.View, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6B99F16B-A178-43AF-B750-60F1A7792D8A
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.View.dll

using System;
using System.Globalization;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace FI.Deloitte.Sisgedd.View
{
  public class relato_m3 : Page
  {
    protected HtmlForm form1;

    protected void Page_Load(object sender, EventArgs e)
    {
      if (this.Page.IsPostBack || this.Page.Request.Params["language"] == null || string.IsNullOrEmpty(this.Page.Request.Params["language"].ToString()))
        return;
      CultureInfo cultureInfo = new CultureInfo(this.Page.Request.Params["language"].ToString(), true);
      Thread.CurrentThread.CurrentCulture = cultureInfo;
      Thread.CurrentThread.CurrentUICulture = cultureInfo;
    }
  }
}
