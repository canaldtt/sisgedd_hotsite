﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.View.FileDownload
// Assembly: FI.Deloitte.Sisgedd.View, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6B99F16B-A178-43AF-B750-60F1A7792D8A
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.View.dll

using FI.Deloitte.Sisgedd.Business;
using System;
using System.IO;
using System.Web;
using System.Web.Services;

namespace FI.Deloitte.Sisgedd.View
{
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  public class FileDownload : IHttpHandler
  {
    private string filename = HttpContext.Current.Request.Headers["X-File-Name"].Replace("%20", "_");
    private const string path = "https://etica.deloitte.com.br//anexos//";

    public string saveAS { get; set; }

    public void ProcessRequest(HttpContext context)
    {
      if (string.IsNullOrEmpty(this.filename) && HttpContext.Current.Request.Files.Count <= 0)
      {
        context.Response.ContentType = "text/plain";
        context.Response.Write("{success:false}");
      }
      else
      {
        string path = "https://etica.deloitte.com.br//anexos//";
        string str1 = DateTime.Now.ToString().Replace('/', '_').Replace('/', '_').Replace(' ', '_').Replace(":", "_").Trim().Replace(".", "_");
        if (!Directory.Exists(path))
          Directory.CreateDirectory(path);
        if (this.filename == null)
        {
          try
          {
            HttpPostedFile file = context.Request.Files[0];
            this.filename = file.FileName.Replace("%20", "_").Replace("%20", "_").Replace("%20", "_").Replace("%20", "_").Replace("%20", "_").Replace("%20", "_").Replace("%20", "_");
            string str2 = str1 + "_" + this.filename;
            file.SaveAs(path + "\\" + str2);
            this.saveAS = this.filename;
            context.Response.Write("{success:true, name:\"" + str2 + "\", path:\"https://etica.deloitte.com.br//anexos///" + str2 + "\"}");
          }
          catch (Exception ex)
          {
            Common.GetException(ex);
            context.Response.Write("{success:false}");
          }
          finally
          {
            if (HtmlFactory.UploadSftp(this.saveAS, 2))
            {
              FileInfo fileInfo = new FileInfo(this.saveAS);
              if (fileInfo.Exists)
                fileInfo.Delete();
            }
          }
        }
        else
        {
          FileStream fileStream = new FileStream(path + "\\" + str1 + "_" + this.filename.Replace("%20", "_"), FileMode.OpenOrCreate);
          try
          {
            HttpContext.Current.Request.InputStream.CopyTo((Stream) fileStream);
            this.saveAS = path + "\\" + str1 + "_" + this.filename.Replace("%20", "_").Replace("%20", "_").Replace("%20", "_");
            context.Response.Write("{success:true, name:\"" + str1 + "_" + this.filename.Replace("%20", "_").Replace("%20", "_").Replace("%20", "_") + "\", path:\"https://etica.deloitte.com.br//anexos///" + str1 + "_" + this.filename.Replace("%20", "_").Replace("%20", "_").Replace("%20", "_") + "\"}");
          }
          catch (Exception ex)
          {
            Common.GetException(ex);
            context.Response.Write("{success:false}");
          }
          finally
          {
            fileStream.Close();
            HtmlFactory.UploadSftp(this.saveAS, 2);
          }
        }
      }
    }

    public bool IsReusable
    {
      get
      {
        return false;
      }
    }

    private void LogRequest(string Log)
    {
      StreamWriter streamWriter = new StreamWriter(HttpContext.Current.Server.MapPath("~/resources/files/") + "Log.txt", true);
      streamWriter.WriteLine(Log);
      streamWriter.Flush();
      streamWriter.Close();
    }
  }
}
