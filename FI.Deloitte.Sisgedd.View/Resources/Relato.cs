﻿// Decompiled with JetBrains decompiler
// Type: Resources.Relato
// Assembly: FI.Deloitte.Sisgedd.View, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6B99F16B-A178-43AF-B750-60F1A7792D8A
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.View.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;

namespace Resources
{
  [GeneratedCode("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "15.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  internal class Relato
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Relato()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (Relato.resourceMan == null)
          Relato.resourceMan = new ResourceManager("Resources.Relato", Assembly.Load("App_GlobalResources"));
        return Relato.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return Relato.resourceCulture;
      }
      set
      {
        Relato.resourceCulture = value;
      }
    }

    internal static string btnEnviarRelato
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (btnEnviarRelato), Relato.resourceCulture);
      }
    }

    internal static string Intro
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (Intro), Relato.resourceCulture);
      }
    }

    internal static string lblAdicionarOutro
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblAdicionarOutro), Relato.resourceCulture);
      }
    }

    internal static string lblAnexo
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblAnexo), Relato.resourceCulture);
      }
    }

    internal static string lblArea
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblArea), Relato.resourceCulture);
      }
    }

    internal static string lblCargo
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblCargo), Relato.resourceCulture);
      }
    }

    internal static string lblCelular
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblCelular), Relato.resourceCulture);
      }
    }

    internal static string lblCidade
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblCidade), Relato.resourceCulture);
      }
    }

    internal static string lblComboSelecioneFilial
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboSelecioneFilial), Relato.resourceCulture);
      }
    }

    internal static string lblComboSelecioneUnidade
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboSelecioneUnidade), Relato.resourceCulture);
      }
    }

    internal static string lblComboTipoRelatoInfo1
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboTipoRelatoInfo1), Relato.resourceCulture);
      }
    }

    internal static string lblComboTipoRelatoInfo10
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboTipoRelatoInfo10), Relato.resourceCulture);
      }
    }

    internal static string lblComboTipoRelatoInfo11
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboTipoRelatoInfo11), Relato.resourceCulture);
      }
    }

    internal static string lblComboTipoRelatoInfo12
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboTipoRelatoInfo12), Relato.resourceCulture);
      }
    }

    internal static string lblComboTipoRelatoInfo13
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboTipoRelatoInfo13), Relato.resourceCulture);
      }
    }

    internal static string lblComboTipoRelatoInfo14
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboTipoRelatoInfo14), Relato.resourceCulture);
      }
    }

    internal static string lblComboTipoRelatoInfo15
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboTipoRelatoInfo15), Relato.resourceCulture);
      }
    }

    internal static string lblComboTipoRelatoInfo16
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboTipoRelatoInfo16), Relato.resourceCulture);
      }
    }

    internal static string lblComboTipoRelatoInfo17
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboTipoRelatoInfo17), Relato.resourceCulture);
      }
    }

    internal static string lblComboTipoRelatoInfo18
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboTipoRelatoInfo18), Relato.resourceCulture);
      }
    }

    internal static string lblComboTipoRelatoInfo19
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboTipoRelatoInfo19), Relato.resourceCulture);
      }
    }

    internal static string lblComboTipoRelatoInfo2
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboTipoRelatoInfo2), Relato.resourceCulture);
      }
    }

    internal static string lblComboTipoRelatoInfo3
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboTipoRelatoInfo3), Relato.resourceCulture);
      }
    }

    internal static string lblComboTipoRelatoInfo4
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboTipoRelatoInfo4), Relato.resourceCulture);
      }
    }

    internal static string lblComboTipoRelatoInfo5
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboTipoRelatoInfo5), Relato.resourceCulture);
      }
    }

    internal static string lblComboTipoRelatoInfo6
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboTipoRelatoInfo6), Relato.resourceCulture);
      }
    }

    internal static string lblComboTipoRelatoInfo7
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboTipoRelatoInfo7), Relato.resourceCulture);
      }
    }

    internal static string lblComboTipoRelatoInfo8
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboTipoRelatoInfo8), Relato.resourceCulture);
      }
    }

    internal static string lblComboTipoRelatoInfo9
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComboTipoRelatoInfo9), Relato.resourceCulture);
      }
    }

    internal static string lblComoFicouCiente
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblComoFicouCiente), Relato.resourceCulture);
      }
    }

    internal static string lblDataEvento
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblDataEvento), Relato.resourceCulture);
      }
    }

    internal static string lblDescrevaDetalhadamente
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblDescrevaDetalhadamente), Relato.resourceCulture);
      }
    }

    internal static string lblDescrevaSeuRelato
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblDescrevaSeuRelato), Relato.resourceCulture);
      }
    }

    internal static string lblEhPossivel
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblEhPossivel), Relato.resourceCulture);
      }
    }

    internal static string lblEmail
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblEmail), Relato.resourceCulture);
      }
    }

    internal static string lblEmailAnonimo
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblEmailAnonimo), Relato.resourceCulture);
      }
    }

    internal static string lblEnviando
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblEnviando), Relato.resourceCulture);
      }
    }

    internal static string lblEscolha
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblEscolha), Relato.resourceCulture);
      }
    }

    internal static string lblEstado
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblEstado), Relato.resourceCulture);
      }
    }

    internal static string lblEvidencias
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblEvidencias), Relato.resourceCulture);
      }
    }

    internal static string lblFilial
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblFilial), Relato.resourceCulture);
      }
    }

    internal static string lblFooter
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblFooter), Relato.resourceCulture);
      }
    }

    internal static string lblGenero
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblGenero), Relato.resourceCulture);
      }
    }

    internal static string lblIdentificacaoDenunciante
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblIdentificacaoDenunciante), Relato.resourceCulture);
      }
    }

    internal static string lblInfo01
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInfo01), Relato.resourceCulture);
      }
    }

    internal static string lblInfo010
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInfo010), Relato.resourceCulture);
      }
    }

    internal static string lblInfo011
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInfo011), Relato.resourceCulture);
      }
    }

    internal static string lblInfo012
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInfo012), Relato.resourceCulture);
      }
    }

    internal static string lblInfo013
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInfo013), Relato.resourceCulture);
      }
    }

    internal static string lblInfo014
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInfo014), Relato.resourceCulture);
      }
    }

    internal static string lblInfo015
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInfo015), Relato.resourceCulture);
      }
    }

    internal static string lblInfo016
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInfo016), Relato.resourceCulture);
      }
    }

    internal static string lblInfo017
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInfo017), Relato.resourceCulture);
      }
    }

    internal static string lblInfo018
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInfo018), Relato.resourceCulture);
      }
    }

    internal static string lblInfo019
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInfo019), Relato.resourceCulture);
      }
    }

    internal static string lblInfo02
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInfo02), Relato.resourceCulture);
      }
    }

    internal static string lblInfo020
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInfo020), Relato.resourceCulture);
      }
    }

    internal static string lblInfo03
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInfo03), Relato.resourceCulture);
      }
    }

    internal static string lblInfo04
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInfo04), Relato.resourceCulture);
      }
    }

    internal static string lblInfo05
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInfo05), Relato.resourceCulture);
      }
    }

    internal static string lblInfo06
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInfo06), Relato.resourceCulture);
      }
    }

    internal static string lblInfo07
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInfo07), Relato.resourceCulture);
      }
    }

    internal static string lblInfo08
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInfo08), Relato.resourceCulture);
      }
    }

    internal static string lblInfo09
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInfo09), Relato.resourceCulture);
      }
    }

    internal static string lblInforme
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblInforme), Relato.resourceCulture);
      }
    }

    internal static string lblJaEfetuou
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblJaEfetuou), Relato.resourceCulture);
      }
    }

    internal static string lblMsgConfirma1
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblMsgConfirma1), Relato.resourceCulture);
      }
    }

    internal static string lblMsgConfirma2
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblMsgConfirma2), Relato.resourceCulture);
      }
    }

    internal static string lblMsgPorFavorInforme
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblMsgPorFavorInforme), Relato.resourceCulture);
      }
    }

    internal static string lblMsgSelecioneLocal
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblMsgSelecioneLocal), Relato.resourceCulture);
      }
    }

    internal static string lblMsgTitAnexo
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblMsgTitAnexo), Relato.resourceCulture);
      }
    }

    internal static string lblNome
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblNome), Relato.resourceCulture);
      }
    }

    internal static string lblOutros
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblOutros), Relato.resourceCulture);
      }
    }

    internal static string lblParticipacao
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblParticipacao), Relato.resourceCulture);
      }
    }

    internal static string lblPessoaIdentificadaRelato
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblPessoaIdentificadaRelato), Relato.resourceCulture);
      }
    }

    internal static string lblProtocoloAnterior
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblProtocoloAnterior), Relato.resourceCulture);
      }
    }

    internal static string lblQuantoAoRelato
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblQuantoAoRelato), Relato.resourceCulture);
      }
    }

    internal static string lblQuantoAoRelatoInfo1
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblQuantoAoRelatoInfo1), Relato.resourceCulture);
      }
    }

    internal static string lblQuantoAoRelatoInfo2
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblQuantoAoRelatoInfo2), Relato.resourceCulture);
      }
    }

    internal static string lblQuantoAoRelatoInfo3
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblQuantoAoRelatoInfo3), Relato.resourceCulture);
      }
    }

    internal static string lblQuantoMaisDetalhes
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblQuantoMaisDetalhes), Relato.resourceCulture);
      }
    }

    internal static string lblQueTipoDeRelatoSeEnquadra
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblQueTipoDeRelatoSeEnquadra), Relato.resourceCulture);
      }
    }

    internal static string lblRecorrencia
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblRecorrencia), Relato.resourceCulture);
      }
    }

    internal static string lblRelacaoNaEmpresa
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblRelacaoNaEmpresa), Relato.resourceCulture);
      }
    }

    internal static string lblRelato
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblRelato), Relato.resourceCulture);
      }
    }

    internal static string lblRemoverOutro
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblRemoverOutro), Relato.resourceCulture);
      }
    }

    internal static string lblSelecione
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblSelecione), Relato.resourceCulture);
      }
    }

    internal static string lblSeTiverConhecimento
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblSeTiverConhecimento), Relato.resourceCulture);
      }
    }

    internal static string lblSeTiverSugestao
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblSeTiverSugestao), Relato.resourceCulture);
      }
    }

    internal static string lblSujeito
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblSujeito), Relato.resourceCulture);
      }
    }

    internal static string lblTelefone
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblTelefone), Relato.resourceCulture);
      }
    }

    internal static string lblTestemunha
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblTestemunha), Relato.resourceCulture);
      }
    }

    internal static string lblTipoPublico
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblTipoPublico), Relato.resourceCulture);
      }
    }

    internal static string lblTipoRelato
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblTipoRelato), Relato.resourceCulture);
      }
    }

    internal static string lblTituloPrincipal
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblTituloPrincipal), Relato.resourceCulture);
      }
    }

    internal static string lblUnidade
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblUnidade), Relato.resourceCulture);
      }
    }

    internal static string lblVoceSabe
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (lblVoceSabe), Relato.resourceCulture);
      }
    }

    internal static string rblAnonimo
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (rblAnonimo), Relato.resourceCulture);
      }
    }

    internal static string rblFeminino
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (rblFeminino), Relato.resourceCulture);
      }
    }

    internal static string rblIdentificado
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (rblIdentificado), Relato.resourceCulture);
      }
    }

    internal static string rblMasculino
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (rblMasculino), Relato.resourceCulture);
      }
    }

    internal static string rblNao
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (rblNao), Relato.resourceCulture);
      }
    }

    internal static string rblSigiloso
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (rblSigiloso), Relato.resourceCulture);
      }
    }

    internal static string rblSim
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (rblSim), Relato.resourceCulture);
      }
    }

    internal static string String
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (String), Relato.resourceCulture);
      }
    }

    internal static string String1
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (String1), Relato.resourceCulture);
      }
    }

    internal static string String2
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (String2), Relato.resourceCulture);
      }
    }

    internal static string String3
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (String3), Relato.resourceCulture);
      }
    }

    internal static string subInformacoesSobreRelato
    {
      get
      {
        return Relato.ResourceManager.GetString(nameof (subInformacoesSobreRelato), Relato.resourceCulture);
      }
    }
  }
}
