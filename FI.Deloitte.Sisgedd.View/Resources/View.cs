﻿// Decompiled with JetBrains decompiler
// Type: Resources.View
// Assembly: FI.Deloitte.Sisgedd.View, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6B99F16B-A178-43AF-B750-60F1A7792D8A
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.View.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;

namespace Resources
{
  [GeneratedCode("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "15.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  internal class View
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal View()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (View.resourceMan == null)
          View.resourceMan = new ResourceManager("Resources.View", Assembly.Load("App_GlobalResources"));
        return View.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return View.resourceCulture;
      }
      set
      {
        View.resourceCulture = value;
      }
    }

    internal static string btnEnviar
    {
      get
      {
        return View.ResourceManager.GetString(nameof (btnEnviar), View.resourceCulture);
      }
    }

    internal static string btnReplica
    {
      get
      {
        return View.ResourceManager.GetString(nameof (btnReplica), View.resourceCulture);
      }
    }

    internal static string lblAnexo
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblAnexo), View.resourceCulture);
      }
    }

    internal static string lblDescricaoRelato
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblDescricaoRelato), View.resourceCulture);
      }
    }

    internal static string lblEnviando
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblEnviando), View.resourceCulture);
      }
    }

    internal static string lblHistoricoRelato
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblHistoricoRelato), View.resourceCulture);
      }
    }

    internal static string lblInfoAdicionais
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblInfoAdicionais), View.resourceCulture);
      }
    }

    internal static string lblPerg11
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblPerg11), View.resourceCulture);
      }
    }

    internal static string lblPerg11a
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblPerg11a), View.resourceCulture);
      }
    }

    internal static string lblPerg11b
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblPerg11b), View.resourceCulture);
      }
    }

    internal static string lblPerg12a
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblPerg12a), View.resourceCulture);
      }
    }

    internal static string lblPerg21
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblPerg21), View.resourceCulture);
      }
    }

    internal static string lblPerg22
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblPerg22), View.resourceCulture);
      }
    }

    internal static string lblPerg3
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblPerg3), View.resourceCulture);
      }
    }

    internal static string lblPerg4
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblPerg4), View.resourceCulture);
      }
    }

    internal static string lblPerg41
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblPerg41), View.resourceCulture);
      }
    }

    internal static string lblPerg5
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblPerg5), View.resourceCulture);
      }
    }

    internal static string lblPerg51
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblPerg51), View.resourceCulture);
      }
    }

    internal static string lblPerg6
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblPerg6), View.resourceCulture);
      }
    }

    internal static string lblPerg61
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblPerg61), View.resourceCulture);
      }
    }

    internal static string lblPesquisaSatisfacao
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblPesquisaSatisfacao), View.resourceCulture);
      }
    }

    internal static string lblProtocolo
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblProtocolo), View.resourceCulture);
      }
    }

    internal static string lblStatusRelato
    {
      get
      {
        return View.ResourceManager.GetString(nameof (lblStatusRelato), View.resourceCulture);
      }
    }

    internal static string Perg3Opt1
    {
      get
      {
        return View.ResourceManager.GetString(nameof (Perg3Opt1), View.resourceCulture);
      }
    }

    internal static string Perg3Opt2
    {
      get
      {
        return View.ResourceManager.GetString(nameof (Perg3Opt2), View.resourceCulture);
      }
    }

    internal static string Perg3Opt3
    {
      get
      {
        return View.ResourceManager.GetString(nameof (Perg3Opt3), View.resourceCulture);
      }
    }

    internal static string Perg3Opt4
    {
      get
      {
        return View.ResourceManager.GetString(nameof (Perg3Opt4), View.resourceCulture);
      }
    }

    internal static string Perg3Opt5
    {
      get
      {
        return View.ResourceManager.GetString(nameof (Perg3Opt5), View.resourceCulture);
      }
    }

    internal static string Perg4Opt1
    {
      get
      {
        return View.ResourceManager.GetString(nameof (Perg4Opt1), View.resourceCulture);
      }
    }

    internal static string Perg4Opt2
    {
      get
      {
        return View.ResourceManager.GetString(nameof (Perg4Opt2), View.resourceCulture);
      }
    }

    internal static string Perg4Opt3
    {
      get
      {
        return View.ResourceManager.GetString(nameof (Perg4Opt3), View.resourceCulture);
      }
    }

    internal static string Perg4Opt4
    {
      get
      {
        return View.ResourceManager.GetString(nameof (Perg4Opt4), View.resourceCulture);
      }
    }

    internal static string Perg5Opt1
    {
      get
      {
        return View.ResourceManager.GetString(nameof (Perg5Opt1), View.resourceCulture);
      }
    }

    internal static string Perg5Opt2
    {
      get
      {
        return View.ResourceManager.GetString(nameof (Perg5Opt2), View.resourceCulture);
      }
    }

    internal static string rblNaoResponder
    {
      get
      {
        return View.ResourceManager.GetString(nameof (rblNaoResponder), View.resourceCulture);
      }
    }

    internal static string subAcompanhe
    {
      get
      {
        return View.ResourceManager.GetString(nameof (subAcompanhe), View.resourceCulture);
      }
    }
  }
}
