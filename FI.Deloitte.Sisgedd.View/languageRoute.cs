﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.View.languageRoute
// Assembly: FI.Deloitte.Sisgedd.View, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6B99F16B-A178-43AF-B750-60F1A7792D8A
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.View.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace FI.Deloitte.Sisgedd.View
{
  public class languageRoute : Page
  {
    protected HtmlForm form1;

    protected void Page_Load(object sender, EventArgs e)
    {
      string empty = string.Empty;
      string url;
      if ((RelatoEntity) this.Session["relato"] != null)
      {
        string str = !string.IsNullOrEmpty(((RelatoEntity) this.Session["relato"]).IDIOMA) ? ((RelatoEntity) this.Session["relato"]).IDIOMA : "pt-BR";
        url = ((RelatoEntity) this.Session["relato"]).RELATO_ID <= 0 ? "~/login.aspx" : string.Format("view.aspx?rid={0}&language={1}", (object) ((RelatoEntity) this.Session["relato"]).RELATO_ID, (object) str);
      }
      else
        url = "~/login.aspx";
      this.Response.Redirect(url, true);
    }
  }
}
