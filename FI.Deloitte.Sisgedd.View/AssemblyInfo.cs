﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SISGESP")]
[assembly: AssemblyDescription("A Fabrica Interactiva Desenvolvimento de Software possui todos os direitos de propriedade intelectual do Software. O Software é licenciado, não vendido.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Fabrica Interactiva Desenvolvimento de Software - www.fabricai.com.br")]
[assembly: AssemblyProduct("SISGEP")]
[assembly: AssemblyCopyright("Copyright ©  2014")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: Guid("ace17a9a-9216-4cc6-b7f6-8ba04e05e67a")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyVersion("1.0.0.0")]
