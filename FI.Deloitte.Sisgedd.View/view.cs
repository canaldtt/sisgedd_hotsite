﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.View.view
// Assembly: FI.Deloitte.Sisgedd.View, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6B99F16B-A178-43AF-B750-60F1A7792D8A
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.View.dll

using FI.Deloitte.Sisgedd.Domain;
using System;
using System.Globalization;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace FI.Deloitte.Sisgedd.View
{
    public class view : Page
    {
        protected HtmlForm form1;

        protected void Page_Load(object sender, EventArgs e)
        {
            //this.Response.Headers.Remove("Server");

            if (!this.Page.IsPostBack && !string.IsNullOrEmpty(this.Page.Request.Params["language"].ToString()))
            {
                CultureInfo cultureInfo = new CultureInfo(this.Page.Request.Params["language"].ToString(), true);
                Thread.CurrentThread.CurrentCulture = cultureInfo;
                Thread.CurrentThread.CurrentUICulture = cultureInfo;
            }
            if ((RelatoEntity)this.Session["relato"] == null)
                this.Response.Redirect("~/login.aspx", true);
            else
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "init", "jQuery(document).ready(function () {VIEW_RELATO(" + (object)((RelatoEntity)this.Session["relato"]).RELATO_ID + "); });", true);

            //this.Response.Headers.Remove("Server");
        }
    }
}
