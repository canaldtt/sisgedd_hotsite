﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Domain.GenericEntity
// Assembly: FI.Deloitte.Sisgedd.Domain, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DB6D7218-11AF-49BB-86E3-DE4782C98D95
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Domain.dll

using System;

namespace FI.Deloitte.Sisgedd.Domain
{
  [Serializable]
  public class GenericEntity
  {
    public int PAGEINDEX { get; set; }

    public int PAGESIZE { get; set; }

    public int COUNTER { get; set; }

    public string ACTION { get; set; }

    public LogEntity Log { get; set; }
  }
}
