﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Domain.RelatoEntity
// Assembly: FI.Deloitte.Sisgedd.Domain, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DB6D7218-11AF-49BB-86E3-DE4782C98D95
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Domain.dll

using System;

namespace FI.Deloitte.Sisgedd.Domain
{
  public class RelatoEntity : GenericEntity
  {
    public string DESCRICAO_DETALHADA { get; set; }

    public DateTime DT_INICIO { get; set; }

    public string TIPO_PUBLICO { get; set; }

    public string GUID { get; set; }

    public string IDENTIFICADO { get; set; }

    public string FLG_FLUXO { get; set; }

    public string FLG_RESP_PESQ { get; set; }

    public string HIST_ATEND { get; set; }

    public string NOME { get; set; }

    public string EMAIL { get; set; }

    public string EMAIL_ANONIMO { get; set; }

    public string TELEFONE { get; set; }

    public string CARGO { get; set; }

    public string CELULAR { get; set; }

    public string EMPRESA { get; set; }

    public int EMPRESA_ID { get; set; }

    public int USER_ID { get; set; }

    public string USER { get; set; }

    public string UNIDADE { get; set; }

    public int UNIDADE_ID { get; set; }

    public string PRODUTO { get; set; }

    public int PRODUTO_ID { get; set; }

    public string AREA { get; set; }

    public int AREA_ID { get; set; }

    public string IDENTIFICAR_PESSOAS { get; set; }

    public string DEN1_CARGO { get; set; }

    public string DEN1_CIDADE { get; set; }

    public string DEN1_EMAIL { get; set; }

    public string DEN1_ESTADO { get; set; }

    public string DEN1_NOME { get; set; }

    public string DEN1_PARTICIPACAO { get; set; }

    public string DEN1_RELACAO { get; set; }

    public string DEN1_TELEFONE { get; set; }

    public string DEN2_CARGO { get; set; }

    public string DEN2_CIDADE { get; set; }

    public string DEN2_EMAIL { get; set; }

    public string DEN2_ESTADO { get; set; }

    public string DEN2_NOME { get; set; }

    public string DEN2_PARTICIPACAO { get; set; }

    public string DEN2_RELACAO { get; set; }

    public string DEN2_TELEFONE { get; set; }

    public string DEN3_CARGO { get; set; }

    public string DEN3_CIDADE { get; set; }

    public string DEN3_EMAIL { get; set; }

    public string DEN3_ESTADO { get; set; }

    public string DEN3_NOME { get; set; }

    public string DEN3_PARTICIPACAO { get; set; }

    public string DEN3_RELACAO { get; set; }

    public string DEN3_TELEFONE { get; set; }

    public string DEN4_CARGO { get; set; }

    public string DEN4_CIDADE { get; set; }

    public string DEN4_EMAIL { get; set; }

    public string DEN4_ESTADO { get; set; }

    public string DEN4_NOME { get; set; }

    public string DEN4_PARTICIPACAO { get; set; }

    public string DEN4_RELACAO { get; set; }

    public string DEN4_TELEFONE { get; set; }

    public string DEN5_CARGO { get; set; }

    public string DEN5_CIDADE { get; set; }

    public string DEN5_EMAIL { get; set; }

    public string DEN5_ESTADO { get; set; }

    public string DEN5_NOME { get; set; }

    public string DEN5_PARTICIPACAO { get; set; }

    public string DEN5_RELACAO { get; set; }

    public string DEN5_TELEFONE { get; set; }

    public string TIPO_MANIFESTACAO { get; set; }

    public string TIPO_RELATO { get; set; }

    public string RELATO { get; set; }

    public int RELATO_ID { get; set; }

    public string NMR_PROTOCOLO_ANTERIOR { get; set; }

    public string PERIODO_EVENTO_OCORREU { get; set; }

    public string EXISTE_RECORRENCIA { get; set; }

    public string COMO_FICOU_CIENTE { get; set; }

    public string PERDA_FINANCEIRA { get; set; }

    public string RELATO_SUGESTAO { get; set; }

    public string QUANTO_AO_RELATO { get; set; }

    public string IDIOMA { get; set; }

    public string NMR_PROTOCOLO { get; set; }

    public string SENHA_ACESSO { get; set; }

    public string STATUS { get; set; }

    public string FORMA_CONTATO { get; set; }

    public string GENERO { get; set; }

    public string ANIMO { get; set; }

    public string NOME_ATENDENTE { get; set; }

    public string RELATO_RESUMO { get; set; }

    public string CRITICIDADE { get; set; }

    public string NATUREZA { get; set; }

    public int NATUREZA_ID { get; set; }

    public string TIPO_NATUREZA { get; set; }

    public int TIPONATUREZA_ID { get; set; }

    public DateTime DT_MAX { get; set; }

    public DateTime DT_COMENTARIO { get; set; }

    public string COMENTARIOS { get; set; }

    public DateTime DT_ENCERRAMENTO { get; set; }

    public DateTime DT_ATUALIZACAO { get; set; }

    public int SEQ { get; set; }

    public string EVIDENCIAS { get; set; }

    public string EMAIL_ID { get; set; }

    public string RESPONSIBLE { get; set; }
  }
}
