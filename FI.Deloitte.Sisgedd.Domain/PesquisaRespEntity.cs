﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Domain.PesquisaRespEntity
// Assembly: FI.Deloitte.Sisgedd.Domain, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DB6D7218-11AF-49BB-86E3-DE4782C98D95
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Domain.dll

using System;

namespace FI.Deloitte.Sisgedd.Domain
{
  public class PesquisaRespEntity : GenericEntity
  {
    public DateTime DT_RESPOSTA { get; set; }

    public string NMR_PROTOCOLO { get; set; }

    public int PESQUISA_RESP_ID { get; set; }

    public string QUESTAO { get; set; }

    public string RESPOSTA { get; set; }
  }
}
