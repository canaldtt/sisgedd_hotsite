﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Domain.AreaEntity
// Assembly: FI.Deloitte.Sisgedd.Domain, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DB6D7218-11AF-49BB-86E3-DE4782C98D95
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Domain.dll

namespace FI.Deloitte.Sisgedd.Domain
{
  public class AreaEntity : GenericEntity
  {
    public int AREA_ID { get; set; }

    public string NOME { get; set; }

    public int PRODUTO_ID { get; set; }
  }
}
