﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Domain.EmpresaEntity
// Assembly: FI.Deloitte.Sisgedd.Domain, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DB6D7218-11AF-49BB-86E3-DE4782C98D95
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Domain.dll

namespace FI.Deloitte.Sisgedd.Domain
{
    public class EmpresaEntity : GenericEntity
    {
        public int EMPRESA_ID { get; set; }

        public int MODELO_ID { get; set; }

        public string NOME { get; set; }

        public string EXIBE_AREA { get; set; }

        public string EXIBE_UNIDADE { get; set; }

        public string EXIBE_PRODUTO { get; set; }

        public string LOGO { get; set; }

        public string TITULO { get; set; }

        public string TITULO_ES { get; set; }

        public string TITULO_EN { get; set; }

        public string TITULO_FR { get; set; }

        public string PREFIXO { get; set; }

        public string EMAIL { get; set; }

        public string FLG_PADRAO_CONFIG { get; set; }

        //Soft-Delete
        public string FLG_ATIVA { get; set; }
    }
}
