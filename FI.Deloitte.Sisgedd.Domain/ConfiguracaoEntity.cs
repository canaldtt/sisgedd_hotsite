﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Domain.ConfiguracaoEntity
// Assembly: FI.Deloitte.Sisgedd.Domain, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DB6D7218-11AF-49BB-86E3-DE4782C98D95
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Domain.dll

namespace FI.Deloitte.Sisgedd.Domain
{
  public class ConfiguracaoEntity : GenericEntity
  {
    public int CONFIGURACAO_ID { get; set; }

    public string CRITICIDADE { get; set; }

    public string INSTRUCAO { get; set; }

    public string TIPO_MANIFESTACAO { get; set; }

    public int PRAZO { get; set; }

    public int TIPONATUREZA_ID { get; set; }

    public int NATUREZA_ID { get; set; }

    public int EMPRESA_ID { get; set; }
  }
}
