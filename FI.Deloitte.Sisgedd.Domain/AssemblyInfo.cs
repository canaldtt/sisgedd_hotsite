﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("FI.Deloitte.Sisgedd.Domain")]
[assembly: AssemblyDescription("A Fabrica Interactiva Desenvolvimento de Software possui todos os direitos de propriedade intelectual do Software. O Software é licenciado, não vendido.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Fabrica Interactiva Desenvolvimento de Software - www.fabricai.com.br")]
[assembly: AssemblyProduct("FI.Deloitte.Sisgedd.Domain")]
[assembly: AssemblyCopyright("Copyright © Fabrica Interactiva 2018")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(true)]
[assembly: Guid("9a9af652-512d-4ba8-9853-b7a73739ee40")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyVersion("1.0.0.0")]
