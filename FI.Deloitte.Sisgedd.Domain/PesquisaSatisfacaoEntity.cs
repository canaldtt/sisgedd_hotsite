﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Domain.PesquisaSatisfacaoEntity
// Assembly: FI.Deloitte.Sisgedd.Domain, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DB6D7218-11AF-49BB-86E3-DE4782C98D95
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Domain.dll

namespace FI.Deloitte.Sisgedd.Domain
{
  public class PesquisaSatisfacaoEntity : GenericEntity
  {
    public int PESQUISA_ID { get; set; }

    public string NMR_PROTOCOLO { get; set; }

    public string PG1 { get; set; }

    public string PG1B { get; set; }

    public string PG1A { get; set; }

    public string PG12A { get; set; }

    public string PG21 { get; set; }

    public string PG21A { get; set; }

    public string PG3 { get; set; }

    public string PG4 { get; set; }

    public string PG41 { get; set; }

    public string PG5 { get; set; }

    public string PG51 { get; set; }

    public string PG6 { get; set; }

    public string PG61 { get; set; }
  }
}
