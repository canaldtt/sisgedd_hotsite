﻿// Decompiled with JetBrains decompiler
// Type: FI.Deloitte.Sisgedd.Domain.EmailEntity
// Assembly: FI.Deloitte.Sisgedd.Domain, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DB6D7218-11AF-49BB-86E3-DE4782C98D95
// Assembly location: C:\PROJETOS.DELOITTE\_PRD\PUBLISHED\SISGEDD.HOTSITE\bin\FI.Deloitte.Sisgedd.Domain.dll

using System;
using System.Collections.Generic;

namespace FI.Deloitte.Sisgedd.Domain
{
  public class EmailEntity : GenericEntity
  {
    public DateTime DT_ENVIO { get; set; }

    public int EMAIL_ID { get; set; }

    public string MESSAGE_ID { get; set; }

    public string STATUS { get; set; }

    public string CONTEUDO { get; set; }

    public string TITULO { get; set; }

    public string EMAIL_FROM { get; set; }

    public string EMAIL_TO { get; set; }

    public List<AnexoEntity> ANEXOS { get; set; }
  }
}
